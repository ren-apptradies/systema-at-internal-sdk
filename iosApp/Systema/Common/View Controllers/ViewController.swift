//
//  ViewController.swift
//  Systema
//
//  Created by iTradie on 8/10/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = Styles.Colors.defaultControllerBackground
  }
  
  @IBAction
  func backButtonTapped(_ sender: AnyObject) {
    if navigationController?.viewControllers.first != self {
      navigationController?.popViewController(animated: true)
    } else if isPresented {
      dismiss(animated: true, completion: nil)
    }
  }
}

extension ViewController {
  var isPresented: Bool {
    return presentingViewController != nil ||
      navigationController?.presentingViewController?.presentedViewController === navigationController ||
        tabBarController?.presentingViewController is UITabBarController
  }
}

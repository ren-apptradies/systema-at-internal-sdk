//
//  WebViewController.swift
//  Systema
//
//  Created by iTradie on 8/11/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation
import WebKit

class WebViewController: ViewController, WKUIDelegate {
  
  var webView: WKWebView!
  var progressView: UIProgressView!

  var webUrlString: String? {
    didSet {
      guard let webUrlString = webUrlString, isViewLoaded else { return }
      webView.load(webUrlString)
    }
  }

  private var kvo: [NSKeyValueObservation] = []

  deinit {
    progressView.removeFromSuperview()
  }

  override func loadView() {
    let webConfiguration = WKWebViewConfiguration()
    webView = WKWebView(frame: .zero, configuration: webConfiguration)
    webView.uiDelegate = self
    view = webView

    setupProgressView()
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    webView.load(webUrlString)
    kvo.append(webView.observe(\.title) { [weak self] aWebView, _ in
      guard self?.title == nil else { return }
      self?.navigationItem.title = aWebView.title
    })
    kvo.append(webView.observe(\.estimatedProgress) { [weak self] aWebView, _ in
      guard let slf = self else { return }
      let progress = Float(aWebView.estimatedProgress)
      slf.progressView.progress = progress
      UIView.animate(withDuration: 0.25, delay: 0.5, options: [], animations: {
        slf.progressView.alpha = progress >= 1 ? 0 : 1
      })
    })
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: animated)
  }
}

// MARK: - Setup
extension WebViewController {
  func setupProgressView() {
    progressView = UIProgressView(progressViewStyle: .default)
    progressView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
    progressView.tintColor = Styles.Colors.primaryColor
    navigationController?.navigationBar.addSubview(progressView)

    let navigationBarBounds = navigationController?.navigationBar.bounds
    progressView.frame = CGRect(x: 0,
                                y: navigationBarBounds!.size.height - 2,
                                width: navigationBarBounds!.size.width,
                                height: 2)
  }
}

// MARK: - WKNavigationDelegate

extension WebViewController: WKNavigationDelegate {
  func webView(_ webView: WKWebView,
               decidePolicyFor navigationAction: WKNavigationAction,
               decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    if navigationAction.request.url != nil {
      decisionHandler(.cancel)
    }
  }

  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    progressView.isHidden = true
  }

  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    progressView.isHidden = false
  }
}


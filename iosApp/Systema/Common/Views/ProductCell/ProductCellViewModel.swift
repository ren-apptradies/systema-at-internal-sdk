//
//  ProductCellViewModel.swift
//  Systema
//
//  Created by iTradie on 30/9/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import shared
import SystemaIosSDK

class ProductCellViewModel {
  private let product: Product

  init(
    product: Product
  ) {
    self.product = product
    print("brand: \(product.title)")
    print("price: \(product.price)")
    print("image: \(product.image)")
    print("currency: \(product.currency)")
  }
}

// MARK: - Methods

extension ProductCellViewModel {
}

// MARK: - Getters

extension ProductCellViewModel {
  var prod: Product { product }
  var name: String? { product.title }
  var price: String? {
    guard let currency = product.currency,
      let price = product.price else {
      return nil
    }

    return "\(currency) \(price)"
  }

  var imageURL: URL? { URL(string: product.image) }
  var isFavorite: Bool { false }
}

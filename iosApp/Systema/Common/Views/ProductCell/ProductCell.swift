//
//  FileCell.swift
//  Systema
//
//  Created by iTradie on 30/9/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import AlamofireImage
import Foundation
import SystemaIosSDK
import UIKit

import shared

class ProductCell: UICollectionViewCell {
  @IBOutlet var nameLabel: UILabel!
  @IBOutlet var priceLabel: UILabel!
  @IBOutlet var productImageView: UIImageView!
  @IBOutlet var bgView: UIView!
  @IBOutlet var favButton: UIButton!
  @IBOutlet var deleteButton: UIButton!

  var onTapProductView: SingleResult<Product>?
  var onTapAddToCart: SingleResult<Product>?
  var onTapRemoveFromCart: SingleResult<Product>?
  var onTapFavorite: DoubleResult<Product, Bool>?

  private var isFavorite = false
  var isEditable = false {
    didSet {
      deleteButton.isHidden = !isEditable
    }
  }

  var viewModel: ProductCellViewModel! {
    didSet {
      refresh()
    }
  }

  override func awakeFromNib() {
    setup()
  }
}

// MARK: - Setup

private extension ProductCell {
  func setup() {
    applyShadow(
      color: R.color.gray_E2E2E2()!,
      opacity: 1,
      offSet: CGSize(width: 0, height: 2),
      radius: 2
    )
  }
}

// MARK: - IBActions

private extension ProductCell {
  @IBAction func toggleFavorite() {
    let product = viewModel.prod
    isFavorite = !isFavorite
    onTapFavorite?(product, isFavorite)
  }

  @IBAction func addToCart() {
    let product = viewModel.prod
    onTapAddToCart?(product)
  }
  
  @IBAction func removeFromCart() {
    let product = viewModel.prod
    onTapRemoveFromCart?(product)
  }

  @IBAction func onTapProductImage() {
    let product = viewModel.prod
    onTapProductView?(product)
  }
}

// MARK: - Refresh

private extension ProductCell {
  func refresh() {
    nameLabel.text = viewModel.name
    priceLabel.text = viewModel.price

    guard let url = viewModel.imageURL else {
      return
    }
    productImageView.af_setImage(withURL: url)
    
    //TODO: For cleanup
    if AppInstance.shared.wishList.contains(viewModel.prod) {
      favButton.setImage(R.image.favorite_active(), for: .normal)
      isFavorite = true
    } else {
      favButton.setImage(R.image.heart(), for: .normal)
      isFavorite = false
    }
  }
}

// MARK: - Helpers

private extension ProductCell {
}

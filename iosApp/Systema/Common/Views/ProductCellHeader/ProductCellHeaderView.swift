//
//  ProductCellHeaderView.swift
//  Systema
//
//  Created by iTradie on 9/10/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import UIKit

class ProductCellHeaderView: UICollectionReusableView {
  @IBOutlet var titleLabel: UILabel!

  override func awakeFromNib() {
    setup()
  }
}

// MARK: - Setup

private extension ProductCellHeaderView {
  func setup() {
  }
}

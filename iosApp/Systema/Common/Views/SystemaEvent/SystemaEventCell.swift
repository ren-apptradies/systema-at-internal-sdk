//
//  FileCell.swift
//  Systema
//
//  Created by iTradie on 11/10/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import UIKit

class SystemaEventCell: UITableViewCell {
  @IBOutlet var label: UILabel!

  override func awakeFromNib() {
    setup()
  }
}

// MARK: - Setup

private extension SystemaEventCell {
  func setup() {
  }
}

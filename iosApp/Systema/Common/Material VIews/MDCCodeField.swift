//
//  MDCCodeField.swift
//  Systema
//
//  Created by iTradie on 8/10/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

protocol MDCCodeFieldFieldDelegate: AnyObject {
  func codeFieldDidDeleteBackward(_ codeField: MDCCodeField, oldText: String?)
}

class MDCCodeField: MDCTextField {
  weak var codeFieldDelegate: MDCCodeFieldFieldDelegate?

  override func deleteBackward() {
    let oldText = text
    super.deleteBackward()
    codeFieldDelegate?.codeFieldDidDeleteBackward(self, oldText: oldText)
  }
  
  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    if action == #selector(cut(_:)) {
      return true
    } else if action == #selector(copy(_:)) {
      return true
    } else if action == #selector(paste(_:)) {
      return true
    }

    return super.canPerformAction(action, withSender: sender)
  }
}

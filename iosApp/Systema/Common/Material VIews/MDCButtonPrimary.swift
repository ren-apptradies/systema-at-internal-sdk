//
//  MDCButtonPrimary.swift
//  Systema
//
//  Created by iTradie on 21/7/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import MaterialComponents.MDCButton
import UIKit

@IBDesignable
class MDCButtonPrimary: MDCButton {
  public override func layoutSubviews() {
    super.layoutSubviews()
    layer.cornerRadius = 10
    setBackgroundColor(R.color.primary()!, for: .normal)
    setBackgroundColor(.lightGray, for: .disabled)
    setTitleColor(.white, for: .normal)
    setTitleColor(.white, for: .disabled)
    disabledAlpha = 1.0
    clipsToBounds = true
  }
}

//
//  TypeAliases.swift
//  Systema
//
//  Created by iTradie on 7/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

// MARK: - Typealiases

// Empty Result + Void Return
typealias EmptyResult<ReturnType> = () -> ReturnType

// Common
typealias VoidResult = EmptyResult<Void> // () -> Void
typealias ErrorResult = SingleResult<Error> // (Error) -> Void
typealias BoolResult = SingleResult<Bool> // (Bool) -> Void

// Custom Result + Custom Return
typealias SingleResultWithReturn<T, ReturnType> = ((T) -> ReturnType)
typealias DoubleResultWithReturn<T1, T2, ReturnType> = ((T1, T2) -> ReturnType)

// Custom Result + Void Return
typealias SingleResult<T> = SingleResultWithReturn<T, Void>
typealias DoubleResult<T1, T2> = DoubleResultWithReturn<T1, T2, Void>

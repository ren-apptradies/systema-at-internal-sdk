//
//  Helpers.swift
//  Systema
//
//  Created by iTradie on 7/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

import shared
import SystemaIosSDK

public typealias JSONDictionary = [String: Any]

typealias S = R.string.localized

struct Helpers {}

// MARK: - Logging

func debugLog(_ message: String, file: String = #file, line: Int = #line, function: String = #function) {
  #if DEBUG
    let fileURL = NSURL(fileURLWithPath: file)
    let fileName = fileURL.deletingPathExtension?.lastPathComponent ?? ""
    print("\(Date().dblog()) \(fileName)::\(function)[L:\(line)] \(message)")
  #endif
  // Nothing to do if not debugging
}

func debugJSON(_ value: AnyObject) {
  #if DEBUG
    //
  #endif
}

// MARK: - File Management

public func jsonDictionaryFromFile(_ name: String, bundle: Bundle = Bundle.main) -> JSONDictionary? {
  let path = bundle.path(forResource: name, ofType: "json")!
  guard let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else { return nil }
  let options = JSONSerialization.ReadingOptions.mutableContainers
  return (try? JSONSerialization.jsonObject(with: data, options: options)) as? JSONDictionary
}

/// - parameter name: Name of HTML file excluding extension.
func htmlStringFromFile(_ name: String, bundle: Bundle = Bundle.main) -> String? {
  let path = bundle.path(forResource: name, ofType: "html")
  return try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
}

// MARK: - Threading

/// Performs an intensive process in the background, then calls the specified completion
/// block, if provided, on the main thread.
func performInBackground(_ work: @escaping VoidResult, completion: (VoidResult)? = nil) {
  DispatchQueue.global(qos: .background).async {
    work()
    DispatchQueue.main.async {
      completion?()
    }
  }
}

func delay(_ seconds: Double, task: @escaping VoidResult) {
  let when = DispatchTime.now() + seconds
  DispatchQueue.main.asyncAfter(deadline: when, execute: task)
}

// MARK: - Others

func isValidEmail(_ text: String) -> Bool {
  let regEx = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
    "\\@" +
    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
    "(" +
    "\\." +
    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
    ")+"
//  "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
  let predicate = NSPredicate(format: "SELF MATCHES %@", regEx)
  return predicate.evaluate(with: text)
}

func attributedStringFromHTMLString(_ html: String) -> NSAttributedString? {
  guard let data = html.data(using: .utf16, allowLossyConversion: false) else { return nil }
  let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
  return try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil)
}

//
func productFromCart(_ prod: CartProduct) -> Product {
  return Product(
    id: prod.itemId,
    brand: nil,
    currency: prod.currency,
    description: nil,
    image: prod.image,
    images: nil,
    inStock: nil,
    itemGroupId: nil,
    link: prod.link ?? "",
    price: KotlinDouble(nonretainedObject: prod.price),
    promotion: nil,
    salePrice: nil,
    title: prod.title ?? "",
    recId: prod.recId ?? "",
    tags: nil,
    attributes: nil
  )
}

//TODO: to implement
//func cartProductFromProduct(_ product: Product) -> CartProduct {
//  return CartProduct(
//    itemId: product.id,
//    title: product.title,
//    price: product.price as! Double,
//    quantity: 1,
//    image: product.image,
//    currency: product.currency,
//    recId: product.recId,
//    link: product.link,
//    description: product.description,
//    inStock: Bool(product.inStock ?? false) ,
//    itemGroupId: product.itemGroupId,
//    images: product.images,
//    salePrice: Float(product.salePrice ?? 0),
//    tags: product.tags,
//    brand: product.brand
//  )
//}

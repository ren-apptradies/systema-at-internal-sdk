//
//  MDCHelper.swift
//  Systema
//
//  Created by iTradie on 8/9/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields_TypographyThemer
import MaterialComponents.MaterialTextFields_ColorThemer

class MDCHelper {
  static let shared = MDCHelper()

  private(set) lazy var containerScheme: MDCContainerScheme = {
    let container = MDCContainerScheme()
    container.typographyScheme = self.typographyScheme
    container.colorScheme = self.colorScheme

    return container
  }()
  
  private(set) lazy var containerSchemeWithShape: MDCContainerScheme = {
    var container = self.containerScheme
    container.shapeScheme = self.shapeScheme
    return container
  }()

  private(set) lazy var shapeScheme: MDCShapeScheme = {
    let scheme = MDCShapeScheme()
    let temp = MDCCornerTreatment.corner(withRadius: 8, valueType: .absolute)
    scheme.smallComponentShape.topLeftCorner = temp
    return scheme
  }()
  
  private(set) lazy var colorScheme: MDCSemanticColorScheme = {
    let scheme = MDCSemanticColorScheme()
    scheme.primaryColor = Styles.Colors.primaryColor
    scheme.errorColor = Styles.Colors.Form.errorColor

    return scheme
  }()
  
  private(set) lazy var typographyScheme: MDCTypographyScheme = {
    let scheme = MDCTypographyScheme()
//    scheme.headline1 = UIFont.largeTitle
//    scheme.subtitle1 = R.font.barlowMedium(size: 15)!
    scheme.caption = UIFont.systemFont(ofSize: 12, weight: .regular)

    return scheme
  }()
}

// MARK: - Underline TextField Style

extension MDCHelper {
  static func underlineInputController(for field: MDCTextField) -> MDCTextInputControllerUnderline {
    let controller = MDCTextInputControllerUnderline(textInput: field)
    controller.applyTheme(withScheme: MDCHelper.shared.containerScheme)

    return controller
  }
}

// MARK: - Filled TextField Style

extension MDCHelper {
  static func inputController(for field: MDCTextField) -> MDCTextInputControllerFilled {
    let controller = MDCTextInputControllerFilled(textInput: field)
    controller.applyTheme(withScheme: MDCHelper.shared.containerScheme)

    return controller
  }
}

// MARK: - Outlined TextField Style

extension MDCHelper {
  static func roundedInputController(for field: MDCTextField) -> MDCTextInputControllerOutlined {
    let controller = MDCTextInputControllerOutlined(textInput: field)
    controller.isFloatingEnabled = true
    controller.applyTheme(withScheme: MDCHelper.shared.containerSchemeWithShape)
    field.font = MDCHelper.shared.typographyScheme.subtitle1
    controller.borderStrokeColor = Styles.Colors.secondaryTextColor

    return controller
  }
}

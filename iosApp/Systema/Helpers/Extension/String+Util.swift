//
//  String+Util.swift
//  Systema
//
//  Created by iTradie on 13/3/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation

extension String {
  func slice(from: String, to: String) -> String? {
    return (range(of: from)?.upperBound).flatMap { substringFrom in
      (range(of: to, range: substringFrom ..< endIndex)?.lowerBound).map { substringTo in
        String(self[substringFrom ..< substringTo])
      }
    }
  }

  func toDate(with format: String) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    dateFormatter.timeZone = TimeZone.utc

    return dateFormatter.date(from: self)
  }

  var isNumeric: Bool {
    return Double(self) != nil
  }
}

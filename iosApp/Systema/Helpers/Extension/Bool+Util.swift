//
//  Bool+Util.swift
//  Systema
//
//  Created by iTradie on 8/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import CoreGraphics
import Foundation

extension Bool {
  func intValue() -> Int {
    self ? 1 : 0
  }

  func floatValue() -> Float {
    Float(intValue())
  }

  func cgFloatValue() -> CGFloat {
    CGFloat(intValue())
  }
}

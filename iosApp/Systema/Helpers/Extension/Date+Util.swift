//
//  Date+Util.swift
//  Systema
//
//  Created by iTradie on 8/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

extension Date {
  static var now: Date { .init() }

  func dblog() -> String {
    return Constants.Formatters.debugConsoleDateFormatter.string(from: self)
  }

  func millisecondsSince1970() -> Double {
    return (timeIntervalSince1970 * 1000).rounded()
  }

  /// Creates a Date instance from a milliseconds value.
  init(millisecondsSince1970: Double) {
    self.init(timeIntervalSince1970: millisecondsSince1970 / 1000)
  }

  func toString() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "d-MM-YY HH:mm:ss"
    return formatter.string(from: self)
  }
}

//
//  TimeZone+Util.swift
//  Systema
//
//  Created by iTradie on 8/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

extension TimeZone {
  static var utc: TimeZone? { TimeZone(identifier: "UTC") }
}

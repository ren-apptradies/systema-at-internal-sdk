//
//  WKWebView+Util.swift
//  Systema
//
//  Created by iTradie on 8/11/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation
import WebKit

extension WKWebView {
  func load(_ urlString: String?) {
    guard let urlStr = urlString, let url = URL(string: urlStr) else { return }
    let request = URLRequest(url: url)
    load(request)
  }
}

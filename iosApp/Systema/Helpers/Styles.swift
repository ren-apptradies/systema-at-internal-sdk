//
//  Styles.swift
//  Systema
//
//  Created by iTradie on 8/9/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import UIKit

struct Styles {
  struct Colors {}
}

extension Styles.Colors {
  static let primaryColor = R.color.primary()!
  static let secondaryColor = R.color.gray_707070()!

  static let textColorDefault = UIColor.black /// for default background
  static let textColorDark = UIColor.white /// for dark mode

  static let primaryTextColor = Self.textColorDefault
  static let secondaryTextColor = R.color.gray_707070()!
  static let linkTextColor = Self.primaryColor
  
  static let defaultControllerBackground = UIColor.white
  struct Form {}
}

extension Styles.Colors.Form {
  static let normalColor = Styles.Colors.primaryColor
  static let focusedColor = Styles.Colors.secondaryColor
  static let errorColor = R.color.red_D03737()!

  static let placeholderTextColor = Styles.Colors.secondaryColor

  struct Button {}
}

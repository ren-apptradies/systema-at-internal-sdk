//
//  Systema.swift
//  iosApp
//
//  Created by Ren Decano on 29/8/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

class Systema {
    
    let proxyApiKey = "PMAK-6113bbd984433f0046e30785-fe032bc85b42f8e1eff7507f1c23d592b9"
    var proxyUrls = [EndpointType.dynamicconfig: "https://daef600f-b1c2-49fc-a739-c56ad0c74ca1.mock.pstmn.io",
                     EndpointType.tracker : "https://daef600f-b1c2-49fc-a739-c56ad0c74ca1.mock.pstmn.io"]
    
    let tagMapping = [
        SystemaTags.init().ProductId : 1,
        SystemaTags.init().RecId : 2,
        SystemaTags.init().ProductUrl : 3,
        SystemaTags.init().ResultId : 4,
        SystemaTags.init().Visible : 5,
        SystemaTags.init().Observed : 6,
        SystemaTags.init().ContainerUrl : 7,
    ]
    
    var systemaAi: SystemaAI? = nil
    
    
    init() {
        SystemaInstance().initialise(clientID: "unreal", apiKey: proxyApiKey,
                                        environment: EnvironmentType.test,
                                        logLevel: SystemaLogLevel.debug,
                                        proxyUrls: proxyUrls,
                                        meta: [SystemaConstants.init().SystemaTagMapping  : tagMapping], completionHandler: { val, error in
                                            print("------ SYSTEMA INIT START ------")
                                            print(val)
                                            self.systemaAi = val
                                            print("------ SYSTEMA INIT END ------")
                                        })
    }

    func getInstance() -> SystemaAI? {
        guard let api = self.systemaAi else {
                return nil
            }
        return api
    }
    
    func testAPI() {
        guard let api = self.systemaAi else {
            return
        }
                
        api.getTrending(payload: nil,
                        requestOptions: nil,
                        completionHandler: {
                            val, error in
                            print("------ TRENDING RESPONSE START ------")
                            print(val)
                            print("------ TRENDING RESPONSE END------")
                        })
        
        api.getPopular(payload: nil,
                        requestOptions: nil,
                        completionHandler: {
                            val, error in
                            print("------ POPULAR RESPONSE START ------")
                            print(val)
                            print("------ POPULAR RESPONSE END------")
                        })
        
    }
}

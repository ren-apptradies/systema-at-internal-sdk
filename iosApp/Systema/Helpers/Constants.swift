//
//  Constants.swift
//  Systema
//
//  Created by iTradie on 8/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

struct Constants {
  struct Formatters {
    static let debugConsoleDateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
      formatter.timeZone = .utc
      return formatter
    }()
    
    static let birthdateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd"
      formatter.timeZone = .utc
      return formatter
    }()
  }
}

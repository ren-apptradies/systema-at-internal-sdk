//
//  CartProduct.swift
//  Systema
//
//  Created by JAG on 10/11/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation

struct CartProduct: Codable {
  let itemId: String
  let title: String?
  let price: Double?
  let quantity: Int
  let image: String
  let currency: String?
  let recId: String?
  let link: String?
    //TODO: to implement
//  let description: String?
//  let inStock: Bool?
//  let itemGroupId: String?
//  let images: [String]?
//  let salePrice: Float?
//  let tags: [String]?
//  let brand: String?
}

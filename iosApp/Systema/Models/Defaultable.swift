//
//  Defaultable.swift
//  Systema
//
//  Created by iTradie on 11/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

protocol Defaultable: RawRepresentable {
  static var defaultValue: Self { get }
}

extension Defaultable {
  static func value(for rawValue: RawValue) -> Self {
    return Self(rawValue: rawValue) ?? Self.defaultValue
  }
}

extension Defaultable where Self.RawValue: Decodable {
  init(from decoder: Decoder) throws {
    self = Self.value(for: try decoder.singleValueContainer().decode(RawValue.self))
  }
}

//
//  AppInstance.swift
//  FairGoLocal
//
//  Created by iTradie on 7/8/20.
//  Copyright © 2020 FairGoLocal. All rights reserved.
//

import UIKit
import Valet

import shared
import SystemaIosSDK

protocol AppConfigType {
  var baseUrl: String { get }
}

struct AppConfig: AppConfigType {
  var baseUrl: String { "https://api.blueprintapp.com/api" }
}

/// This is our main application object. This holds instances of all the services available
/// in the app like the APIService, SessionService, etc.
///
/// IMPORTANT:
/// - Defer creation of service instance up to the point where it's first needed.
///

class AppInstance {
  enum Environment: String {
    case development
    case staging
    case production
  }

  static let shared = AppInstance()

  static var environment: Environment {
    // These values are set in their corresponding Targets.
    // See: Target > Build Settings > Other Swift Flags.
    #if DEV
      return .development
    #else
      return .production
    #endif
  }

  static let valet = Valet.valet(
    with: Identifier(nonEmpty: AppInstance.bundleIdentifier!)!,
    accessibility: .whenUnlocked
  )

  private(set) var config: AppConfigType!

  private(set) var api: API!

  private(set) var fetchService: FetchingProductsService!

  private(set) var eventsService: SystemaEventsService!

  private(set) var trackWishListService: TrackWishlistService!

  private(set) var wishlistService: WishlistService!

  private(set) var trackSearchService: TrackSearchService!

  private(set) var cartService: CartService!

  private(set) var trackCartService: TrackCartService!

  private(set) var onClickService: TrackItemClickService!

  private(set) var trackContainerService: TrackContainerService!
  
  var wishList: [Product] = []

  // MARK: Initialization

  init() {
    debugLog("env: \(AppInstance.environment.rawValue)")

    api = API()

    fetchService = FetchingProductsService(api: api)

    trackWishListService = TrackWishlistService(api: api)

    wishlistService = WishlistService()

    trackSearchService = TrackSearchService(api: api)

    trackCartService = TrackCartService(api: api)

    onClickService = TrackItemClickService(api: api)

    trackContainerService = TrackContainerService(api: api)

    eventsService = SystemaEventsService()

    cartService = CartService()
  }

  func recordError(_ error: Error, info: [String: Any]? = nil) {
    debugLog(String(describing: error))

    if info != nil {
      debugLog("other info: \(String(describing: info!))")
    }
  }
}

// MARK: - App Info

extension AppInstance {
  static var bundleIdentifier: String? {
    return Bundle.main.bundleIdentifier
  }

  /// A dictionary, constructed from the bundle’s Info.plist file.
  static var info: [String: Any] {
    return Bundle.main.infoDictionary ?? [:]
  }

  static var displayName: String {
    return (info["CFBundleDisplayName"] as? String) ?? "FairGoLocal"
  }

  /// Alias for `CFBundleShortVersionString`.
  static var releaseVersion: String {
    return (info["CFBundleShortVersionString"] as? String) ?? "1.0"
  }

  /// Alias for `CFBundleVersion`.
  static var buildNumber: String {
    return (info["CFBundleVersion"] as? String) ?? "1"
  }
}

/// Use this for all App-level errors.
// TODO: Add conformance to CustomNSError.
enum AppError: Error {
  case unauthorized(_ reason: String)
  case unknown
}

extension AppError: LocalizedError {
  var errorDescription: String? {
    switch self {
    case .unauthorized:
      return S.errorDevAuthorization()
    default:
      return S.errorDevSomethingWrong()
    }
  }

  var failureReason: String? {
    switch self {
    case let .unauthorized(reason):
      return reason
    default:
      return S.errorDevUnknown()
    }
  }
}

//
//  AppDelegate+RootView.swift
//  Systema
//
//  Created by iTradie on 13/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import UIKit

extension AppDelegate {
  func updateRootViewController() {
    switchToDashboard()
  }

  func switchToDashboard() {
    if !(window?.rootViewController?.isKind(of: UITabBarController.self) ?? true) {
      window?.rootViewController = R.storyboard.mainTabbar.instantiateInitialViewController()
    }
  }

  func setupNotificationObservers() {
    //setup notification here
  }
}

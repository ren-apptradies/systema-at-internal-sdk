//
//  UserParams.swift
//  Systema
//
//  Created by iTradie on 8/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

struct AuthUserParams: APIRequestParameters, APIModel {
  var email: String?
  var firstName: String?
  var middleName: String?
  var lastName: String?
  var userType: String?
  var password: String?
  var dob: String?
  var photo: Data?
  var mobileNumber: String?
  var verificationTicketId: String?
  var verificationToken: String?
//  var lat: String?
//  var long: String?
//  var address1: String?
//  var address2: String?
//  var suburb: String?
//  var state: String?
//  var postalCode: String?

  enum CodingKeys: String, CodingKey {
    case email
    case firstName = "first_name"
    case middleName = "middle_name"
    case lastName = "last_name"
    case userType = "user_type"
    case dob
    case password
    case photo
    case mobileNumber = "phone_number"
    case verificationTicketId = "verification_ticket_id"
    case verificationToken = "verification_token"
//    case lat = "cust_lat"
//    case long = "cust_lng"
//    case address1 = "cust_address_1"
//    case address2 = "cust_address_2"
//    case suburb = "cust_suburb"
//    case state = "cust_state"
//    case postalCode = "cust_postal_code"
  }
}

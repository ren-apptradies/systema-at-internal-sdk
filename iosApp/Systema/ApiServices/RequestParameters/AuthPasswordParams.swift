//
//  AuthPasswordParams.swift
//  Systema
//
//  Created by iTradie on 8/14/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

struct AuthPasswordParams: APIRequestParameters {
  var mobileNumber: String?
  var verificationToken: String?
  var verificationId: String?
  var password: String?

  enum CodingKeys: String, CodingKey {
    case mobileNumber = "phone_number"
    case verificationToken = "verification_token"
    case verificationId = "verification_ticket_id"
    case password
  }
}

//
//  AuthProfileParams.swift
//  Systema
//
//  Created by iTradie on 8/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

struct AuthProfileParams: APIRequestParameters {
  var fullName: String?
  var phoneNumber: String?
  var email: String?
}

extension AuthProfileParams: Codable {
  enum CodingKeys: String, CodingKey {
    case fullName = "full_name"
    case phoneNumber = "phone_number"
    case email
  }
}

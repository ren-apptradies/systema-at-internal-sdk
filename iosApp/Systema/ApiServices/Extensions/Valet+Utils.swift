//
//  Valet+Utils.swift
//  Systema
//
//  Created by iTradie on 8/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

import Foundation
import Valet

extension Valet {
  /// Removes the previous value if new value set is Nil.
  func setString(_ value: String?, forKey: String) {
    if value == nil {
      try? removeObject(forKey: forKey)
    } else {
      try? setString(value!, forKey: forKey)
    }
  }

  func getString(forKey: String) -> String? {
    guard canAccessKeychain() else {
      debugLog("Can't access KeyChain.")
      return nil
    }
    return try? string(forKey: forKey)
  }

  /// Removes the previous value if new value set is Nil.
  func setData(_ value: Data?, forKey: String) {
    if value == nil {
      try? removeObject(forKey: forKey)
    } else {
      try? setObject(value!, forKey: forKey)
    }
  }

  func getData(forKey: String) -> Data? {
    guard canAccessKeychain() else {
      debugLog("Can't access KeyChain.")
      return nil
    }
    return try? object(forKey: forKey)
  }
}

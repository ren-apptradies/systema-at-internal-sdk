//
//  AppService.swift
//  Systema
//
//  Created by iTradie on 7/8/20.
//  Copyright © 2020 Systema. All rights reserved.
//

import Foundation

protocol AppService: AnyObject {
}

extension AppService {
  var app: AppInstance {
    return AppInstance.shared
  }
}

//
//  CategoryViewController.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxSwift
import SVProgressHUD
import UIKit

import shared
import SystemaIosSDK

private enum Tab: Int, CaseIterable {
  case trending = 0
  case popular
}

class CategoryViewController: ViewController {
  private var viewModel = CategoryViewModel()

  private let cellIdentifier = "ProductCell"
  private let cellHeaderIdentifier = "ProductCellHeaderView"

  @IBOutlet var trendingCollectionView: UICollectionView!
  @IBOutlet var categoryTableView: UITableView!
  private let refreshControl = UIRefreshControl()

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    setupBindings()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)

    if viewModel.popularMenProducts.count > 0, viewModel.trendingWomenProducts.count > 0 {
      viewModel.trackContainerPopular()
      viewModel.trackContainerTrending()
      viewModel.monitorPageEvent()
    } else {
      fetchData()
    }
  }

  private func setupBindings() {}

  @objc
  private func fetchData() {
    viewModel.getProducts()
  }
}

// MARK: - Setup

private extension CategoryViewController {
  func setup() {
    navigationController?.setNavigationBarHidden(true, animated: false)
    setupCollectionView()
    viewModel.onSuccessFetch = handleSuccessFetch()
    viewModel.onError = handleFetchError()

    categoryTableView.addSubview(refreshControl)
    refreshControl.addTarget(self, action: #selector(fetchData), for: .valueChanged)
  }

  func setupCollectionView() {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.minimumLineSpacing = 10
    flowLayout.minimumInteritemSpacing = 0
    flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
    let width = (UIScreen.main.bounds.width / 2) - 10
    flowLayout.itemSize = CGSize(width: width, height: 210)

    flowLayout.scrollDirection = .vertical

    trendingCollectionView.delegate = self
    trendingCollectionView.dataSource = self
    trendingCollectionView.collectionViewLayout = flowLayout

    trendingCollectionView.register(UINib(
      nibName: cellIdentifier,
      bundle: nil
    ), forCellWithReuseIdentifier: cellIdentifier)

    trendingCollectionView.register(
      UINib(resource: R.nib.productCellHeaderView),
      forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
      withReuseIdentifier: R.nib.productCellHeaderView.name
    )

    flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 50)
  }
}

// MARK: - UITableView

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.categories.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(
      withIdentifier: R.reuseIdentifier.categoryViewCell,
      for: indexPath
    )
    else { return UITableViewCell() }

    cell.textLabel?.text = viewModel.categories[indexPath.row]
    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

// MARK: - UICollectionViewDelegate

extension CategoryViewController: UICollectionViewDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    print(" selected at \(indexPath.row)")
    var product: Product?
    switch indexPath.section {
    case Tab.trending.rawValue:
      product = viewModel.trendingWomenProducts[indexPath.row]
    case Tab.popular.rawValue:
      product = viewModel.popularMenProducts[indexPath.row]
    default:
      print("invalid section")
    }

    guard let selectedProduct = product else { return }
    viewModel.trackOnClick(for: selectedProduct)

    let viewController = R.storyboard.product.productViewController()!
    viewController.viewModel = ProductViewModel(product: selectedProduct)
    navigationController?.pushViewController(viewController, animated: true)
  }
}

// MARK: - UICollectionView

extension CategoryViewController: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return Tab.allCases.count
  }

  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    switch section {
    case Tab.trending.rawValue:
      return viewModel.trendingWomenProducts.count
    case Tab.popular.rawValue:
      return viewModel.popularMenProducts.count
    default:
      return 0
    }
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    guard
      let productCell = collectionView.dequeueReusableCell(
        withReuseIdentifier: cellIdentifier,
        for: indexPath
      ) as? ProductCell
    else {
      return ProductCell()
    }

    switch indexPath.section {
    case Tab.trending.rawValue:
      let vm = viewModel.cellVM(with: viewModel.trendingWomenProducts[indexPath.row])
      productCell.viewModel = vm
    case Tab.popular.rawValue:
      let vmPopular = viewModel.cellVM(with: viewModel.popularMenProducts[indexPath.row])
      productCell.viewModel = vmPopular
    default:
      print("invalid section")
    }

    productCell.onTapFavorite = handleOnTapFavorite()
    productCell.onTapAddToCart = handleOnTapCart()

    return productCell
  }

  func collectionView(
    _ collectionView: UICollectionView,
    viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath
  ) -> UICollectionReusableView {
    guard
      let header = collectionView.dequeueReusableSupplementaryView(
        ofKind: kind,
        withReuseIdentifier: cellHeaderIdentifier, for: indexPath
      )
      as? ProductCellHeaderView
    else {
      return ProductCellHeaderView()
    }

    switch indexPath.section {
    case Tab.trending.rawValue:
      header.titleLabel.text = "Trending in Women"
    case Tab.popular.rawValue:
      header.titleLabel.text = "Popular in Men"
    default:
      header.titleLabel.text = ""
    }

    return header
  }
}

// MARK: - Event Handlers

private extension CategoryViewController {
  func handleFetchError() -> ErrorResult {
    return { error in
      self.refreshControl.endRefreshing()
      print("error \(error.localizedDescription)")
    }
  }

  func handleSuccessFetch() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.refreshControl.endRefreshing()
      self.viewModel.trackContainerPopular()
      self.viewModel.trackContainerTrending()
      self.viewModel.monitorPageEvent()
      self.trendingCollectionView.reloadData()
    }
  }

  func handleOnTapFavorite() -> DoubleResult<Product, Bool> {
    return { [weak self] product, selected in
      guard let self = self else { return }
      self.viewModel.trackWishlist(selected, for: product)
      self.viewModel.updateWishlist(selected, for: product)
      self.trendingCollectionView.reloadData()
    }
  }

  func handleOnTapCart() -> SingleResult<Product> {
    return { [weak self] product in
      guard let self = self else { return }

      self.viewModel.trackItemAquired(for: product)
      print("--- adding \(product.title) to cart ---")

      let cartProduct = CartProduct(
        itemId: product.id,
        title: product.title,
        price: product.price as! Double,
        quantity: 1,
        image: product.image,
        currency: product.currency,
        recId: product.recId,
        link: product.link
      )

      self.viewModel.appendProduct(cartProduct)
      DispatchQueue.main.async {
        SVProgressHUD.showSuccess(withStatus: "Successfully Added to Cart")
      }
    }
  }
}

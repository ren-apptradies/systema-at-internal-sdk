//
//  CategoryViewModel.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxCocoa
import RxSwift

import shared
import SystemaIosSDK

class CategoryViewModel {
  var onSuccessFetch: VoidResult?
  var onSuccessTrack: VoidResult?
  var onError: ErrorResult?

  private let fetchService: FetchingProductsService!
  private let eventsService: SystemaEventsService!
  private let trackWishlistService: TrackWishlistService!
  private let trackCartService: TrackCartService!
  private let cartService: CartService!

  private let onClickService: TrackItemClickService!
  private let containerService: TrackContainerService!

  private var cachedPopularInMenProducts: [Product] = []
  private var cachedTrendingInWomenProducts: [Product] = []
  private var cachedCategories: [String] = ["Latest", "Men", "Women", "Kids", "Accessories"]

  var cartProducts: [CartProduct] = [] {
    didSet {
      cartService.replaceCartItemsWith(cartProducts)
    }
  }

  init(
    fetchService: FetchingProductsService = AppInstance.shared.fetchService,
    eventsService: SystemaEventsService = AppInstance.shared.eventsService,
    trackWishlistService: TrackWishlistService = AppInstance.shared.trackWishListService,
    onClickService: TrackItemClickService = AppInstance.shared.onClickService,
    containerService: TrackContainerService = AppInstance.shared.trackContainerService,
    cartService: CartService = AppInstance.shared.cartService,
    trackCartService: TrackCartService = AppInstance.shared.trackCartService
  ) {
    self.fetchService = fetchService
    self.trackCartService = trackCartService
    self.eventsService = eventsService
    self.trackWishlistService = trackWishlistService
    self.onClickService = onClickService
    self.containerService = containerService
    self.cartService = cartService
  }
}

// MARK: - Methods

extension CategoryViewModel {
  func appendProduct(_ product: CartProduct) {
    cartService.appendProduct(product)
  }

  func updateWishlist(_ isSelected: Bool, for product: Product) {
    guard isSelected else {
      AppInstance.shared.wishList.removeAll(where: { $0.id == product.id })
      return
    }

    if !AppInstance.shared.wishList.contains(product) {
      AppInstance.shared.wishList.append(product)
    }
  }

  func cellVM(with product: Product) -> ProductCellViewModel {
    return ProductCellViewModel(product: product)
  }

  func getProducts() {
    let group = DispatchGroup()

    group.enter()
    fetchService.fetchCategoryPopularMen { [weak self] products in
      guard let slf = self else { return }
      slf.cachedPopularInMenProducts.removeAll()
      slf.cachedPopularInMenProducts.append(contentsOf: products)
      slf.eventsService.addEvent(.category, event: .getCategoryPopularMen)
      group.leave()
    } onError: { [weak self] error in
      guard let slf = self else { return }
      slf.onError?(error)
    }

    group.enter()
    fetchService.fetchCategoryTrendingWomen { [weak self] products in
      guard let slf = self else { return }
      slf.cachedTrendingInWomenProducts.removeAll()
      slf.cachedTrendingInWomenProducts.append(contentsOf: products)
      slf.eventsService.addEvent(.category, event: .getCategoryTrendingWomen)
      group.leave()
    } onError: { [weak self] error in
      guard let slf = self else { return }
      slf.onError?(error)
    }

    group.notify(queue: .main) {
      self.onSuccessFetch?()
    }
  }

  func trackOnClick(for product: Product) {
    onClickService.trackItemClicked(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.category, event: .trackItemClicked)
    } onError: { [weak self] error in
      guard let self = self else { return }
      self.onError?(error)
    }
  }

  func trackContainer() {
    guard cachedPopularInMenProducts.count > 0 else { return }

    containerService.trackContainer(for: .category, on: cachedPopularInMenProducts) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.category, event: .trackContainerShownTrending)
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackContainerPopular() {
    guard cachedPopularInMenProducts.count > 0 else { return }

    containerService.trackContainer(for: .category, on: cachedPopularInMenProducts) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.category, event: .trackContainerShownMenPopular)

    } onError: { error in
      self.onError?(error)
    }
  }

  func trackContainerTrending() {
    guard cachedTrendingInWomenProducts.count > 0 else { return }

    containerService.trackContainer(for: .category, on: cachedTrendingInWomenProducts) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.category, event: .trackContainerShownWomenTrending)
    } onError: { error in
      self.onError?(error)
    }
  }

  func monitorPageEvent() {
    guard cachedPopularInMenProducts.count > 0, let product = cachedPopularInMenProducts.first else { return }

    containerService.monitorPageEvent(on: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.category, event: .monitorPageEvent)
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackWishlist(_ isFavorite: Bool, for product: Product) {
    guard isFavorite else {
      return trackWishlistService.trackWishListRelinquished(product: product, isSelected: isFavorite) { [weak self] _ in
        guard let self = self else { return }
        self.eventsService.addEvent(.category, event: .trackWishListRelinquished)
        self.onSuccessTrack?()
      } onError: { error in
        self.onError?(error)
      }
    }

    trackWishlistService.trackWishListAcquired(product: product, isSelected: isFavorite) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.category, event: .trackWishListAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }
  
  func trackItemAquired(for product: Product) {
    trackCartService.trackItemAcquired(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.category, event: .trackItemAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }
}

// MARK: - Getters

extension CategoryViewModel {
  var popularMenProducts: [Product] { cachedPopularInMenProducts }
  var trendingWomenProducts: [Product] { cachedTrendingInWomenProducts }
  var categories: [String] { cachedCategories }
}

//
//  ProductViewController.swift
//  Systema
//
//  Created by JAG on 10/19/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import NSObject_Rx
import RxSwift
import SVProgressHUD
import UIKit

import shared
import SystemaIosSDK

private enum Tab: Int, CaseIterable {
  case related = 0
  case complementary
}

class ProductViewController: ViewController {
  var viewModel: ProductViewModel!

  private let cellIdentifier = "ProductCell"
  private let cellHeaderIdentifier = "ProductCellHeaderView"

  @IBOutlet var priceLabel: UILabel!
  @IBOutlet var nameLabel: UILabel!
  @IBOutlet var productImageView: UIImageView!
  @IBOutlet var descriptionTextView: UITextView!
  @IBOutlet var addToCartButton: UIButton!
  @IBOutlet var faveButton: UIButton!
  @IBOutlet var collectionView: UICollectionView!

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)

    if viewModel.relatedProducts.count > 0, viewModel.complementaryProducts.count > 0 {
      viewModel.trackContainerComplimentary()
      viewModel.trackContainerRelated()
      viewModel.monitorPageEvent()
      collectionView.reloadData()
    } else {
      fetchData()
    }
  }

  private func fetchData() {
    viewModel.getProducts()
  }
}

// MARK: - Setup

private extension ProductViewController {
  func setupCollectionView() {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.minimumLineSpacing = 10
    flowLayout.minimumInteritemSpacing = 0
    flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
    let width = (UIScreen.main.bounds.width / 2) - 10
    flowLayout.itemSize = CGSize(width: width, height: 210)

    flowLayout.scrollDirection = .vertical

    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.collectionViewLayout = flowLayout

    collectionView.register(UINib(
      nibName: cellIdentifier,
      bundle: nil
    ), forCellWithReuseIdentifier: cellIdentifier)

    collectionView.register(
      UINib(resource: R.nib.productCellHeaderView),
      forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
      withReuseIdentifier: R.nib.productCellHeaderView.name
    )

    flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 50)
  }

  private func setup() {
    refreshViews()
    setupCollectionView()
    viewModel.onSuccessFetch = handleSuccessFetch()
    viewModel.onError = handleFetchError()
  }

  private func refreshViews() {
    addToCartButton.imageView?.tintColor = .white
    nameLabel.text = viewModel.name
    priceLabel.text = viewModel.price
    descriptionTextView.text = viewModel.description

    guard let url = viewModel.imageURL else {
      return
    }
    productImageView.af_setImage(withURL: url)

    if AppInstance.shared.wishList.contains(viewModel.prod) {
      faveButton.setImage(R.image.favorite_active(), for: .normal)
    } else {
      faveButton.setImage(R.image.heart(), for: .normal)
    }
  }
}

// MARK: IBActions

extension ProductViewController {
  @IBAction
  func addToCart(_ sender: AnyObject) {
    viewModel.trackItemAquired(for: viewModel.prod)
    print("--- adding \(viewModel.prod.title) to cart ---")

    let cartProduct = CartProduct(
      itemId: viewModel.prod.id,
      title: viewModel.prod.title,
      price: viewModel.prod.price as! Double,
      quantity: 1,
      image: viewModel.prod.image,
      currency: viewModel.prod.currency,
      recId: viewModel.prod.recId,
      link: viewModel.prod.link
    )

    viewModel.appendProduct(cartProduct)
    DispatchQueue.main.async {
      SVProgressHUD.showSuccess(withStatus: "Successfully Added to Cart")
    }
  }

  @IBAction
  func addToWishlist(_ sender: AnyObject) {
    viewModel.trackWishlist(!viewModel.isFavorite, for: viewModel.prod)
    viewModel.updateWishlist(!viewModel.isFavorite, for: viewModel.prod)
    refreshViews()
  }
}

// MARK: - UICollectionViewDelegate

extension ProductViewController: UICollectionViewDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    print(" selected at \(indexPath.row)")
    var product: Product?
    switch indexPath.section {
    case Tab.related.rawValue:
      product = viewModel.relatedProducts[indexPath.row]
    case Tab.complementary.rawValue:
      product = viewModel.complementaryProducts[indexPath.row]
    default:
      print("invalid section")
    }
    guard let selectedProduct = product else { return }
    viewModel.trackOnClick(for: selectedProduct)

    let viewController = R.storyboard.product.productViewController()!
    viewController.viewModel = ProductViewModel(product: selectedProduct)
    navigationController?.pushViewController(viewController, animated: true)
  }
}

// MARK: - UICollectionView

extension ProductViewController: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return Tab.allCases.count
  }

  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    switch section {
    case Tab.related.rawValue:
      return viewModel.relatedProducts.count
    case Tab.complementary.rawValue:
      return viewModel.complementaryProducts.count
    default:
      return 0
    }
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    guard
      let productCell = collectionView.dequeueReusableCell(
        withReuseIdentifier: cellIdentifier,
        for: indexPath
      ) as? ProductCell
    else {
      return ProductCell()
    }

    switch indexPath.section {
    case Tab.related.rawValue:
      let vm = viewModel.cellVM(with: viewModel.relatedProducts[indexPath.row])
      productCell.viewModel = vm
    case Tab.complementary.rawValue:
      let vmPopular = viewModel.cellVM(with: viewModel.complementaryProducts[indexPath.row])
      productCell.viewModel = vmPopular
    default:
      print("invalid section")
    }

    productCell.onTapFavorite = handleOnTapFavorite()
    productCell.onTapAddToCart = handleOnTapCart()

    return productCell
  }

  func collectionView(
    _ collectionView: UICollectionView,
    viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath
  ) -> UICollectionReusableView {
    guard
      let header = collectionView.dequeueReusableSupplementaryView(
        ofKind: kind,
        withReuseIdentifier: cellHeaderIdentifier, for: indexPath
      )
      as? ProductCellHeaderView
    else {
      return ProductCellHeaderView()
    }

    switch indexPath.section {
    case Tab.related.rawValue:
      header.titleLabel.text = "Related Products"
    case Tab.complementary.rawValue:
      header.titleLabel.text = "Complementary Products"
    default:
      header.titleLabel.text = ""
    }

    return header
  }
}

// MARK: - Event Handlers

private extension ProductViewController {
  func handleFetchError() -> ErrorResult {
    return { error in
      print("error \(error.localizedDescription)")
    }
  }

  func handleSuccessFetch() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.viewModel.trackContainerComplimentary()
      self.viewModel.trackContainerRelated()
      self.viewModel.monitorPageEvent()
      self.collectionView.reloadData()
    }
  }

  func handleOnTapFavorite() -> DoubleResult<Product, Bool> {
    return { [weak self] product, selected in
      guard let self = self else { return }
      self.viewModel.trackWishlist(selected, for: product)
      self.viewModel.updateWishlist(selected, for: product)
      self.collectionView.reloadData()
    }
  }

  func handleOnTapCart() -> SingleResult<Product> {
    return { [weak self] product in
      guard let self = self else { return }

      self.viewModel.trackItemAquired(for: product)
      print("--- adding \(product.title) to cart ---")

      let cartProduct = CartProduct(
        itemId: product.id,
        title: product.title,
        price: product.price as! Double,
        quantity: 1,
        image: product.image,
        currency: product.currency,
        recId: product.recId,
        link: product.link
      )

      self.viewModel.appendProduct(cartProduct)
      DispatchQueue.main.async {
        SVProgressHUD.showSuccess(withStatus: "Successfully Added to Cart")
      }
    }
  }
}

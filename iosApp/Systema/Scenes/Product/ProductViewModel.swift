//
//  ProductViewModel.swift
//  Systema
//
//  Created by JAG on 10/19/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import NSObject_Rx
import RxCocoa
import RxSwift

import shared
import SystemaIosSDK

class ProductViewModel {
  var onError: ErrorResult?
  var onSuccessFetch: VoidResult?
  var onSuccessTrack: VoidResult?

  private let product: Product
  private var cachedIsFavorite = false

  private let fetchService: FetchingProductsService!
  private let eventsService: SystemaEventsService!
  private let trackCartService: TrackCartService!
  private let trackWishlistService: TrackWishlistService!
  private let trackItemClickService: TrackItemClickService!
  private let containerService: TrackContainerService!
  private let cartService: CartService!

  private var cachedRelatedProducts: [Product] = []
  private var cachedComplementaryProducts: [Product] = []

  init(
    product: Product,
    fetchService: FetchingProductsService = AppInstance.shared.fetchService,
    trackCartService: TrackCartService = AppInstance.shared.trackCartService,
    eventsService: SystemaEventsService = AppInstance.shared.eventsService,
    trackWishlistService: TrackWishlistService = AppInstance.shared.trackWishListService,
    trackItemClickService: TrackItemClickService = AppInstance.shared.onClickService,
    cartService: CartService = AppInstance.shared.cartService,
    containerService: TrackContainerService = AppInstance.shared.trackContainerService
  ) {
    self.eventsService = eventsService
    self.trackWishlistService = trackWishlistService
    self.trackItemClickService = trackItemClickService
    self.containerService = containerService
    self.trackCartService = trackCartService
    self.cartService = cartService
    self.fetchService = fetchService

    self.product = product
    print("brand: \(product.title)")
    print("price: \(product.price)")
    print("image: \(product.image)")
    print("currency: \(product.currency)")
    print("description: \(product.description)")
  }
}

// MARK: - Methods

extension ProductViewModel {
  func appendProduct(_ product: CartProduct) {
    cartService.appendProduct(product)
  }

  func cellVM(with product: Product) -> ProductCellViewModel {
    return ProductCellViewModel(product: product)
  }

  func updateWishlist(_ isSelected: Bool, for product: Product) {
    cachedIsFavorite = isSelected
    guard isSelected else {
      AppInstance.shared.wishList.removeAll(where: { $0.id == product.id })
      return
    }

    if !AppInstance.shared.wishList.contains(product) {
      AppInstance.shared.wishList.append(product)
    }
  }

  func trackWishlist(_ isFavorite: Bool, for product: Product) {
    guard isFavorite else {
      return trackWishlistService.trackWishListRelinquished(product: product, isSelected: isFavorite) { [weak self] _ in
        guard let self = self else { return }
        self.eventsService.addEvent(.product, event: .trackWishListRelinquished)
        self.onSuccessTrack?()
      } onError: { error in
        self.onError?(error)
      }
    }

    trackWishlistService.trackWishListAcquired(product: product, isSelected: isFavorite) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.product, event: .trackWishListAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }

  func monitorPageEvent() {
    containerService.monitorPageEvent(on: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.product, event: .monitorPageEvent)
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackOnClick(for product: Product) {
    trackItemClickService.trackItemClicked(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.product, event: .trackItemClicked)
    } onError: { [weak self] error in
      guard let self = self else { return }
      self.onError?(error)
    }
  }

  func trackItemAquired(for product: Product) {
    trackCartService.trackItemAcquired(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.product, event: .trackItemAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackContainerComplimentary() {
    guard complementaryProducts.count > 0 else { return }
    
    containerService.trackContainer(for: .product, on: complementaryProducts) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.product, event: .trackContainerShownProductComplementary)
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackContainerRelated() {
    guard relatedProducts.count > 0 else { return }
    
    containerService.trackContainer(for: .product, on: relatedProducts) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.product, event: .trackContainerShownProductRelated)
    } onError: { error in
      self.onError?(error)
    }
  }

  func getProducts() {
    let group = DispatchGroup()

    group.enter()
    fetchService.fetchComplementary(productId: product.id) { [weak self] products in
      guard let slf = self else { return }
      slf.cachedComplementaryProducts.removeAll()
      slf.cachedComplementaryProducts.append(contentsOf: products)
      slf.eventsService.addEvent(.product, event: .getComplementary)
      group.leave()
    } onError: { [weak self] error in
      guard let slf = self else { return }
      slf.onError?(error)
    }

    group.enter()
    fetchService.fetchRelatedProducts(productId: product.id) { [weak self] products in
      guard let slf = self else { return }
      slf.cachedRelatedProducts.removeAll()
      slf.cachedRelatedProducts.append(contentsOf: products)
      slf.eventsService.addEvent(.product, event: .getRelated)
      group.leave()
    } onError: { [weak self] error in
      guard let slf = self else { return }
      slf.onError?(error)
    }

    group.notify(queue: .main) {
      self.onSuccessFetch?()
    }
  }
}

// MARK: - Getters

extension ProductViewModel {
  var relatedProducts: [Product] { cachedRelatedProducts }
  var complementaryProducts: [Product] { cachedComplementaryProducts }

  var prod: Product { product }
  var name: String? { product.title }
  var price: String? {
    guard let currency = product.currency,
      let price = product.price else {
      return nil
    }

    return "\(currency) \(price)"
  }

  var imageURL: URL? { URL(string: product.image) }
  var description: String { product.description }
  var isFavorite: Bool { cachedIsFavorite }
}

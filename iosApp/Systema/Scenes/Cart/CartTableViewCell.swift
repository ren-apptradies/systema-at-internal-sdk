//
//  CartTableViewCell.swift
//  Systema
//
//  Created by JAG on 10/9/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import AlamofireImage
import Foundation
import UIKit

// TODO: Add/Minus buttons
class CartTableViewCell: UITableViewCell {
  
  @IBOutlet weak var productCostLabel: UILabel!
  @IBOutlet weak var productNameLabel: UILabel!
  @IBOutlet weak var productQtyLabel: UILabel!
  @IBOutlet weak var productImageView: UIImageView!
  @IBOutlet weak var subtractButton: UIButton!
  
  var onTapRemove: VoidResult?
  var onTappingAdd: VoidResult?
  var onTappingSubstract: VoidResult?

  var viewModel: CartCellViewModel! {
    didSet {
      refresh()
    }
  }
}

// MARK: - IBActions

private extension CartTableViewCell {
  @IBAction func remove() {
    onTapRemove?()
  }
  
  @IBAction func add() {
    onTappingAdd?()
  }
  
  @IBAction func subtract() {
    onTappingSubstract?()
  }
}

// MARK: - Refresh

private extension CartTableViewCell {
  func refresh() {
    productNameLabel.text = viewModel.name
    productCostLabel.text = viewModel.price
    productQtyLabel.text = "\(viewModel.quantity!)"
  
    guard let url = viewModel.imageURL else {
      return
    }
    productImageView.af_setImage(withURL: url)
    
    guard let qty = viewModel.quantity, qty > 1 else {
      subtractButton.setImage(R.image.minusInactive(), for: .normal)
      subtractButton.isEnabled = false
      return
    }

    subtractButton.setImage(R.image.minusActive(), for: .normal)
    subtractButton.isEnabled = true
  }
}

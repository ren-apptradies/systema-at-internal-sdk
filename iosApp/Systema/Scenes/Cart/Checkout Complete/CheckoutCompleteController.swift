//
//  CheckoutCompleteController.swift
//  Systema
//
//  Created by JAG on 10/7/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import UIKit

class CheckoutCompleteController: ViewController {
  private var viewModel = CheckoutCompleteViewModel()

  @IBOutlet weak var amountLabel: UILabel!
  @IBOutlet weak var itemLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupBindings()
    fetchData()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)
  }

  private func setupBindings() { }

  private func fetchData() { }
}

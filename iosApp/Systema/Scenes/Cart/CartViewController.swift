//
//  CartViewController.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import RxSwift
import shared
import SVProgressHUD
import SystemaIosSDK
import UIKit

class CartViewController: ViewController {
  private var viewModel = CartViewModel()

  private let cellIdentifier = "ProductCell"
  private let cellHeaderIdentifier = "ProductCellHeaderView"
  private let refreshControl = UIRefreshControl()

  @IBOutlet var itemLabel: UILabel!
  @IBOutlet var itemTotalLabel: UILabel!
  @IBOutlet var shippingTotalLabel: UILabel!
  @IBOutlet var totalLabel: UILabel!
  @IBOutlet var footerView: UIView!

  @IBOutlet var collectionView: UICollectionView!
  @IBOutlet var tableview: UITableView!

  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  // TODO: Handle new cart addition
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)

    if viewModel.complementaryProducts.count > 0 {
      viewModel.trackContainer()
      viewModel.monitorPageEvent()
    }

    if viewModel.complementaryProducts.isEmpty ||
      (viewModel.cartProducts.count != viewModel.currentCart.count) {
      fetchData()
    }

    refresh()
  }

  private func refresh() {
    viewModel.refresh()

    // Reload views
    tableview.reloadData()
    collectionView.reloadData()
    setupCheckoutDetails()
  }

  @objc
  private func fetchData() {
    viewModel.getComplementaryProducts()
  }
}

// MARK: - Setup

private extension CartViewController {
  func setup() {
    navigationController?.setNavigationBarHidden(true, animated: false)
    setupCollectionView()

    viewModel.onSuccessFetch = handleSuccessFetch()
    viewModel.onError = handleFetchError()

    tableview.addSubview(refreshControl)
    refreshControl.addTarget(self, action: #selector(fetchData), for: .valueChanged)
  }

  func setupCollectionView() {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.minimumLineSpacing = 10
    flowLayout.minimumInteritemSpacing = 0
    flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
    let width = (UIScreen.main.bounds.width / 2) - 10
    flowLayout.itemSize = CGSize(width: width, height: 210)
    flowLayout.scrollDirection = .vertical

    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.collectionViewLayout = flowLayout

    collectionView.register(UINib(
      nibName: cellIdentifier,
      bundle: nil
    ), forCellWithReuseIdentifier: cellIdentifier)

    flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 50)

    collectionView.register(
      UINib(resource: R.nib.productCellHeaderView),
      forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
      withReuseIdentifier: R.nib.productCellHeaderView.name
    )
  }

  func setupCheckoutDetails() {
    itemLabel.text = viewModel.itemCount
    itemTotalLabel.text = viewModel.itemTotal
    shippingTotalLabel.text = viewModel.shippingTotal
    totalLabel.text = viewModel.cartTotal
    footerView.isHidden = viewModel.cartProducts.isEmpty
  }
}

extension CartViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.cartProducts.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(
      withIdentifier: R.reuseIdentifier.cartViewCell,
      for: indexPath
    )
    else { return UITableViewCell() }

    let cartProduct = viewModel.cartProducts[indexPath.row]

    let vm = viewModel.cellVM(with: cartProduct)
    cell.viewModel = vm
    cell.onTapRemove = {
      let product = productFromCart(cartProduct)
      self.viewModel.trackItemRelinquished(for: product)

      self.viewModel.removeItemAtIndex(index: indexPath.row)
      self.refresh()
    }

    cell.onTappingAdd = { [self] in
      let product = productFromCart(cartProduct)

      let quantity = cartProduct.quantity + 1
      self.viewModel.trackItemAquired(for: product)
      self.viewModel.replaceItemAtIndex(index: indexPath.row, quantity: quantity)
      self.refresh()
    }

    cell.onTappingSubstract = {
      let product = productFromCart(cartProduct)
      self.viewModel.trackItemRelinquished(for: product)

      let quantity = cartProduct.quantity - 1
      self.viewModel.replaceItemAtIndex(index: indexPath.row, quantity: quantity)
      self.refresh()
    }

    return cell
  }
}

// MARK: - UICollectionViewDelegate

extension CartViewController: UICollectionViewDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    print(" selected at \(indexPath.row)")
    let product = viewModel.complementaryProducts[indexPath.row]
    viewModel.trackOnClick(for: product)

    let viewController = R.storyboard.product.productViewController()!
    viewController.viewModel = ProductViewModel(product: product)
    navigationController?.pushViewController(viewController, animated: true)
  }
}

// MARK: - UICollectionView

extension CartViewController: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    return viewModel.complementaryProducts.count
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    guard
      let productCell = collectionView.dequeueReusableCell(
        withReuseIdentifier: cellIdentifier,
        for: indexPath
      ) as? ProductCell
    else {
      return ProductCell()
    }

    let vm = viewModel.productCellVM(with: viewModel.complementaryProducts[indexPath.row])
    productCell.viewModel = vm
    productCell.onTapFavorite = handleOnTapFavorite()
    productCell.onTapAddToCart = handleOnTapCart()

    return productCell
  }

  func collectionView(
    _ collectionView: UICollectionView,
    viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath
  ) -> UICollectionReusableView {
    guard
      let header = collectionView.dequeueReusableSupplementaryView(
        ofKind: kind,
        withReuseIdentifier: cellHeaderIdentifier, for: indexPath
      )
      as? ProductCellHeaderView
    else {
      return ProductCellHeaderView()
    }

    header.titleLabel.text = "Complementary Products"

    return header
  }
}

// MARK: - Event Handlers

private extension CartViewController {
  func handleFetchError() -> ErrorResult {
    return { error in
      print("error \(error.localizedDescription)")

      self.refreshControl.endRefreshing()
    }
  }

  func handleSuccessFetch() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      print(self.viewModel.complementaryProducts)

      self.refreshControl.endRefreshing()
      self.viewModel.trackContainer()
      self.viewModel.monitorPageEvent()

      self.refresh()
    }
  }

  func handleOnTapFavorite() -> DoubleResult<Product, Bool> {
    return { [weak self] product, selected in
      guard let self = self else { return }
      self.viewModel.trackWishlist(selected, for: product)
      self.viewModel.updateWishlist(selected, for: product)

      self.refresh()
    }
  }

  func handleOnTapCart() -> SingleResult<Product> {
    return { [weak self] product in
      guard let self = self else { return }

      self.viewModel.trackItemAquired(for: product)
      print("--- adding \(product.title) to cart ---")

      let cartProduct = CartProduct(
        itemId: product.id,
        title: product.title,
        price: product.price as! Double,
        quantity: 1,
        image: product.image,
        currency: product.currency,
        recId: product.recId,
        link: product.link
      )

      self.viewModel.appendProduct(cartProduct)
      self.viewModel.getComplementaryProducts()
      self.refresh()

      DispatchQueue.main.async {
        SVProgressHUD.showSuccess(withStatus: "Successfully Added to Cart")
      }
    }
  }
}

//
//  CheckoutViewModel.swift
//  Systema
//
//  Created by JAG on 10/7/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import NSObject_Rx
import RxCocoa
import RxSwift

import SystemaIosSDK

import shared

class CheckoutViewModel {
  var onSuccessFetch: VoidResult?
  var onSuccessTrack: VoidResult?
  var onError: ErrorResult?

  private let trackCartService: TrackCartService!
  private let eventsService: SystemaEventsService!
  private let cartService: CartService!
  private var cachedCartProducts: [CartProduct] = []

  init(
    trackCartService: TrackCartService = AppInstance.shared.trackCartService,
    eventsService: SystemaEventsService = AppInstance.shared.eventsService,
    cartService: CartService = AppInstance.shared.cartService
  ) {
    self.trackCartService = trackCartService
    self.eventsService = eventsService
    self.cartService = cartService
    cachedCartProducts = cartService.products()
  }
}

// MARK: - Methods

extension CheckoutViewModel {
  func trackOrderCompleted() {
    let orderItems = cachedCartProducts.map({ product in
      OrderItem(
        itemId: product.itemId,
        quantity: Int32(product.quantity),
        unitCost: product.price ?? 0.0,
        unitTaxAmount: 12.5,
        currency: "AUD"
      )
    })

    let order = PurchaseOrder(
      orderId: "DUMMY_ORDER_ID",
      chargedAmount: 250.0,
      totalAmount: 250.0,
      taxAmount: 12.5,
      shippingAmount: 10.0,
      discountAmount: 0.0,
      discountCodes: nil,
      currency: Currency.aud.value,
      shippingAddress: ShippingAddress(city: "Sydney", state: "NSW", postCode: "2000", country: "Australia"),
      items: orderItems
    )
    
    return trackCartService.trackCartCompleted(
      order: order,
      url: "systema://ios-demo-app/checkout/order-page",
      referrer: "",
      onSuccess: { [weak self] _ in
        guard let self = self else { return }
        self.eventsService.addEvent(.cart, event: .trackAcquisitionComplete)
        self.emptyCart()
        self.onSuccessTrack?()
      }, onError: { error in
        self.onError?(error)
      }
    )
  }

  func emptyCart() {
    cartService.empty()
  }
}

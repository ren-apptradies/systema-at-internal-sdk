//
//  CheckoutController.swift
//  Systema
//
//  Created by JAG on 10/7/21.gm
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation

import shared
import SystemaIosSDK

class CheckoutController: ViewController {
  private var viewModel = CheckoutViewModel()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupBindings()
    fetchData()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)
  }

  private func setupBindings() { }

  private func fetchData() { }
}

extension CheckoutController {
  // TODO: Add params
  @IBAction func confirm() {
    viewModel.trackOrderCompleted()
    let viewController = R.storyboard.cart.checkoutCompleteController()!
    navigationController?.pushViewController(viewController, animated: true)
  }
}

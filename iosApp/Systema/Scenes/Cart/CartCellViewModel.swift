//
//  CartCellViewModel.swift
//  Systema
//
//  Created by JAG on 10/11/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import shared
import SystemaIosSDK

class CartCellViewModel {
  private let cartProduct: CartProduct

  init(
    cartProduct: CartProduct
  ) {
    self.cartProduct = cartProduct
  }
}

// MARK: - Methods

extension CartCellViewModel {
}

// MARK: - Getters

extension CartCellViewModel {
  var name: String? { cartProduct.title }
  var price: String? {
    guard let currency = cartProduct.currency,
      let price = cartProduct.price else {
      return nil
    }

    return "\(currency) \(price)"
  }
  
  var quantity: Int? { cartProduct.quantity }

  var imageURL: URL? { URL(string: cartProduct.image) }
}

//
//  CartViewModel.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxCocoa
import RxSwift

import shared
import SystemaIosSDK

class CartViewModel {
  var onSuccessFetch: VoidResult?
  var onSuccessTrack: VoidResult?
  var onError: ErrorResult?

  private var cachedCartProducts: [CartProduct] = []
  private var cachedComplementaryProducts: [Product] = []
  private let cartService: CartService!

  private let fetchService: FetchingProductsService!
  private let trackWishlistService: TrackWishlistService!
  private let trackCartService: TrackCartService!
  private let onClickService: TrackItemClickService!
  private let containerService: TrackContainerService!
  private let eventsService: SystemaEventsService!

  private var total: Double {
    var total = 0.0
    for product in cartProducts {
      let temp = product.price! * Double(product.quantity)
      total += temp
    }
    return total
  }

  private var shipping: Double {
    let prices = cachedCartProducts.map({ $0.price })
    let total = prices.reduce(0.0, { x, y in
      x + y!
    })

    return total * 0.05
  }

  init(
    cartService: CartService = AppInstance.shared.cartService,
    eventsService: SystemaEventsService = AppInstance.shared.eventsService,
    fetchService: FetchingProductsService = AppInstance.shared.fetchService,
    trackWishlistService: TrackWishlistService = AppInstance.shared.trackWishListService,
    onClickService: TrackItemClickService = AppInstance.shared.onClickService,
    containerService: TrackContainerService = AppInstance.shared.trackContainerService,
    trackCartService: TrackCartService = AppInstance.shared.trackCartService
  ) {
    self.cartService = cartService
    self.trackCartService = trackCartService
    self.eventsService = eventsService
    self.fetchService = fetchService
    self.trackWishlistService = trackWishlistService
    self.onClickService = onClickService
    self.containerService = containerService
    cachedCartProducts = cartService.products()
  }
}

// MARK: - Methods

extension CartViewModel {
  func getComplementaryProducts() {
    let productIDs = currentCart.map({ $0.itemId })
    fetchService.fetchCartComplementary(productIDs: productIDs) { [weak self] products in
      guard let slf = self else { return }
      slf.cachedComplementaryProducts.removeAll()
      slf.cachedComplementaryProducts.append(contentsOf: products)
      slf.eventsService.addEvent(.cart, event: .getCartComplementary)
      slf.onSuccessFetch?()
    } onError: { [weak self] error in
      guard let slf = self else { return }
      debugLog(error.localizedDescription)
      slf.onError?(error)
    }
  }

  func productCellVM(with product: Product) -> ProductCellViewModel {
    return ProductCellViewModel(product: product)
  }

  func cellVM(with cartProduct: CartProduct) -> CartCellViewModel {
    return CartCellViewModel(cartProduct: cartProduct)
  }

  // Cart Methods
  func refresh() {
    cachedCartProducts.removeAll()
    cachedCartProducts = cartService.products()
  }

  func removeItemAtIndex(index: Int) {
    cachedCartProducts.remove(at: index)
    cartService.replaceCartItemsWith(cachedCartProducts)
  }

  func replaceItemAtIndex(index: Int, quantity: Int) {
    let itemAtIndex = cachedCartProducts[index]
    let temp = CartProduct(
      itemId: itemAtIndex.itemId,
      title: itemAtIndex.title,
      price: itemAtIndex.price,
      quantity: quantity,
      image: itemAtIndex.image,
      currency: itemAtIndex.currency,
      recId: itemAtIndex.recId,
      link: itemAtIndex.link
    )
    cachedCartProducts[index] = temp
    cartService.replaceCartItemsWith(cachedCartProducts)
  }

  func appendProduct(_ product: CartProduct) {
    cartService.appendProduct(product)
  }

  func updateWishlist(_ isSelected: Bool, for product: Product) {
    guard isSelected else {
      AppInstance.shared.wishList.removeAll(where: { $0.id == product.id })
      return
    }

    if !AppInstance.shared.wishList.contains(product) {
      AppInstance.shared.wishList.append(product)
    }
  }

  // Track Events

  func trackOnClick(for product: Product) {
    onClickService.trackItemClicked(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.cart, event: .trackItemClicked)
    } onError: { [weak self] error in
      guard let self = self else { return }
      self.onError?(error)
    }
  }

  func trackContainer() {
    guard complementaryProducts.count > 0 else { return }
    containerService.trackContainer(for: .cart, on: complementaryProducts) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.cart, event: .trackContainerShownCart)
    } onError: { error in
      self.onError?(error)
    }
  }

  func monitorPageEvent() {
    guard complementaryProducts.count > 0, let product = complementaryProducts.first else { return }

    containerService.monitorPageEvent(on: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.cart, event: .monitorPageEvent)
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackWishlist(_ isFavorite: Bool, for product: Product) {
    guard isFavorite else {
      return trackWishlistService.trackWishListRelinquished(product: product, isSelected: isFavorite) { [weak self] _ in
        guard let self = self else { return }
        self.eventsService.addEvent(.cart, event: .trackWishListRelinquished)
        self.onSuccessTrack?()
      } onError: { error in
        self.onError?(error)
      }
    }

    trackWishlistService.trackWishListAcquired(product: product, isSelected: isFavorite) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.cart, event: .trackWishListAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackItemAquired(for product: Product) {
//    print("track:\(product.id) and quantity:\(quantity)")
    trackCartService.trackItemAcquired(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.cart, event: .trackItemAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackItemRelinquished(for product: Product) {
    trackCartService.trackItemRelinquished(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.cart, event: .trackItemRelinquished)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }
}

// MARK: - Getters

extension CartViewModel {
  var complementaryProducts: [Product] { cachedComplementaryProducts }

  var cartProducts: [CartProduct] { cachedCartProducts }

  var currentCart: [CartProduct] { cartService.products() }
  
  var itemTotal: String {
    let totalStr: String = String(format: "$%.2f", total)
    return totalStr
  }

  var itemCount: String {
    let count = cartProducts.count
    return "Items (\(count))"
  }

  var shippingTotal: String {
    let shippingStr: String = String(format: "$%.2f", shipping)
    return shippingStr
  }

  var cartTotal: String {
    let cartTotal = shipping + total
    return String(format: "$%.2f", cartTotal)
  }
}

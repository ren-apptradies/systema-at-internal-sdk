//
//  HomeViewController.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxSwift
import SVProgressHUD
import SystemaIosSDK
import UIKit

import shared

private enum Tab: Int, CaseIterable {
  case trending = 0
  case popular
}

class HomeViewController: ViewController {
  private var viewModel = HomeViewModel()

  private let cellIdentifier = "ProductCell"
  private let cellHeaderIdentifier = "ProductCellHeaderView"

  @IBOutlet private(set) var collectionView: UICollectionView!
  @IBOutlet private(set) var searchTextField: UITextField!
  private let refreshControl = UIRefreshControl()

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    setupBindings()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)

    if viewModel.popularProducts.count > 0, viewModel.trendingProducts.count > 0 {
      viewModel.trackContainerPopular()
      viewModel.trackContainerTrending()
      viewModel.monitorPageEvent()
    } else {
      fetchData()
    }

    collectionView.reloadData()
  }

  private func setupBindings() {
    searchTextField
      .rx
      .text
      .orEmpty
      .throttle(.milliseconds(1000), scheduler: MainScheduler.instance)
      .distinctUntilChanged()
      .subscribe(onNext: { [weak self] searchStr in
        guard let self = self else { return }
        print("search:\(searchStr)")
        guard self.viewModel.trendingProducts.count > 0
        else {
          return
        }
        let product = self.viewModel.trendingProducts[0]
        self.viewModel.smartSearch(searchString: searchStr, product: product)
      })
      .disposed(by: rx.disposeBag)
  }

  @objc
  private func fetchData() {
    viewModel.getProducts()
  }
}

// MARK: - Setup

private extension HomeViewController {
  func setup() {
    navigationController?.setNavigationBarHidden(true, animated: false)
    setupCollectionView()
    viewModel.onSuccessFetch = handleSuccessFetch()
    viewModel.onError = handleFetchError()
  }

  func setupCollectionView() {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.minimumLineSpacing = 10
    flowLayout.minimumInteritemSpacing = 0
    flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
    let width = (UIScreen.main.bounds.width / 2) - 10
    flowLayout.itemSize = CGSize(width: width, height: 210)

    flowLayout.scrollDirection = .vertical

    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.collectionViewLayout = flowLayout

    collectionView.register(UINib(
      nibName: cellIdentifier,
      bundle: nil
    ), forCellWithReuseIdentifier: cellIdentifier)

    collectionView.register(
      UINib(resource: R.nib.productCellHeaderView),
      forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
      withReuseIdentifier: R.nib.productCellHeaderView.name
    )

    flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 50)

    collectionView.addSubview(refreshControl)
    refreshControl.addTarget(self, action: #selector(fetchData), for: .valueChanged)
  }
}

// MARK: - UICollectionViewDelegate

extension HomeViewController: UICollectionViewDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    print(" selected at \(indexPath.row)")
    var product: Product?
    switch indexPath.section {
    case Tab.trending.rawValue:
      product = viewModel.trendingProducts[indexPath.row]
    case Tab.popular.rawValue:
      product = viewModel.popularProducts[indexPath.row]
    default:
      print("invalid section")
    }
    guard let selectedProduct = product else { return }
    viewModel.trackOnClick(for: selectedProduct)

    let viewController = R.storyboard.product.productViewController()!
    viewController.viewModel = ProductViewModel(product: selectedProduct)
    navigationController?.pushViewController(viewController, animated: true)
  }
}

// MARK: - UICollectionViewDataSource

extension HomeViewController: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return Tab.allCases.count
  }

  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    switch section {
    case Tab.trending.rawValue:
      return viewModel.trendingProducts.count
    case Tab.popular.rawValue:
      return viewModel.popularProducts.count
    default:
      return 0
    }
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    guard
      let productCell = collectionView.dequeueReusableCell(
        withReuseIdentifier: cellIdentifier,
        for: indexPath
      ) as? ProductCell
    else {
      return ProductCell()
    }

    switch indexPath.section {
    case Tab.trending.rawValue:
      let vm = viewModel.cellVM(with: viewModel.trendingProducts[indexPath.row])
      productCell.viewModel = vm
    case Tab.popular.rawValue:
      let vmPopular = viewModel.cellVM(with: viewModel.popularProducts[indexPath.row])
      productCell.viewModel = vmPopular
    default:
      print("invalid section")
    }
    productCell.onTapFavorite = handleOnTapFavorite()
    productCell.onTapAddToCart = handleOnTapCart()

    return productCell
  }

  func collectionView(
    _ collectionView: UICollectionView,
    viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath
  ) -> UICollectionReusableView {
    guard
      let header = collectionView.dequeueReusableSupplementaryView(
        ofKind: kind,
        withReuseIdentifier: cellHeaderIdentifier, for: indexPath
      )
      as? ProductCellHeaderView
    else {
      return ProductCellHeaderView()
    }

    switch indexPath.section {
    case Tab.trending.rawValue:
      header.titleLabel.text = "Trending Products"
    case Tab.popular.rawValue:
      header.titleLabel.text = "Popular Products"
    default:
      header.titleLabel.text = ""
    }

    return header
  }
}

// MARK: - IBActions

private extension HomeViewController {
  @IBAction func settingsTapped() {
  }
}

// MARK: - Event Handlers

private extension HomeViewController {
  func handleFetchError() -> ErrorResult {
    return { error in
      self.refreshControl.endRefreshing()
      print("error \(error.localizedDescription)")
    }
  }

  func handleSuccessFetch() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.refreshControl.endRefreshing()
      self.viewModel.trackContainerPopular()
      self.viewModel.trackContainerTrending()
      self.viewModel.monitorPageEvent()
      self.collectionView.reloadData()
    }
  }

  func handleOnTapFavorite() -> DoubleResult<Product, Bool> {
    return { [weak self] product, selected in
      guard let self = self else { return }
      self.viewModel.trackWishlist(selected, for: product)
      self.viewModel.updateWishlist(selected, for: product)
      self.collectionView.reloadData()
    }
  }

  func handleOnTapCart() -> SingleResult<Product> {
    return { [weak self] product in
      guard let self = self else { return }

      self.viewModel.trackItemAquired(for: product)
      print("--- adding \(product.title) to cart ---")

      let cartProduct = CartProduct(
        itemId: product.id,
        title: product.title,
        price: product.price as! Double,
        quantity: 1,
        image: product.image,
        currency: product.currency,
        recId: product.recId,
        link: product.link
      )

      self.viewModel.appendProduct(cartProduct)
      DispatchQueue.main.async {
        SVProgressHUD.showSuccess(withStatus: "Successfully Added to Cart")
      }
    }
  }
}

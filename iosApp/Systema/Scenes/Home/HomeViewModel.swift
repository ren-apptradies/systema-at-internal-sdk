//
//  HomeViewModel.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxCocoa
import RxSwift
import shared
import SystemaIosSDK

class HomeViewModel {
  var onSuccessFetch: VoidResult?
  var onSuccessTrack: VoidResult?
  var onError: ErrorResult?

  private let fetchService: FetchingProductsService!
  private let eventsService: SystemaEventsService!
  private let trackWishlistService: TrackWishlistService!
  private let wishlistService: WishlistService!
  private let trackCartService: TrackCartService!
  private let cartService: CartService!
  private let trackSearchService: TrackSearchService!

  private let onClickService: TrackItemClickService!
  private let containerService: TrackContainerService!

  private var cachedTrendingProducts: [Product] = []
  private var cachedPopularProducts: [Product] = []

  init(
    fetchService: FetchingProductsService = AppInstance.shared.fetchService,
    eventsService: SystemaEventsService = AppInstance.shared.eventsService,
    wishlistService: WishlistService = AppInstance.shared.wishlistService,
    trackWishlistService: TrackWishlistService = AppInstance.shared.trackWishListService,
    onClickService: TrackItemClickService = AppInstance.shared.onClickService,
    containerService: TrackContainerService = AppInstance.shared.trackContainerService,
    cartService: CartService = AppInstance.shared.cartService,
    trackCartService: TrackCartService = AppInstance.shared.trackCartService,
    trackSearchService: TrackSearchService = AppInstance.shared.trackSearchService
  ) {
    self.fetchService = fetchService
    self.eventsService = eventsService
    self.wishlistService = wishlistService
    self.trackWishlistService = trackWishlistService
    self.onClickService = onClickService
    self.containerService = containerService
    self.cartService = cartService
    self.trackCartService = trackCartService
    self.trackSearchService = trackSearchService
  }
}

// MARK: - Methods

extension HomeViewModel {
  func appendProduct(_ product: CartProduct) {
    cartService.appendProduct(product)
  }
  
  func updateWishlist(_ isSelected: Bool, for product: Product) {
    guard isSelected else {
      AppInstance.shared.wishList.removeAll(where: { $0.id == product.id })
      return
    }
    
    if !AppInstance.shared.wishList.contains(product) {
      AppInstance.shared.wishList.append(product)
    }
  }

  func cellVM(with product: Product) -> ProductCellViewModel {
    return ProductCellViewModel(product: product)
  }

  func getProducts() {
    let group = DispatchGroup()

    group.enter()
    fetchService.fetchTrendingProducts { [weak self] products in
      guard let self = self else { return }
      self.cachedTrendingProducts.removeAll()
      self.cachedTrendingProducts.append(contentsOf: products)
      self.eventsService.addEvent(.home, event: .getTrending)
      group.leave()
    } onError: { [weak self] error in
      guard let self = self else { return }
      self.onError?(error)
    }

    group.enter()
    fetchService.fetchPopularProducts { [weak self] products in
      guard let self = self else { return }
      self.cachedPopularProducts.removeAll()
      self.cachedPopularProducts.append(contentsOf: products)
      self.eventsService.addEvent(.home, event: .getPopular)
      group.leave()
    } onError: { [weak self] error in
      guard let self = self else { return }
      self.onError?(error)
    }

    group.notify(queue: .main) {
      self.onSuccessFetch?()
    }
  }

  func trackOnClick(for product: Product) {
    onClickService.trackItemClicked(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.home, event: .trackItemClicked)
    } onError: { [weak self] error in
      guard let self = self else { return }
      self.onError?(error)
    }
  }

  func trackContainerPopular() {
    guard cachedPopularProducts.count > 0 else { return }

    containerService.trackContainer(for: .home, on: cachedPopularProducts) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.home, event: .trackContainerShownPopular)

    } onError: { error in
      self.onError?(error)
    }
  }

  func trackContainerTrending() {
    guard cachedTrendingProducts.count > 0 else { return }

    containerService.trackContainer(for: .home, on: cachedTrendingProducts) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.home, event: .trackContainerShownTrending)
    } onError: { error in
      self.onError?(error)
    }
  }

  func monitorPageEvent() {
    guard cachedTrendingProducts.count > 0, let product = cachedTrendingProducts.first else { return }

    containerService.monitorPageEvent(on: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.home, event: .monitorPageEvent)
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackWishlist(_ isFavorite: Bool, for product: Product) {
    guard isFavorite else {
      return trackWishlistService.trackWishListRelinquished(product: product, isSelected: isFavorite) { [weak self] _ in
        guard let self = self else { return }
        self.eventsService.addEvent(.home, event: .trackWishListRelinquished)
        self.onSuccessTrack?()
      } onError: { error in
        self.onError?(error)
      }
    }

    trackWishlistService.trackWishListAcquired(product: product, isSelected: isFavorite) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.home, event: .trackWishListAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackItemAquired(for product: Product) {
    trackCartService.trackItemAcquired(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.home, event: .trackItemAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }

  func smartSearch(searchString: String, product: Product) {
    trackSearchService.smartSearch(searchString: searchString, product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.home, event: .smartSearch)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }
}

// MARK: - Getters

extension HomeViewModel {
  var trendingProducts: [Product] { cachedTrendingProducts }
  var popularProducts: [Product] { cachedPopularProducts }
}

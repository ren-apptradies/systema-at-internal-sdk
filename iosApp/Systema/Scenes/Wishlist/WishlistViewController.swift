//
//  WishlistViewController.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import AlamofireImage
import Foundation
import UIKit

import shared
import SVProgressHUD
import SystemaIosSDK

private enum Tab: Int, CaseIterable {
  case related = 0
}

class WishlistViewController: ViewController {
  private var viewModel = WishlistViewModel()

  private let cellIdentifier = "ProductCell"
  private let cellHeaderIdentifier = "ProductCellHeaderView"

  @IBOutlet var totalLabel: UILabel!
  @IBOutlet var wishlistContainerView: UIView!
  @IBOutlet var wishlistCollectionView: UICollectionView!
  @IBOutlet var collectionView: UICollectionView!
  private let refreshControl = UIRefreshControl()

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)

    if viewModel.relatedProducts.count > 0 {
      viewModel.trackContainer()
      viewModel.monitorPageEvent()
    }
    
    if viewModel.relatedProducts.isEmpty ||
        (viewModel.currentWishlist.count != viewModel.wishlistItems.count) {
      fetchData()
    }
    
    refresh()
  }
    
  @objc
  private func fetchData() {
    viewModel.getRelatedProducts()
  }

  private func refresh() {
    viewModel.refresh()
    
    // Reload views
    wishlistCollectionView.reloadData()
    collectionView.reloadData()
    totalLabel.text = "\(viewModel.wishlistItems.count) products"
  }
}

// MARK: - Setup

private extension WishlistViewController {
  func setup() {
    navigationController?.setNavigationBarHidden(true, animated: false)
    setupCollectionView()

    viewModel.onSuccessFetch = handleSuccessFetch()
    viewModel.onError = handleFetchError()

    wishlistContainerView.applyShadow(
      color: R.color.gray_E2E2E2()!,
      opacity: 1,
      offSet: CGSize(width: 0, height: 2),
      radius: 2
    )
  }

  func setupCollectionView() {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.minimumLineSpacing = 10
    flowLayout.minimumInteritemSpacing = 0
    flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
    let width = (UIScreen.main.bounds.width / 2) - 10
    flowLayout.itemSize = CGSize(width: width, height: 210)

    flowLayout.scrollDirection = .vertical

    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.collectionViewLayout = flowLayout

    collectionView.register(UINib(
      nibName: cellIdentifier,
      bundle: nil
    ), forCellWithReuseIdentifier: cellIdentifier)

    collectionView.register(
      UINib(resource: R.nib.productCellHeaderView),
      forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
      withReuseIdentifier: R.nib.productCellHeaderView.name
    )

    flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 50)

    collectionView.addSubview(refreshControl)
    refreshControl.addTarget(self, action: #selector(fetchData), for: .valueChanged)
  }
}

// MARK: - UICollectionViewDelegate

extension WishlistViewController: UICollectionViewDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    if collectionView != wishlistCollectionView {
      print(" selected at \(indexPath.row)")
      let product = viewModel.relatedProducts[indexPath.row]
      viewModel.trackOnClick(for: product)

      let viewController = R.storyboard.product.productViewController()!
      viewController.viewModel = ProductViewModel(product: product)
      navigationController?.pushViewController(viewController, animated: true)
    }
  }
}

extension WishlistViewController: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    if collectionView == wishlistCollectionView {
      return viewModel.wishlistItems.count
    }
    return viewModel.relatedProducts.count
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    if collectionView == wishlistCollectionView {
      guard
        let wishlistCell = collectionView.dequeueReusableCell(
          withReuseIdentifier: "WishlistPreviewCell",
          for: indexPath
        ) as? UICollectionViewCell
      else {
        return UICollectionViewCell()
      }

      let product = viewModel.wishlistItems[indexPath.row]

      // TODO: Replace create custom cell
      if let prodImageView = wishlistCell.viewWithTag(10) as? UIImageView,
        let imageURL = URL(string: product.image) {
        prodImageView.af_setImage(withURL: imageURL)
      }

      return wishlistCell
    }

    guard
      let productCell = collectionView.dequeueReusableCell(
        withReuseIdentifier: cellIdentifier,
        for: indexPath
      ) as? ProductCell
    else {
      return ProductCell()
    }

    let vm = viewModel.cellVM(with: viewModel.relatedProducts[indexPath.row])
    productCell.viewModel = vm
    productCell.onTapFavorite = handleOnTapFavorite()
    productCell.onTapAddToCart = handleOnTapCart()

    return productCell
  }

  func collectionView(
    _ collectionView: UICollectionView,
    viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath
  ) -> UICollectionReusableView {
    if collectionView == wishlistCollectionView {
      return UIView() as! UICollectionReusableView

    } else {
      guard
        let header = collectionView.dequeueReusableSupplementaryView(
          ofKind: kind,
          withReuseIdentifier: cellHeaderIdentifier, for: indexPath
        )
        as? ProductCellHeaderView
      else {
        return ProductCellHeaderView()
      }

      header.titleLabel.text = "Related Products"

      return header
    }
  }
}

// MARK: - Event Handlers

private extension WishlistViewController {
  func handleFetchError() -> ErrorResult {
    return { error in
      print("error \(error.localizedDescription)")
      self.refreshControl.endRefreshing()
    }
  }

  func handleSuccessFetch() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      print(self.viewModel.relatedProducts)

      self.refreshControl.endRefreshing()
      self.viewModel.trackContainer()
      self.viewModel.monitorPageEvent()
      
      self.refresh()
    }
  }

  func handleOnTapFavorite() -> DoubleResult<Product, Bool> {
    return { [weak self] product, selected in
      guard let self = self else { return }
      self.viewModel.trackWishlist(selected, for: product)
      self.viewModel.updateWishlist(selected, for: product)
      
      self.viewModel.getRelatedProducts()
      self.refresh()
    }
  }

  func handleOnTapCart() -> SingleResult<Product> {
    return { [weak self] product in
      guard let self = self else { return }

      self.viewModel.trackItemAquired(for: product)
      print("--- adding \(product.title) to cart ---")

      let cartProduct = CartProduct(
        itemId: product.id,
        title: product.title,
        price: product.price as! Double,
        quantity: 1,
        image: product.image,
        currency: product.currency,
        recId: product.recId,
        link: product.link
      )

      self.viewModel.appendProduct(cartProduct)
      self.refresh()
      
      DispatchQueue.main.async {
        SVProgressHUD.showSuccess(withStatus: "Successfully Added to Cart")
      }
    }
  }
}

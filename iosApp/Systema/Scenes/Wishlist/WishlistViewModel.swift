//
//  WishlistViewModel.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxCocoa
import RxSwift

import shared
import SystemaIosSDK

class WishlistViewModel {
  var onSuccessFetch: VoidResult?
  var onSuccessTrack: VoidResult?
  var onError: ErrorResult?

  private let fetchService: FetchingProductsService!
  private let eventsService: SystemaEventsService!
  private let trackWishlistService: TrackWishlistService!
  private let trackCartService: TrackCartService!
  private let cartService: CartService!

  private let onClickService: TrackItemClickService!
  private let containerService: TrackContainerService!

  private var cachedRelatedProducts: [Product] = []
  private var cachedWishlistItems: [Product] = []

  init(
    fetchService: FetchingProductsService = AppInstance.shared.fetchService,
    eventsService: SystemaEventsService = AppInstance.shared.eventsService,
    trackWishlistService: TrackWishlistService = AppInstance.shared.trackWishListService,
    trackCartService: TrackCartService = AppInstance.shared.trackCartService,
    onClickService: TrackItemClickService = AppInstance.shared.onClickService,
    containerService: TrackContainerService = AppInstance.shared.trackContainerService,
    cartService: CartService = AppInstance.shared.cartService
  ) {
    self.fetchService = fetchService
    self.trackCartService = trackCartService
    self.eventsService = eventsService
    self.trackWishlistService = trackWishlistService
    self.onClickService = onClickService
    self.containerService = containerService
    self.cartService = cartService
    cachedWishlistItems = AppInstance.shared.wishList
  }
}

// MARK: - Methods

extension WishlistViewModel {
  func cellVM(with product: Product) -> ProductCellViewModel {
    return ProductCellViewModel(product: product)
  }

  // Cart / Wishlist Methods
  func refresh() {
    cachedWishlistItems.removeAll()
    cachedWishlistItems = AppInstance.shared.wishList
  }

  func getRelatedProducts() {
    let productIDs = currentWishlist.map({ $0.id })
    fetchService.fetchCartRelatedProducts(productIDs: productIDs) { [weak self] products in
      guard let slf = self else { return }
      print("heyheyhey")
      print(products)
      slf.cachedRelatedProducts.removeAll()
      slf.cachedRelatedProducts.append(contentsOf: products)
      slf.eventsService.addEvent(.wishlist, event: .getCartRelated)
      slf.onSuccessFetch?()
    } onError: { [weak self] error in
      guard let slf = self else { return }
      slf.onError?(error)
    }
  }

  func appendProduct(_ product: CartProduct) {
    cartService.appendProduct(product)
  }

  func updateWishlist(_ isSelected: Bool, for product: Product) {
    guard isSelected else {
      AppInstance.shared.wishList.removeAll(where: { $0.id == product.id })
      return
    }

    if !AppInstance.shared.wishList.contains(product) {
      AppInstance.shared.wishList.append(product)
    }
  }

  // Track Events

  func trackOnClick(for product: Product) {
    onClickService.trackItemClicked(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.wishlist, event: .trackItemClicked)
    } onError: { [weak self] error in
      guard let self = self else { return }
      self.onError?(error)
    }
  }

  func trackContainer() {
    guard cachedRelatedProducts.count > 0 else { return }

    containerService.trackContainer(for: .wishlist, on: cachedRelatedProducts) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.wishlist, event: .trackContainerShownWishlist)
    } onError: { error in
      self.onError?(error)
    }
  }

  func monitorPageEvent() {
    guard cachedRelatedProducts.count > 0, let product = cachedRelatedProducts.first else { return }

    containerService.monitorPageEvent(on: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.wishlist, event: .monitorPageEvent)
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackWishlist(_ isFavorite: Bool, for product: Product) {
    guard isFavorite else {
      return trackWishlistService.trackWishListRelinquished(product: product, isSelected: isFavorite) { [weak self] _ in
        guard let self = self else { return }
        self.eventsService.addEvent(.wishlist, event: .trackWishListRelinquished)
        self.onSuccessTrack?()
      } onError: { error in
        self.onError?(error)
      }
    }

    trackWishlistService.trackWishListAcquired(product: product, isSelected: isFavorite) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.wishlist, event: .trackWishListAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }

  func trackItemAquired(for product: Product) {
    trackCartService.trackItemAcquired(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.wishlist, event: .trackItemAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }
}

// MARK: - Getters

extension WishlistViewModel {
  var relatedProducts: [Product] { cachedRelatedProducts }
  var wishlistItems: [Product] { cachedWishlistItems }
  var currentWishlist: [Product] { AppInstance.shared.wishList }
}

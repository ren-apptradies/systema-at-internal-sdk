//
//  WishlistDetailViewModel.swift
//  Systema
//
//  Created by JAG on 10/20/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import NSObject_Rx
import RxCocoa
import RxSwift

import shared
import SystemaIosSDK

class WishlistDetailViewModel {
  var onSuccessFetch: VoidResult?
  var onSuccessTrack: VoidResult?
  var onError: ErrorResult?

  private var cachedWishlist: [Product] = []

  private let wishlistService: WishlistService!
  private let eventsService: SystemaEventsService!
  private let trackWishlistService: TrackWishlistService!
  private let trackCartService: TrackCartService!
  private let cartService: CartService!

  init(
    wishlistService: WishlistService = AppInstance.shared.wishlistService,
    trackWishlistService: TrackWishlistService = AppInstance.shared.trackWishListService,
    cartService: CartService = AppInstance.shared.cartService,
    eventsService: SystemaEventsService = AppInstance.shared.eventsService,
    trackCartService: TrackCartService = AppInstance.shared.trackCartService
  ) {
    self.wishlistService = wishlistService
    self.trackCartService = trackCartService
    self.trackWishlistService = trackWishlistService
    self.cartService = cartService
    self.eventsService = eventsService
    refreshCart()
  }
}

// MARK: - Methods

extension WishlistDetailViewModel {
  func appendProduct(_ product: CartProduct) {
    cartService.appendProduct(product)
  }

  func updateWishlist(_ isSelected: Bool, for product: Product) {
    guard isSelected else {
      AppInstance.shared.wishList.removeAll(where: { $0.id == product.id })
      return
    }

    if !AppInstance.shared.wishList.contains(product) {
      AppInstance.shared.wishList.append(product)
    }
  }

  func removeFromWishlist(_ product: Product) {
    AppInstance.shared.wishList.removeAll(where: { $0.id == product.id })
  }

  // TODO: Create this as protocol
  func cellVM(with product: Product) -> ProductCellViewModel {
    return ProductCellViewModel(product: product)
  }

  func refreshCart() {
    cachedWishlist = AppInstance.shared.wishList
  }

  func trackWishlist(_ isFavorite: Bool, for product: Product) {
    guard isFavorite else {
      return trackWishlistService.trackWishListRelinquished(product: product, isSelected: isFavorite) { [weak self] _ in
        guard let self = self else { return }
        self.eventsService.addEvent(.wishlist, event: .trackWishListRelinquished)
        self.onSuccessTrack?()
      } onError: { error in
        self.onError?(error)
      }
    }

    trackWishlistService.trackWishListAcquired(product: product, isSelected: isFavorite) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.wishlist, event: .trackWishListAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }
  
  func trackItemAquired(for product: Product) {
    trackCartService.trackItemAcquired(product: product) { [weak self] _ in
      guard let self = self else { return }
      self.eventsService.addEvent(.wishlist, event: .trackItemAcquired)
      self.onSuccessTrack?()
    } onError: { error in
      self.onError?(error)
    }
  }
}

// MARK: - Getters

extension WishlistDetailViewModel {
  var wishlist: [Product] { cachedWishlist }
}

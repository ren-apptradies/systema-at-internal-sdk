//
//  WishlistDetailController.swift
//  Systema
//
//  Created by JAG on 10/20/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import UIKit

import shared
import SVProgressHUD
import SystemaIosSDK

class WishlistDetailController: ViewController {
  private var viewModel = WishlistDetailViewModel()

  private let cellIdentifier = "ProductCell"

  @IBOutlet var collectionView: UICollectionView!

  override func viewDidLoad() {
    super.viewDidLoad()
    setupBindings()
//    fetchData()
    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)

    collectionView.reloadData()
  }

  func refresh() {
    viewModel.refreshCart()
    collectionView.reloadData()
  }
}

// MARK: - Setup

private extension WishlistDetailController {
  func setup() {
    navigationController?.setNavigationBarHidden(true, animated: false)
    setupCollectionView()
  }

  func setupCollectionView() {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.minimumLineSpacing = 10
    flowLayout.minimumInteritemSpacing = 0
    flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
    let width = (UIScreen.main.bounds.width / 2) - 10
    flowLayout.itemSize = CGSize(width: width, height: 210)

    flowLayout.scrollDirection = .vertical

    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.collectionViewLayout = flowLayout

    collectionView.register(UINib(
      nibName: cellIdentifier,
      bundle: nil
    ), forCellWithReuseIdentifier: cellIdentifier)
  }

  private func setupBindings() { }
}

// MARK: - UICollectionView

extension WishlistDetailController: UICollectionViewDelegate, UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    return viewModel.wishlist.count
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    guard
      let productCell = collectionView.dequeueReusableCell(
        withReuseIdentifier: cellIdentifier,
        for: indexPath
      ) as? ProductCell
    else {
      return ProductCell()
    }

    let vm = viewModel.cellVM(with: viewModel.wishlist[indexPath.row])
    productCell.viewModel = vm
    productCell.onTapFavorite = handleOnTapFavorite()
    productCell.onTapRemoveFromCart = handleOnRemoveCart()
    productCell.onTapAddToCart = handleOnTapCart()

    productCell.isEditable = true
    return productCell
  }
}

// MARK: - Event Handlers

private extension WishlistDetailController {
  func handleOnTapFavorite() -> DoubleResult<Product, Bool> {
    return { [weak self] product, selected in
      guard let self = self else { return }
      self.viewModel.trackWishlist(selected, for: product)
      self.viewModel.updateWishlist(selected, for: product)
      self.refresh()
    }
  }

  func handleOnRemoveCart() -> SingleResult<Product> {
    return { [weak self] product in
      guard let self = self else { return }
      self.viewModel.trackWishlist(false, for: product)
      self.viewModel.removeFromWishlist(product)
      self.refresh()
    }
  }

  func handleOnTapCart() -> SingleResult<Product> {
    return { [weak self] product in
      guard let self = self else { return }

      self.viewModel.trackItemAquired(for: product)
      print("--- adding \(product.title) to cart ---")

      let cartProduct = CartProduct(
        itemId: product.id,
        title: product.title,
        price: product.price as! Double,
        quantity: 1,
        image: product.image,
        currency: product.currency,
        recId: product.recId,
        link: product.link
      )

      self.viewModel.appendProduct(cartProduct)
      DispatchQueue.main.async {
        SVProgressHUD.showSuccess(withStatus: "Successfully Added to Cart")
      }
    }
  }
}

//
//  MainTabbarController.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxSwift
import SVProgressHUD
import UIKit

class MainTabbarController: UITabBarController {
  enum TabIndex: Int {
    case home = 0
    case category
    case wishlist
    case cart
  }

  private var viewModel = MainTabbarViewModel()

  override func viewDidLoad() {
    super.viewDidLoad()
    delegate = self
    setupBindings()
    fetchData()
    setupTrackerView()
  }

  private func setupTrackerView() {
    guard let trackerView = Bundle.main.loadNibNamed(
      "EventTrackerMiniView",
      owner: nil,
      options: nil
    )?.first as? EventTrackerMiniView else {
      return
    }

    let tabFrame = tabBar.frame
    let trackerViewY = tabFrame.origin.y - (100 + tabFrame.height + view.safeAreaInsets.bottom)// + 20 116
    trackerView.frame = CGRect(x: 0, y: trackerViewY, width: tabFrame.width, height: 116)
    return view.addSubview(trackerView)
  }

  private func setupBindings() { }

  private func fetchData() { }
}

extension MainTabbarController: UITabBarControllerDelegate {
}

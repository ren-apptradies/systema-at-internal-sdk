//
//  MainTabbarViewModel.swift
//  Systema
//
//  Created by JAG on 10/1/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxCocoa
import RxSwift

protocol MainTabbarViewModelProtocol {}

class MainTabbarViewModel: MainTabbarViewModelProtocol {}

//
//  EventTrackerMiniView.swift
//  Systema
//
//  Created by JAG on 10/3/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import Hero
import UIKit

class EventTrackerMiniView: UIView {
  private var viewModel = EventTrackerViewModel()
  @IBOutlet private(set) var tableView: UITableView!

  override func awakeFromNib() {
    setup()
  }
}

extension EventTrackerMiniView {
  @IBAction
  func fullscreenTapped(_ sender: AnyObject) {
    showEventFullScreen()
  }

  func showEventFullScreen() {
    guard let containingController = viewContainingController() else { return }

    let vc = R.storyboard.eventTrackerView.eventTrackerViewController()!
    vc.modalPresentationStyle = .overFullScreen
    vc.modalTransitionStyle = .crossDissolve
    vc.hero.isEnabled = true
    containingController.present(vc, animated: true)
  }
}

// MARK: - Setup

private extension EventTrackerMiniView {
  func setup() {
    setupTableView()
    NotificationCenter.default.addObserver(self, selector: #selector(handleAddedEvent), name: .didUpdateSystemaEvent, object: nil)

    tableView.reloadData()
  }

  func setupTableView() {
    tableView.delegate = self
    tableView.dataSource = self

    tableView.register(
      UINib(nibName: R.nib.systemaEventCell.name, bundle: nil),
      forCellReuseIdentifier: R.nib.systemaEventCell.name
    )
  }
}

// MARK: - UITableViewViewDelegate

extension EventTrackerMiniView: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  }
}

// MARK: - UITableViewViewDataSource

extension EventTrackerMiniView: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.events.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard
      let cell = tableView.dequeueReusableCell(
        withIdentifier: R.nib.systemaEventCell.name,
        for: indexPath
      ) as? SystemaEventCell
    else {
      return SystemaEventCell()
    }

    cell.label.text = viewModel.events[indexPath.row]
    return cell
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 28
  }
}

// MARK: - Event Handlers

private extension EventTrackerMiniView {
  @objc
  func handleAddedEvent() {
    tableView.reloadData()
  }
}

//
//  EventTrackerViewController.swift
//  Systema
//
//  Created by JAG on 10/3/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxSwift
import SVProgressHUD
import UIKit

class EventTrackerViewController: UIViewController {
  private var viewModel = EventTrackerViewModel()

  @IBOutlet private(set) var tableView: UITableView!

  @IBAction func dismissView() {
    dismiss(animated: true)
  }
}

// MARK: - Lifecycle

extension EventTrackerViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
}

// MARK: - Setup

private extension EventTrackerViewController {
  func setup() {
    setupTableView()
    NotificationCenter.default.addObserver(self, selector: #selector(handleAddedEvent), name: .didUpdateSystemaEvent, object: nil)

    tableView.reloadData()
  }

  func setupTableView() {
    tableView.delegate = self
    tableView.dataSource = self

    tableView.register(
      UINib(nibName: R.nib.systemaEventCell.name, bundle: nil),
      forCellReuseIdentifier: R.nib.systemaEventCell.name
    )
  }
}

// MARK: - UITableViewViewDelegate

extension EventTrackerViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  }
}

// MARK: - UITableViewViewDataSource

extension EventTrackerViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.events.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard
      let cell = tableView.dequeueReusableCell(
        withIdentifier: R.nib.systemaEventCell.name,
        for: indexPath
      ) as? SystemaEventCell
    else {
      return SystemaEventCell()
    }

    cell.label.text = viewModel.events[indexPath.row]
    return cell
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 28
  }
}

// MARK: - Event Handlers

private extension EventTrackerViewController {
  @objc
  func handleAddedEvent() {
    tableView.reloadData()
  }
}

//
//  EventTrackerViewModel.swift
//  Systema
//
//  Created by JAG on 10/3/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import NSObject_Rx
import RxCocoa
import RxSwift

extension Notification.Name {
  static let didUpdateSystemaEvent = Notification.Name("didUpdateSystemaEvent")
}

class EventTrackerViewModel {

  private let service: SystemaEventsService!

  init(
    service: SystemaEventsService = AppInstance.shared.eventsService
  ) {
    self.service = service
  }
}

extension EventTrackerViewModel {
  var events: [String] { service.systemaEvents }
}

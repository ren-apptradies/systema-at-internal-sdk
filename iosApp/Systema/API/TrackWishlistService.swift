//
//  TrackWishlistService.swift
//  Systema
//
//  Created by iTradie on 13/10/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation

import SystemaIosSDK

import shared

class TrackWishlistService {
  private let api: API

  init(
    api: API
  ) {
    self.api = api
  }
}

// MARK: - Methods

extension TrackWishlistService {
  func trackWishListAcquired(
    product: Product,
    isSelected: Bool,
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    var wishlistItems: [WishlistItem] = []
    wishlistItems.append(WishlistItem(itemId: product.id))
    api.instance?.trackWishlistAcquired(
      // productId: product.id, TODO: need to check on new sdk
      productId: product.id,
      items: wishlistItems,
      url: product.link,
      referrer: product.link, // TODO: need to check on new sdk
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val else { return }

        onSuccess(true)
      }
    )
  }

  func trackWishListRelinquished(
    product: Product,
    isSelected: Bool,
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    let wishListItem = WishlistItem(itemId: product.id)

    api.instance?.trackWishlistRelinquished(
      productId: product.id,
      item: wishListItem,
      url: product.link,
      referrer: product.link, // TODO: need to check on new sdk
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val else { return }

        onSuccess(true)
      }
    )
  }
}

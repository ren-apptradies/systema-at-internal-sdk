//
//  TrackSearchService.swift
//  Systema
//
//  Created by iTradie on 25/10/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation

import shared

import SystemaIosSDK

class TrackSearchService {
  private let api: API

  init(
    api: API
  ) {
    self.api = api
  }
}

// MARK: - Methods

extension TrackSearchService {
  func smartSearch(
    searchString: String,
    product: Product,
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    let qItem = QueryItem(
      id: product.id,
      type: "product"
    )
    let filter = Filter(
      id: nil,
      text: searchString,
      brand: nil,
      category: nil,
      price: nil,
      salePrice: nil,
      onSaleOnly: nil,
      inStockOnly: nil,
      tags: nil,
      meta: nil
    )

    let request = SmartSearchRequest(
      environment: nil,
      user: nil, query: [qItem],
      filter: filter,
      exclusion: nil,
      size: nil,
      language: nil,
      start: nil,
      facetSize: nil,
      facets: nil,
      score: nil,
      meta: nil
    )

    api.instance?.smartSearch(
      payload: request,
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val else { return }

        onSuccess(true)
      }
    )
  }
}

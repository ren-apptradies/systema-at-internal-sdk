//
//  FetchingProductsService.swift
//  Systema
//
//  Created by iTradie on 9/10/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation

import SystemaIosSDK

import shared

class FetchingProductsService {
  private let api: API

  init(
    api: API
  ) {
    self.api = api
  }
}

// MARK: - Methods

extension FetchingProductsService {
  func fetchTrendingProducts(
    onSuccess: @escaping SingleResult<[Product]>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getTrending(
      payload: api.getDummyReq(),
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in
        print("------ Core SDK RESPONSE START ------")
        print(val.debugDescription)
        print("------ Core SDK RESPONSE END------")

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
              result.isSuccessful,
              let value = result.value() else {
          return onSuccess([])
        }
        onSuccess(value.results)
      }
    )
  }

  func fetchPopularProducts(
    onSuccess: @escaping SingleResult<[Product]>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getPopular(
      payload: api.getDummyReq(),
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
              result.isSuccessful,
              let value = result.value() else {
          return onSuccess([])
        }
        onSuccess(value.results)
      }
    )
  }

  func fetchCartRelatedProducts(
    productIDs: [String]?,
    onSuccess: @escaping SingleResult<[Product]>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getCartRelated(
      payload: api.getReqWithProductIds(productIDs),
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
              result.isSuccessful,
              let value = result.value() else {
          return onSuccess([])
        }
        onSuccess(value.results)
      }
    )
  }

  func fetchRelatedProducts(
    productId: String? = nil,
    onSuccess: @escaping SingleResult<[Product]>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getRelated(
      payload: api.getReqWithProductId(productId),
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
              result.isSuccessful,
              let value = result.value() else {
          return onSuccess([])
        }
        onSuccess(value.results)
      }
    )
  }

  func fetchComplementary(
    productId: String? = nil,
    onSuccess: @escaping SingleResult<[Product]>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getComplementary(
      payload: api.getReqWithProductId(productId), //apply to fetch related
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
              result.isSuccessful,
              let value = result.value() else {
          return onSuccess([])
        }
        onSuccess(value.results)
      }
    )
  }

  func fetchCartComplementary(
    productIDs: [String]?,
    onSuccess: @escaping SingleResult<[Product]>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getCartComplementary(
      payload: api.getReqWithProductIds(productIDs),
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
              result.isSuccessful,
              let value = result.value() else {
          return onSuccess([])
        }
        onSuccess(value.results)
      }
    )
  }

  func fetchCategoryPopularMen(
    onSuccess: @escaping SingleResult<[Product]>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getCategoryPopular(
      payload: api.getDummyReq(["Men"]),
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
              result.isSuccessful,
              let value = result.value() else {
          return onSuccess([])
        }
        onSuccess(value.results)
      }
    )
  }

  func fetchCategoryTrendingWomen(
    onSuccess: @escaping SingleResult<[Product]>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getCategoryTrending(
      payload: api.getDummyReq(["Women"]),
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
          result.isSuccessful,
          let value = result.value() else {
          return onSuccess([])
        }
        onSuccess(value.results)
      }
    )
  }
}

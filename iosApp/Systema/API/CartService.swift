//
//  CartService.swift
//  Systema
//
//  Created by JAG on 10/22/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import shared
import SystemaIosSDK

class CartService {}

extension CartService {
  func products() -> [CartProduct] {
    var products: [CartProduct] = []
    let userDefaults = UserDefaults.standard
    do {
      products = try userDefaults.getObject(forKey: "products", castTo: [CartProduct].self)
    } catch {
      print(error.localizedDescription)
    }
    return products
  }

  func appendProduct(_ product: CartProduct) {
    var prods = products()
    prods.append(product)

    let userDefaults = UserDefaults.standard
    do {
      try userDefaults.setObject(prods, forKey: "products")
    } catch {
      print(error.localizedDescription)
    }
  }

  func replaceCartItemsWith(_ items: [CartProduct]) {
    let userDefaults = UserDefaults.standard
    do {
      try userDefaults.setObject(items, forKey: "products")
    } catch {
      print(error.localizedDescription)
    }
  }

  func empty() {
    let items: [CartProduct] = []
    let userDefaults = UserDefaults.standard
    do {
      try userDefaults.setObject(items, forKey: "products")
    } catch {
      print(error.localizedDescription)
    }
  }
}

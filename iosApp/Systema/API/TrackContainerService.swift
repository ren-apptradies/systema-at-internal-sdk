//
//  TrackContainerService.swift
//  Systema
//
//  Created by iTradie on 20/10/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation

import shared

import SystemaIosSDK

class TrackContainerService: SystemaTracker {
  private let api: API

  init(
    api: API
  ) {
    self.api = api
  }
}

// MARK: - Methods

extension TrackContainerService {
  func trackContainer(
    for screen: ScreenName,
    on products: [Product],
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    trackContainerShown(
      for: products,
      screenName: screen.rawValue
    ) { success, error in
      if let errorRet = error {
        return onError(errorRet)
      }
      return onSuccess(success)
    }
  }

  func monitorPageEvent(
    on product: Product,
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    
   trackPageViewed(
      for: product
    ) { success, error in
      if let errorRet = error {
        return onError(errorRet)
      }
      return onSuccess(success)
    }
  }
}

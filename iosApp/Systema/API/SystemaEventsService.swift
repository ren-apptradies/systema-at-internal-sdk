//
//  SystemaEventsService.swift
//  Systema
//
//  Created by iTradie on 11/10/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation

import shared

enum ScreenName: String {
  case home = "Home"
  case favorites = "Favorites"
  case wishlist = "WishList"
  case cart = "Cart"
  case product = "Product"
  case category = "Category"
}

enum Event: String {
  case getTrending // get trending
  case getPopular // get popular

  case getCategoryPopularMen // getpopular men
  case getCategoryTrendingWomen // getrending women

  case getRelated // get related
  case getComplementary // get complementary
  case getCartComplementary // get cart complementary
  case getCartRelated // get cart related

  case trackWishListAcquired // add to wishlist
  case trackWishListRelinquished // remove from cart
  case trackItemAcquired // add to cart
  case trackItemRelinquished // remove from cart
  case trackAcquisitionComplete // checkout
  case trackItemClicked // when item is clicked

  case trackContainerShownMenPopular // when men popular view is shown
  case trackContainerShownWomenTrending // when women trending view is shown
  case trackContainerShownPopular // when popular view is shown
  case trackContainerShownTrending // when trending view is shown
  case trackContainerShownWishlist // when wishlist view is shown
  case trackContainerShownCart // when cart view is shown
  case trackContainerShownProductComplementary // when product complementary view is shown
  case trackContainerShownProductRelated // when product related view is shown

  case monitorPageEvent // when wishlist view is shown
  case smartSearch // smart search
}

class SystemaEventsService {

  var systemaEvents: [String] = []

  func addEvent(
    _ screenName: ScreenName,
    event: Event
  ) {
    let toAdd = Date.now.toString() + " - " + screenName.rawValue + " - " + event.rawValue
    systemaEvents.insert(toAdd, at: 0)
    NotificationCenter.default.post(name: .didUpdateSystemaEvent, object: nil)
  }
}

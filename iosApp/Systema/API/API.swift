//
//  APIService.swift
//  Systema
//
//  Created by iTradie on 9/10/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import shared
import SystemaIosSDK

class API {
  let basURL = "https://www.reddit.com/"

  private let systema: SystemaIos = SystemaIos(
    with: "PMAK-6113bbd984433f0046e30785-fe032bc85b42f8e1eff7507f1c23d592b9",
    and: "https://853f87e6-a3ef-41d2-b098-8a9153a06315.mock.pstmn.io"
  )

  var instance: SystemaAI? { systema.getInstance() }

  func getDummyReq(_ category: [String]? = nil) -> RecommendationRequest {
    return RecommendationRequest(
      environment: nil,
      user: nil,
      id: nil,
      category: category,
      size: 2,
      start: nil,
      filter: nil,
      exclusion: nil,
      paginationTimestamp: nil,
      language: nil,
      display: nil,
      displayVariants: nil,
      meta: nil
    )
  }
  
  func getReqWithProductId(_ productId: String? = nil) -> RecommendationRequest {
    return RecommendationRequest(
      environment: nil,
      user: nil,
      id: productId,
      category: nil,
      size: 2,
      start: nil,
      filter: nil,
      exclusion: nil,
      paginationTimestamp: nil,
      language: nil,
      display: nil,
      displayVariants: nil,
      meta: nil
    )
  }
  
  
  func getReqWithProductIds(_ ids: [String]? = nil) -> CartRecommendationRequest {
    return CartRecommendationRequest(
      environment: nil,
      user: nil,
      id: ids,
      category: nil,
      size: 2,
      start: nil,
      filter: nil,
      exclusion: nil,
      paginationTimestamp: nil,
      language: nil,
      display: nil,
      displayVariants: nil,
      meta: nil
    )
  }
}

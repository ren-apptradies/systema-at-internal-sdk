//
//  OnClickService.swift
//  Systema
//
//  Created by iTradie on 20/10/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation

import shared

import SystemaIosSDK

class TrackItemClickService: SystemaTracker {
  private let api: API

  init(
    api: API
  ) {
    self.api = api
  }
}

// MARK: - Methods

extension TrackItemClickService {
  func trackItemClicked(
    product: Product,
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    trackItemClicked(for: product) { success, error in
      if let errorRet = error {
        return onError(errorRet)
      }
      return onSuccess(success)
    }
  }

  func getComplimentary(
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getComplementary(
      payload: api.getDummyReq(),
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in
        print("------ Core SDK RESPONSE START ------")
        print(val.debugDescription)
        print("------ Core SDK RESPONSE END------")

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
          result.isSuccessful,
          let value = result.value() else {
          return onSuccess(false)
        }
        // TODO: need to return value?
        onSuccess(true)
      }
    )
  }

  func getRelated(
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.getRelated(
      payload: api.getDummyReq(),
      requestOptions: nil,
      completionHandler: { [weak self]
        val, error in
        print("------ Core SDK RESPONSE START ------")
        print(val.debugDescription)
        print("------ Core SDK RESPONSE END------")

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val,
          result.isSuccessful,
          let value = result.value() else {
          return onSuccess(false)
        }
        // TODO: need to return value?
        onSuccess(true)
      }
    )
  }
}

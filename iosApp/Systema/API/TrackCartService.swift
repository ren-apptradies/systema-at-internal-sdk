//
//  TrackCartService.swift
//  Systema
//
//  Created by JAG on 10/19/21.
//  Copyright © 2021 Systema. All rights reserved.
//

import Foundation
import SystemaIosSDK

import shared

class TrackCartService {
  private let api: API

  init(
    api: API
  ) {
    self.api = api
  }
}

// MARK: - Methods

extension TrackCartService {
  func trackCartCompleted(
    order: PurchaseOrder,
    url: String,
    referrer: String,
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    api.instance?.trackAcquisitionComplete(
      order: order,
      url: url,
      referrer: referrer,
      completionHandler: { [weak self] val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val else { return }
        onSuccess(true)
      }
    )
  }

  func trackItemAcquired(
    product: Product,
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    let cartItem = CartItem(
      itemId: product.id,
      quantity: 1,
      price: nil,
      currency: nil
    )
    api.instance?.trackItemAcquired(
      productId: product.id,
      items: [cartItem],
      url: product.link,
      referrer: product.link,
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val else { return }

        onSuccess(true)
      }
    )
  }

  func trackItemRelinquished(
    product: Product,
    onSuccess: @escaping SingleResult<Bool>,
    onError: @escaping ErrorResult
  ) {
    let cartItem = CartItem(
      itemId: product.id,
      quantity: 1,
      price: nil,
      currency: nil
    )
    api.instance?.trackItemRelinquished(
      productId: product.id,
      item: cartItem,
      url: product.link,
      referrer: product.link, // TODO: need to check on new sdk
      completionHandler: { [weak self]
        val, error in

        if let errorRet = error {
          return onError(errorRet)
        }

        guard let result = val else { return }

        onSuccess(true)
      }
    )
  }
}

//
//  SDK.swift
//  SystemaIosSDK
//
//  Created by John Ivan Lacuesta on 22/9/21.
//

import Foundation
import shared

// This class is internal only to this SDK
class SDK {
  static let shared = SDK()

  var systemaAi: SystemaAI?

  var clientId: String? {
    return systemaAi?.credentials.clientID.toString()
  }

  var dummyReq: RecommendationRequest

  init() {
    dummyReq = RecommendationRequest(environment: nil, user: nil, id: nil, category: nil, size: nil, start: nil, filter: nil, exclusion: nil, paginationTimestamp: nil, language: nil, display: nil, displayVariants: nil, meta: nil)
  }
}

//
//  SystemaTagger.swift
//  SystemaIosSDK
//
//  Created by John Ivan Lacuesta on 22/9/21.
//

import Foundation
import shared

/* TODO: not sure if i should have this, will serve as a reference, will delete
 enum TagKeys: String {
 case productId = "systema_product_id"
 case productUrl = "systema_product_url"
 case productPrice = "systema_product_price"
 case productCurrency = "systema_product_currency"
 case resultId = "systema_result_id"
 case recId = "systema_rec_id"
 case referrerUrl = "systema_referrer_url"
 case observed = "systema_observed"
 case visible = "systema_visible"
 }
 */

class SystemaTagger {
  private let product: Product

  var productId: String {
    return product.id
  }

  var productCurrency: String? {
    return product.currency
  }

  var recId: String {
    return product.recId
  }

  var resultId: String {
//    didSet {
//      observed = false
//    }
    let components = product.recId.components(separatedBy: ":")
    guard let result = components.first else {
      return ""
    }

    return result
  }

  var referrerUrl: String?
  var containerUrl: String?

  var observed: Bool
  var visible: Bool

  var productUrl: String?
  var productPrice: NSNumber?

  init(with product: Product) {
    self.product = product
    observed = false
    visible = false

    productUrl = generateProductUrl()
    productPrice = getPrice()
  }
}

// Methods
extension SystemaTagger {
  // "https://$clientID.apps.systema.ai/containers/$containerId"
  func setContainerUrl(with containerId: String) {
    guard let clientId = SDK.shared.clientId else {
      print("No clientID ")
      return
    }
    containerUrl = "https://\(clientId).apps.systema.ai/containers/\(containerId)"
  }
}

// Helpers
private extension SystemaTagger {
  func generateProductUrl() -> String {
    guard let clientId = SDK.shared.clientId else {
      return product.link
    }
    
    guard product.link.lowercased().hasPrefix("http") else {
      return" https://\(clientId).apps.systema.ai" + product.link
    }

    // productUrl = "https://$clientID.apps.systema.ai/${product.link.trimStart('/')}"

    var trimmed = product.link
    if let index = product.link.firstIndex(of: "/") {
      trimmed = String(product.link.suffix(from: index))
    }
    return "https://\(clientId).apps.systema.ai/\(trimmed)"
  }

  // TODO: Create a helper class of this instead
  func getPrice() -> NSNumber? {
    guard let doubleKotlinValue = product.price else {
      return nil
    }

    return NSNumber(nonretainedObject: doubleKotlinValue)
  }
}

//
//  UIView+Listener.swift
//  SystemaIosSDK
//
//  Created by John Ivan Lacuesta on 20/9/21.
//

import shared
import UIKit

extension UIView {
  // not sure yet; set to public if needed
  func track(with product: Product) {
    let tagger = SystemaTagger(with: product)
    print(" track from UIview is shown")
  }

  public func listenForClickEvents(for product: Product) {
    isUserInteractionEnabled = true
    let tagger = SystemaTagger(with: product)
    tagger.observed = true
    let gestureRecognizer = SystemaTapGestureRecognizer(target: self, action: #selector(onClickView(_:)), systemaTagger: tagger)
    addGestureRecognizer(gestureRecognizer)
  }

  @objc internal func onClickView(_ recognizer: SystemaTapGestureRecognizer) {
    let tagger = recognizer.systemaTagger

    print("triggered tagger productId: \(tagger.productId)")

    SDK.shared.systemaAi?.trackItemClicked(
      productId: tagger.productId,
      url: tagger.productUrl ?? "",
      recId: tagger.recId,
      referrer: tagger.referrerUrl ?? "",
      completionHandler: { coreResponse, _ in
        print("coreResponse: \(String(describing: coreResponse))")
      }
    )
  }
}

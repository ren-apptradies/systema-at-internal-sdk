#
#  Be sure to run `pod spec lint SystemaIos.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|


  spec.name         = "SystemaIosSDK"
  spec.version      = "0.4"
  spec.summary      = "A short description of SystemaIos."

  spec.description  = "This is the systema iOS SDK"

  spec.homepage     = "http://EXAMPLE/SystemaIos"
  
  spec.static_framework         = true
  spec.vendored_frameworks      = "framework/SystemaIosSDK.framework"

  spec.license      = ""
  # spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  spec.author             = ""
  #spec.platform     = :ios, "14.1"
  spec.ios.deployment_target = "14.1"

  spec.source       = { :git => "Not Published", :tag => "#{spec.version}" }

  #spec.source_files  = "Classes", "Classes/**/*.{h,m}"
  spec.source_files  = "../SystemaIosSDK/**/*.{swift}"

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.dependency "shared", "~> 1.0"
  #spec.swift_versions = "5.0"

end

Pod::Spec.new do |s|
s.name          = "shared"
s.version       = "1.0"
s.summary       = "This is a test Systema iOS framework"
s.homepage      = "https://gitlab.com/ren-apptradies/systema-at-internal-sdk"
s.license       = "Apache"
s.author        = { "Ren Decano" => "ren@apptradies.com" }
s.vendored_frameworks = 'shared.xcframework'
s.source        = { :git => "git@gitlab.com:ren-apptradies/systema-at-internal-sdk.git", :tag => "#{s.version}" }
s.exclude_files = "Classes/Exclude"
end

package ai.systema.android.home.presentation

import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.response.RecommendationResponse

sealed class HomeState {
    object ShowLoading : HomeState()

    object HideLoading : HomeState()

    object EmptyProducts : HomeState()

    data class RenderProducts(
        val productList: List<ProductWrapper>,
        val trendingResp: RecommendationResponse,
        val popularResp: RecommendationResponse,
    ) : HomeState()

    data class Error(val message: String) : HomeState()
}
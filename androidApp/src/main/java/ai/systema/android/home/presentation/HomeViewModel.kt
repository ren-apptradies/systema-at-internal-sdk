package ai.systema.android.home.presentation

import ai.systema.android.R
import ai.systema.android.common.ResourceManager
import ai.systema.android.common.Systema
import ai.systema.android.common.domain.model.ProductResponse
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.common.domain.usecases.AddToCart
import ai.systema.android.common.domain.usecases.ClearLogEvents
import ai.systema.android.common.domain.usecases.GetLogEvents
import ai.systema.android.common.domain.usecases.SetLogEvent
import ai.systema.android.common.domain.usecases.SmartSearchProducts
import ai.systema.android.common.domain.usecases.UpdateWishListItem
import ai.systema.android.common.toCartItem
import ai.systema.android.home.domain.usecases.GetHomeProducts
import ai.systema.android.monitorAddToCartButton
import ai.systema.android.monitorProductPage
import ai.systema.android.monitorRecContainer
import ai.systema.model.index.Product
import ai.systema.model.response.RecommendationResponse
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val resourceManager: ResourceManager,
    private val getHomeProducts: GetHomeProducts,
    private val getLogEvents: GetLogEvents,
    private val setLogEvent: SetLogEvent,
    private val clearLogEvents: ClearLogEvents,
    private val updateWishListItem: UpdateWishListItem,
    private val addToCart: AddToCart,
    private val smartSearchProducts: SmartSearchProducts
) : ViewModel() {

    private val _uiState by lazy {
        MutableStateFlow<HomeState>(HomeState.ShowLoading)
    }

    val uiState: StateFlow<HomeState> = _uiState

    private val _cart by lazy {
        MutableStateFlow<Boolean>(false)
    }

    val cart: StateFlow<Boolean> = _cart

    private val _wishList by lazy {
        MutableStateFlow<Boolean>(false)
    }

    val wishlist: StateFlow<Boolean> = _wishList

    private val _search by lazy {
        MutableStateFlow<List<ProductWrapper>>(listOf())
    }

    val search: StateFlow<List<ProductWrapper>> = _search

    private var productResponse: ProductResponse? = null

    init {
        viewModelScope.launch {
            clearLogEvents.execute()

            getProducts()?.let { products ->
                _uiState.emit(HomeState.HideLoading)
                _uiState.emit(
                    if (products.first.isNullOrEmpty()) {
                        HomeState.EmptyProducts
                    } else {
                        productResponse = products
                        HomeState.RenderProducts(
                            products.first,
                            products.second,
                            products.third
                        )
                    }
                )
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_home),
                        "getTrending"
                    )
                )
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_home),
                        "getPopular"
                    )
                )
            }
        }
    }

    fun monitorPageEvent(
        view: View,
        attachStateChangeListener: View.OnAttachStateChangeListener
    ) {
        Systema.getInstance().monitorProductPage(
            view,
            product = null,
            attachStateChangeListener
        ) {
            viewModelScope.launch {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_home),
                        "pageViewEvent"
                    )
                )
            }
        }
    }

    fun monitorContainer(
        container: RecyclerView,
        layoutListener: View.OnLayoutChangeListener,
        recommendationResponse: RecommendationResponse,
        type: ProductType
    ) {

        Systema.getInstance().monitorRecContainer(
            container,
            recommendationResponse,
            layoutListener
        ) {
            // error handler if needed
        }

        viewModelScope.launch {
            setLogEvent.execute(
                Pair(
                    resourceManager.getString(R.string.menu_home),
                    "monitorRecContainer-${type.name}"
                )
            )
        }
    }

    fun monitorAddToCartButton(view: View, product: Product, clickListener: View.OnClickListener) {
        Systema.getInstance().monitorAddToCartButton(
            view,
            product,
            { listOf(product.toCartItem()) },
            clickListener
        ) { }
    }

    fun addToCart(product: Product) {
        viewModelScope.launch {
            addToCart.execute(product).run {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_home),
                        "monitorAddToCartButton"
                    )
                )

                _cart.emit(true)
                _cart.emit(false)
            }
        }
    }

    fun addToWishlist(product: Product, isSelected: Boolean) {
        viewModelScope.launch {
            if (updateWishListItem.execute(Pair(product, isSelected)).isSuccess) {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_home),
                        if (isSelected) "trackWishlistAcquired" else "trackWishlistRelinquished"
                    )
                )

                // Update product list with the new wishlist value
                productResponse?.let { newProduct ->
                    newProduct.first.firstOrNull { it.product.id == product.id }?.apply {
                        isFavorite = isSelected
                    }

                    if (isSelected) {
                        _wishList.emit(true)
                    }
                    _wishList.emit(false)

                    _uiState.emit(
                        HomeState.RenderProducts(
                            newProduct.first,
                            newProduct.second,
                            newProduct.third
                        )
                    )
                }
            }
        }
    }

    fun smartSearch(query: String?) {
        viewModelScope.launch {
            val productId = productResponse?.first?.first()?.product?.id
            if (!query.isNullOrEmpty() && !productId.isNullOrEmpty()) {
                // TODO: Is this really needed? Is query parameter a required field?
                //  Passing the first product id from the list
                smartSearchProducts.execute(Pair(productId, query)).fold(onSuccess = {
                    _search.emit(it.first)
                }, onFailure = {
                    // Ignore error for search
                })

                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_home),
                        "smartSearch"
                    )
                )
            }

        }
    }

    private suspend fun getProducts(): ProductResponse? {
        return getHomeProducts.execute().fold(onSuccess = {
            it
        }, onFailure = {
            _uiState.emit(
                HomeState.Error(
                    it.localizedMessage ?: resourceManager.getString(R.string.generic_error)
                )
            )
            null
        })
    }

    suspend fun getLogs(): Flow<List<String>> {
        return getLogEvents.execute(resourceManager.getString(R.string.menu_home))
    }
}
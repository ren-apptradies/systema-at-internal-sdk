package ai.systema.android.home.domain.usecases

import ai.systema.android.base.domain.SuspendUseCase
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.android.common.domain.model.ProductResponse
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.response.RecommendationResponse
import kotlinx.coroutines.flow.firstOrNull
import javax.inject.Inject

class GetHomeProducts @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCase<Result<ProductResponse>> {

    override suspend fun execute(): Result<ProductResponse> {
        val trending = productsRepository.getTrendingProductList()
        val popular = productsRepository.getPopularProductList()
        val productList = mutableListOf<ProductWrapper>()

        val emptyResponse = RecommendationResponse(
            listOf(),
            -1,
            paginationTimestamp = 0L,
            resultId = "no_id",
            time = ""
        )

        if (trending.isSuccess) {
            productList.addAll(trending.getOrNull()?.first?.map { product ->
                ProductWrapper(
                    product,
                    ProductType.TRENDING,
                    isFavorite = productsRepository.isPartOfWishlist(product.id).firstOrNull()
                )
            } ?: listOf())
        }

        if (popular.isSuccess) {
            productList.addAll(popular.getOrNull()?.first?.map { product ->
                ProductWrapper(
                    product,
                    ProductType.POPULAR,
                    isFavorite = productsRepository.isPartOfWishlist(product.id).firstOrNull()
                )
            } ?: listOf())
        }

        return Result.success(
            ProductResponse(
                productList,
                trending.getOrNull()?.second ?: emptyResponse,
                popular.getOrNull()?.second ?: emptyResponse,
            )
        )
    }
}
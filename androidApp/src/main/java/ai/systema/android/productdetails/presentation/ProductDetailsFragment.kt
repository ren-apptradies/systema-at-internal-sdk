package ai.systema.android.productdetails.presentation

import ai.systema.android.R
import ai.systema.android.common.FragmentBinding
import ai.systema.android.common.PARAM_PRODUCT
import ai.systema.android.common.Systema
import ai.systema.android.common.domain.model.ProductItem
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.common.presentation.event.EventsListAdapter
import ai.systema.android.common.presentation.product.ProductListAdapter
import ai.systema.android.databinding.FragmentProductDetailsBinding
import ai.systema.constants.SystemaTags
import android.animation.Animator
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.doOnAttach
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.squareup.picasso.Picasso
import kotlinx.coroutines.flow.collect
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class ProductDetailsFragment : Fragment(), View.OnAttachStateChangeListener,
    View.OnLayoutChangeListener {

    private val binding by FragmentBinding<FragmentProductDetailsBinding>(R.layout.fragment_product_details)

    private val viewModel: ProductDetailsViewModel by hiltNavGraphViewModels(R.id.productDetailsFragment)
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    private val eventAdapter by lazy {
        EventsListAdapter()
    }

    private val actionAnimationLister by lazy {
        object : Animator.AnimatorListener {
            override fun onAnimationStart(p0: Animator?) {
                binding.imgProduct.alpha = 0.3f
            }

            override fun onAnimationEnd(p0: Animator?) {
                binding.actionAnimationView.visibility = View.GONE
                binding.imgProduct.alpha = 1.0f
            }

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationRepeat(p0: Animator?) {

            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        arguments?.getString(PARAM_PRODUCT)?.let { item ->
            val productWrapper = Json.decodeFromString(ProductWrapper.serializer(), item)
            viewModel.monitorProductPage(
                binding.root,
                productWrapper.product,
                this@ProductDetailsFragment
            )
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(false)
        setupBottomSheetEventView()

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.detailsState.collect { state ->
                handleDetailsState(state)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.complementaryState.collect { state ->
                handleProductComplementaryState(state)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.getLogs().collect { state ->
                binding.bottomSheet.isLoading = state.isEmpty()
                eventAdapter.submitList(state.reversed())
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.cart.collect { state ->
                if (state) {
                    binding.actionAnimationView.apply {
                        visibility = View.VISIBLE
                        setAnimation(R.raw.add_to_basket)
                        playAnimation()
                        addAnimatorListener(actionAnimationLister)
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.wishlist.collect { state ->
                if (state) {
                    binding.actionAnimationView.apply {
                        visibility = View.VISIBLE
                        setAnimation(R.raw.add_to_wishlist)
                        playAnimation()
                        addAnimatorListener(actionAnimationLister)
                    }
                }
            }
        }
    }

    private fun handleDetailsState(state: ProductDetailsState) {
        when (state) {
            is ProductDetailsState.RenderProductDetails -> {
                binding.item = state.product

                binding.imgWishlist.isSelected = state.product.isFavorite ?: false

                viewModel.monitorProductPage(
                    binding.imgProduct,
                    state.product.product,
                    attachListener
                )

                Picasso.get()
                    .load(state.product.product.image)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(binding.imgProduct)

                // Add to cart
                val cartClickListener = View.OnClickListener {
                    viewModel.addToCart(state.product.product)
                }
                viewModel.monitorAddToCartButton(
                    binding.btnAddToCart,
                    state.product.product,
                    cartClickListener
                )

                binding.imgWishlist.setOnClickListener {
                    it.isSelected = !it.isSelected
                    viewModel.addToWishlist(state.product.product, it.isSelected)
                }
            }

            is ProductDetailsState.Error -> {
                Toast.makeText(requireContext(), state.message, Toast.LENGTH_SHORT).show()
            }
            ProductDetailsState.Initial -> {
                // No implementation
            }
        }
    }

    private fun handleProductComplementaryState(state: ProductDetailsComplementState) {
        when (state) {
            ProductDetailsComplementState.ShowLoading -> {
                binding.isLoading = true
                binding.isEmpty = false
            }
            ProductDetailsComplementState.HideLoading -> {
                binding.isLoading = false
            }
            is ProductDetailsComplementState.Error -> {
                Toast.makeText(requireContext(), state.message, Toast.LENGTH_SHORT).show()
            }
            is ProductDetailsComplementState.RenderProducts -> {
                generateAdapter(state.productList)
                viewModel.monitorContainer(
                    binding.rvProducts,
                    this@ProductDetailsFragment, state.relatedResp, ProductType.TRENDING
                )

                viewModel.monitorContainer(
                    binding.rvProducts,
                    this@ProductDetailsFragment,
                    state.complementaryResp,
                    ProductType.POPULAR
                )
                binding.isEmpty = false
            }
            ProductDetailsComplementState.EmptyProducts -> {
                binding.isEmpty = true
            }
        }
    }

    private fun generateAdapter(productList: List<ProductWrapper>) {
        val list = productList.groupBy { product -> product.type }
            .flatMap { (section, products) ->
                listOf<ProductItem>(ProductItem.SectionData(section.nameType)) + products.map { productViewItem ->
                    ProductItem.ProductViewData(productViewItem)
                }
            }

        val productAdapter = ProductListAdapter(list,
            { item ->
                findNavController().navigate(
                    R.id.action_productDetails_self,
                    bundleOf(PARAM_PRODUCT to Json.encodeToString(item))
                )
            },
            addToWishListClickListener = { _, item, isSelected ->
                viewModel.addToWishlist(item.product, isSelected)
            },
            addToCartClickListener = { item ->
                viewModel.addToCart(item.product)
            },
            onBindListener = { view, product, listener ->
                viewModel.monitorAddToCartButton(view, product.product, listener)
            })

        val gridLayoutManager = GridLayoutManager(activity, 2)
        binding.rvProducts.apply {
            adapter = productAdapter
            layoutManager = gridLayoutManager
            itemAnimator = DefaultItemAnimator()
        }

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (productAdapter.getItemViewType(position) == ProductListAdapter.TYPE_SECTION) gridLayoutManager.spanCount else 1
            }
        }
    }

    override fun onViewAttachedToWindow(v: View?) {
        Log.d(TAG, v?.getTag(Systema.tagMapping[SystemaTags.ProductId] as Int) as String)
        Log.d(TAG, "onViewAttachedToWindow: ${v.id}")
    }

    override fun onViewDetachedFromWindow(v: View?) {
    }

    private val attachListener = object: View.OnAttachStateChangeListener {
        override fun onViewAttachedToWindow(p0: View?) {
            Log.d(TAG, "attach" + p0?.getTag(Systema.tagMapping[SystemaTags.ProductId] as Int) as String)
        }

        override fun onViewDetachedFromWindow(p0: View?) {
            Log.d(TAG, "detach" + p0?.getTag(Systema.tagMapping[SystemaTags.ProductId] as Int) as String)
        }
    }

    override fun onLayoutChange(
        v: View?,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int,
        oldLeft: Int,
        oldTop: Int,
        oldRight: Int,
        oldBottom: Int
    ) {
        Log.d(TAG, "Layout changed for View: ${v?.id}")
    }

    private fun setupBottomSheetEventView() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet.persistentBottomSheet)
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_collapse)
                    }

                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_expand)
                    }

                    else -> {
                        // No implementation
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })

        binding.bottomSheet.btnAction.setOnClickListener {
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_collapse)
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_expand)
            }
        }

        val decorator: RecyclerView.ItemDecoration = DividerItemDecoration(
            requireContext(),
            DividerItemDecoration.VERTICAL
        )

        binding.bottomSheet.rvEvents.apply {
            adapter = eventAdapter
            addItemDecoration(decorator)
            layoutManager = LinearLayoutManager(requireActivity())
            itemAnimator = DefaultItemAnimator()
        }

        binding.bottomSheet.isLoading = true
    }

    companion object {
        private const val TAG = "ProductDetails"
    }
}
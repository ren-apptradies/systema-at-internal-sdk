package ai.systema.android.productdetails.presentation

import ai.systema.android.R
import ai.systema.android.common.PARAM_PRODUCT
import ai.systema.android.common.ResourceManager
import ai.systema.android.common.Systema
import ai.systema.android.common.domain.model.ProductResponse
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.common.domain.usecases.AddToCart
import ai.systema.android.common.domain.usecases.GetLogEvents
import ai.systema.android.common.domain.usecases.SetLogEvent
import ai.systema.android.common.domain.usecases.UpdateWishListItem
import ai.systema.android.common.toCartItem
import ai.systema.android.monitorAddToCartButton
import ai.systema.android.monitorProductPage
import ai.systema.android.monitorRecContainer
import ai.systema.android.productdetails.domain.usecases.GetComplementaryProducts
import ai.systema.model.index.Product
import ai.systema.model.response.RecommendationResponse
import android.view.View
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import javax.inject.Inject

@HiltViewModel
class ProductDetailsViewModel @Inject constructor(
    private val stateHandle: SavedStateHandle,
    private val resourceManager: ResourceManager,
    private val getComplementaryProducts: GetComplementaryProducts,
    private val getLogEvents: GetLogEvents,
    private val setLogEvent: SetLogEvent,
    private val updateWishListItem: UpdateWishListItem,
    private val addToCart: AddToCart
) : ViewModel() {

    private val _detailsState by lazy {
        MutableStateFlow<ProductDetailsState>(ProductDetailsState.Initial)
    }

    val detailsState: StateFlow<ProductDetailsState> = _detailsState

    private val _complementaryState by lazy {
        MutableStateFlow<ProductDetailsComplementState>(ProductDetailsComplementState.ShowLoading)
    }

    val complementaryState: StateFlow<ProductDetailsComplementState> = _complementaryState

    private val _cart by lazy {
        MutableStateFlow<Boolean>(false)
    }

    val cart: StateFlow<Boolean> = _cart

    private val _wishList by lazy {
        MutableStateFlow<Boolean>(false)
    }

    val wishlist: StateFlow<Boolean> = _wishList

    private var productWrapper: ProductWrapper? = null

    init {
        viewModelScope.launch {

            stateHandle.get<String>(PARAM_PRODUCT)?.let { item ->
                productWrapper = Json.decodeFromString(ProductWrapper.serializer(), item)
                productWrapper?.let {
                    _detailsState.emit(ProductDetailsState.RenderProductDetails(it))
                }
                    ?: _detailsState.emit(ProductDetailsState.Error(resourceManager.getString(R.string.generic_error)))
            }

            getProducts()?.let { products ->
                _complementaryState.emit(ProductDetailsComplementState.HideLoading)
                _complementaryState.emit(
                    if (products.first.isNullOrEmpty()) {
                        ProductDetailsComplementState.EmptyProducts
                    } else {
                        ProductDetailsComplementState.RenderProducts(
                            products.first,
                            products.second,
                            products.third
                        )
                    }
                )
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.product_details),
                        "getRelated"
                    )
                )
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.product_details),
                        "getComplementary"
                    )
                )
            }
        }
    }

    fun monitorProductPage(
        view: View,
        product: Product,
        attachStateChangeListener: View.OnAttachStateChangeListener
    ) {

        Systema.getInstance().monitorProductPage(
            view,
            product,
            attachStateChangeListener
        ) {

        }

        viewModelScope.launch {
            setLogEvent.execute(
                Pair(
                    resourceManager.getString(R.string.product_details),
                    "monitorProductPage"
                )
            )
        }
    }

    fun monitorContainer(
        container: RecyclerView,
        layoutListener: View.OnLayoutChangeListener,
        recommendationResponse: RecommendationResponse,
        type: ProductType
    ) {

        Systema.getInstance().monitorRecContainer(
            container,
            recommendationResponse,
            layoutListener
        ) {}

        viewModelScope.launch {
            setLogEvent.execute(
                Pair(
                    resourceManager.getString(R.string.menu_home),
                    "monitorRecContainer-${type.name}"
                )
            )
        }
    }

    fun monitorAddToCartButton(view: View, product: Product, clickListener: View.OnClickListener) {
        Systema.getInstance().monitorAddToCartButton(
            view,
            product,
            { listOf(product.toCartItem()) },
            clickListener
        ) {

        }
    }

    fun addToCart(product: Product) {
        viewModelScope.launch {
            addToCart.execute(product).run {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.product_details),
                        "monitorAddToCartButton"
                    )
                )

                _cart.emit(true)
                _cart.emit(false)
            }
        }
    }

    fun addToWishlist(product: Product, isSelected: Boolean) {
        viewModelScope.launch {
            if (updateWishListItem.execute(Pair(product, isSelected)).isSuccess) {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.product_details),
                        if (isSelected) "trackWishlistAcquired" else "trackWishlistRelinquished"
                    )
                )

                if (isSelected) {
                    _wishList.emit(true)
                }
                _wishList.emit(false)

                // Update product with the new wishlist value
                productWrapper?.let { newProduct ->
                    newProduct.apply {
                        isFavorite = isSelected
                    }
                    _detailsState.emit(ProductDetailsState.RenderProductDetails(newProduct))
                }
            }
        }
    }


    private suspend fun getProducts(): ProductResponse? {
        return productWrapper?.let {
            getComplementaryProducts.execute(it.product.id).fold(onSuccess = { resp ->
                resp
            }, onFailure = {
                _complementaryState.emit(
                    ProductDetailsComplementState.Error(
                        it.localizedMessage ?: resourceManager.getString(R.string.generic_error)
                    )
                )
                null
            })
        }
    }

    suspend fun getLogs(): Flow<List<String>> {
        return getLogEvents.execute(resourceManager.getString(R.string.product_details))
    }
}
package ai.systema.android.productdetails.presentation

import ai.systema.android.common.domain.model.ProductWrapper

sealed class ProductDetailsState {
    object Initial : ProductDetailsState()

    data class RenderProductDetails(val product: ProductWrapper) : ProductDetailsState()

    data class Error(val message: String) : ProductDetailsState()
}
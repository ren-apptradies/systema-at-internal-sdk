package ai.systema.android.productdetails.presentation

import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.response.RecommendationResponse

sealed class ProductDetailsComplementState {
    object ShowLoading : ProductDetailsComplementState()

    object HideLoading : ProductDetailsComplementState()

    object EmptyProducts : ProductDetailsComplementState()

    data class RenderProducts(
        val productList: List<ProductWrapper>,
        val relatedResp: RecommendationResponse,
        val complementaryResp: RecommendationResponse,
    ) : ProductDetailsComplementState()

    data class Error(val message: String) : ProductDetailsComplementState()
}
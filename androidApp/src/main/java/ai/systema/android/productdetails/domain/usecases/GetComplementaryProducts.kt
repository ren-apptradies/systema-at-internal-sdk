package ai.systema.android.productdetails.domain.usecases

import ai.systema.android.base.domain.SuspendUseCaseWithParam
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.android.common.domain.model.ProductResponse
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.index.Product
import ai.systema.model.response.RecommendationResponse
import javax.inject.Inject

class GetComplementaryProducts @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCaseWithParam<String, Result<ProductResponse>> {

    override suspend fun execute(param: String): Result<ProductResponse> {
        val related = productsRepository.getRelatedProductList(productId = param)
        val complementary = productsRepository.getComplementaryProductList(productId = param)
        val productList = mutableListOf<ProductWrapper>()

        val emptyResponse = RecommendationResponse(
            listOf(),
            -1,
            paginationTimestamp = 0L,
            resultId = "no_id",
            time = ""
        )

        if (related.isSuccess) {
            productList.addAll(related.getOrNull()?.first?.map {
                ProductWrapper(it, ProductType.RELATED)
            } ?: listOf())
        }

        if (complementary.isSuccess) {
            productList.addAll(complementary.getOrNull()?.first?.map {
                ProductWrapper(it, ProductType.COMPLEMENTARY)
            } ?: listOf())
        }

        return Result.success(
            ProductResponse(
                productList,
                related.getOrNull()?.second ?: emptyResponse,
                complementary.getOrNull()?.second ?: emptyResponse,
            )
        )
    }
}


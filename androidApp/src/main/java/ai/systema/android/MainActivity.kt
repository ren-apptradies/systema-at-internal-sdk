package ai.systema.android

import ai.systema.android.common.Systema
import ai.systema.android.databinding.ActivityMainBinding
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Handle the splash screen transition.
        installSplashScreen()

        // Initialise Systema
        // Using non-coroutine
//        Systema.initialise(this)

        // Using coroutines
        Systema.initialise(this, lifecycleScope)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Set toolbar
        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(false)
            setHomeButtonEnabled(false)
            setDisplayShowTitleEnabled(false)
        }

        val navController = findNavController(R.id.main_nav_host_fragment)

        // Setup bottom bar for navigation
        binding.bottomNavView.setupWithNavController(navController)

        val topLevelDestinationList = setOf(
            R.id.homeFragment,
            R.id.categoriesFragment,
            R.id.wishListFragment,
            R.id.cartFragment
        )

        // Setup toolbar for navigation
        val appBarConfiguration = AppBarConfiguration(topLevelDestinationList)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)

        binding.bottomNavView.setOnItemReselectedListener { }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            // Hide bottom navigation
            binding.showBottomNavigation = topLevelDestinationList.contains(destination.id)
        }
    }
}
package ai.systema.android.common.domain

import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.index.Product
import ai.systema.model.response.RecommendationResponse
import ai.systema.model.response.SmartSearchResponse
import kotlinx.coroutines.flow.Flow

interface ProductsRepository {

    suspend fun getTrendingProductList(): Result<Pair<List<Product>, RecommendationResponse>>

    suspend fun getPopularProductList(): Result<Pair<List<Product>, RecommendationResponse>>

    suspend fun getRelatedProductList(productId: String): Result<Pair<List<Product>, RecommendationResponse>>

    suspend fun getComplementaryProductList(productId: String): Result<Pair<List<Product>, RecommendationResponse>>

    suspend fun getCategoryTrendingProductList(category: String): Result<Pair<List<Product>, RecommendationResponse>>

    suspend fun getCategoryPopularProductList(category: String): Result<Pair<List<Product>, RecommendationResponse>>

    suspend fun getCartComplementaryProductList(productId: List<String>): Result<Pair<List<Product>, RecommendationResponse>>

    suspend fun getCartRelatedProductList(productId: List<String>): Result<Pair<List<Product>, RecommendationResponse>>

    suspend fun smartSearch(
        productId: String,
        query: String
    ): Result<Pair<List<Product>, SmartSearchResponse>>

    suspend fun trackWishlist(product: Product, isSelected: Boolean): Result<Boolean>

    suspend fun getWishList(): Flow<List<ProductWrapper>>

    suspend fun isPartOfWishlist(productId: String): Flow<Boolean>

    suspend fun addToWishList(product: Product)

    suspend fun removeFromWishList(productId: String)

    suspend fun addToCart(product: Product)

    suspend fun updateCartItemCount(isIncrement: Boolean, product: Product)

    suspend fun removeFromCart(product: Product)

    suspend fun clearCart()

    suspend fun getCart(): Flow<List<ProductWrapper>>
}
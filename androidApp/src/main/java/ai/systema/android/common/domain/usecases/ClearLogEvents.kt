package ai.systema.android.common.domain.usecases

import ai.systema.android.base.domain.SuspendUseCase
import ai.systema.android.common.domain.LoggerRepository
import javax.inject.Inject

class ClearLogEvents @Inject constructor(
    private val loggerRepository: LoggerRepository
) : SuspendUseCase<Any> {

    override suspend fun execute(): Any = loggerRepository.clearAll()
}
package ai.systema.android.common.domain.model

import ai.systema.model.index.Product
import ai.systema.model.response.RecommendationResponse
import ai.systema.model.response.SmartSearchResponse
import kotlinx.serialization.Serializable

@Serializable
data class ProductWrapper(
    val product: Product,
    val type: ProductType,
    var isFavorite: Boolean? = false,
    var count: Int = 0
)

@Serializable
data class Wishlist(
    val productId: String,
    val imageUrl: String,
    val productName: String,
    val productPrice: Double,
)

@Serializable
data class CartItem(
    val productId: String,
    val imageUrl: String,
    val productName: String,
    val productPrice: Double,
    var itemCount: Int
)

@Serializable
data class CartTotal(
    val itemCount: Int,
    val itemTotal: Double,
    val shippingTotal: Double,
    val totalPrice: Double,
    val listProducts: List<ProductWrapper>
) {
    companion object {
        fun empty() = CartTotal(0, 0.0, 0.0, 0.0, listOf())
    }
}

@Serializable
enum class ProductType(val nameType: String) {
    TRENDING("Trending Products"),
    POPULAR("Popular Products"),
    TRENDING_CATEGORY_WOMEN("Trending in Women"),
    POPULAR_CATEGORY_MEN("Popular in Men"),
    RELATED("Related Products"),
    COMPLEMENTARY("Complementary Products"),
}

typealias ProductResponse = Triple<List<ProductWrapper>, RecommendationResponse, RecommendationResponse>

typealias SearchResponse = Pair<List<ProductWrapper>, SmartSearchResponse>

sealed class ProductItem {
    data class ProductViewData(val productWrapper: ProductWrapper) : ProductItem()
    data class SectionData(val title: String) : ProductItem()
}
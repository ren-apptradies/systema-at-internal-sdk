package ai.systema.android.common.domain.usecases

import ai.systema.android.base.domain.SuspendUseCaseWithParam
import ai.systema.android.common.domain.LoggerRepository
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.model.index.Product
import javax.inject.Inject

class RemoveFromCart @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCaseWithParam<Product, Any> {

    override suspend fun execute(param: Product): Any {
        return productsRepository.removeFromCart(param)
    }
}
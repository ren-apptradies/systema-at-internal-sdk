package ai.systema.android.common.domain.usecases

import ai.systema.android.base.domain.SuspendUseCaseWithParam
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.model.index.Product
import javax.inject.Inject

class UpdateWishListItem @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCaseWithParam<Pair<Product, Boolean>, Result<Boolean>> {

    override suspend fun execute(param: Pair<Product, Boolean>): Result<Boolean> {
        val result = productsRepository.trackWishlist(param.first, param.second)
        
        if (param.second) {
            productsRepository.addToWishList(param.first)
        } else {
            productsRepository.removeFromWishList(param.first.id)
        }

        return if (result.isSuccess) {
            if (param.second) {
                productsRepository.addToWishList(param.first)

            } else {
                productsRepository.removeFromWishList(param.first.id)
            }
            Result.success(true)
        } else {
            Result.failure(exception = result.exceptionOrNull() ?: Exception())
        }
    }
}
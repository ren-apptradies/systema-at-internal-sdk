package ai.systema.android.common.presentation.product

import ai.systema.model.index.Product
import androidx.recyclerview.widget.DiffUtil

class ProductDiffUtilCallback : DiffUtil.ItemCallback<Product>() {
    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
        return oldItem.id == newItem.id &&
                return oldItem.description == newItem.description
    }
}

package ai.systema.android.common.presentation.product

import ai.systema.android.R
import ai.systema.android.common.Systema
import ai.systema.android.common.domain.model.ProductItem
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.databinding.ItemProductBinding
import ai.systema.android.databinding.ItemSectionBinding
import ai.systema.android.monitorRecItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class ProductListAdapter(
    private val sectionedProducts: List<ProductItem>,
    private val itemClickListener: (ProductWrapper) -> Unit,
    private val addToWishListClickListener: (View, ProductWrapper, Boolean) -> Unit,
    private val addToCartClickListener: (ProductWrapper) -> Unit,
    private val onBindListener: (View, ProductWrapper, View.OnClickListener) -> Unit,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        TYPE_SECTION -> SectionViewHolder(
            ItemSectionBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        else -> ProductViewHolder(
            ItemProductBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), itemClickListener, addToWishListClickListener, addToCartClickListener, onBindListener
        )
    }

    override fun getItemCount() = sectionedProducts.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = sectionedProducts[holder.bindingAdapterPosition]) {
            is ProductItem.ProductViewData -> (holder as ProductViewHolder).bind(item)
            is ProductItem.SectionData -> (holder as SectionViewHolder).bind(item)
        }
    }

    override fun getItemViewType(position: Int) = when (sectionedProducts[position]) {
        is ProductItem.ProductViewData -> TYPE_PRODUCT
        is ProductItem.SectionData -> TYPE_SECTION
    }

    companion object {
        const val TYPE_SECTION = 100
        const val TYPE_PRODUCT = 200
    }
}

abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(obj: T)
}

class SectionViewHolder(private val itemBinding: ItemSectionBinding) :
    BaseViewHolder<ProductItem.SectionData>(itemBinding.root) {
    override fun bind(obj: ProductItem.SectionData) {
        itemBinding.title = obj.title
    }
}

class ProductViewHolder(
    private val itemBinding: ItemProductBinding,
    private val itemClickListener: (ProductWrapper) -> Unit,
    private val addToWishListClickListener: (View, ProductWrapper, Boolean) -> Unit,
    private val addToCartClickListener: (ProductWrapper) -> Unit,
    private val onBindListener: (View, ProductWrapper, View.OnClickListener) -> Unit
) : BaseViewHolder<ProductItem.ProductViewData>(itemBinding.root) {

    override fun bind(obj: ProductItem.ProductViewData) {
        itemBinding.item = obj.productWrapper

        // Add to cart
        val cartClickListener = View.OnClickListener {
            addToCartClickListener.invoke(obj.productWrapper)
        }

        onBindListener.invoke(itemBinding.txtAddToCart, obj.productWrapper, cartClickListener)

        // Wishlist
        itemBinding.imgWishlist.isSelected = obj.productWrapper.isFavorite ?: false
        itemBinding.imgWishlist.setOnClickListener {
            it.isSelected = !it.isSelected
            addToWishListClickListener.invoke(
                itemBinding.txtAddToCart,
                obj.productWrapper, it.isSelected
            )
        }

        val clickListener = View.OnClickListener {
            itemClickListener.invoke(obj.productWrapper)
        }

        Picasso.get()
            .load(obj.productWrapper.product.image)
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher)
            .into(itemBinding.imgProduct)

        Systema.getInstance()
            .monitorRecItem(itemBinding.root, obj.productWrapper.product, clickListener) {}
    }
}

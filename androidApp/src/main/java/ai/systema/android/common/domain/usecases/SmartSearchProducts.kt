package ai.systema.android.common.domain.usecases

import ai.systema.android.base.domain.SuspendUseCaseWithParam
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.common.domain.model.SearchResponse
import ai.systema.model.response.SmartSearchResponse
import kotlinx.coroutines.flow.firstOrNull
import javax.inject.Inject

class SmartSearchProducts @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCaseWithParam<Pair<String, String>, Result<SearchResponse>> {

    override suspend fun execute(param: Pair<String, String>): Result<SearchResponse> {
        val search = productsRepository.smartSearch(param.first, param.second)
        val productList = mutableListOf<ProductWrapper>()

        val emptyResponse = SmartSearchResponse(
            listOf(),
            -1,
            paginationTimestamp = 0L,
            resultId = "no_id",
            time = "",
            total = 0
        )

        if (search.isSuccess) {
            productList.addAll(search.getOrNull()?.first?.map { product ->
                ProductWrapper(
                    product,
                    ProductType.TRENDING,
                    isFavorite = productsRepository.isPartOfWishlist(product.id).firstOrNull()
                )
            } ?: listOf())
        }

        return Result.success(
            SearchResponse(
                productList,
                search.getOrNull()?.second ?: emptyResponse
            )
        )
    }
}
package ai.systema.android.common

import ai.systema.constants.Currency
import ai.systema.model.index.Product
import ai.systema.model.tracker.cart.CartItem
import android.view.LayoutInflater
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class FragmentBinding<out T : ViewDataBinding>(
    @LayoutRes private val resId: Int
) : ReadOnlyProperty<Fragment, T> {

    private var binding: T? = null

    override operator fun getValue(
        thisRef: Fragment,
        property: KProperty<*>
    ): T = binding ?: createBinding(thisRef).also { binding = it }

    private fun createBinding(
        fragment: Fragment
    ): T = DataBindingUtil.inflate(LayoutInflater.from(fragment.context), resId, null, true)
}

fun String.formatLog(tag: String): String {
    val date = LocalDateTime.now()
    val formatter = DateTimeFormatter.ofPattern("yy-MM-dd HH:mm:ss")
    return "${date.format(formatter)} - $tag - $this"
}

fun Product.toCartItem(): CartItem = CartItem(this.id, 1, this.price, Currency.AUD)

inline fun <reified T> Gson.fromJson(json: String) =
    fromJson<T>(json, object : TypeToken<T>() {}.type)

const val PARAM_PRODUCT = "param_product"
const val PARAM_CART_TOTAL = "param_cart_total"
const val SPAN_COUNT_LIST = 2
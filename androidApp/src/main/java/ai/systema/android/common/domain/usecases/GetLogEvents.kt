package ai.systema.android.common.domain.usecases

import ai.systema.android.base.domain.SuspendUseCaseWithParam
import ai.systema.android.common.domain.LoggerRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetLogEvents @Inject constructor(
    private val loggerRepository: LoggerRepository
) : SuspendUseCaseWithParam<String, Flow<List<String>>> {

    override suspend fun execute(param: String): Flow<List<String>> {
        return loggerRepository.getLogs(param)
    }
}
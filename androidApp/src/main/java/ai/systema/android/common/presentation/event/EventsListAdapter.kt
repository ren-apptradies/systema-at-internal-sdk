package ai.systema.android.common.presentation.event

import ai.systema.android.databinding.ItemEventBinding
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class EventsListAdapter :
    ListAdapter<String, RecyclerView.ViewHolder>(EventDiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EventViewHolder(
        ItemEventBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as EventViewHolder).bind(getItem(position))
    }

    class EventViewHolder(private val itemBinding: ItemEventBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(obj: String) {
            itemBinding.event = obj
        }
    }
}

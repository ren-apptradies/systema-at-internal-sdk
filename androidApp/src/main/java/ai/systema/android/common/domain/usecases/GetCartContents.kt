package ai.systema.android.common.domain.usecases

import ai.systema.android.base.domain.SuspendUseCase
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.android.common.domain.model.CartItem
import ai.systema.android.common.domain.model.ProductWrapper
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCartContents @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCase<Flow<List<ProductWrapper>>> {

    override suspend fun execute(): Flow<List<ProductWrapper>> {
        return productsRepository.getCart()
    }
}
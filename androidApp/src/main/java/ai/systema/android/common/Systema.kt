package ai.systema.android.common

import ai.systema.android.BuildConfig
import ai.systema.android.SystemaAIClient
import ai.systema.client.SystemaAI
import ai.systema.constants.SystemaConstants
import ai.systema.constants.SystemaTags
import ai.systema.enums.EndpointType
import ai.systema.enums.EnvironmentType
import ai.systema.helper.logging.SystemaLogLevel
import android.content.Context
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.*

internal object Systema {

    private lateinit var systemaAI: SystemaAI

    // map of systema tags to android ids
    // TODO move this to SDK
    val tagMapping = mapOf(
        SystemaTags.ProductId to ai.systema.R.id.systema_product_id,
        SystemaTags.RecId to ai.systema.R.id.systema_rec_id,
        SystemaTags.ProductUrl to ai.systema.R.id.systema_product_url,
        SystemaTags.ResultId to ai.systema.R.id.systema_result_id,
        SystemaTags.Visible to ai.systema.R.id.systema_visible,
        SystemaTags.Observed to ai.systema.R.id.systema_observed,
        SystemaTags.ContainerUrl to ai.systema.R.id.systema_container_url,
        SystemaTags.ProductPrice to ai.systema.R.id.systema_product_price,
        SystemaTags.ProductCurrency to ai.systema.R.id.systema_product_currency,
        SystemaTags.ReferrerUrl to ai.systema.R.id.systema_referrer_url,
    )

    fun getInstance(): SystemaAI = systemaAI

    fun initialise(context: Context) {
        SystemaAIClient.initialize(
            clientID = BuildConfig.SYSTEMA_CLIENT_ID,
            apiKey = BuildConfig.SYSTEMA_PROXY_API_KEY,
            environment = EnvironmentType.TEST,
            logLevel = if (BuildConfig.DEBUG) SystemaLogLevel.DEBUG else SystemaLogLevel.NONE,
            proxyUrls = mapOf(
                EndpointType.DynamicConfig to BuildConfig.SYSTEMA_PROXY_URL,
                EndpointType.Tracker to BuildConfig.SYSTEMA_PROXY_URL,
                EndpointType.Recommend to BuildConfig.SYSTEMA_PROXY_URL,
                EndpointType.Search to BuildConfig.SYSTEMA_PROXY_URL,
            ),
            meta = mapOf(SystemaConstants.SystemaTagMapping to tagMapping),
            context = context
        ) {
            it.onSuccess { systema ->
                systemaAI = systema
            }
        }
    }

    fun initialise(context: Context, coroutineScope: CoroutineScope) {
        coroutineScope.launch {
            systemaAI = SystemaAIClient.initialize(
                clientID = BuildConfig.SYSTEMA_CLIENT_ID,
                apiKey = BuildConfig.SYSTEMA_PROXY_API_KEY,
                environment = EnvironmentType.TEST,
                logLevel = if (BuildConfig.DEBUG) SystemaLogLevel.DEBUG else SystemaLogLevel.NONE,
                proxyUrls = mapOf(
                    EndpointType.DynamicConfig to BuildConfig.SYSTEMA_PROXY_URL,
                    EndpointType.Tracker to BuildConfig.SYSTEMA_PROXY_URL,
                    EndpointType.Recommend to BuildConfig.SYSTEMA_PROXY_URL,
                    EndpointType.Search to BuildConfig.SYSTEMA_PROXY_URL,
                ),
                meta = mapOf(SystemaConstants.SystemaTagMapping to tagMapping),
                context = context
            )

            systemaAI.setUserIdHash(
                Base64.getEncoder().encodeToString("systema_demo_user".toByteArray())
            )
        }
    }
}
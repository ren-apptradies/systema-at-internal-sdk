package ai.systema.android.common.domain

import kotlinx.coroutines.flow.Flow

interface LoggerRepository {

    suspend fun saveLog(screenTag: String, eventName: String)

    suspend fun getLogs(screenTag: String): Flow<List<String>>

    suspend fun clearAll()
}
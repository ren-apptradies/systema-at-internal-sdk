package ai.systema.android.common.data

import ai.systema.android.BuildConfig
import ai.systema.android.common.Systema
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.index.Product
import ai.systema.model.request.CartRecommendationRequest
import ai.systema.model.request.Filter
import ai.systema.model.request.QueryItem
import ai.systema.model.request.RecommendationRequest
import ai.systema.model.request.SmartSearchRequest
import ai.systema.model.response.RecommendationResponse
import ai.systema.model.response.SmartSearchResponse
import ai.systema.model.tracker.wishlist.WishlistItem
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject

private val Context.wishListDataStore: DataStore<Preferences> by preferencesDataStore(name = "systema_wishlist")
private val Context.cartDataStore: DataStore<Preferences> by preferencesDataStore(name = "systema_cart")

class ProductsRepositoryImpl @Inject constructor(@ApplicationContext private val context: Context) :
    ProductsRepository {

    override suspend fun getTrendingProductList(): Result<Pair<List<Product>, RecommendationResponse>> {
        val response = Systema.getInstance().getTrending()

        return if (response.isSuccessful) {
            Result.success(Pair(response.value().results, response.value()))
        } else {
            if (BuildConfig.DEBUG) {
                response.error().printStackTrace()
            }
            Result.failure(response.error())
        }
    }

    override suspend fun getPopularProductList(): Result<Pair<List<Product>, RecommendationResponse>> {
        val response = Systema.getInstance().getPopular()
        return if (response.isSuccessful) {
            Result.success(Pair(response.value().results, response.value()))
        } else {
            if (BuildConfig.DEBUG) {
                response.error().printStackTrace()
            }
            Result.failure(response.error())
        }
    }

    override suspend fun getRelatedProductList(productId: String): Result<Pair<List<Product>, RecommendationResponse>> {
        val response = Systema.getInstance().getRelated(RecommendationRequest(id = productId))
        return if (response.isSuccessful) {
            Result.success(Pair(response.value().results, response.value()))
        } else {
            if (BuildConfig.DEBUG) {
                response.error().printStackTrace()
            }
            Result.failure(response.error())
        }
    }

    override suspend fun getComplementaryProductList(productId: String): Result<Pair<List<Product>, RecommendationResponse>> {
        val response = Systema.getInstance().getComplementary(RecommendationRequest(id = productId))
        return if (response.isSuccessful) {
            Result.success(Pair(response.value().results, response.value()))
        } else {
            if (BuildConfig.DEBUG) {
                response.error().printStackTrace()
            }
            Result.failure(response.error())
        }
    }

    override suspend fun getCategoryTrendingProductList(category: String): Result<Pair<List<Product>, RecommendationResponse>> {
        val response = Systema.getInstance()
            .getCategoryTrending(RecommendationRequest(category = listOf(category)))
        return if (response.isSuccessful) {
            Result.success(Pair(response.value().results, response.value()))
        } else {
            if (BuildConfig.DEBUG) {
                response.error().printStackTrace()
            }
            Result.failure(response.error())
        }
    }

    override suspend fun getCategoryPopularProductList(category: String): Result<Pair<List<Product>, RecommendationResponse>> {
        val response = Systema.getInstance()
            .getCategoryPopular(RecommendationRequest(category = listOf(category)))
        return if (response.isSuccessful) {
            Result.success(Pair(response.value().results, response.value()))
        } else {
            if (BuildConfig.DEBUG) {
                response.error().printStackTrace()
            }
            Result.failure(response.error())
        }
    }

    override suspend fun getCartComplementaryProductList(productId: List<String>): Result<Pair<List<Product>, RecommendationResponse>> {
        val response = Systema.getInstance()
            .getCartComplementary(CartRecommendationRequest(id = productId))
        return if (response.isSuccessful) {
            Result.success(Pair(response.value().results, response.value()))
        } else {
            if (BuildConfig.DEBUG) {
                response.error().printStackTrace()
            }
            Result.failure(response.error())
        }
    }

    override suspend fun getCartRelatedProductList(productId: List<String>): Result<Pair<List<Product>, RecommendationResponse>> {
        val response = Systema.getInstance()
            .getCartRelated(CartRecommendationRequest(id = productId))
        return if (response.isSuccessful) {
            Result.success(Pair(response.value().results, response.value()))
        } else {
            if (BuildConfig.DEBUG) {
                response.error().printStackTrace()
            }
            Result.failure(response.error())
        }
    }

    override suspend fun smartSearch(
        productId: String,
        query: String
    ): Result<Pair<List<Product>, SmartSearchResponse>> {
        val response = Systema.getInstance()
            .smartSearch(
                SmartSearchRequest(
                    query = listOf(
                        QueryItem(
                            id = productId,
                            type = "product"
                        )
                    ), filter = Filter(text = query)
                )
            )

        return if (response.isSuccessful) {
            Result.success(Pair(response.value().results, response.value()))
        } else {
            if (BuildConfig.DEBUG) {
                response.error().printStackTrace()
            }
            Result.failure(response.error())
        }
    }

    override suspend fun trackWishlist(product: Product, isSelected: Boolean): Result<Boolean> {
        return try {
            if (isSelected) {
                Systema.getInstance().trackWishlistAcquired(
                    product.id,
                    listOf(WishlistItem(itemId = product.id)),
                    url = product.link
                )
            } else {
                Systema.getInstance().trackWishlistRelinquished(
                    product.id,
                    WishlistItem(itemId = product.id),
                    url = product.link
                )
            }
            Result.success(true)
        } catch (ex: Exception) {
            if (BuildConfig.DEBUG) {
                ex.printStackTrace()
            }
            Result.failure(exception = ex)
        }
    }

    override suspend fun getWishList(): Flow<List<ProductWrapper>> {
        return context.wishListDataStore.data.map { keys ->
            keys.asMap().entries.map { pref ->
                Json.decodeFromString(ProductWrapper.serializer(), pref.value as String)
            }
        }
    }

    override suspend fun isPartOfWishlist(productId: String): Flow<Boolean> {
        val key = stringPreferencesKey(productId)
        return context.wishListDataStore.data.map {
            !it[key].isNullOrEmpty()
        }
    }

    override suspend fun addToWishList(product: Product) {
        val key = stringPreferencesKey(product.id)
        context.wishListDataStore.edit { keys ->
            keys[key] = Json.encodeToString(
                ProductWrapper(product = product, ProductType.TRENDING, count = 1)
            )
        }
    }

    override suspend fun removeFromWishList(productId: String) {
        val key = stringPreferencesKey(productId)
        context.wishListDataStore.edit { keys ->
            keys.remove(key)
        }
    }

    override suspend fun addToCart(product: Product) {
        val key = stringPreferencesKey(product.id)
        context.cartDataStore.edit { keys ->
            keys[key] = Json.encodeToString(
                ProductWrapper(product = product, ProductType.TRENDING, count = 1)
            )
        }
    }

    override suspend fun updateCartItemCount(isIncrement: Boolean, product: Product) {
        val key = stringPreferencesKey(product.id)
        context.cartDataStore.edit { keys ->
            keys[key] = keys[key]?.let { cartItem ->
                val item = Json.decodeFromString(ProductWrapper.serializer(), cartItem)
                Json.encodeToString(item.apply {
                    count = if (isIncrement) {
                        count + 1
                    } else {
                        if (count > 1) {
                            // ignore
                            count - 1
                        } else {
                            count
                        }
                    }

                })
            } ?: ""
        }
    }

    override suspend fun removeFromCart(product: Product) {
        val key = stringPreferencesKey(product.id)
        context.cartDataStore.edit { keys ->
            keys.remove(key)
        }
    }

    override suspend fun clearCart() {
        context.cartDataStore.edit {
            it.clear()
        }
    }

    override suspend fun getCart(): Flow<List<ProductWrapper>> {
        return context.cartDataStore.data.map { keys ->
            keys.asMap().entries.map { pref ->
                Json.decodeFromString(ProductWrapper.serializer(), pref.value as String)
            }
        }
    }
}
package ai.systema.android.common.data

import ai.systema.android.common.domain.LoggerRepository
import ai.systema.android.common.formatLog
import ai.systema.android.common.fromJson
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "systema_logs")

class LoggerRepositoryImpl @Inject constructor(@ApplicationContext private val context: Context) :
    LoggerRepository {

    private val gson = Gson()
    private var logList = mutableListOf<String>()

    override suspend fun saveLog(screenTag: String, eventName: String) {
        val tag = stringPreferencesKey(screenTag)
        context.dataStore.edit { logs ->
            val listLogs = logs[tag]
            listLogs?.let { listString ->
                try {
                    logList = gson.fromJson(listString)
                } catch (ex: Exception) {
                    // No op. No existing logs
                }
            }

            logList.add(eventName.formatLog(screenTag))
            logs[tag] = gson.toJson(logList)
        }
    }

    override suspend fun getLogs(screenTag: String): Flow<List<String>> {
        val tag = stringPreferencesKey(screenTag)
        return context.dataStore.data
            .map { logs ->
                logs[tag]?.let {
                    gson.fromJson<List<String>>(it)
                } ?: listOf()
            }
    }

    override suspend fun clearAll() {
        context.dataStore.edit { logs ->
            logs.clear()
        }
    }
}
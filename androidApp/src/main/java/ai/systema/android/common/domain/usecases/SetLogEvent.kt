package ai.systema.android.common.domain.usecases

import ai.systema.android.base.domain.SuspendUseCaseWithParam
import ai.systema.android.common.domain.LoggerRepository
import javax.inject.Inject

class SetLogEvent @Inject constructor(
    private val loggerRepository: LoggerRepository
) : SuspendUseCaseWithParam<Pair<String, String>, Any> {

    override suspend fun execute(param: Pair<String, String>): Any {
        return loggerRepository.saveLog(param.first, param.second)
    }
}
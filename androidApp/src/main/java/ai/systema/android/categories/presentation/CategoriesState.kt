package ai.systema.android.categories.presentation

import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.response.RecommendationResponse

sealed class CategoriesState {
    object ShowLoading : CategoriesState()

    object HideLoading : CategoriesState()

    object EmptyProducts : CategoriesState()

    data class RenderProducts(
        val productList: List<ProductWrapper>,
        val trendingResp: RecommendationResponse,
        val popularResp: RecommendationResponse,
    ) : CategoriesState()

    data class Error(val message: String) : CategoriesState()
}
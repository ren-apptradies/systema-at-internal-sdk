package ai.systema.android

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SystemaDemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
    }
}
package ai.systema.android.cart.domain.usecases

import ai.systema.android.base.domain.SuspendUseCase
import ai.systema.android.common.domain.ProductsRepository
import javax.inject.Inject

class ClearCart @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCase<Any> {

    override suspend fun execute(): Any {
        return productsRepository.clearCart()
    }
}
package ai.systema.android.cart.domain.usecases

import ai.systema.android.base.domain.SuspendUseCase
import ai.systema.android.base.domain.SuspendUseCaseWithParam
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.android.common.domain.model.ProductResponse
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.response.RecommendationResponse
import kotlinx.coroutines.flow.firstOrNull
import javax.inject.Inject

class GetCartComplementaryProducts @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCaseWithParam<List<String>, Result<ProductResponse>> {

    override suspend fun execute(param: List<String>): Result<ProductResponse> {
        val complementaryProducts = productsRepository.getCartComplementaryProductList(param)
        val productList = mutableListOf<ProductWrapper>()

        val emptyResponse = RecommendationResponse(
            listOf(),
            -1,
            paginationTimestamp = 0L,
            resultId = "no_id",
            time = ""
        )

        if (complementaryProducts.isSuccess) {
            productList.addAll(complementaryProducts.getOrNull()?.first?.map { product ->
                ProductWrapper(
                    product,
                    ProductType.COMPLEMENTARY,
                    isFavorite = productsRepository.isPartOfWishlist(product.id).firstOrNull()
                )
            } ?: listOf())
        }


        return Result.success(
            ProductResponse(
                productList,
                complementaryProducts.getOrNull()?.second ?: emptyResponse,
                emptyResponse,
            )
        )
    }
}
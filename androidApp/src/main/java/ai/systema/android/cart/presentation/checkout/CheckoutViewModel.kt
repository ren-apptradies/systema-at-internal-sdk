package ai.systema.android.cart.presentation.checkout

import ai.systema.android.R
import ai.systema.android.cart.domain.usecases.ClearCart
import ai.systema.android.common.PARAM_CART_TOTAL
import ai.systema.android.common.ResourceManager
import ai.systema.android.common.Systema
import ai.systema.android.common.domain.model.CartTotal
import ai.systema.android.common.domain.usecases.GetLogEvents
import ai.systema.android.common.domain.usecases.SetLogEvent
import ai.systema.android.monitorProductPage
import ai.systema.model.tracker.cart.OrderItem
import ai.systema.model.tracker.cart.PurchaseOrder
import ai.systema.model.tracker.cart.ShippingAddress
import android.view.View
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val resourceManager: ResourceManager,
    private val stateHandle: SavedStateHandle,
    private val getLogEvents: GetLogEvents,
    private val setLogEvent: SetLogEvent,
    private val clearCart: ClearCart
) : ViewModel() {

    private val _checkoutState by lazy {
        MutableStateFlow<CheckoutState>(CheckoutState.Default)
    }

    val checkoutState: StateFlow<CheckoutState> = _checkoutState

    private var cartTotal: CartTotal? = null

    init {
        viewModelScope.launch {
            stateHandle.get<String>(PARAM_CART_TOTAL)?.let { item ->
                val total = Json.decodeFromString(CartTotal.serializer(), item)
                cartTotal = total
                _checkoutState.emit(CheckoutState.RenderCartTotal(total))
            }
        }
    }

    fun confirmPurchase() {
        viewModelScope.launch {
            _checkoutState.emit(CheckoutState.Confirm)

            // Simulate payment
            delay(3000)

            cartTotal?.let {
                _checkoutState.emit(CheckoutState.PurchaseSuccessful(it))

                val orderUrl =
                    "systema://android-demo-app/${resourceManager.getString(R.string.checkout)}/order-page"
                Systema.getInstance().trackAcquisitionComplete(
                    PurchaseOrder(
                        "DUMMY_ORDER_ID",
                        it.totalPrice,
                        it.totalPrice,
                        12.5,
                        it.shippingTotal,
                        0.0,
                        null,
                        currency = "AUD",
                        shippingAddress = ShippingAddress("Sydney", "NSW", "2000", "Australia"),
                        items = it.listProducts.map { product ->
                            OrderItem(
                                product.product.id,
                                it.itemCount,
                                product.product.price ?: 0.0,
                                12.5,
                                "AUD"
                            )
                        } // Order list items
                    ), orderUrl
                )

                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.checkout),
                        "trackAcquisitionComplete"
                    )
                )

                clearCart.execute()
            }

            _checkoutState.emit(CheckoutState.Default)
        }
    }

    fun monitorPageEvent(
        view: View,
        attachStateChangeListener: View.OnAttachStateChangeListener
    ) {
        Systema.getInstance().monitorProductPage(
            view,
            product = null,
            attachStateChangeListener
        ) {
            viewModelScope.launch {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.checkout),
                        "pageViewEvent"
                    )
                )
            }
        }
    }

    suspend fun getLogs(): Flow<List<String>> {
        return getLogEvents.execute(resourceManager.getString(R.string.checkout))
    }
}
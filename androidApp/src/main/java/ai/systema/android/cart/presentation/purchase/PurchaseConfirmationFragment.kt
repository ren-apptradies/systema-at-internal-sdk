package ai.systema.android.cart.presentation.purchase

import ai.systema.android.R
import ai.systema.android.common.FragmentBinding
import ai.systema.android.databinding.FragmentPurchaseConfirmationBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collect

class PurchaseConfirmationFragment : Fragment() {

    private val binding by FragmentBinding<FragmentPurchaseConfirmationBinding>(R.layout.fragment_purchase_confirmation)
    private val viewModel: PurchaseConfirmationViewModel by hiltNavGraphViewModels(R.id.purchaseConfirmationFragment)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel.monitorPageEvent(binding.root,
            attachStateChangeListener = object : View.OnAttachStateChangeListener {
                override fun onViewAttachedToWindow(p0: View?) {
                    // No implementation
                }

                override fun onViewDetachedFromWindow(p0: View?) {
                    // No implementation
                }
            })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(false)

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.purchaseConfirmationState.collect { state ->
                handleCheckoutState(state)
            }
        }
    }

    private fun handleCheckoutState(state: PurchaseConfirmationState) {
        when (state) {
            is PurchaseConfirmationState.RenderConfirmation -> {
                binding.itemsCount = state.cartTotal.itemCount.toString()
                binding.totalPrice = state.cartTotal.totalPrice.toString()
                binding.isSuccess = true
            }
        }
    }

    companion object {
        private const val TAG = "PurchaseConfirmationFragment"
    }
}
package ai.systema.android.cart.presentation.purchase

import ai.systema.android.common.domain.model.CartTotal

sealed class PurchaseConfirmationState {
    object Default : PurchaseConfirmationState()

    data class RenderConfirmation(
        val cartTotal: CartTotal
    ) : PurchaseConfirmationState()
}
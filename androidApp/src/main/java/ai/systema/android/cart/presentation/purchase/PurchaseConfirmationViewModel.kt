package ai.systema.android.cart.presentation.purchase

import ai.systema.android.R
import ai.systema.android.common.PARAM_CART_TOTAL
import ai.systema.android.common.ResourceManager
import ai.systema.android.common.Systema
import ai.systema.android.common.domain.model.CartTotal
import ai.systema.android.common.domain.usecases.SetLogEvent
import ai.systema.android.monitorProductPage
import android.view.View
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import javax.inject.Inject

@HiltViewModel
class PurchaseConfirmationViewModel @Inject constructor(
    private val resourceManager: ResourceManager,
    private val stateHandle: SavedStateHandle,
    private val setLogEvent: SetLogEvent
) : ViewModel() {

    private val _purchaseConfirmationState by lazy {
        MutableStateFlow<PurchaseConfirmationState>(PurchaseConfirmationState.Default)
    }

    val purchaseConfirmationState: StateFlow<PurchaseConfirmationState> = _purchaseConfirmationState

    init {
        viewModelScope.launch {
            stateHandle.get<String>(PARAM_CART_TOTAL)?.let { item ->
                val cartTotal = Json.decodeFromString(CartTotal.serializer(), item)
                _purchaseConfirmationState.emit(
                    PurchaseConfirmationState.RenderConfirmation(
                        cartTotal
                    )
                )
            }
        }
    }

    fun monitorPageEvent(
        view: View,
        attachStateChangeListener: View.OnAttachStateChangeListener
    ) {
        Systema.getInstance().monitorProductPage(
            view,
            product = null,
            attachStateChangeListener
        ) {
            viewModelScope.launch {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.purchase_confirmation),
                        "pageViewEvent"
                    )
                )
            }
        }
    }
}
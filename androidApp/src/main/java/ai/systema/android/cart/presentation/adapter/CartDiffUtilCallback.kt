package ai.systema.android.cart.presentation.adapter

import ai.systema.android.common.domain.model.ProductWrapper
import androidx.recyclerview.widget.DiffUtil

class CartDiffUtilCallback : DiffUtil.ItemCallback<ProductWrapper>() {
    override fun areItemsTheSame(oldItem: ProductWrapper, newItem: ProductWrapper): Boolean {
        return oldItem.product.id == newItem.product.id
    }

    override fun areContentsTheSame(oldItem: ProductWrapper, newItem: ProductWrapper): Boolean {
        return oldItem.product.id == newItem.product.id && oldItem.count == newItem.count
    }
}

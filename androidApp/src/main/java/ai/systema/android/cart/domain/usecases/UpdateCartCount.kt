package ai.systema.android.cart.domain.usecases

import ai.systema.android.base.domain.SuspendUseCaseWithParam
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.model.index.Product
import javax.inject.Inject

class UpdateCartCount @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCaseWithParam<Pair<Boolean, Product>, Any> {

    override suspend fun execute(param: Pair<Boolean, Product>): Any {
        return productsRepository.updateCartItemCount(param.first, param.second)
    }
}
package ai.systema.android.cart.presentation

import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.response.RecommendationResponse

sealed class CartComplementState {
    object ShowLoading : CartComplementState()

    object HideLoading : CartComplementState()

    object EmptyProducts : CartComplementState()

    data class RenderProducts(
        val productList: List<ProductWrapper>,
        val relatedResp: RecommendationResponse,
        val complementaryResp: RecommendationResponse,
    ) : CartComplementState()

    data class Error(val message: String) : CartComplementState()
}
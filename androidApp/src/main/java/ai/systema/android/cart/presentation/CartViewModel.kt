package ai.systema.android.cart.presentation

import ai.systema.android.R
import ai.systema.android.cart.domain.usecases.GetCartComplementaryProducts
import ai.systema.android.cart.domain.usecases.UpdateCartCount
import ai.systema.android.common.ResourceManager
import ai.systema.android.common.Systema
import ai.systema.android.common.domain.model.CartTotal
import ai.systema.android.common.domain.model.ProductResponse
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.common.domain.usecases.AddToCart
import ai.systema.android.common.domain.usecases.GetCartContents
import ai.systema.android.common.domain.usecases.GetLogEvents
import ai.systema.android.common.domain.usecases.RemoveFromCart
import ai.systema.android.common.domain.usecases.SetLogEvent
import ai.systema.android.common.domain.usecases.UpdateWishListItem
import ai.systema.android.common.toCartItem
import ai.systema.android.monitorAddToCartButton
import ai.systema.android.monitorProductPage
import ai.systema.android.monitorRecContainer
import ai.systema.android.monitorRemoveFromCartButton
import ai.systema.model.index.Product
import ai.systema.model.response.RecommendationResponse
import ai.systema.model.tracker.cart.CartItem
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.math.RoundingMode
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val resourceManager: ResourceManager,
    private val getCartComplementaryProducts: GetCartComplementaryProducts,
    private val getLogEvents: GetLogEvents,
    private val setLogEvent: SetLogEvent,
    private val addToCart: AddToCart,
    private val removeFromCart: RemoveFromCart,
    private val getCartContents: GetCartContents,
    private val updateCartCount: UpdateCartCount,
    private val updateWishListItem: UpdateWishListItem
) : ViewModel() {

    private val _complementaryState by lazy {
        MutableStateFlow<CartComplementState>(CartComplementState.ShowLoading)
    }

    val complementaryState: StateFlow<CartComplementState> = _complementaryState

    private val _cart by lazy {
        MutableStateFlow(false)
    }

    val cart: StateFlow<Boolean> = _cart

    private val _cartTotal by lazy {
        MutableStateFlow(CartTotal.empty())
    }

    val cartTotal: StateFlow<CartTotal> = _cartTotal

    private val _wishList by lazy {
        MutableStateFlow(false)
    }

    val wishlist: StateFlow<Boolean> = _wishList

    private var productResponse: ProductResponse? = null

    fun monitorContainer(
        container: RecyclerView,
        layoutListener: View.OnLayoutChangeListener,
        recommendationResponse: RecommendationResponse,
        type: ProductType
    ) {

        Systema.getInstance().monitorRecContainer(
            container,
            recommendationResponse,
            layoutListener
        ) {}

        viewModelScope.launch {
            setLogEvent.execute(
                Pair(
                    resourceManager.getString(R.string.menu_cart),
                    "monitorRecContainer-${type.name}"
                )
            )
        }
    }

    fun monitorPageEvent(
        view: View,
        attachStateChangeListener: View.OnAttachStateChangeListener
    ) {
        Systema.getInstance().monitorProductPage(
            view,
            product = null,
            attachStateChangeListener
        ) {
            viewModelScope.launch {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_cart),
                        "pageViewEvent"
                    )
                )
            }
        }
    }

    fun monitorAddToCartButton(view: View, product: Product, clickListener: View.OnClickListener?) {
        Systema.getInstance().monitorAddToCartButton(
            view,
            product,
            { listOf(product.toCartItem()) },
            clickListener
        ) {}
    }

    fun addToCart(product: Product) {
        viewModelScope.launch {
            addToCart.execute(product).run {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_cart),
                        "monitorAddToCartButton"
                    )
                )

                _cart.emit(true)
                _cart.emit(false)
            }
        }
    }

    fun monitorRemoveFromCartButton(
        view: View,
        product: Product,
        clickListener: View.OnClickListener
    ) {
        Systema.getInstance().monitorRemoveFromCartButton(
            view,
            product,
            { CartItem(itemId = product.id, quantity = 1) },
            clickListener,
        ) {}
    }

    fun removeFromCart(view: View, product: Product) {
        viewModelScope.launch {
            removeFromCart.execute(product).run {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_cart),
                        "monitorRemoveFromCartButton"
                    )
                )
            }
        }
    }

    fun addToWishlist(product: Product, isSelected: Boolean) {
        viewModelScope.launch {
            if (updateWishListItem.execute(Pair(product, isSelected)).isSuccess) {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_categories),
                        if (isSelected) "trackWishlistAcquired" else "trackWishlistRelinquished"
                    )
                )

                // Update product list with the new wishlist value
                productResponse?.let { newProduct ->
                    newProduct.first.firstOrNull { it.product.id == product.id }?.apply {
                        isFavorite = isSelected
                    }

                    if (isSelected) {
                        _wishList.emit(true)
                    }
                    _wishList.emit(false)

                    _complementaryState.emit(
                        CartComplementState.RenderProducts(
                            newProduct.first,
                            newProduct.second,
                            newProduct.third
                        )
                    )
                }
            }
        }
    }

    fun updateCartCount(isIncrement: Boolean, product: Product) {
        viewModelScope.launch {
            updateCartCount.execute(Pair(isIncrement, product)).run {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_cart),
                        if (isIncrement) "monitorAddToCartButton" else "monitorRemoveFromCartButton"
                    )
                )
            }
        }
    }

    private suspend fun getProducts(productIds: List<String>): ProductResponse? {
        return getCartComplementaryProducts.execute(productIds).fold(onSuccess = { resp ->
            resp
        }, onFailure = {
            _complementaryState.emit(
                CartComplementState.Error(
                    it.localizedMessage ?: resourceManager.getString(R.string.generic_error)
                )
            )
            null
        })
    }

    suspend fun getLogs(): Flow<List<String>> {
        return getLogEvents.execute(resourceManager.getString(R.string.menu_cart))
    }

    suspend fun getCart(): Flow<List<ProductWrapper>> {
        return getCartContents.execute().map { items ->
            if (!items.isNullOrEmpty()) {

                // Update cart totals
                val count = items.sumOf {
                    it.count
                }

                val itemCountTotal = items.sumOf { product ->
                    product.product.price?.let {
                        it * product.count
                    } ?: 0.0
                }.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()

                val shipping =
                    (itemCountTotal * 0.05).toBigDecimal().setScale(2, RoundingMode.UP).toDouble()
                val totalPrice =
                    (itemCountTotal + shipping).toBigDecimal().setScale(2, RoundingMode.UP)
                        .toDouble()

                _cartTotal.emit(CartTotal(count, itemCountTotal, shipping, totalPrice, items))

                // Update complementary products
                viewModelScope.launch {
                    getProducts(items.map { it.product.id })?.let { products ->
                        productResponse = products
                        _complementaryState.emit(CartComplementState.HideLoading)
                        _complementaryState.emit(
                            if (products.first.isNullOrEmpty()) {
                                CartComplementState.EmptyProducts
                            } else {
                                CartComplementState.RenderProducts(
                                    products.first,
                                    products.second,
                                    products.third
                                )
                            }
                        )
                        setLogEvent.execute(
                            Pair(
                                resourceManager.getString(R.string.menu_cart),
                                "getCartComplementary"
                            )
                        )
                    }
                }
            }
            items
        }
    }
}
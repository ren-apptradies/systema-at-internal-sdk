package ai.systema.android.cart.presentation.checkout

import ai.systema.android.common.domain.model.CartTotal

sealed class CheckoutState {
    object Default : CheckoutState()

    object Confirm : CheckoutState()

    data class PurchaseSuccessful(val cartTotal: CartTotal) : CheckoutState()

    data class RenderCartTotal(
        val cartTotal: CartTotal
    ) : CheckoutState()
}
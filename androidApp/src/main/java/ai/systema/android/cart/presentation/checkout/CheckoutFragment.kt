package ai.systema.android.cart.presentation.checkout

import ai.systema.android.R
import ai.systema.android.common.FragmentBinding
import ai.systema.android.common.PARAM_CART_TOTAL
import ai.systema.android.common.PARAM_PRODUCT
import ai.systema.android.common.presentation.event.EventsListAdapter
import ai.systema.android.databinding.FragmentCheckoutBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.coroutines.flow.collect
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class CheckoutFragment : Fragment() {

    private val binding by FragmentBinding<FragmentCheckoutBinding>(R.layout.fragment_checkout)

    private val viewModel: CheckoutViewModel by hiltNavGraphViewModels(R.id.checkoutFragment)
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    private val eventAdapter by lazy {
        EventsListAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel.monitorPageEvent(binding.root,
            attachStateChangeListener = object : View.OnAttachStateChangeListener {
                override fun onViewAttachedToWindow(p0: View?) {
                    // No implementation
                }

                override fun onViewDetachedFromWindow(p0: View?) {
                    // No implementation
                }
            })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(false)
        setupBottomSheetEventView()

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.checkoutState.collect { state ->
                handleCheckoutState(state)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.getLogs().collect { state ->
                binding.bottomSheet.isLoading = state.isEmpty()
                eventAdapter.submitList(state.reversed())
            }
        }
    }

    private fun handleCheckoutState(state: CheckoutState) {
        when (state) {
            is CheckoutState.RenderCartTotal -> {
                binding.itemsCount = state.cartTotal.itemCount.toString()
                binding.totalItems = state.cartTotal.itemTotal.toString()
                binding.shippingPrice = state.cartTotal.shippingTotal.toString()
                binding.totalPrice = state.cartTotal.totalPrice.toString()

                binding.btnConfirm.setOnClickListener {
                    viewModel.confirmPurchase()
                }
            }
            CheckoutState.Confirm -> {
                binding.isLoading = true
                binding.actionAnimationView.apply {
                    visibility = View.VISIBLE
                    playAnimation()
                }
            }
            is CheckoutState.PurchaseSuccessful -> {
                binding.isLoading = false
                binding.actionAnimationView.apply {
                    visibility = View.GONE
                    cancelAnimation()
                }
                findNavController().navigate(
                    R.id.action_checkoutFragment_to_purchaseConfirmationFragment,
                    bundleOf(PARAM_CART_TOTAL to Json.encodeToString(state.cartTotal))
                )
            }
            CheckoutState.Default -> {
                binding.isLoading = false
                binding.actionAnimationView.apply {
                    visibility = View.GONE
                    cancelAnimation()
                }
            }
        }
    }

    private fun setupBottomSheetEventView() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet.persistentBottomSheet)
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_collapse)
                    }

                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_expand)
                    }

                    else -> {
                        // No implementation
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })

        binding.bottomSheet.btnAction.setOnClickListener {
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_collapse)
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_expand)
            }
        }

        val decorator: RecyclerView.ItemDecoration = DividerItemDecoration(
            requireContext(),
            DividerItemDecoration.VERTICAL
        )

        binding.bottomSheet.rvEvents.apply {
            adapter = eventAdapter
            addItemDecoration(decorator)
            layoutManager = LinearLayoutManager(requireActivity())
            itemAnimator = DefaultItemAnimator()
        }

        binding.bottomSheet.isLoading = true
    }

    companion object {
        private const val TAG = "CheckoutFragment"
    }
}
package ai.systema.android.cart.presentation.adapter

import ai.systema.android.R
import ai.systema.android.cart.presentation.CartViewModel
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.databinding.ItemProductCartBinding
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class CartAdapter(private val viewModel: CartViewModel) :
    ListAdapter<ProductWrapper, RecyclerView.ViewHolder>(CartDiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CartViewHolder(
        ItemProductCartBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ), viewModel
    )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CartViewHolder).bind(getItem(position))
    }

    class CartViewHolder(
        private val itemBinding: ItemProductCartBinding,
        private val viewModel: CartViewModel
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(obj: ProductWrapper) {
            itemBinding.item = obj.product
            itemBinding.txtCount.text = obj.count.toString()

            // Add count
            val addCount = View.OnClickListener {
                viewModel.updateCartCount(true, obj.product)
            }
            viewModel.monitorAddToCartButton(itemBinding.btnAddCount, obj.product, addCount)

            // Minus count
            val removeCount = View.OnClickListener {
                viewModel.updateCartCount( false, obj.product)
            }
            viewModel.monitorRemoveFromCartButton(itemBinding.btnMinusCount, obj.product, removeCount)

            // Remove from cart
            val removeFromCart = View.OnClickListener {
                viewModel.removeFromCart(itemBinding.txtRemove, obj.product)
            }
            viewModel.monitorRemoveFromCartButton(itemBinding.txtRemove, obj.product, removeFromCart)

            Picasso.get()
                .load(obj.product.image)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(itemBinding.imgProduct)
        }
    }
}

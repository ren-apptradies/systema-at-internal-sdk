package ai.systema.android.di

import ai.systema.android.common.data.LoggerRepositoryImpl
import ai.systema.android.common.domain.LoggerRepository
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.android.common.data.ProductsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@InstallIn(ActivityRetainedComponent::class)
@Module
abstract class RepositoryModule {

    @Binds
    abstract fun providesProductsRepository(
        productsRepositoryImpl: ProductsRepositoryImpl
    ): ProductsRepository

    @Binds
    abstract fun providesLoggerRepository(
        loggerRepositoryImpl: LoggerRepositoryImpl
    ): LoggerRepository
}

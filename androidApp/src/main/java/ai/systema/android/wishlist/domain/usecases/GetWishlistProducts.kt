package ai.systema.android.wishlist.domain.usecases

import ai.systema.android.base.domain.SuspendUseCase
import ai.systema.android.common.domain.ProductsRepository
import ai.systema.android.common.domain.model.ProductWrapper
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetWishlistProducts @Inject constructor(
    private val productsRepository: ProductsRepository
) : SuspendUseCase<Flow<List<ProductWrapper>>> {

    override suspend fun execute(): Flow<List<ProductWrapper>> {
        return productsRepository.getWishList()
    }
}
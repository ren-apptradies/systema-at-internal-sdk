package ai.systema.android.wishlist.presentation

import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.model.response.RecommendationResponse

sealed class WishlistProductsState {
    object ShowLoading : WishlistProductsState()

    object HideLoading : WishlistProductsState()

    object EmptyProducts : WishlistProductsState()

    data class RenderProducts(
        val productList: List<ProductWrapper>,
        val trendingResp: RecommendationResponse
    ) : WishlistProductsState()

    data class Error(val message: String) : WishlistProductsState()
}
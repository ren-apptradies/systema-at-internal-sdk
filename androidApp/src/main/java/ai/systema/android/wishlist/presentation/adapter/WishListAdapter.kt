package ai.systema.android.wishlist.presentation.adapter

import ai.systema.android.R
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.common.domain.model.Wishlist
import ai.systema.android.databinding.ItemWishlistBinding
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class WishListAdapter(private val removeListener: (View, ProductWrapper) -> Unit) :
    ListAdapter<ProductWrapper, RecyclerView.ViewHolder>(WishlistDiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = WishlistViewHolder(
        ItemWishlistBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ), removeListener
    )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WishlistViewHolder).bind(getItem(position))
    }

    class WishlistViewHolder(
        private val itemBinding: ItemWishlistBinding,
        private val removeListener: (View, ProductWrapper) -> Unit
    ) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(obj: ProductWrapper) {

            itemBinding.imgRemove.setOnClickListener {
                removeListener.invoke(it, obj)
            }

            Picasso.get()
                .load(obj.product.image)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(itemBinding.imgProduct)
        }
    }
}

package ai.systema.android.wishlist.presentation

import ai.systema.android.R
import ai.systema.android.cart.domain.usecases.GetCartRelatedProducts
import ai.systema.android.common.ResourceManager
import ai.systema.android.common.Systema
import ai.systema.android.common.domain.model.ProductResponse
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.common.domain.usecases.AddToCart
import ai.systema.android.common.domain.usecases.GetLogEvents
import ai.systema.android.common.domain.usecases.SetLogEvent
import ai.systema.android.common.domain.usecases.UpdateWishListItem
import ai.systema.android.common.toCartItem
import ai.systema.android.monitorAddToCartButton
import ai.systema.android.monitorProductPage
import ai.systema.android.monitorRecContainer
import ai.systema.android.wishlist.domain.usecases.GetWishlistProducts
import ai.systema.model.index.Product
import ai.systema.model.response.RecommendationResponse
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val resourceManager: ResourceManager,
    private val getWishlistProducts: GetWishlistProducts,
    private val getCartRelatedProducts: GetCartRelatedProducts,
    private val getLogEvents: GetLogEvents,
    private val setLogEvent: SetLogEvent,
    private val updateWishListItem: UpdateWishListItem,
    private val addToCart: AddToCart
) : ViewModel() {

    private val _uiProductsState by lazy {
        MutableStateFlow<WishlistProductsState>(WishlistProductsState.ShowLoading)
    }

    val uiProductsState: StateFlow<WishlistProductsState> = _uiProductsState

    private val _cart by lazy {
        MutableStateFlow<Boolean>(false)
    }

    val cart: StateFlow<Boolean> = _cart

    private val _wishList by lazy {
        MutableStateFlow<Boolean>(false)
    }

    val wishlist: StateFlow<Boolean> = _wishList

    private var productResponse: ProductResponse? = null

    fun monitorContainer(
        container: RecyclerView,
        layoutListener: View.OnLayoutChangeListener,
        recommendationResponse: RecommendationResponse,
        type: ProductType
    ) {

        Systema.getInstance().monitorRecContainer(
            container,
            recommendationResponse,
            layoutListener
        ) {}

        viewModelScope.launch {
            setLogEvent.execute(
                Pair(
                    resourceManager.getString(R.string.menu_wishlist),
                    "monitorRecContainer-${type.name}"
                )
            )
        }
    }

    fun monitorPageEvent(
        view: View,
        attachStateChangeListener: View.OnAttachStateChangeListener
    ) {
        Systema.getInstance().monitorProductPage(
            view,
            product = null,
            attachStateChangeListener
        ) {
            viewModelScope.launch {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_wishlist),
                        "pageViewEvent"
                    )
                )
            }
        }
    }

    fun monitorAddToCartButton(view: View, product: Product, clickListener: View.OnClickListener) {
        Systema.getInstance().monitorAddToCartButton(
            view,
            product,
            { listOf(product.toCartItem()) },
            clickListener
        ) { }
    }

    fun addToCart(product: Product) {
        viewModelScope.launch {
            addToCart.execute(product).run {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_wishlist),
                        "monitorAddToCartButton"
                    )
                )

                _cart.emit(true)
                _cart.emit(false)
            }
        }
    }

    fun updateWishlist(product: Product, isSelected: Boolean) {
        viewModelScope.launch {
            if (updateWishListItem.execute(Pair(product, isSelected)).isSuccess) {
                setLogEvent.execute(
                    Pair(
                        resourceManager.getString(R.string.menu_wishlist),
                        if (isSelected) "trackWishlistAcquired" else "trackWishlistRelinquished"
                    )
                )

                // Update product list with the new wishlist value
                productResponse?.let { newProduct ->
                    newProduct.first.firstOrNull { it.product.id == product.id }?.apply {
                        isFavorite = isSelected
                    }

                    if (isSelected) {
                        _wishList.emit(true)
                    }
                    _wishList.emit(false)

                    _uiProductsState.emit(
                        WishlistProductsState.RenderProducts(
                            newProduct.first,
                            newProduct.second
                        )
                    )
                }
            }
        }
    }

    private suspend fun getProducts(listProductId: List<String>): ProductResponse? {
        return getCartRelatedProducts.execute(listProductId).fold(onSuccess = {
            it
        }, onFailure = {
            _uiProductsState.emit(
                WishlistProductsState.Error(
                    it.localizedMessage ?: resourceManager.getString(R.string.generic_error)
                )
            )
            null
        })
    }

    suspend fun getLogs(): Flow<List<String>> {
        return getLogEvents.execute(resourceManager.getString(R.string.menu_wishlist))
    }

    suspend fun getWishList(): Flow<List<ProductWrapper>> {
        return getWishlistProducts.execute().map { wishList ->
            if (!wishList.isNullOrEmpty()) {
                viewModelScope.launch {
                    getProducts(wishList.map { product -> product.product.id })?.let { products ->
                        _uiProductsState.emit(WishlistProductsState.HideLoading)
                        _uiProductsState.emit(
                            if (products.first.isNullOrEmpty()) {
                                WishlistProductsState.EmptyProducts
                            } else {
                                productResponse = products
                                WishlistProductsState.RenderProducts(
                                    products.first,
                                    products.second
                                )
                            }
                        )
                        setLogEvent.execute(
                            Pair(
                                resourceManager.getString(R.string.menu_wishlist),
                                "getCartRelated"
                            )
                        )
                    }
                }
            } else {
                _uiProductsState.emit(WishlistProductsState.HideLoading)
                _uiProductsState.emit(WishlistProductsState.EmptyProducts)
            }

            wishList
        }
    }
}
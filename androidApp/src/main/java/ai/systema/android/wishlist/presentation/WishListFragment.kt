package ai.systema.android.wishlist.presentation

import ai.systema.android.R
import ai.systema.android.common.FragmentBinding
import ai.systema.android.common.PARAM_PRODUCT
import ai.systema.android.common.SPAN_COUNT_LIST
import ai.systema.android.common.domain.model.ProductItem
import ai.systema.android.common.domain.model.ProductType
import ai.systema.android.common.domain.model.ProductWrapper
import ai.systema.android.common.presentation.event.EventsListAdapter
import ai.systema.android.common.presentation.product.ProductListAdapter
import ai.systema.android.databinding.FragmentWishlistBinding
import ai.systema.android.wishlist.presentation.adapter.WishListAdapter
import android.animation.Animator
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.coroutines.flow.collect
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class WishListFragment : Fragment(), View.OnLayoutChangeListener {

    private val binding by FragmentBinding<FragmentWishlistBinding>(R.layout.fragment_wishlist)
    private val viewModel: WishlistViewModel by hiltNavGraphViewModels(R.id.mobile_navigation) // Scoped to main navigation to avoid calling the services again
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    private val eventAdapter by lazy {
        EventsListAdapter()
    }

    private val wishlistAdapter by lazy {
        WishListAdapter { _, wishList ->
            viewModel.updateWishlist(wishList.product, false)
        }
    }

    private val actionAnimationLister by lazy {
        object : Animator.AnimatorListener {
            override fun onAnimationStart(p0: Animator?) {
                binding.rvProducts.alpha = 0.3f
            }

            override fun onAnimationEnd(p0: Animator?) {
                binding.actionAnimationView.visibility = View.GONE
                binding.rvProducts.alpha = 1.0f
            }

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationRepeat(p0: Animator?) {

            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel.monitorPageEvent(binding.root,
            attachStateChangeListener = object : View.OnAttachStateChangeListener {
                override fun onViewAttachedToWindow(p0: View?) {
                    // No implementation
                }

                override fun onViewDetachedFromWindow(p0: View?) {
                    // No implementation
                }
            })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(false)

        setupWishlistContainerView()
        setupBottomSheetEventView()

        // Using coroutines
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.uiProductsState.collect { state ->
                handleState(state)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.getLogs().collect { state ->
                binding.bottomSheet.isLoading = state.isEmpty()
                eventAdapter.submitList(state.reversed())
            }
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.getWishList().collect { wishList ->
                wishlistAdapter.submitList(wishList)
                binding.itemNumber = wishList.size
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.cart.collect { state ->
                if (state) {
                    binding.actionAnimationView.apply {
                        visibility = View.VISIBLE
                        setAnimation(R.raw.add_to_basket)
                        playAnimation()
                        addAnimatorListener(actionAnimationLister)
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.wishlist.collect { state ->
                if (state) {
                    binding.actionAnimationView.apply {
                        visibility = View.VISIBLE
                        setAnimation(R.raw.add_to_wishlist)
                        playAnimation()
                        addAnimatorListener(actionAnimationLister)
                    }
                }
            }
        }
    }

    private fun handleState(state: WishlistProductsState) {
        when (state) {
            WishlistProductsState.ShowLoading -> {
                binding.isLoading = true
                binding.isEmpty = false
            }
            WishlistProductsState.HideLoading -> {
                binding.isLoading = false
            }
            is WishlistProductsState.Error -> {
                Toast.makeText(requireContext(), state.message, Toast.LENGTH_SHORT).show()
            }
            is WishlistProductsState.RenderProducts -> {
                generateAdapter(state.productList)
                viewModel.monitorContainer(
                    binding.rvProducts,
                    this@WishListFragment, state.trendingResp, ProductType.TRENDING
                )

                binding.isEmpty = false
            }
            WishlistProductsState.EmptyProducts -> {
                binding.isEmpty = true
                binding.bottomSheet.isLoading = false
            }
        }
    }

    private fun generateAdapter(productList: List<ProductWrapper>) {
        val list = productList.groupBy { product -> product.type }
            .flatMap { (section, products) ->
                listOf<ProductItem>(ProductItem.SectionData(section.nameType)) + products.map { productViewItem ->
                    ProductItem.ProductViewData(productViewItem)
                }
            }

        val productAdapter = ProductListAdapter(list,
            itemClickListener = { item ->
                findNavController().navigate(
                    R.id.action_wishListFragment_to_productDetailsFragment,
                    bundleOf(PARAM_PRODUCT to Json.encodeToString(item))
                )
            },
            addToWishListClickListener = { _, item, isSelected ->
                viewModel.updateWishlist(item.product, isSelected)
            },
            addToCartClickListener = { item ->
                viewModel.addToCart(item.product)
            },
            onBindListener = { view, product, listener ->
                viewModel.monitorAddToCartButton(view, product.product, listener)
            })

        val gridLayoutManager = GridLayoutManager(activity, SPAN_COUNT_LIST)
        binding.rvProducts.apply {
            adapter = productAdapter
            layoutManager = gridLayoutManager
            itemAnimator = DefaultItemAnimator()
        }

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (productAdapter.getItemViewType(position) == ProductListAdapter.TYPE_SECTION) gridLayoutManager.spanCount else 1
            }
        }
    }

    private fun setupWishlistContainerView() {
        binding.rvWishlist.apply {
            adapter = wishlistAdapter
            layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
            itemAnimator = DefaultItemAnimator()
        }
    }

    private fun setupBottomSheetEventView() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet.persistentBottomSheet)
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_collapse)
                    }

                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_expand)
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })

        binding.bottomSheet.btnAction.setOnClickListener {
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_collapse)
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                binding.bottomSheet.btnAction.setImageResource(R.drawable.ic_expand)
            }
        }

        val decorator: RecyclerView.ItemDecoration = DividerItemDecoration(
            requireContext(),
            DividerItemDecoration.VERTICAL
        )

        binding.bottomSheet.rvEvents.apply {
            adapter = eventAdapter
            addItemDecoration(decorator)
            layoutManager = LinearLayoutManager(requireActivity())
            itemAnimator = DefaultItemAnimator()
        }

        binding.bottomSheet.isLoading = true
    }

    override fun onLayoutChange(
        v: View?,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int,
        oldLeft: Int,
        oldTop: Int,
        oldRight: Int,
        oldBottom: Int
    ) {
        Log.d(TAG, "Layout changed for View: ${v?.id}")
    }

    companion object {
        const val TAG = "WishListFragment"
    }
}
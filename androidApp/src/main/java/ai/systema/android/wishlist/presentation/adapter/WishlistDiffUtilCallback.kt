package ai.systema.android.wishlist.presentation.adapter

import ai.systema.android.common.domain.model.ProductWrapper
import androidx.recyclerview.widget.DiffUtil

class WishlistDiffUtilCallback : DiffUtil.ItemCallback<ProductWrapper>() {
    override fun areItemsTheSame(oldItem: ProductWrapper, newItem: ProductWrapper): Boolean {
        return oldItem.product.id == newItem.product.id
    }

    override fun areContentsTheSame(oldItem: ProductWrapper, newItem: ProductWrapper): Boolean {
        return oldItem.product.title == oldItem.product.title
    }
}

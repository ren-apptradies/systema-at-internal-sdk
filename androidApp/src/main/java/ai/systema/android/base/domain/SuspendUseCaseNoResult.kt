package ai.systema.android.base.domain

interface SuspendUseCaseNoResult<I : Any> {
    suspend fun execute(param : I)
}

package ai.systema.android.base.domain

interface SuspendUseCaseWithParam<I : Any, R : Any> {
    suspend fun execute(param: I): R
}

package ai.systema.android.base.domain

interface SuspendUseCase<R : Any> {
    suspend fun execute(): R
}

plugins {
    id("com.android.application")
    id("dagger.hilt.android.plugin")
    kotlin("android")
    kotlin("kapt")
    kotlin("plugin.serialization") version "1.5.21"
}

dependencies {
    val kotlinVersion = "1.5.21"
    implementation(project(":shared"))

    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion")
    implementation("androidx.core:core-ktx:1.6.0")
    implementation("androidx.appcompat:appcompat:1.3.1")

    implementation("com.google.android.material:material:1.4.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.1")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.3.1")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1")
    implementation("androidx.navigation:navigation-fragment-ktx:2.3.5")
    implementation("androidx.navigation:navigation-ui-ktx:2.3.5")
    implementation("androidx.recyclerview:recyclerview:1.2.1")
    implementation("androidx.core:core-splashscreen:1.0.0-alpha02")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.4.0-rc01")

    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.3.1")

    implementation("androidx.datastore:datastore-preferences:1.0.0")

    implementation("androidx.hilt:hilt-navigation-fragment:1.0.0")

    implementation ("io.ktor:ktor-client-android:1.6.2")

    implementation("com.squareup.picasso:picasso:2.71828")
    implementation("com.airbnb.android:lottie:3.4.0")

    implementation("com.google.code.gson:gson:2.8.8")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.2.2")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.2.2")

    implementation("com.google.dagger:hilt-android:2.38.1")
    kapt("com.google.dagger:hilt-android-compiler:2.38.1")
}

android {
    compileSdkVersion(31)
    defaultConfig {
        applicationId = "ai.systema.android"
        minSdkVersion(26)
        targetSdkVersion(31)
        versionCode = 1
        versionName = "1.0"

        buildConfigField(
            "String",
            "SYSTEMA_PROXY_URL",
            "\"https://853f87e6-a3ef-41d2-b098-8a9153a06315.mock.pstmn.io\""
        )
        buildConfigField(
            "String",
            "SYSTEMA_PROXY_API_KEY",
            "\"PMAK-61272c286612d70046cbb0e0-8e167bf79ddab6e53763ddf36287981a39\""
        )
        buildConfigField("String", "SYSTEMA_CLIENT_ID", "\"unreal\"")
    }


    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
    buildFeatures {
        viewBinding = true
        dataBinding = true
    }
}
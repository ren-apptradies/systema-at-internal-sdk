package ai.systema.exception

/**
 * Runtime Exceptions
 */

public sealed class SystemaRuntimeException(
    message: String? = null,
    cause: Throwable? = null
) : RuntimeException(message, cause)

public class SystemaApiException(ex: Throwable) : SystemaRuntimeException("API error occurred", ex)
public class DataNotFoundException(ex: Throwable) : SystemaRuntimeException("Data not found", ex)

/**
 * SystemaListenerException is triggered if any exception happens in even processing pipeline throw listeners
 */
public class SystemaListenerException(
    ex: Throwable
) : SystemaRuntimeException("Exception occurred in listener", ex)

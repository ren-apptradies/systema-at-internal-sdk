package ai.systema.annotations

public expect annotation class JsFunction(val name: String)

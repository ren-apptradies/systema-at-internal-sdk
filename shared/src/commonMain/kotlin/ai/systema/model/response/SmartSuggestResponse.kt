package ai.systema.model.response

import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class SmartSuggestResponse(
    @SerialName(SystemaKeys.Query) val query: String,
    @SerialName(SystemaKeys.Results) val results: SmartSuggestResult,
    @SerialName(SystemaKeys.Time) val time: String,
    @SerialName(SystemaKeys.ResultId) val resultId: String,
)

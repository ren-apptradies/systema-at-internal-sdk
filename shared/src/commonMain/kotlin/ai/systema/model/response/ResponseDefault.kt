package ai.systema.model.response

import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class ResponseDefault<T>(
    @SerialName(SystemaKeys.Result) val result: T
)

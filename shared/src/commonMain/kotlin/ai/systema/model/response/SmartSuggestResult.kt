package ai.systema.model.response

import ai.systema.constants.SystemaKeys
import ai.systema.model.index.Product
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class SmartSuggestResult(
    @SerialName(SystemaKeys.Terms) val terms: List<String>,
    @SerialName(SystemaKeys.DidYouMean) val didYouMean: List<String>,
    @SerialName(SystemaKeys.Categories) val categories: List<String>,
    @SerialName(SystemaKeys.Products) val products: List<Product>,
)

package ai.systema.model.response

import ai.systema.constants.SystemaKeys
import ai.systema.model.index.Product
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
public data class SmartSearchResponse(
    @SerialName(SystemaKeys.Results) val results: List<Product>,
    @SerialName(SystemaKeys.Size) val size: Int,
    @SerialName(SystemaKeys.Total) val total: Int,
    @SerialName(SystemaKeys.Facets) val facets: Map<String, JsonElement>? = mapOf(), // Array of Double | Boolean| FacetInsight
    @SerialName(SystemaKeys.Time) val time: String,
    @SerialName(SystemaKeys.PaginationTimestamp) val paginationTimestamp: Long,
    @SerialName(SystemaKeys.ResultId) val resultId: String,
)

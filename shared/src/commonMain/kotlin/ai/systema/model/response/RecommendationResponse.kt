package ai.systema.model.response

import ai.systema.constants.SystemaKeys
import ai.systema.model.index.Product
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class RecommendationResponse(
    @SerialName(SystemaKeys.Results) val results: List<Product>,
    @SerialName(SystemaKeys.Size) val size: Int,
    @SerialName(SystemaKeys.Total) val total: Int? = null,
    @SerialName(SystemaKeys.PaginationTimestamp) val paginationTimestamp: Long,
    @SerialName(SystemaKeys.Time) val time: String,
    @SerialName(SystemaKeys.ResultId) val resultId: String,
)

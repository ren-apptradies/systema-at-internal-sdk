package ai.systema.model.config

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
public data class DynamicConfig(
    val endPoints: Map<String, String>?,
    val settings: Map<String, DynamicSettings>,
    val maintenance: ApiMaintenance?,
    val sessionId: String?,
    val expiresAt: Instant?,
    val createdAt: Instant,
    val version: String?,
)

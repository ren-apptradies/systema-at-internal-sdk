package ai.systema.model.config

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
public data class ApiMaintenance(
    val scheduled: Boolean,
    val message: String?,
    val startAt: Instant?,
    val endAt: Instant?
)

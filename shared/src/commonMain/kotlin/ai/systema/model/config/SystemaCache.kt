package ai.systema.model.config

import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class SystemaCache(
    @SerialName(SystemaKeys.Values) var values: MutableMap<String, String>,
)

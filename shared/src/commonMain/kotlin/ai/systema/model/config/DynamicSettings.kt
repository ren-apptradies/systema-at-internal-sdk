package ai.systema.model.config

import ai.systema.constants.SystemaKeys
import ai.systema.model.KeyValue
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class DynamicSettings(
    @SerialName(SystemaKeys.Extend) val extend: String,
    @SerialName(SystemaKeys.QueryParams) val queryParams: List<KeyValue>,
    @SerialName(SystemaKeys.Payload) val payload: PayloadTemplate,
)

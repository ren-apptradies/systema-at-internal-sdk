package ai.systema.model

import kotlinx.serialization.Serializable

@Serializable
public data class KeyValue(
    val name: String,
    val value: String,
)

package ai.systema.model

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

/**
 * Impersonated systema user data
 *
 * This serializable instance is created from internal "ClientUser" instance rather than returning "ClientUser"
 * to ensure "ClientUser" is only externally modifiable through the exposed APIs only.
 */

@Serializable
public data class SystemaUser(
    val fingerprint: String = "",
    val sessionId: String = "",
    val userIdHash: String = "",
    val userAgent: String = "",
    val sequence: Int = -1,
    val sessionCreatedAt: Instant? = null,
    val snapshotAt: Instant,
)

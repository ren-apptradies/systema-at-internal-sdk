package ai.systema.model.event

import kotlinx.datetime.Instant

public data class SystemaErrorEvent(
    val type: String?,
    val payload: Throwable,
    val createdAt: Instant
)

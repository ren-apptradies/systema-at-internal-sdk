package ai.systema.model.event

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
public data class SystemaLogEvent(
    val type: String?,
    val payload: String,
    val createdAt: Instant
)

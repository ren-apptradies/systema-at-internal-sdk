package ai.systema.model

import ai.systema.configuration.SystemaKVStore
import ai.systema.constants.SystemaConstants
import ai.systema.constants.SystemaKeys
import ai.systema.helper.internal.UUID
import ai.systema.helper.logging.SystemaLogger
import ai.systema.model.request.RequestUser
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.ExperimentalTime

public class ClientUser(
    public val fingerprint: String,
    public var sessionId: String = "",
    public var userIdHash: String = "",
    public val userAgent: String = "",
) {

    private var sequence: Int = 0
    private var sessionCreatedAt = Clock.System.now().toString()
    private val mutex = Mutex()

    public suspend fun nexSeq(): Int {
        return mutex.withLock {
            this.sequence++
        }
    }

    @OptIn(ExperimentalTime::class)
    public suspend fun refreshSession(kvStore: SystemaKVStore) {
        val sid = kvStore.read(SystemaKeys.SessionId)
        val createdAt = kvStore.read(SystemaKeys.SessionCreatedAt)
        SystemaLogger.debug("Cached sessionId: $sessionId, createdAt: $createdAt")

        if (createdAt != null && sid != null && createdAt != "null" && sid != "null") {
            val sessionCreatedAt = kotlinx.datetime.Instant.parse(createdAt)
            SystemaLogger.debug("Parsed sessionCreatedAt: $sessionCreatedAt")
            if (sessionCreatedAt > Clock.System.now().minus(SystemaConstants.MaxSessionDuration)) {
                SystemaLogger.debug("Using cached sessionId: $sessionId, createdAt: $createdAt, sequence: $sequence")
                this.sessionId = sid
                return
            }
        }

        // set to new
        SystemaLogger.debug("Generating new sessionId...")
        this.sessionId = UUID.randomUUID()
        this.sequence = 0
        this.sessionCreatedAt = Clock.System.now().toString()

        kvStore.write(SystemaKeys.SessionId, this.sessionId)
        kvStore.write(SystemaKeys.SessionCreatedAt, this.sessionCreatedAt)
        SystemaLogger.debug("Generated sessionId: ${this.sessionId}, sessionCreatedAt: ${this.sessionCreatedAt}, sequence: $sequence")
    }

    /**
     * RequestUser is for request payload
     */
    internal fun toRequestUser(): RequestUser {
        return RequestUser(fid = this.fingerprint, sid = this.sessionId, uid = this.userIdHash)
    }

    /**
     * Create a snapshot of the impersonated ClientUser as SystemaUser
     */
    internal fun getSnapshot(): SystemaUser {
        return SystemaUser(
            fingerprint = this.fingerprint,
            sessionId = this.sessionId,
            userIdHash = this.userIdHash,
            userAgent = this.userAgent,
            sequence = this.sequence,
            sessionCreatedAt = Instant.parse(this.sessionCreatedAt),
            snapshotAt = Clock.System.now()
        )
    }
}

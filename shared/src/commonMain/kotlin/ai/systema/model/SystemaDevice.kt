package ai.systema.model

import ai.systema.constants.SystemaConstants
import ai.systema.helper.internal.JsonNoDefaults
import io.ktor.http.encodeURLParameter
import io.ktor.util.InternalAPI
import io.ktor.util.encodeBase64
import kotlinx.serialization.Serializable

@Serializable
public data class SystemaDevice(
    val osVersion: String? = null,
    val model: String? = null,
    val deviceId: String? = null,
    val deviceName: String? = null,
    val fingerprint: String? = null,
    val release: String? = null,
    val product: String? = null,
    val brand: String? = null,
    val display: String? = null,
    val manufacturer: String? = null,
)

@OptIn(InternalAPI::class)
public fun SystemaDevice.toUserAgent(maxLen: Int = 1500): String {
    val deviceInfo = JsonNoDefaults.encodeToString(SystemaDevice.serializer(), this)
    val sep = "; "
    val b = StringBuilder()

    // SDK version
    b.append("SystemaSDK/${SystemaConstants.Version}")

    // common device details
    b.append(" (")
    this.osVersion?.let {
        b.append(this.osVersion.encodeURLParameter(), sep)
    }

    this.model?.let {
        b.append(this.model.encodeURLParameter(), sep)
    }

    this.deviceId?.let {
        b.append(this.deviceId.encodeURLParameter(), sep)
    }

    this.release?.let {
        b.append(this.release.encodeURLParameter(), sep)
    }

    b.append(") ")

    // add full DeviceInfo as JSON
    // add JSON and encode to Base64 to avoid andy URL encoding issue
    b.append("(SystemaDevice=", deviceInfo.encodeBase64(), ")")

    val userAgent = b.toString()
    if (userAgent.length > maxLen) {
        throw IllegalArgumentException("DeviceInfo is too long to be used as UserAgent")
    }

    return userAgent
}

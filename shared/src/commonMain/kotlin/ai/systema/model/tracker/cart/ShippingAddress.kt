package ai.systema.model.tracker.cart

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class ShippingAddress(
    val city: String,
    val state: String,
    @SerialName("postal-code") val postCode: String,
    val country: String,
)

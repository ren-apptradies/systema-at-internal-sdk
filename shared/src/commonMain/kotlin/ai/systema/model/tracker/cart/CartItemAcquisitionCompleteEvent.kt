package ai.systema.model.tracker.cart

import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.TrackEventDate
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class CartItemAcquisitionCompleteEvent(
    @SerialName("ClientId") public val clientId: String,
    @SerialName("Environment") public val environment: String,
    @SerialName("Fingerprint") public val fingerprint: String,
    @SerialName("SessionId") public val sessionId: String,
    @SerialName("UserAgent") public val userAgent: String,
    @SerialName("UserName") public val userName: String?,
    @SerialName("Sequence") public val sequence: Int,
    @SerialName("Type") public val type: TrackerEventType,
    @SerialName("Referer") public val referrer: String?,
    @SerialName("Url") public val url: String,
    @SerialName("EventDate") public val eventDate: TrackEventDate,
    @SerialName("Order") public val order: PurchaseOrder,
    @SerialName("Version") public val version: String,
) {
    init {
        require(type == TrackerEventType.Purchase) {
            "Invalid \"Type\". Expecting \"Purchase\""
        }
        require(order.items.isNotEmpty()) {
            "Invalid \"Order.Items\". Cannot be empty."
        }
        require(url.isNotBlank()) {
            "Invalid \"Url\". Cannot be blank."
        }
    }
}

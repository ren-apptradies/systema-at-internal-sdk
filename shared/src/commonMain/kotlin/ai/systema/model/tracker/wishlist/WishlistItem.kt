package ai.systema.model.tracker.wishlist

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class WishlistItem(
    @SerialName("item-id") val itemId: String,
)

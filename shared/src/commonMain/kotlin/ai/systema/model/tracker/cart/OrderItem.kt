package ai.systema.model.tracker.cart

import ai.systema.constants.Currency
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class OrderItem(
    @SerialName("item-id") val itemId: String,
    @SerialName("quantity") val quantity: Int,
    @SerialName("unit-amount") val unitCost: Double,
    @SerialName("unit-tax-amount") val unitTaxAmount: Double = 0.0,
    @SerialName("currency") val currency: String = Currency.AUD.value,
)

package ai.systema.model.tracker.view

import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class ItemContainer(
    @SerialName("Products") public val recItems: List<Map<String, String>>,
    @SerialName("ResultId") public val resultId: String,
)

public fun prepContainerItem(recId: String): Map<String, String> {
    return mapOf(
        SystemaKeys.TrackerRecId to recId
    )
}

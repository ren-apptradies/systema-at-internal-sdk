package ai.systema.model.tracker.cart

import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.TrackEventDate
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class CartItemAcquiredEvent(
    @SerialName("ClientId") public val clientId: String,
    @SerialName("Environment") public val environment: String,
    @SerialName("Fingerprint") public val fingerprint: String,
    @SerialName("SessionId") public val sessionId: String,
    @SerialName("UserAgent") public val userAgent: String,
    @SerialName("UserName") public val userName: String?,
    @SerialName("Sequence") public val sequence: Int,
    @SerialName("Type") public val type: TrackerEventType,
    @SerialName("ProductId") public val productId: String,
    @SerialName("Referrer") public val referrer: String?,
    @SerialName("Url") public val url: String,
    @SerialName("EventDate") public val eventDate: TrackEventDate,
    @SerialName("Items") public val items: List<CartItem>,
    @SerialName("Version") public val version: String,
) {
    init {
        require(type == TrackerEventType.AddToCart) {
            "Invalid \"Type\". Expecting \"AddToCart\""
        }
        require(productId.isNotBlank()) {
            "Invalid \"ProductId\". Cannot be blank."
        }
        require(url.isNotBlank()) {
            "Invalid \"Url\". Cannot be blank."
        }
    }
}

package ai.systema.model.tracker.view

import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.TrackEventDate
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class ItemClickEvent(
    @SerialName("ClientId") public val clientId: String,
    @SerialName("Environment") public val environment: String,
    @SerialName("Fingerprint") public val fingerprint: String,
    @SerialName("SessionId") public val sessionId: String,
    @SerialName("UserAgent") public val userAgent: String,
    @SerialName("UserName") public val userName: String?,
    @SerialName("Sequence") public val sequence: Int,
    @SerialName("ProductId") public val productId: String,
    @SerialName("RecId") public val recId: String,
    @SerialName("Type") public val type: TrackerEventType,
    @SerialName("Referer") public val referrer: String?,
    @SerialName("Url") public val url: String,
    @SerialName("EventDate") public val eventDate: TrackEventDate,
    @SerialName("Version") public val version: String,
) {
    init {
        require(type == TrackerEventType.ItemClicked) {
            "Invalid \"Type\". Expecting \"ItemClicked\""
        }
        require(productId.isNotBlank()) {
            "Invalid \"ProductId\". Cannot be blank."
        }
        require(recId.isNotBlank()) {
            "Invalid \"RecId\". Cannot be blank."
        }
        require(url.isNotBlank()) {
            "Invalid \"Url\". Cannot be blank."
        }
    }
}

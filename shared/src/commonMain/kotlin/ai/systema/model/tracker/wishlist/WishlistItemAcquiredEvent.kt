package ai.systema.model.tracker.wishlist

import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.TrackEventDate
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class WishlistItemAcquiredEvent(
    @SerialName("ClientId") public val clientId: String,
    @SerialName("Environment") public val environment: String,
    @SerialName("Fingerprint") public val fingerprint: String,
    @SerialName("SessionId") public val sessionId: String,
    @SerialName("UserAgent") public val userAgent: String,
    @SerialName("UserName") public val userName: String?,
    @SerialName("Sequence") public val sequence: Int,
    @SerialName("Type") public val type: TrackerEventType,
    @SerialName("ProductId") public val productId: String,
    @SerialName("Referer") public val referrer: String?,
    @SerialName("Url") public val url: String,
    @SerialName("EventDate") public val eventDate: TrackEventDate,
    @SerialName("Items") public val items: List<WishlistItem>,
    @SerialName("Version") public val version: String,
) {
    init {
        require(type == TrackerEventType.AddToWishlist) {
            "Invalid \"Type\". Expecting \"AddToWishlist\""
        }
        require(productId.isNotBlank()) {
            "Invalid \"ProductId\". Cannot be blank."
        }
        require(url.isNotBlank()) {
            "Invalid \"Url\". Cannot be blank."
        }
    }
}

package ai.systema.model.tracker

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class TrackEventDate(
    @SerialName("LocalDate") val localDate: String,
    @SerialName("Timezone") val timeZone: String,
    @SerialName("UtcDate") val utcDate: String,
)

public val CurTrackEventDate: TrackEventDate
    get() {
        val currentMoment: Instant = Clock.System.now()
        return TrackEventDate(
            localDate = currentMoment.toLocalDateTime(TimeZone.currentSystemDefault()).toString(),
            timeZone = TimeZone.currentSystemDefault().id,
            utcDate = currentMoment.toLocalDateTime(TimeZone.UTC).toString()
        )
    }

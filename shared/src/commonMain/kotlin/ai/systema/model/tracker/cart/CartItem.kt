package ai.systema.model.tracker.cart

import ai.systema.constants.Currency
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class CartItem(
    @SerialName("item-id") val itemId: String,
    @SerialName("quantity") val quantity: Int,
    // As per - https://systema-ai.atlassian.net/browse/MOBILE-46, `AddToCart` event type needs price & currency; while,
    // `RemoveFromCart` does not need it.

    // To avoid creating a duplicate `CartItems` DTO, including price & currency as nullable properties here.
    // kotlinx.serialization has @Optional decorated against all non-default properties.
    // i.e. it will ensure to NOT include price & currency in the serialized payload if they are "null".
    // e.g. Json.encodeToString(CartItem(itemId = "cart_item_id_1", quantity = 1)) will result in:
    // {
    // 		"item-id": "cart_item_id_1",
    // 		"quantity": 1
    // 	}
    @SerialName("price") val price: Double? = null,
    @SerialName("currency") val currency: Currency? = null,
)

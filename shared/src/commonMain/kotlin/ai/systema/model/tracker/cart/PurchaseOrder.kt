package ai.systema.model.tracker.cart

import ai.systema.constants.Currency
import ai.systema.helper.enumContains
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class PurchaseOrder(
    @SerialName("order-id") val orderId: String,
    @SerialName("charged-amount") val chargedAmount: Double,
    @SerialName("total-amount") val totalAmount: Double,
    @SerialName("tax-amount") val taxAmount: Double,
    @SerialName("shipping-amount") val shippingAmount: Double,
    @SerialName("discount-amount") val discountAmount: Double? = null,
    @SerialName("discount-codes") val discountCodes: String? = "",
    @SerialName("currency") val currency: String? = null,
    @SerialName("shipping-address") val shippingAddress: ShippingAddress? = null,
    @SerialName("items") val items: List<OrderItem>,
) {
    init {

        require(enumContains<Currency>(currency.toString())) {
            "Invalid/Unsupported \"Currency\"."
        }

        require(shippingAddress != null) {
            "Invalid \"shipping-address\". Cannot be null."
        }
    }
}

package ai.systema.model.internal

internal interface Basic<T> {

    val basic: T
}

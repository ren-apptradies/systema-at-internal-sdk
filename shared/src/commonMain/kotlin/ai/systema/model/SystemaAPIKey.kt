package ai.systema.model

import ai.systema.constants.SystemaConstants
import ai.systema.helper.toAPIKey
import ai.systema.model.internal.Basic
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(SystemaAPIKey.Companion::class)
public data class SystemaAPIKey(override val basic: String) : Basic<String> {

    init {
        if (basic.length > SystemaConstants.MaxKeyLen) throw IllegalArgumentException("APIKey is too long")
    }

    override fun toString(): String {
        return basic
    }

    @Serializer(SystemaAPIKey::class)
    @OptIn(ExperimentalSerializationApi::class)
    public companion object : KSerializer<SystemaAPIKey> {

        private val serializer = String.serializer()

        override fun serialize(encoder: Encoder, value: SystemaAPIKey) {
            serializer.serialize(encoder, value.basic)
        }

        override fun deserialize(decoder: Decoder): SystemaAPIKey {
            return serializer.deserialize(decoder).toAPIKey()
        }

        override val descriptor: SerialDescriptor
            get() = TODO("Not yet implemented")
    }
}

package ai.systema.model.request

import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class RequestUser(
    @SerialName(SystemaKeys.FID) val fid: String? = null,
    @SerialName(SystemaKeys.SID) val sid: String,
    @SerialName(SystemaKeys.UID) val uid: String? = null,
)

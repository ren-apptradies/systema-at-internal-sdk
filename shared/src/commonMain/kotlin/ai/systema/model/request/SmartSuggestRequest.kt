package ai.systema.model.request

import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
public data class SmartSuggestRequest(
    @SerialName(SystemaKeys.Environment) var environment: String? = null,
    @SerialName(SystemaKeys.UserId) var user: RequestUser? = null,
    @SerialName(SystemaKeys.Query) val query: String,
    @SerialName(SystemaKeys.Meta) val meta: Map<String, JsonElement>? = null, // this is for future customized extension
)

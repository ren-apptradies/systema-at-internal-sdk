package ai.systema.model.request

import ai.systema.constants.Languages
import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
public data class CartRecommendationRequest(
    @SerialName(SystemaKeys.Environment) var environment: String? = null,
    @SerialName(SystemaKeys.UserId) var user: RequestUser? = null,
    @SerialName(SystemaKeys.Id) val id: List<String>? = null,
    @SerialName(SystemaKeys.Category) val category: List<String>? = null,
    @SerialName(SystemaKeys.Size) val size: Int? = 10,
    @SerialName(SystemaKeys.Start) val start: Int? = 0,
    @SerialName(SystemaKeys.Filter) val filter: Filter? = null,
    @SerialName(SystemaKeys.Exclusion) val exclusion: Filter? = null,
    @SerialName(SystemaKeys.PaginationTimestamp) val paginationTimestamp: Long? = null,
    @SerialName(SystemaKeys.Language) val language: String? = Languages.English,
    @SerialName(SystemaKeys.Display) val display: List<String>? = null,
    @SerialName(SystemaKeys.DisplayVariants) val displayVariants: List<String>? = null,
    @SerialName(SystemaKeys.Meta) val meta: Map<String, JsonElement>? = null, // this is for future customized extension
)

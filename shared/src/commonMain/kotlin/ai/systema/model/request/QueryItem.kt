package ai.systema.model.request

import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class QueryItem(
    @SerialName(SystemaKeys.Id) val id: String,
    @SerialName(SystemaKeys.Type) val type: String,
)

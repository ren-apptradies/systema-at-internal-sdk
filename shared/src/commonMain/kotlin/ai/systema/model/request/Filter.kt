package ai.systema.model.request

import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class Filter(
    @SerialName(SystemaKeys.Id) val id: List<String>? = listOf(),
    @SerialName(SystemaKeys.Text) val text: String? = "",
    @SerialName(SystemaKeys.Brand) val brand: String? = "",
    @SerialName(SystemaKeys.Category) val category: List<String>? = listOf(),
    @SerialName(SystemaKeys.Price) val price: Map<String, List<Double>>? = null,
    @SerialName(SystemaKeys.SalePrice) val salePrice: Map<String, List<Double>>? = mapOf(),
    @SerialName(SystemaKeys.OnSaleOnly) val onSaleOnly: Boolean? = false,
    @SerialName(SystemaKeys.InStockOnly) val inStockOnly: Boolean? = false,
    @SerialName(SystemaKeys.Tags) val tags: List<String>? = listOf(),
    @SerialName(SystemaKeys.Meta) val meta: Map<String, String>? = null, // this is for future customized extension
)

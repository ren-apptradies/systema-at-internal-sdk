package ai.systema.model.request

import ai.systema.constants.Languages
import ai.systema.constants.SystemaKeys
import ai.systema.enums.ScoreType
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
public data class SmartSearchRequest(
    @SerialName(SystemaKeys.Environment) var environment: String? = null,
    @SerialName(SystemaKeys.UserId) var user: RequestUser? = null,
    @SerialName(SystemaKeys.Query) val query: List<QueryItem>? = null,
    @SerialName(SystemaKeys.Filter) val filter: Filter? = null,
    @SerialName(SystemaKeys.Exclusion) val exclusion: Filter? = null,
    @SerialName(SystemaKeys.Size) val size: Int? = 10,
    @SerialName(SystemaKeys.Language) val language: String? = Languages.English,
    @SerialName(SystemaKeys.Start) val start: Int? = 0,
    @SerialName(SystemaKeys.FacetsSize) val facetSize: Int? = 10,
    @SerialName(SystemaKeys.Facets) val facets: List<String>? = null,
    @SerialName(SystemaKeys.Score) val score: String? = ScoreType.Relevance.value,
    @SerialName(SystemaKeys.Meta) val meta: Map<String, JsonElement>? = null, // this is for future customized extension
)

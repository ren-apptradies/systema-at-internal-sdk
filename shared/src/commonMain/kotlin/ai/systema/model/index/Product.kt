package ai.systema.model.index

import ai.systema.constants.SystemaKeys
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
public data class Product(
    @SerialName(SystemaKeys.Id) val id: String,
    @SerialName(SystemaKeys.Brand) val brand: String? = null,
    @SerialName(SystemaKeys.Currency) val currency: String? = null,
    @SerialName(SystemaKeys.Description) val description: String? = null,
    @SerialName(SystemaKeys.Image) val image: String,
    @SerialName(SystemaKeys.Images) val images: List<String>? = null,
    @SerialName(SystemaKeys.InStock) val inStock: Boolean? = null,
    @SerialName(SystemaKeys.ItemGroupId) val itemGroupId: String? = null,
    @SerialName(SystemaKeys.Link) val link: String,
    @SerialName(SystemaKeys.Price) val price: Double? = null,
    @SerialName(SystemaKeys.Promotion) val promotion: String? = null,
    @SerialName(SystemaKeys.SalePrice) val salePrice: Float? = null,
    @SerialName(SystemaKeys.Title) val title: String,
    @SerialName(SystemaKeys.RecId) val recId: String,
    @SerialName(SystemaKeys.Tags) val tags: List<String>? = null,
    @SerialName(SystemaKeys.Attributes) val attributes: Map<String, JsonElement>? = null,
)

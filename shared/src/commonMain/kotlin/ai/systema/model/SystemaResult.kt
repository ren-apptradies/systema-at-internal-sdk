package ai.systema.model

@Suppress("UNCHECKED_CAST")
public class SystemaResult<out T>(private val value: Any) {

    public val isSuccessful: Boolean get() = value !is Exception

    public val isError: Boolean get() = value is Exception

    public fun value(): T = value as T

    public fun error(): Exception = value as Exception

    public companion object {
        public fun <T : Any> success(value: T): SystemaResult<T> = SystemaResult(value)

        public fun <T> error(error: Exception): SystemaResult<T> = SystemaResult(error)

        public fun <R, T> error(result: SystemaResult<T>): SystemaResult<R> = SystemaResult(result.error())
    }
}
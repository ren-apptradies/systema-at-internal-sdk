package ai.systema.model

import ai.systema.constants.SystemaConstants
import ai.systema.model.internal.Basic

public data class SystemaClientID(override val basic: String) : Basic<String> {

    init {
        if (basic.isBlank()) throw IllegalArgumentException("Systema ClientID cannot be empty")
        if (basic.length > SystemaConstants.MaxKeyLen) throw IllegalArgumentException("Systema ClientID is too long")
    }

    override fun toString(): String {
        return basic
    }
}

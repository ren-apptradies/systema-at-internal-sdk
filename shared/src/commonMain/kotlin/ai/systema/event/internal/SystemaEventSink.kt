package ai.systema.event.internal

import ai.systema.helper.logging.SystemaLogger
import ai.systema.model.event.SystemaErrorEvent
import ai.systema.model.event.SystemaLogEvent

/**
 * Default event sink to absorb the unhandled events
 */
internal object SystemaEventSink {
    fun onSystemaLog(ev: SystemaLogEvent) {
        SystemaLogger.debug("Systema Log - ${ev.type}: ${ev.payload}")
    }

    fun onSystemaError(ev: SystemaErrorEvent) {
        // TODO we can send this to systema for triage and debugging
        SystemaLogger.debug("Systema Error - ${ev.type}:", ev.payload)
    }
}

package ai.systema.event

import ai.systema.event.internal.SystemaEventSink
import ai.systema.helper.internal.JsonNoDefaults
import ai.systema.model.event.SystemaErrorEvent
import ai.systema.model.event.SystemaLogEvent
import kotlinx.datetime.Clock
import kotlinx.serialization.SerializationStrategy

internal interface EventTriggerer {
    fun <T> triggerLogEvent(
        serializer: SerializationStrategy<T>,
        value: T,
        onLog: ((SystemaLogEvent) -> Unit) = { ev -> SystemaEventSink.onSystemaLog(ev) },
    ) {
        val ev = SystemaLogEvent(
            type = serializer::class.simpleName,
            payload = JsonNoDefaults.encodeToString(serializer, value),
            createdAt = Clock.System.now()
        )

        onLog.invoke(ev)
    }

    fun triggerErrorEvent(
        ex: Throwable,
        onError: ((SystemaErrorEvent) -> Unit) = { ev -> SystemaEventSink.onSystemaError(ev) },
    ) {
        val ev = SystemaErrorEvent(
            type = ex::class.simpleName,
            payload = ex,
            createdAt = Clock.System.now()
        )
        onError.invoke(ev)
    }
}

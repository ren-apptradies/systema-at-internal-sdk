package ai.systema.event

import ai.systema.exception.SystemaListenerException
import ai.systema.helper.logging.SystemaLogger
import io.ktor.client.statement.HttpResponse

/**
 * Reusable singleton to invoke registered callbacks for different APIs
 *
 * This is to allow us to have a centralized place to invoke callbacks so that
 * we may enrich with any other logging or modifications, for example, as in MOBILE-106,
 * we could call a special backend API to log SDK exceptions for future debugging and
 * customer support.
 */
internal object CallbackInvoker {
    /**
     * Success response from tracker API
     */
    fun trackerSuccess(resp: HttpResponse, callback: (Result<HttpResponse>) -> Unit) {
        SystemaLogger.debug("Callback tracker success status: ${resp.status}")
        callback(Result.success(resp))
    }

    /**
     * Error from tracker API
     */
    fun trackerFailure(ex: Throwable, callback: (Result<HttpResponse>) -> Unit) {
        SystemaLogger.debug("Callback tracker error: ${ex.message}")
        callback(Result.failure(SystemaListenerException(ex)))
    }
}

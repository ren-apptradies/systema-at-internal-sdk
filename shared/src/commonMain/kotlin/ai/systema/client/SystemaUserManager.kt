package ai.systema.client

import ai.systema.client.internal.SystemaUserManagerImpl
import ai.systema.connection.internal.Connector
import ai.systema.model.SystemaUser

/**
 * Interfaces to get and set impersonated user information
 */
public interface SystemaUserManager {

    /**
     * Get a snapshot of the impersonated user details
     */
    public suspend fun getUserSnapshot(): SystemaUser

    public suspend fun setUserIdHash(uid: String)

    public suspend fun clearUserIdHash()
}

internal fun SystemaUserManager(
    connector: Connector
): SystemaUserManager = SystemaUserManagerImpl(
    connector,
)

package ai.systema.client

import ai.systema.client.internal.TrackerImpl
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointTracker
import ai.systema.model.SystemaResult
import ai.systema.model.tracker.cart.CartItem
import ai.systema.model.tracker.cart.PurchaseOrder
import ai.systema.model.tracker.view.ItemContainer
import ai.systema.model.tracker.wishlist.WishlistItem
import io.ktor.client.statement.HttpResponse

public interface Tracker {
    /**
     * Track container shown event
     *
     * @param productId Optional as container shown event can also be necessary in NON-PDP pages/views. In PDP, this is the product ID.
     * @param containers List of ItemContainer where each container includes Products & ResultId.
     * @param url The URL associated with the container. This makes sense in browser based env, in mobiles, this will be a made up string that validates OK for a URL.
     * @param referrer The URL from where user ended up into container in scope. This is again valid in a browser based env, will be simulated string in Mobile.
     * @return HTTP response from ContainerShown tracker event.
     */
    public suspend fun trackContainerShown(
        productId: String? = null,
        containers: List<ItemContainer>,
        url: String? = "",
        referrer: String? = ""
    ): SystemaResult<HttpResponse>

    public fun trackContainerShown(
        productId: String? = null,
        containers: List<ItemContainer>,
        url: String? = "",
        referrer: String? = "",
        result: (SystemaResult<HttpResponse>) -> Unit
    )

    /**
     * Track page view event
     *
     * @param productId Product ID in displayed page/view
     * @param url URL of the page/view - in Mobile, a simulated string.
     * @param recId Recommendation ID associated with the products in displayed page/view
     * @param referrer The URL from where user ended up into container in scope. This is again valid in a browser based env, will be simulated string in Mobile.
     * @return HTTP response from tracker, for page view event.
     */
    public suspend fun trackPageViewed(
        productId: String,
        url: String = "",
        recId: String = "",
        referrer: String = ""
    ): SystemaResult<HttpResponse>

    public fun trackPageViewed(
        productId: String,
        url: String = "",
        recId: String = "",
        referrer: String = "",
        result: (SystemaResult<HttpResponse>) -> Unit
    )

    /**
     * Page View for NON-PDP pages.
     */
    public suspend fun trackPageViewed(
        url: String = "",
        recId: String = "",
        referrer: String = ""
    ): SystemaResult<HttpResponse>

    public fun trackPageViewed(
        url: String = "",
        recId: String = "",
        referrer: String = "",
        result: (SystemaResult<HttpResponse>) -> Unit
    )

    /**
     * Track Item Clicked event.
     *
     * @param productId Product ID of the Item that was clicked.
     * @param url URL of the page/view - in Mobile, a simulated string.
     * @param recId Recommendation ID associated with the products in displayed page/view.
     * @param referrer The URL from where user ended up into container in scope. This is again valid in a browser based env, will be simulated string in Mobile.
     * @return HTTP response from tracker for Item Clicked event.
     */
    public suspend fun trackItemClicked(
        productId: String,
        url: String = "",
        recId: String = "",
        referrer: String = ""
    ): SystemaResult<HttpResponse>

    public fun trackItemClicked(
        productId: String,
        url: String = "",
        recId: String = "",
        referrer: String = "",
        result: (SystemaResult<HttpResponse>) -> Unit
    )

    /**
     * Track Item Acquired event. Triggered when a "tracked" item makes its way into the cart.
     * @param productId Product ID of the item that was checked into the cart.
     * @param items List of CartItems that made its way into the cart.
     * @param url URL of the page/view where product resided before it was added to cart - in Mobile, a simulated string.
     * @return HTTP response from tracker for Item Acquired event.
     */

    public suspend fun trackItemAcquired(
        productId: String,
        items: List<CartItem>,
        url: String,
        referrer: String? = ""
    ): SystemaResult<HttpResponse>

    public fun trackItemAcquired(
        productId: String,
        items: List<CartItem>,
        url: String,
        referrer: String? = "",
        result: (SystemaResult<HttpResponse>) -> Unit
    )

    /**
     * Track Item Relinquished event. Triggered when a "tracked" item is removed from the cart.
     * @param productId Product ID of the item that was removed from the cart.
     * @param items List of CartItems that were removed from the cart.
     * @param url URL of the page/view while items were removed from the cart - in Mobile, a simulated string.
     * @return HTTP response from tracker for Item Relinquished event.
     */

    public suspend fun trackItemRelinquished(
        productId: String,
        item: CartItem,
        url: String,
        referrer: String? = ""
    ): SystemaResult<HttpResponse>

    public fun trackItemRelinquished(
        productId: String,
        item: CartItem,
        url: String,
        referrer: String? = "",
        result: (SystemaResult<HttpResponse>) -> Unit
    )

    /**
     * Track Item Acquisition Complete event. Triggered when a "tracked" item from cart was purchased.
     * @param productId Product ID of the item that was purchased from the cart.
     * @param items List of CartItems that were purchased.
     * @param url URL of the page/view while purchase was made - in Mobile, a simulated string.
     * @return HTTP response from tracker for Item Acquisition Complete event.
     */

    public suspend fun trackAcquisitionComplete(
        order: PurchaseOrder,
        url: String,
        referrer: String? = ""
    ): SystemaResult<HttpResponse>

    public suspend fun trackAcquisitionComplete(
        order: PurchaseOrder,
        url: String,
        referrer: String? = "",
        result: (SystemaResult<HttpResponse>) -> Unit
    )

    /**
     * Track Wishlist Acquired event. Triggered when a an item makes its way into the wishlist.
     * @param productId Product ID of the item that was added to the wishlist.
     * @param items List of items that were added to the wishlist.
     * @param url URL of the page/view while items were added to the wishlist - in Mobile, a simulated string.
     * @return HTTP response from tracker for Wishlist Acquired event.
     */
    public suspend fun trackWishlistAcquired(
        productId: String,
        items: List<WishlistItem>,
        url: String,
        referrer: String? = ""
    ): SystemaResult<HttpResponse>

    public fun trackWishlistAcquired(
        productId: String,
        items: List<WishlistItem>,
        url: String,
        referrer: String? = "",
        result: (SystemaResult<HttpResponse>) -> Unit
    )

    /**
     * Track Wishlist Relinquished event. Triggered when a an item makes its way out of the wishlist.
     * @param productId Product ID of the item that was removed from the wishlist.
     * @param items List of items that were removed from the wishlist.
     * @param url URL of the page/view while items were removed from the wishlist - in Mobile, a simulated string.
     * @return HTTP response from tracker for Wishlist Relinquished event.
     */
    public suspend fun trackWishlistRelinquished(
        productId: String,
        item: WishlistItem,
        url: String,
        referrer: String? = ""
    ): SystemaResult<HttpResponse>

    public fun trackWishlistRelinquished(
        productId: String,
        item: WishlistItem,
        url: String,
        referrer: String? = "",
        result: (SystemaResult<HttpResponse>) -> Unit
    )
}

internal fun Tracker(
    connector: Connector
): Tracker = TrackerImpl(
    connector,
    EndpointTracker(connector)
)

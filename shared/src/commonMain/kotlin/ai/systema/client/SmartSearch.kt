package ai.systema.client

import ai.systema.client.internal.SmartSearchImpl
import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointSmartSearch
import ai.systema.model.SystemaResult
import ai.systema.model.request.SmartSearchRequest
import ai.systema.model.response.SmartSearchResponse

public interface SmartSearch {

    /**
     * Smart search API (Coroutine)
     *
     * @param payload smart search request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun smartSearch(
        payload: SmartSearchRequest,
        requestOptions: RequestOptions? = null
    ): SystemaResult<SmartSearchResponse>

    /**
     * Smart search API (Non-coroutine)
     *
     * @param payload smart search request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<SmartSearchResponse> object.
     */
    public fun smartSearch(
        payload: SmartSearchRequest,
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<SmartSearchResponse>) -> Unit
    )
}

internal fun SmartSearch(
    connector: Connector
): SmartSearch = SmartSearchImpl(
    connector,
    EndpointSmartSearch(connector)
)

package ai.systema.client

import ai.systema.client.internal.SmartSuggestImpl
import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointSmartSuggest
import ai.systema.model.SystemaResult
import ai.systema.model.request.SmartSuggestRequest
import ai.systema.model.response.SmartSuggestResponse

public interface SmartSuggest {

    /**
     * Smart suggest API (Coroutine)
     *
     * @param payload smart suggest request.
     * @param requestOptions additional request options.
     * @return the smart suggest response
     */
    public suspend fun smartSuggest(
        payload: SmartSuggestRequest,
        requestOptions: RequestOptions? = null
    ): SystemaResult<SmartSuggestResponse>

    /**
     * Smart suggest API (Non-coroutine)
     *
     * @param payload smart suggest request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<SmartSuggestResponse> object.
     */
    public fun smartSuggest(
        payload: SmartSuggestRequest,
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<SmartSuggestResponse>) -> Unit
    )
}

internal fun SmartSuggest(
    connector: Connector
): SmartSuggest = SmartSuggestImpl(
    connector,
    EndpointSmartSuggest(connector)
)

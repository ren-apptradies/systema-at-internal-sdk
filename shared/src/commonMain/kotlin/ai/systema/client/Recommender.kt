package ai.systema.client

import ai.systema.client.internal.RecommenderImpl
import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointRecommendation
import ai.systema.model.SystemaResult
import ai.systema.model.request.CartRecommendationRequest
import ai.systema.model.request.RecommendationRequest
import ai.systema.model.response.RecommendationResponse

/**
 * Recommender API
 */
public interface Recommender {

    /**
     * Get related API (Coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun getRelated(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): SystemaResult<RecommendationResponse>

    /**
     * Get related API (Non-coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<RecommendationResponse> object.
     */
    public fun getRelated(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    )

    /**
     * Get complementary API (Coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun getComplementary(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): SystemaResult<RecommendationResponse>

    /**
     * Get complementary API (Non-coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<RecommendationResponse> object.
     */
    public fun getComplementary(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    )

    /**
     * Get similar API (Coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun getSimilar(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): SystemaResult<RecommendationResponse>

    /**
     * Get similar API (Non-coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<RecommendationResponse> object.
     */
    public fun getSimilar(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    )

    /**
     * Get category trending API (Coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun getCategoryTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): SystemaResult<RecommendationResponse>

    /**
     * Get category trending API (Non-coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<RecommendationResponse> object.
     */
    public fun getCategoryTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    )

    /**
     * Get category popular API (Coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun getCategoryPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): SystemaResult<RecommendationResponse>

    /**
     * Get category trending API (Non-coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<RecommendationResponse> object.
     */
    public fun getCategoryPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    )

    /**
     * Get trending API (Coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun getTrending(
        payload: RecommendationRequest = RecommendationRequest(),
        requestOptions: RequestOptions? = null,
    ): SystemaResult<RecommendationResponse>

    /**
     * Get trending API (Non-coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<RecommendationResponse> object.
     */
    public fun getTrending(
        payload: RecommendationRequest = RecommendationRequest(),
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    )

    /**
     * Get popular API (Coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun getPopular(
        payload: RecommendationRequest = RecommendationRequest(),
        requestOptions: RequestOptions? = null
    ): SystemaResult<RecommendationResponse>

    /**
     * Get popular API (Non-coroutine)
     *
     * @param payload recommendation request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<RecommendationResponse> object.
     */
    public fun getPopular(
        payload: RecommendationRequest = RecommendationRequest(),
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    )

    /**
     * Get cart related API (Coroutine)
     *
     * @param payload cart recommendation request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun getCartRelated(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): SystemaResult<RecommendationResponse>

    /**
     * Get cart related API (Non-coroutine)
     *
     * @param payload cart recommendation request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<RecommendationResponse> object.
     */
    public fun getCartRelated(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    )

    /**
     * Get cart complementary API (Coroutine)
     *
     * @param payload cart recommendation request.
     * @param requestOptions additional request options.
     * @return the recommendation response
     */
    public suspend fun getCartComplementary(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): SystemaResult<RecommendationResponse>

    /**
     * Get cart complementary API (Non-coroutine)
     *
     * @param payload cart recommendation request.
     * @param requestOptions additional request options.
     * @param result function that returns a Result<RecommendationResponse> object.
     */
    public fun getCartComplementary(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions? = null,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    )
}

internal fun Recommender(
    connector: Connector
): Recommender = RecommenderImpl(
    connector,
    EndpointRecommendation(connector)
)

package ai.systema.client

import ai.systema.client.internal.SystemaImpl
import ai.systema.configuration.Configuration
import ai.systema.configuration.Credentials
import ai.systema.configuration.SystemaKVStore
import ai.systema.connection.internal.Connector
import ai.systema.enums.EndpointType
import ai.systema.enums.EnvironmentType
import ai.systema.helper.SystemaDeviceManager
import ai.systema.helper.logging.SystemaLogLevel
import ai.systema.helper.toAPIKey
import ai.systema.helper.toClientID
import io.ktor.http.Url

public interface SystemaAI :
    Configuration,
    Credentials,
    Tracker,
    Recommender,
    SmartSearch,
    SmartSuggest,
    SystemaUserManager {

    // Some meta configuration
    // Each platform needs to cast the Any type as required
    public val meta: Map<String, Any>

    public suspend fun initialize()
}

internal fun SystemaAI(
    clientID: String,
    apiKey: String = "NOT_REQUIRED_YET",
    environment: EnvironmentType = EnvironmentType.DEV,
    logLevel: SystemaLogLevel = SystemaLogLevel.INFO,
    proxyUrls: Map<EndpointType, String> = mapOf(),
    meta: Map<String, Any> = mapOf(),
    kvStore: SystemaKVStore,
    deviceManager: SystemaDeviceManager,
): SystemaAI = SystemaImpl(
    Connector(
        Configuration(
            credentials = Credentials(
                clientID = clientID.toClientID(),
                apiKey = apiKey.toAPIKey(),
                environment = environment,
                proxyUrls = proxyUrls.entries.associate { it.key to Url(it.value) }
            ),
            logLevel = logLevel,
//            engine = httpClientEngine,
            kvStore = kvStore,
            deviceManager = deviceManager,
        ),
    ),
    meta,
)

package ai.systema.client.internal

import ai.systema.client.Tracker
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointTracker
import ai.systema.helper.TrackEventBuilder
import ai.systema.helper.internal.ApplicationDispatcher
import ai.systema.model.SystemaResult
import ai.systema.model.tracker.cart.CartItem
import ai.systema.model.tracker.cart.PurchaseOrder
import ai.systema.model.tracker.view.ItemContainer
import ai.systema.model.tracker.wishlist.WishlistItem
import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

internal class TrackerImpl(
    private val connector: Connector,
    private val endpoint: EndpointTracker
) : Tracker {

    override suspend fun trackContainerShown(
        productId: String?,
        containers: List<ItemContainer>,
        url: String?,
        referrer: String?
    ): SystemaResult<HttpResponse> {
        return try {
            val event = TrackEventBuilder(connector)
                .setContainers(containers)
                .setReferrer(referrer)
                .setUrl(url)
                .buildContainerShownEvent()
            val resp = endpoint.sendContainerShownEvent(event)
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun trackContainerShown(
        productId: String?,
        containers: List<ItemContainer>,
        url: String?,
        referrer: String?,
        result: (SystemaResult<HttpResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = trackContainerShown(productId, containers, url, referrer)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun trackItemClicked(
        productId: String,
        url: String,
        recId: String,
        referrer: String,
    ): SystemaResult<HttpResponse> {
        return try {
            val event = TrackEventBuilder(connector)
                .setProductId(productId)
                .setRecId(recId)
                .setReferrer(referrer)
                .setUrl(url)
                .buildItemClickEvent()

            val resp = endpoint.sendItemClickEvent(event)
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun trackItemClicked(
        productId: String,
        url: String,
        recId: String,
        referrer: String,
        result: (SystemaResult<HttpResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = trackItemClicked(productId, url, recId, referrer)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun trackPageViewed(
        productId: String,
        url: String,
        recId: String,
        referrer: String,
    ): SystemaResult<HttpResponse> {
        return try {
            val event = TrackEventBuilder(connector)
                .setProductId(productId)
                .setRecId(recId)
                .setReferrer(referrer)
                .setUrl(url)
                .buildPageViewEvent()

            val resp = endpoint.sendPageViewEvent(event)
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun trackPageViewed(
        productId: String,
        url: String,
        recId: String,
        referrer: String,
        result: (SystemaResult<HttpResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = trackPageViewed(productId, url, recId, referrer)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun trackPageViewed(
        url: String,
        recId: String,
        referrer: String,
    ): SystemaResult<HttpResponse> {
        return try {
            val event = TrackEventBuilder(connector)
                .setRecId(recId)
                .setReferrer(referrer)
                .setUrl(url)
                .buildPageViewEvent()

            val resp = endpoint.sendPageViewEvent(event)
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun trackPageViewed(
        url: String,
        recId: String,
        referrer: String,
        result: (SystemaResult<HttpResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = trackPageViewed(url, recId, referrer)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun trackItemAcquired(
        productId: String,
        items: List<CartItem>,
        url: String,
        referrer: String?
    ): SystemaResult<HttpResponse> {
        return try {
            val event = TrackEventBuilder(connector)
                .setProductId(productId)
                .setCartItems(items)
                .setReferrer(referrer)
                .setUrl(url)
                .buildCartItemAcquired()

            val resp = endpoint.sendCartItemAcquiredEvent(event)
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun trackItemAcquired(
        productId: String,
        items: List<CartItem>,
        url: String,
        referrer: String?,
        result: (SystemaResult<HttpResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = trackItemAcquired(productId, items, url, referrer)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun trackItemRelinquished(
        productId: String,
        item: CartItem,
        url: String,
        referrer: String?
    ): SystemaResult<HttpResponse> {
        return try {
            val event = TrackEventBuilder(connector)
                .setProductId(productId)
                .setCartItem(item)
                .setReferrer(referrer)
                .setUrl(url)
                .buildCartItemRelinquished()

            val resp = endpoint.sendCartItemRelinquishedEvent(event)
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun trackItemRelinquished(
        productId: String,
        item: CartItem,
        url: String,
        referrer: String?,
        result: (SystemaResult<HttpResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = trackItemRelinquished(productId, item, url, referrer)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun trackAcquisitionComplete(
        order: PurchaseOrder,
        url: String,
        referrer: String?
    ): SystemaResult<HttpResponse> {
        return try {
            val event = TrackEventBuilder(connector)
                .setPurchaseOrder(order)
                .setReferrer(referrer)
                .setUrl(url)
                .buildCartItemAcquisitionComplete()

            val resp = endpoint.sendCartItemAcquisitionCompleteEvent(event)
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override suspend fun trackAcquisitionComplete(
        order: PurchaseOrder,
        url: String,
        referrer: String?,
        result: (SystemaResult<HttpResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = trackAcquisitionComplete(order, url, referrer)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun trackWishlistAcquired(
        productId: String,
        items: List<WishlistItem>,
        url: String,
        referrer: String?
    ): SystemaResult<HttpResponse> {
        return try {
            val event = TrackEventBuilder(connector)
                .setProductId(productId)
                .setWishlistItems(items)
                .setReferrer(referrer)
                .setUrl(url)
                .buildWishlistItemAcquired()

            val resp = endpoint.sendWishlistItemAcquiredEvent(event)
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun trackWishlistAcquired(
        productId: String,
        items: List<WishlistItem>,
        url: String,
        referrer: String?,
        result: (SystemaResult<HttpResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = trackWishlistAcquired(productId, items, url, referrer)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun trackWishlistRelinquished(
        productId: String,
        item: WishlistItem,
        url: String,
        referrer: String?
    ): SystemaResult<HttpResponse> {
        return try {
            val event = TrackEventBuilder(connector)
                .setProductId(productId)
                .setWishlistItem(item)
                .setReferrer(referrer)
                .setUrl(url)
                .buildWishlistItemRelinquished()

            val resp = endpoint.sendWishlistItemRelinquishedEvent(event)
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun trackWishlistRelinquished(
        productId: String,
        item: WishlistItem,
        url: String,
        referrer: String?,
        result: (SystemaResult<HttpResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = trackWishlistRelinquished(productId, item, url, referrer)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }
}

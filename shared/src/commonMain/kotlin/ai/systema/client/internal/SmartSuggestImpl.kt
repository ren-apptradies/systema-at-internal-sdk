package ai.systema.client.internal

import ai.systema.client.SmartSuggest
import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointSmartSuggest
import ai.systema.helper.internal.ApplicationDispatcher
import ai.systema.model.SystemaResult
import ai.systema.model.request.SmartSuggestRequest
import ai.systema.model.response.SmartSuggestResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

internal class SmartSuggestImpl(
    private val connector: Connector,
    private val endpoint: EndpointSmartSuggest
) : SmartSuggest {

    override suspend fun smartSuggest(
        payload: SmartSuggestRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<SmartSuggestResponse> {
        return try {
            val resp = endpoint.smartSuggest(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun smartSuggest(
        payload: SmartSuggestRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<SmartSuggestResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = smartSuggest(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }
}

package ai.systema.client.internal

import ai.systema.client.Recommender
import ai.systema.client.SmartSearch
import ai.systema.client.SmartSuggest
import ai.systema.client.SystemaAI
import ai.systema.client.SystemaUserManager
import ai.systema.client.Tracker
import ai.systema.configuration.Configuration
import ai.systema.configuration.Credentials
import ai.systema.connection.internal.Connector

internal open class SystemaImpl(
    internal val connector: Connector,
    override val meta: Map<String, Any>,
) : SystemaAI,
    Configuration by connector,
    Credentials by connector.credentials,
    Tracker by Tracker(connector),
    Recommender by Recommender(connector),
    SmartSearch by SmartSearch(connector),
    SmartSuggest by SmartSuggest(connector),
    SystemaUserManager by SystemaUserManager(connector) {

    override suspend fun initialize() {
        connector.initialize()
    }
}

package ai.systema.client.internal

import ai.systema.client.SmartSearch
import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointSmartSearch
import ai.systema.helper.internal.ApplicationDispatcher
import ai.systema.model.SystemaResult
import ai.systema.model.request.SmartSearchRequest
import ai.systema.model.response.SmartSearchResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

internal class SmartSearchImpl(
    private val connector: Connector,
    private val endpoint: EndpointSmartSearch
) : SmartSearch {

    override suspend fun smartSearch(
        payload: SmartSearchRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<SmartSearchResponse> {
        return try {
            val resp = endpoint.smartSearch(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun smartSearch(
        payload: SmartSearchRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<SmartSearchResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = smartSearch(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }
}

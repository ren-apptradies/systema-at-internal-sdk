package ai.systema.client.internal

import ai.systema.client.SystemaUserManager
import ai.systema.connection.internal.Connector
import ai.systema.model.SystemaUser

internal class SystemaUserManagerImpl(
    val connector: Connector
) : SystemaUserManager {

    override suspend fun getUserSnapshot(): SystemaUser {
        // we allow retrieving readonly impersonated user data as a snapshot
        return connector.clientUser.getSnapshot()
    }

    override suspend fun setUserIdHash(uid: String) {
        if (uid.isBlank()) {
            throw IllegalArgumentException("UserID hash cannot be null or empty")
        }

        // just a simple sanity check that the input is not an email
        if (uid.contains('@')) {
            throw IllegalArgumentException("UserID hash should not contain characters such as '@'. Did you forget to hash?")
        }

        connector.clientUser.userIdHash = uid
    }

    override suspend fun clearUserIdHash() {
        connector.clientUser.userIdHash = ""
    }
}

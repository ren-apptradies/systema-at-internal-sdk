package ai.systema.client.internal

import ai.systema.client.Recommender
import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointRecommendation
import ai.systema.helper.internal.ApplicationDispatcher
import ai.systema.model.SystemaResult
import ai.systema.model.request.CartRecommendationRequest
import ai.systema.model.request.RecommendationRequest
import ai.systema.model.response.RecommendationResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

internal class RecommenderImpl(
    private val connector: Connector,
    private val endpoint: EndpointRecommendation
) : Recommender {

    override suspend fun getRelated(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<RecommendationResponse> {
        return try {
            val resp = endpoint.getRelated(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun getRelated(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = getRelated(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun getComplementary(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<RecommendationResponse> {
        return try {
            val resp = endpoint.getComplementary(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun getComplementary(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = getComplementary(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun getSimilar(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<RecommendationResponse> {
        return try {
            val resp = endpoint.getSimilar(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun getSimilar(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = getSimilar(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun getCategoryTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<RecommendationResponse> {
        return try {
            val resp = endpoint.getCategoryTrending(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun getCategoryTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = getCategoryTrending(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun getCategoryPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<RecommendationResponse> {
        return try {
            val resp = endpoint.getCategoryPopular(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun getCategoryPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = getCategoryPopular(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun getTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<RecommendationResponse> {
        return try {
            val resp = endpoint.getTrending(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun getTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = getTrending(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun getPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<RecommendationResponse> {
        return try {
            val resp = endpoint.getPopular(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun getPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = getPopular(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun getCartRelated(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<RecommendationResponse> {
        return try {
            val resp = endpoint.getCartRelated(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun getCartRelated(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = getCartRelated(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }

    override suspend fun getCartComplementary(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions?
    ): SystemaResult<RecommendationResponse> {
        return try {
            val resp = endpoint.getCartComplementary(connector.injectSystemaSettings(payload))
            SystemaResult.success(resp)
        } catch (ex: Exception) {
            SystemaResult.error(ex)
        }
    }

    override fun getCartComplementary(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions?,
        result: (SystemaResult<RecommendationResponse>) -> Unit
    ) {
        CoroutineScope(ApplicationDispatcher).launch {
            val response = getCartComplementary(payload, requestOptions)
            if (response.isSuccessful) {
                result.invoke(SystemaResult.success(response.value()))
            } else {
                result.invoke(SystemaResult.error(response.error()))
            }
        }
    }
}

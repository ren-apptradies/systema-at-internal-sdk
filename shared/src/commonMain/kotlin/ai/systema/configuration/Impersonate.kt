package ai.systema.configuration

import ai.systema.constants.SystemaKeys
import ai.systema.helper.SystemaDeviceManager
import ai.systema.helper.internal.UUID
import ai.systema.model.ClientUser

internal object Impersonate {
    internal suspend fun clientUser(kvStore: SystemaKVStore, deviceManager: SystemaDeviceManager): ClientUser {
        val clientUser = ClientUser(
            fingerprint = genFingerprint(kvStore),
            userAgent = deviceManager.getUserAgent()
        )

        clientUser.refreshSession(kvStore)

        return clientUser
    }

    private suspend fun genFingerprint(kvStore: SystemaKVStore): String {
        var fingerprint = kvStore.read(SystemaKeys.Fingerprint)
        if (fingerprint != null && fingerprint != "null") {
            return fingerprint
        }

        fingerprint = UUID.randomUUID()
        kvStore.write(SystemaKeys.Fingerprint, fingerprint)

        // clear session from kvStore if it exists
        kvStore.delete(SystemaKeys.SessionId) // delete if it exists
        kvStore.delete(SystemaKeys.SessionCreatedAt)

        return fingerprint
    }
}

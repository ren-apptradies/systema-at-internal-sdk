package ai.systema.configuration

import ai.systema.connection.RequestOptions
import ai.systema.enums.EndpointType

internal interface DynamicConfigurationManager {
    suspend fun initialize(requestOptions: RequestOptions? = null)

    suspend fun refreshDynamicConfig(requestOptions: RequestOptions? = null)

    suspend fun prepDynamicRoute(endpointType: EndpointType, actionType: String): String

    suspend fun prepDynamicPayload(actionType: String): String
}

package ai.systema.configuration

public interface SystemaKVStore {
    public suspend fun read(key: String): String?
    public suspend fun delete(key: String): String?
    public suspend fun write(key: String, value: String)
}

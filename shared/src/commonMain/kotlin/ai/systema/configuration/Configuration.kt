package ai.systema.configuration

import ai.systema.configuration.internal.ConfigurationImpl
import ai.systema.connection.internal.hosts
import ai.systema.enums.EndpointType
import ai.systema.helper.SystemaDeviceManager
import ai.systema.helper.logging.SystemaLogLevel
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.http.Url

public interface Configuration {
    public val credentials: Credentials

    public val logLevel: SystemaLogLevel

    public val engine: HttpClientEngine?

    public val hosts: Map<EndpointType, Url>

    public val httpClient: HttpClient

    public val kvStore: SystemaKVStore

    public val deviceManager: SystemaDeviceManager
}

public fun Configuration(
    credentials: Credentials,
    logLevel: SystemaLogLevel = SystemaLogLevel.INFO,
    hosts: Map<EndpointType, Url> = credentials.hosts,
    engine: HttpClientEngine? = null,
    kvStore: SystemaKVStore,
    deviceManager: SystemaDeviceManager
): Configuration = ConfigurationImpl(
    credentials,
    logLevel = logLevel,
    hosts,
    engine,
    kvStore,
    deviceManager,
)

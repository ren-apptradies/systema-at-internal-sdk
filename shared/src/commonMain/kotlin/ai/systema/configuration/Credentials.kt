package ai.systema.configuration

import ai.systema.configuration.internal.CredentialsImpl
import ai.systema.enums.EndpointType
import ai.systema.enums.EnvironmentType
import ai.systema.model.SystemaAPIKey
import ai.systema.model.SystemaClientID
import io.ktor.http.Url

public interface Credentials {
    public val clientID: SystemaClientID

    public val apiKey: SystemaAPIKey

    public val environment: EnvironmentType

    // this is for debugging the application
    // set proxy to an interceptor url which will forward the requests to correct destinations
    public val proxyUrls: Map<EndpointType, Url>
}

public fun Credentials(
    clientID: SystemaClientID,
    apiKey: SystemaAPIKey,
    environment: EnvironmentType = EnvironmentType.DEV,
    proxyUrls: Map<EndpointType, Url> = mapOf()
): Credentials = CredentialsImpl(clientID, apiKey, environment, proxyUrls)

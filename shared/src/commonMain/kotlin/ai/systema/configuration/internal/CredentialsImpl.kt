package ai.systema.configuration.internal

import ai.systema.configuration.Credentials
import ai.systema.enums.EndpointType
import ai.systema.enums.EnvironmentType
import ai.systema.model.SystemaAPIKey
import ai.systema.model.SystemaClientID
import io.ktor.http.Url

internal class CredentialsImpl(
    override val clientID: SystemaClientID,
    override val apiKey: SystemaAPIKey,
    override val environment: EnvironmentType,
    override val proxyUrls: Map<EndpointType, Url> = mapOf()
) : Credentials

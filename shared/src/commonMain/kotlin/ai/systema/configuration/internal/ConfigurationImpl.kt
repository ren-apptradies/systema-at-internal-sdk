package ai.systema.configuration.internal

import ai.systema.configuration.Configuration
import ai.systema.configuration.Credentials
import ai.systema.configuration.SystemaKVStore
import ai.systema.configuration.internal.extension.prepareHttpClient
import ai.systema.enums.EndpointType
import ai.systema.helper.SystemaDeviceManager
import ai.systema.helper.logging.SystemaLogLevel
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.http.Url

internal class ConfigurationImpl(
    override val credentials: Credentials,
    override val logLevel: SystemaLogLevel,
    override val hosts: Map<EndpointType, Url>,
    override val engine: HttpClientEngine?,
    override val kvStore: SystemaKVStore,
    override val deviceManager: SystemaDeviceManager,
) : Configuration {
    override val httpClient: HttpClient = prepareHttpClient(logLevel, deviceManager)
}

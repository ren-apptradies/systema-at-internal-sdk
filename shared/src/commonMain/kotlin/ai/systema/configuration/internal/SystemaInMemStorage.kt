package ai.systema.configuration.internal

import ai.systema.configuration.SystemaKVStore
import ai.systema.model.config.SystemaCache
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

internal class SystemaInMemStorage : SystemaKVStore {
    private var cache: SystemaCache = SystemaCache(values = mutableMapOf())
    private val cacheMutex = Mutex()

    override suspend fun read(key: String): String? {
        if (this.cache.values.containsKey(key)) {
            return this.cache.values[key] as String
        }

        return null
    }

    override suspend fun delete(key: String): String? {
        cacheMutex.withLock {
            return this.cache.values.remove(key)
        }
    }

    override suspend fun write(key: String, value: String) {
        cacheMutex.withLock {
            this.cache.values[key] = value
        }
    }
}

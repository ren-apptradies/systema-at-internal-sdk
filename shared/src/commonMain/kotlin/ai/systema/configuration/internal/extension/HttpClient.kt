package ai.systema.configuration.internal.extension

import ai.systema.configuration.Configuration
import ai.systema.helper.SystemaDeviceManager
import ai.systema.helper.internal.JsonNonStrict
import ai.systema.helper.logging.SystemaLogLevel
import ai.systema.helper.logging.toKtorLogLevel
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.features.HttpTimeout
import io.ktor.client.features.UserAgent
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.features.logging.SIMPLE

internal fun Configuration.prepareHttpClient(logLevel: SystemaLogLevel, deviceManager: SystemaDeviceManager) =
    engine?.let {
        HttpClient(it) { configure(logLevel, deviceManager) }
    } ?: HttpClient { configure(logLevel, deviceManager) }

internal fun HttpClientConfig<*>.configure(logLevel: SystemaLogLevel, deviceManager: SystemaDeviceManager) {
    // FIXME: Added to avoid InvalidMutabilityException. Need to check later on what's the best approach
    // https://youtrack.jetbrains.com/issue/KTOR-915
    // https://youtrack.jetbrains.com/issue/KTOR-1223
    // https://youtrack.jetbrains.com/issue/KTOR-915
    // https://youtrack.jetbrains.com/issue/KTOR-1223
//    CoroutineScope(ApplicationDispatcher).launch {
// }
    // FIXME: need to verify if we really need to wrap installs with CoroutineScope or not for iOS after the refactor

    install(JsonFeature) {
        serializer = KotlinxSerializer(JsonNonStrict)
    }

    install(Logging) {
        level = toKtorLogLevel(logLevel)
        logger = Logger.SIMPLE
    }

    install(UserAgent) {
        agent = deviceManager.getUserAgent()
    }

    install(HttpTimeout)
}

package ai.systema.helper

import ai.systema.connection.internal.Connector
import ai.systema.constants.SystemaConstants
import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.CurTrackEventDate
import ai.systema.model.tracker.TrackEventDate
import ai.systema.model.tracker.cart.CartItem
import ai.systema.model.tracker.cart.CartItemAcquiredEvent
import ai.systema.model.tracker.cart.CartItemAcquisitionCompleteEvent
import ai.systema.model.tracker.cart.CartItemRelinquishedEvent
import ai.systema.model.tracker.cart.PurchaseOrder
import ai.systema.model.tracker.view.ContainerShownEvent
import ai.systema.model.tracker.view.ItemClickEvent
import ai.systema.model.tracker.view.ItemContainer
import ai.systema.model.tracker.view.PageViewEvent
import ai.systema.model.tracker.wishlist.WishlistItem
import ai.systema.model.tracker.wishlist.WishlistItemAcquiredEvent
import ai.systema.model.tracker.wishlist.WishlistItemRelinquishedEvent

/**
 * TrackEventBuilder is the common builder class to build various tracker events
 *
 * Issue with this single builder is that SDK user needs to know how what parameters are
 * required to build an event. TrackerEvent data classes have the validation on the mandatory fields.
 *
 * Ideally we could have separate builders to enforce various other validation, which is left as future work.
 */
internal class TrackEventBuilder(
    private val connector: Connector,
    private val eventDate: TrackEventDate = CurTrackEventDate,
) {

    private var productId: String = ""
    private var recId: String = ""
    private var referrer: String = ""
    private var url: String = ""
    private var containers: List<ItemContainer>? = null

    private var cartItem: CartItem? = null
    private var cartItems: List<CartItem>? = null

    private var wishlistItem: WishlistItem? = null
    private var wishlistItems: List<WishlistItem>? = null
    private var purchaseOrder: PurchaseOrder? = null

    internal fun setProductId(productId: String?): TrackEventBuilder {
        productId?.let {
            this.productId = productId.trim()
        }

        return this
    }

    internal fun setRecId(recId: String?): TrackEventBuilder {
        recId?.let {
            this.recId = recId.trim()
        }

        return this
    }

    internal fun setReferrer(referrer: String?): TrackEventBuilder {
        referrer?.let {
            this.referrer = referrer.trim()
        }

        return this
    }

    internal fun setUrl(url: String?): TrackEventBuilder {
        url?.let {
            this.url = url.trim()
        }

        return this
    }

    internal fun setContainers(containers: List<ItemContainer>?): TrackEventBuilder {
        this.containers = containers
        return this
    }

    internal fun setCartItem(item: CartItem?): TrackEventBuilder {
        this.cartItem = item
        return this
    }

    internal fun setCartItems(items: List<CartItem>?): TrackEventBuilder {
        this.cartItems = items
        return this
    }

    internal fun setWishlistItem(item: WishlistItem?): TrackEventBuilder {
        this.wishlistItem = item
        return this
    }

    internal fun setWishlistItems(items: List<WishlistItem>?): TrackEventBuilder {
        this.wishlistItems = items
        return this
    }

    internal fun setPurchaseOrder(purchaseOrder: PurchaseOrder?): TrackEventBuilder {
        this.purchaseOrder = purchaseOrder
        return this
    }

    internal suspend fun buildContainerShownEvent(): ContainerShownEvent {
        connector.refreshDynamicConfig()

        return when (productId) {
            // empty string "" is ensured in the setter if productId is null or blank
            // so when it is "", we don't build the event with productId
            "" -> ContainerShownEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.ContainerShown,
                referrer = referrer,
                url = url,
                eventDate = eventDate,
                containers = containers,
                version = SystemaConstants.Version,
            )
            else -> ContainerShownEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.ContainerShown,
                referrer = referrer,
                url = url,
                eventDate = eventDate,
                containers = containers,
                productId = productId,
                version = SystemaConstants.Version,
            )
        }
    }

    internal suspend fun buildItemClickEvent(): ItemClickEvent {
        connector.refreshDynamicConfig()

        return ItemClickEvent(
            clientId = connector.credentials.clientID.basic,
            environment = connector.credentials.environment.value,
            fingerprint = connector.clientUser.fingerprint,
            sessionId = connector.clientUser.sessionId,
            userAgent = connector.clientUser.userAgent,
            userName = connector.clientUser.userIdHash,
            sequence = connector.clientUser.nexSeq(),
            productId = productId,
            recId = recId,
            type = TrackerEventType.ItemClicked,
            referrer = referrer,
            url = url,
            eventDate = eventDate,
            version = SystemaConstants.Version,
        )
    }

    internal suspend fun buildPageViewEvent(): PageViewEvent {
        connector.refreshDynamicConfig()

        return when (productId) {
            "" -> PageViewEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                recId = recId,
                type = TrackerEventType.PageView,
                referrer = referrer,
                url = url,
                eventDate = eventDate,
                version = SystemaConstants.Version,
            )
            else -> PageViewEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                productId = productId,
                recId = recId,
                type = TrackerEventType.PageView,
                referrer = referrer,
                url = url,
                eventDate = eventDate,
                version = SystemaConstants.Version,
            )
        }
    }

    internal suspend fun buildCartItemAcquired(): CartItemAcquiredEvent {
        connector.refreshDynamicConfig()

        return CartItemAcquiredEvent(
            clientId = connector.credentials.clientID.basic,
            environment = connector.credentials.environment.value,
            fingerprint = connector.clientUser.fingerprint,
            sessionId = connector.clientUser.sessionId,
            userAgent = connector.clientUser.userAgent,
            userName = connector.clientUser.userIdHash,
            sequence = connector.clientUser.nexSeq(),
            type = TrackerEventType.AddToCart,
            productId = productId,
            referrer = referrer,
            url = url,
            eventDate = eventDate,
            items = cartItems!!,
            version = SystemaConstants.Version,
        )
    }

    internal suspend fun buildCartItemRelinquished(): CartItemRelinquishedEvent {
        connector.refreshDynamicConfig()

        return CartItemRelinquishedEvent(
            clientId = connector.credentials.clientID.basic,
            environment = connector.credentials.environment.value,
            fingerprint = connector.clientUser.fingerprint,
            sessionId = connector.clientUser.sessionId,
            userAgent = connector.clientUser.userAgent,
            userName = connector.clientUser.userIdHash,
            sequence = connector.clientUser.nexSeq(),
            type = TrackerEventType.RemoveFromCart,
            productId = productId,
            referrer = referrer,
            url = url,
            eventDate = eventDate,
            item = cartItem!!,
            version = SystemaConstants.Version,
        )
    }

    internal suspend fun buildCartItemAcquisitionComplete(): CartItemAcquisitionCompleteEvent {
        connector.refreshDynamicConfig()

        return CartItemAcquisitionCompleteEvent(
            clientId = connector.credentials.clientID.basic,
            environment = connector.credentials.environment.value,
            fingerprint = connector.clientUser.fingerprint,
            sessionId = connector.clientUser.sessionId,
            userAgent = connector.clientUser.userAgent,
            userName = connector.clientUser.userIdHash,
            sequence = connector.clientUser.nexSeq(),
            type = TrackerEventType.Purchase,
            referrer = referrer,
            url = url,
            eventDate = eventDate,
            order = purchaseOrder!!,
            version = SystemaConstants.Version,
        )
    }

    internal suspend fun buildWishlistItemAcquired(): WishlistItemAcquiredEvent {
        connector.refreshDynamicConfig()

        return WishlistItemAcquiredEvent(
            clientId = connector.credentials.clientID.basic,
            environment = connector.credentials.environment.value,
            fingerprint = connector.clientUser.fingerprint,
            sessionId = connector.clientUser.sessionId,
            userAgent = connector.clientUser.userAgent,
            userName = connector.clientUser.userIdHash,
            sequence = connector.clientUser.nexSeq(),
            type = TrackerEventType.AddToWishlist,
            productId = productId,
            referrer = referrer,
            url = url,
            eventDate = eventDate,
            items = wishlistItems!!,
            version = SystemaConstants.Version,
        )
    }

    internal suspend fun buildWishlistItemRelinquished(): WishlistItemRelinquishedEvent {
        connector.refreshDynamicConfig()

        return WishlistItemRelinquishedEvent(
            clientId = connector.credentials.clientID.basic,
            environment = connector.credentials.environment.value,
            fingerprint = connector.clientUser.fingerprint,
            sessionId = connector.clientUser.sessionId,
            userAgent = connector.clientUser.userAgent,
            userName = connector.clientUser.userIdHash,
            sequence = connector.clientUser.nexSeq(),
            type = TrackerEventType.RemoveFromWishlist,
            productId = productId,
            referrer = referrer,
            url = url,
            eventDate = eventDate,
            item = wishlistItem!!,
            version = SystemaConstants.Version,
        )
    }
}

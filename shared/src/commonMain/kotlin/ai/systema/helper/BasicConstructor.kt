package ai.systema.helper

import ai.systema.model.SystemaAPIKey
import ai.systema.model.SystemaClientID

public fun String.toClientID(): SystemaClientID {
    return SystemaClientID(this)
}

public fun String.toAPIKey(): SystemaAPIKey {
    return SystemaAPIKey(this)
}

package ai.systema.helper.logging

import io.ktor.client.features.logging.LogLevel

/**
 * Systema Log Level
 * enum order represents amount of logs visible in the System.out
 *
 * This is primarily used to translate to kotor's log level
 * All other logging in the library are done using ktolin-logging
 */
public enum class SystemaLogLevel {
    NONE,
    ERROR,
    WARN,
    INFO,
    DEBUG,
}

/**
 * Mapping of SystemaLogLevel to ktor's LogLevel
 */
internal val logLevelMapping: Map<SystemaLogLevel, LogLevel> = mapOf(
    SystemaLogLevel.ERROR to LogLevel.NONE,
    SystemaLogLevel.WARN to LogLevel.NONE,
    SystemaLogLevel.INFO to LogLevel.INFO,
    SystemaLogLevel.DEBUG to LogLevel.ALL,
    SystemaLogLevel.NONE to LogLevel.NONE,
)

/**
 * Map to ktor log level
 */
internal fun toKtorLogLevel(level: SystemaLogLevel): LogLevel {
    if (logLevelMapping.containsKey(level)) return logLevelMapping[level] as LogLevel
    return LogLevel.NONE
}

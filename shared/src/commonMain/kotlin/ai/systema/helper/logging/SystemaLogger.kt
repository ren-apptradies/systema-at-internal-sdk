package ai.systema.helper.logging

import io.github.aakira.napier.Napier

internal object SystemaLogger {
    internal fun debug(message: String, throwable: Throwable? = null, tag: String? = null) {
        Napier.d(message, throwable, tag)
    }

    internal fun verbose(message: String, throwable: Throwable? = null, tag: String? = null) {
        Napier.v(message, throwable, tag)
    }

    internal fun info(message: String, throwable: Throwable? = null, tag: String? = null) {
        Napier.i(message, throwable, tag)
    }

    internal fun warn(message: String, throwable: Throwable? = null, tag: String? = null) {
        Napier.w(message, throwable, tag)
    }

    internal fun error(message: String, throwable: Throwable? = null, tag: String? = null) {
        Napier.e(message, throwable, tag)
    }

    internal fun assert(message: String, throwable: Throwable? = null, tag: String? = null) {
        Napier.wtf(message, throwable, tag)
    }
}

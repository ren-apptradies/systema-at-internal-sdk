package ai.systema.helper

import ai.systema.model.SystemaDevice

public interface SystemaDeviceManager {
    public fun getDeviceInfo(): SystemaDevice
    public fun getUserAgent(): String
}

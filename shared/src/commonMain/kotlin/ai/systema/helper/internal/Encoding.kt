package ai.systema.helper.internal

internal expect fun String.encodeUTF8(): String

internal expect fun String.sha256Hex(): String

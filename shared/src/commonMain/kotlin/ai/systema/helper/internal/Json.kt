package ai.systema.helper.internal

import kotlinx.serialization.json.Json

internal val JsonNoDefaults = Json {
    encodeDefaults = false
}

internal val JsonNonStrict = Json {
    ignoreUnknownKeys = true
    isLenient = true
    allowSpecialFloatingPointValues = true
    encodeDefaults = true
}

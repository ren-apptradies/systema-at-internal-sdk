package ai.systema.helper.internal

import ai.systema.annotations.JsFunction

internal expect object UUID {
    @JsFunction("randomUUID")
    fun randomUUID(): String
}

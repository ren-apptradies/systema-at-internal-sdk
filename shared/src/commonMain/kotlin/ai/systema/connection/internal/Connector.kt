package ai.systema.connection.internal

import ai.systema.configuration.Configuration
import ai.systema.configuration.DynamicConfigurationManager
import ai.systema.configuration.Impersonate
import ai.systema.connection.RequestOptions
import ai.systema.constants.SystemaConstants
import ai.systema.enums.EndpointType
import ai.systema.exception.SystemaApiException
import ai.systema.helper.logging.SystemaLogger
import ai.systema.model.ClientUser
import ai.systema.model.config.DynamicConfig
import ai.systema.model.request.CartRecommendationRequest
import ai.systema.model.request.RecommendationRequest
import ai.systema.model.request.RequestUser
import ai.systema.model.request.SmartSearchRequest
import ai.systema.model.request.SmartSuggestRequest
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.request
import io.ktor.http.HttpMethod
import io.ktor.http.URLProtocol
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.ExperimentalTime

/**
 * Connector class prepares Http connections with Systema Dynamic Configurations
 */
internal class Connector(
    private val configuration: Configuration,
) : Configuration by configuration,
    DynamicConfigurationManager {

    private val reqSettingsErrMsg = "should not be set already in the request. SDK needs to set it."

    private lateinit var dynamicConfig: DynamicConfig

    @OptIn(ExperimentalTime::class)
    private var lasRefreshedAt: Instant? = null

    private val configMutex = Mutex()

    internal lateinit var clientUser: ClientUser

    private fun httpRequestBuilder(
        httpMethod: HttpMethod,
        path: String,
        protocol: URLProtocol?,
        requestOptions: RequestOptions?,
        body: String?,
    ): HttpRequestBuilder {
        return HttpRequestBuilder().apply {
            url.path(path)
            url.protocol = protocol ?: URLProtocol.HTTPS
            method = httpMethod
            prepareBody(body)
            setCredentials(configuration.credentials)
            setFingerprint(clientUser.fingerprint) // for tracing
            setRequestOptions(requestOptions)
        }
    }

    /**
     * Core function to send Http requests
     */
    suspend inline fun <reified T> callApi(
        httpMethod: HttpMethod,
        endpointType: EndpointType,
        path: String,
        requestOptions: RequestOptions?,
        body: String? = null,
    ): T {
        val hostUrl = hosts[endpointType] ?: throw IllegalStateException("No API endpoints are available")

        // build with default settings
        val requestBuilder = httpRequestBuilder(httpMethod, path, URLProtocol.HTTPS, requestOptions, body)

        // set based on the hostUrl
        requestBuilder.url.host = hostUrl.host
        requestBuilder.url.protocol = hostUrl.protocol // override https if hostUrl is set as http
        requestBuilder.url.port = hostUrl.port

        try {
            return httpClient.request<T>(requestBuilder)
        } catch (exception: Exception) {
            throw SystemaApiException(exception)
        }
    }

    override suspend fun initialize(requestOptions: RequestOptions?) {
        this.clientUser = Impersonate.clientUser(kvStore, deviceManager)
        this.refreshDynamicConfig()
    }

    @OptIn(ExperimentalTime::class)
    override suspend fun refreshDynamicConfig(requestOptions: RequestOptions?) {
        return configMutex.withLock {
            if (this.lasRefreshedAt == null || this.lasRefreshedAt!! < Clock.System.now()
                .minus(SystemaConstants.MaxSessionDuration)
            ) {
                // TODO uncomment when dynamic configuration feature is finalized
//                logger.debug { "Refreshing dynamic config" }
//                this.dynamicConfig = this.callApi<ResponseDefault<DynamicConfig>>(
//                    HttpMethod.Get,
//                    EndpointType.DynamicConfig,
//                    SystemaRoutes.DynamicConfig,
//                    requestOptions
//                ).result

                this.lasRefreshedAt = Clock.System.now()
                clientUser.refreshSession(kvStore)
                SystemaLogger.debug("Refreshed dynamic config at: $lasRefreshedAt")
            }
        }
    }

    override suspend fun prepDynamicRoute(endpointType: EndpointType, actionType: String): String {
        this.refreshDynamicConfig()
        TODO("Implement using dynamic config")
    }

    override suspend fun prepDynamicPayload(actionType: String): String {
        this.refreshDynamicConfig()
        TODO("Implement using dynamic config")
    }

    internal suspend fun injectSystemaSettings(payload: RecommendationRequest?): RecommendationRequest {
        val req = payload ?: RecommendationRequest()

        if (req.environment != null)
            throw IllegalArgumentException("'environment' $reqSettingsErrMsg")

        if (req.user != null)
            throw IllegalArgumentException("'user' $reqSettingsErrMsg")

        req.environment = credentials.environment.value
        req.user = getRequestUser()

        return req
    }

    internal suspend fun injectSystemaSettings(payload: SmartSuggestRequest?): SmartSuggestRequest {
        val req = payload ?: SmartSuggestRequest(query = "")

        if (req.environment != null)
            throw IllegalArgumentException("'environment' $reqSettingsErrMsg")

        if (req.user != null)
            throw IllegalArgumentException("'user' $reqSettingsErrMsg")

        req.environment = credentials.environment.value
        req.user = getRequestUser()

        return req
    }

    internal suspend fun injectSystemaSettings(payload: SmartSearchRequest?): SmartSearchRequest {
        val req = payload ?: SmartSearchRequest()

        if (req.environment != null)
            throw IllegalArgumentException("'environment' $reqSettingsErrMsg")

        if (req.user != null)
            throw IllegalArgumentException("'user' $reqSettingsErrMsg")

        req.environment = credentials.environment.value
        req.user = getRequestUser()

        return req
    }

    internal suspend fun injectSystemaSettings(payload: CartRecommendationRequest?): CartRecommendationRequest {
        val req = payload ?: CartRecommendationRequest()

        if (req.environment != null)
            throw IllegalArgumentException("'environment' $reqSettingsErrMsg")

        if (req.user != null)
            throw IllegalArgumentException("'user' $reqSettingsErrMsg")

        req.environment = credentials.environment.value
        req.user = getRequestUser()

        return req
    }

    internal suspend fun getRequestUser(): RequestUser {
        this.clientUser.refreshSession(kvStore)
        return this.clientUser.toRequestUser()
    }
}

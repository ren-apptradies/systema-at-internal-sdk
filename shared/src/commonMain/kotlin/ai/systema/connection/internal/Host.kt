package ai.systema.connection.internal

import ai.systema.configuration.Credentials
import ai.systema.enums.EndpointType
import io.ktor.http.Url

/**
 * Hosts related configurations
 */
internal val Credentials.hosts
    get() = mapOf(
        EndpointType.DynamicConfig to dynamicConfigHost,
        EndpointType.Recommend to recommendHost,
        EndpointType.Search to searchHost,
        EndpointType.Tracker to trackerHost
    )

internal val Credentials.dynamicConfigHost
    get() = proxyUrls[EndpointType.DynamicConfig] ?: Url("http://discover.systema.cloud")

internal val Credentials.trackerHost
    get() = proxyUrls[EndpointType.Tracker]
        ?: Url("http://tracker.${this.clientID}.${this.environment.value}.systema.cloud")

internal val Credentials.searchHost
    get() = proxyUrls[EndpointType.Search]
        ?: Url("http://search.${this.clientID}.${this.environment.value}.systema.cloud")

internal val Credentials.recommendHost
    get() = proxyUrls[EndpointType.Recommend]
        ?: Url("http://recommend.${this.clientID}.${this.environment.value}.systema.cloud")

package ai.systema.connection.internal

import ai.systema.configuration.Credentials
import ai.systema.connection.RequestOptions
import ai.systema.constants.SystemaKeys
import ai.systema.enums.EnvironmentType
import ai.systema.model.SystemaAPIKey
import ai.systema.model.SystemaClientID
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.client.request.parameter

internal fun HttpRequestBuilder.setCredentials(credentials: Credentials?) {
    setClientId(credentials?.clientID)
    setApiKey(credentials?.apiKey)
    setEnvironment(credentials?.environment)
}

internal fun HttpRequestBuilder.setClientId(clientID: SystemaClientID?) {
    header(SystemaKeys.SystemaClientID, clientID?.basic)
}

internal fun HttpRequestBuilder.setApiKey(apiKey: SystemaAPIKey?) {
    header(SystemaKeys.SystemaAPIKey, apiKey?.basic)
}

internal fun HttpRequestBuilder.setEnvironment(env: EnvironmentType?) {
    header(SystemaKeys.SystemaEnvironment, env?.value)
}

internal fun HttpRequestBuilder.setFingerprint(fid: String) {
    header(SystemaKeys.SystemaFingerprint, fid)
}

internal fun HttpRequestBuilder.prepareBody(payload: String?) {
    if (payload != null) {
        body = payload
    }
}

internal fun HttpRequestBuilder.setRequestOptions(requestOptions: RequestOptions?) {
    requestOptions?.headers?.forEach { header(it.key, it.value) }
    requestOptions?.urlParameters?.forEach { parameter(it.key, it.value) }
    requestOptions?.body?.let { body = it }
}

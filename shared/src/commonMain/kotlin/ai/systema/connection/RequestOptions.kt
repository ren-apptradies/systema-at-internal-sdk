package ai.systema.connection

import kotlinx.serialization.json.JsonObject

public class RequestOptions {

    public val headers: MutableMap<String, Any> = mutableMapOf()
    public val urlParameters: MutableMap<String, Any> = mutableMapOf()
    public var body: JsonObject? = null

    public fun parameter(key: String, value: Any?) {
        value?.let { urlParameters[key] = it }
    }

    public fun header(key: String, value: Any?) {
        value?.let { headers[key] = it }
    }
}

package ai.systema.endpoint

public object SystemaRoutes {
    public const val DynamicConfig: String = "v1/sdk/config"
    public const val Tracker: String = "v1/tracker"

    public const val CartRelated: String = "v1/cart/related"
    public const val CartComplementary: String = "v1/cart/complementary"

    public const val ProductRelated: String = "v1/product/related"
    public const val ProductComplementary: String = "v1/product/complementary"
    public const val ProductSimilar: String = "v1/product/similar"

    public const val CategoryTrending: String = "v1/category/trending"
    public const val CategoryPopular: String = "v1/category/popular"

    public const val Trending: String = "v1/main/trending"
    public const val Popular: String = "v1/main/popular"

    public const val SmartSearch: String = "v1/smart-search"
    public const val SmartSuggest: String = "v1/suggest"
}

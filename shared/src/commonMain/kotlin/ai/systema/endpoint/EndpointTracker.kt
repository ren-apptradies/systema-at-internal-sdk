package ai.systema.endpoint

import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.internal.EndpointTrackerImpl
import ai.systema.model.tracker.cart.CartItemAcquiredEvent
import ai.systema.model.tracker.cart.CartItemAcquisitionCompleteEvent
import ai.systema.model.tracker.cart.CartItemRelinquishedEvent
import ai.systema.model.tracker.view.ContainerShownEvent
import ai.systema.model.tracker.view.ItemClickEvent
import ai.systema.model.tracker.view.PageViewEvent
import ai.systema.model.tracker.wishlist.WishlistItemAcquiredEvent
import ai.systema.model.tracker.wishlist.WishlistItemRelinquishedEvent
import io.ktor.client.statement.HttpResponse

public interface EndpointTracker {
    public suspend fun sendPageViewEvent(event: PageViewEvent, requestOptions: RequestOptions? = null): HttpResponse

    public suspend fun sendItemClickEvent(event: ItemClickEvent, requestOptions: RequestOptions? = null): HttpResponse

    public suspend fun sendContainerShownEvent(
        event: ContainerShownEvent,
        requestOptions: RequestOptions? = null
    ): HttpResponse

    public suspend fun sendCartItemAcquiredEvent(
        event: CartItemAcquiredEvent,
        requestOptions: RequestOptions? = null
    ): HttpResponse

    public suspend fun sendCartItemRelinquishedEvent(
        event: CartItemRelinquishedEvent,
        requestOptions: RequestOptions? = null
    ): HttpResponse

    public suspend fun sendCartItemAcquisitionCompleteEvent(
        event: CartItemAcquisitionCompleteEvent,
        requestOptions: RequestOptions? = null
    ): HttpResponse

    public suspend fun sendWishlistItemAcquiredEvent(
        event: WishlistItemAcquiredEvent,
        requestOptions: RequestOptions? = null
    ): HttpResponse

    public suspend fun sendWishlistItemRelinquishedEvent(
        event: WishlistItemRelinquishedEvent,
        requestOptions: RequestOptions? = null
    ): HttpResponse
}

/**
 * Create an [EndpointTracker] instance.
 */
internal fun EndpointTracker(
    connector: Connector,
): EndpointTracker = EndpointTrackerImpl(connector)

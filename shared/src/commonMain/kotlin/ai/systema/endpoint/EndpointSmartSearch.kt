package ai.systema.endpoint

import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.internal.EndpointSmartSearchImpl
import ai.systema.model.request.SmartSearchRequest
import ai.systema.model.response.SmartSearchResponse

public interface EndpointSmartSearch {
    public suspend fun smartSearch(
        payload: SmartSearchRequest,
        requestOptions: RequestOptions? = null
    ): SmartSearchResponse
}

/**
 * Create an [EndpointSmartSearch] instance.
 */
internal fun EndpointSmartSearch(
    connector: Connector,
): EndpointSmartSearch = EndpointSmartSearchImpl(connector)

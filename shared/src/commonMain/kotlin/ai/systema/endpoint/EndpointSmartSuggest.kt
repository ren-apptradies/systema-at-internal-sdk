package ai.systema.endpoint

import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.internal.EndpointSmartSuggestImpl
import ai.systema.model.request.SmartSuggestRequest
import ai.systema.model.response.SmartSuggestResponse

public interface EndpointSmartSuggest {
    public suspend fun smartSuggest(
        payload: SmartSuggestRequest,
        requestOptions: RequestOptions? = null
    ): SmartSuggestResponse
}

/**
 * Create an [EndpointSmartSuggest] instance.
 */
internal fun EndpointSmartSuggest(
    connector: Connector,
): EndpointSmartSuggest = EndpointSmartSuggestImpl(connector)

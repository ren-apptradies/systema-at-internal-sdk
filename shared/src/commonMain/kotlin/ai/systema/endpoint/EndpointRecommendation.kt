package ai.systema.endpoint

import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.internal.EndpointRecommendationImpl
import ai.systema.model.request.CartRecommendationRequest
import ai.systema.model.request.RecommendationRequest
import ai.systema.model.response.RecommendationResponse

public interface EndpointRecommendation {
    public suspend fun getCartRelated(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): RecommendationResponse

    public suspend fun getCartComplementary(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): RecommendationResponse

    public suspend fun getRelated(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): RecommendationResponse

    public suspend fun getComplementary(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): RecommendationResponse

    public suspend fun getSimilar(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): RecommendationResponse

    public suspend fun getCategoryTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): RecommendationResponse

    public suspend fun getCategoryPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): RecommendationResponse

    public suspend fun getTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): RecommendationResponse

    public suspend fun getPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions? = null,
    ): RecommendationResponse
}

/**
 * Create an [EndpointRecommendation] instance.
 */
internal fun EndpointRecommendation(
    connector: Connector,
): EndpointRecommendation = EndpointRecommendationImpl(connector)

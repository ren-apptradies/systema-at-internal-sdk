package ai.systema.endpoint.internal

import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointSmartSuggest
import ai.systema.endpoint.SystemaRoutes
import ai.systema.enums.EndpointType
import ai.systema.helper.internal.JsonNoDefaults
import ai.systema.model.request.SmartSuggestRequest
import ai.systema.model.response.SmartSuggestResponse
import io.ktor.http.HttpMethod

internal class EndpointSmartSuggestImpl(
    private val connector: Connector,
) : EndpointSmartSuggest {

    override suspend fun smartSuggest(
        payload: SmartSuggestRequest,
        requestOptions: RequestOptions?
    ): SmartSuggestResponse {
        if (payload.query.isBlank()) {
            throw IllegalArgumentException("'query' field cannot be blank")
        }

        return this.sendRequest(payload, SystemaRoutes.SmartSuggest, requestOptions)
    }

    private suspend fun sendRequest(
        payload: SmartSuggestRequest,
        path: String,
        requestOptions: RequestOptions?
    ): SmartSuggestResponse {
        val body = JsonNoDefaults.encodeToString(SmartSuggestRequest.serializer(), payload)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Search,
            path,
            requestOptions,
            body
        )
    }
}

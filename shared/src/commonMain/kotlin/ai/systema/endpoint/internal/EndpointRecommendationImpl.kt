package ai.systema.endpoint.internal

import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointRecommendation
import ai.systema.endpoint.SystemaRoutes
import ai.systema.enums.EndpointType
import ai.systema.helper.internal.JsonNoDefaults
import ai.systema.model.request.CartRecommendationRequest
import ai.systema.model.request.RecommendationRequest
import ai.systema.model.response.RecommendationResponse
import io.ktor.http.HttpMethod

internal class EndpointRecommendationImpl(
    private val connector: Connector,
) : EndpointRecommendation {

    override suspend fun getCartRelated(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        if (payload.id == null || payload.id.isEmpty()) {
            throw IllegalArgumentException("'id' field cannot be empty")
        }

        return this.sendCartRecommendationRequest(payload, SystemaRoutes.CartRelated, requestOptions)
    }

    override suspend fun getCartComplementary(
        payload: CartRecommendationRequest,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        if (payload.id == null || payload.id.isEmpty()) {
            throw IllegalArgumentException("'id' field cannot be empty")
        }

        return this.sendCartRecommendationRequest(payload, SystemaRoutes.CartComplementary, requestOptions)
    }

    override suspend fun getRelated(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        if (payload.id == null || payload.id.isBlank()) {
            throw IllegalArgumentException("'id' field cannot be blank")
        }

        return this.sendRequest(payload, SystemaRoutes.ProductRelated, requestOptions)
    }

    override suspend fun getComplementary(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        if (payload.id == null || payload.id.isBlank()) {
            throw IllegalArgumentException("'id' field cannot be blank")
        }

        return this.sendRequest(payload, SystemaRoutes.ProductComplementary, requestOptions)
    }

    override suspend fun getSimilar(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        if (payload.id == null || payload.id.isBlank()) {
            throw IllegalArgumentException("'id' field cannot be blank")
        }

        return this.sendRequest(payload, SystemaRoutes.ProductSimilar, requestOptions)
    }

    override suspend fun getCategoryTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        if (payload.category == null || payload.category.isEmpty()) {
            throw IllegalArgumentException("'category' field cannot be empty")
        }

        if (payload.id != null) {
            throw IllegalArgumentException("'id' field should be null")
        }

        return this.sendRequest(payload, SystemaRoutes.CategoryTrending, requestOptions)
    }

    override suspend fun getCategoryPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        if (payload.category == null || payload.category.isEmpty()) {
            throw IllegalArgumentException("'category' field cannot be empty")
        }

        if (payload.id != null) {
            throw IllegalArgumentException("'id' field should be null")
        }

        return this.sendRequest(payload, SystemaRoutes.CategoryPopular, requestOptions)
    }

    override suspend fun getTrending(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        if (payload.id != null || payload.category != null) {
            throw IllegalArgumentException("'category' or 'id' field should be null")
        }

        return this.sendRequest(payload, SystemaRoutes.Trending, requestOptions)
    }

    override suspend fun getPopular(
        payload: RecommendationRequest,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        if (payload.id != null || payload.category != null) {
            throw IllegalArgumentException("'category' or 'id' field should be null")
        }

        return this.sendRequest(payload, SystemaRoutes.Popular, requestOptions)
    }

    private suspend fun sendRequest(
        payload: RecommendationRequest,
        path: String,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        val body = JsonNoDefaults.encodeToString(RecommendationRequest.serializer(), payload)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Recommend,
            path,
            requestOptions,
            body
        )
    }

    private suspend fun sendCartRecommendationRequest(
        payload: CartRecommendationRequest,
        path: String,
        requestOptions: RequestOptions?
    ): RecommendationResponse {
        val body = JsonNoDefaults.encodeToString(CartRecommendationRequest.serializer(), payload)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Recommend,
            path,
            requestOptions,
            body
        )
    }
}

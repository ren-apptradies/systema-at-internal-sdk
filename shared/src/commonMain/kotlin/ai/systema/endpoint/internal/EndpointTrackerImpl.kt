package ai.systema.endpoint.internal

import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointTracker
import ai.systema.endpoint.SystemaRoutes
import ai.systema.enums.EndpointType
import ai.systema.helper.internal.JsonNoDefaults
import ai.systema.model.tracker.cart.CartItemAcquiredEvent
import ai.systema.model.tracker.cart.CartItemAcquisitionCompleteEvent
import ai.systema.model.tracker.cart.CartItemRelinquishedEvent
import ai.systema.model.tracker.view.ContainerShownEvent
import ai.systema.model.tracker.view.ItemClickEvent
import ai.systema.model.tracker.view.PageViewEvent
import ai.systema.model.tracker.wishlist.WishlistItemAcquiredEvent
import ai.systema.model.tracker.wishlist.WishlistItemRelinquishedEvent
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpMethod

internal class EndpointTrackerImpl(
    private val connector: Connector,
) : EndpointTracker {

    override suspend fun sendPageViewEvent(event: PageViewEvent, requestOptions: RequestOptions?): HttpResponse {
        val body = JsonNoDefaults.encodeToString(PageViewEvent.serializer(), event)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Tracker,
            SystemaRoutes.Tracker,
            requestOptions,
            body
        )
    }

    override suspend fun sendItemClickEvent(event: ItemClickEvent, requestOptions: RequestOptions?): HttpResponse {
        val body = JsonNoDefaults.encodeToString(ItemClickEvent.serializer(), event)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Tracker,
            SystemaRoutes.Tracker,
            requestOptions,
            body
        )
    }

    override suspend fun sendContainerShownEvent(
        event: ContainerShownEvent,
        requestOptions: RequestOptions?
    ): HttpResponse {
        val body = JsonNoDefaults.encodeToString(ContainerShownEvent.serializer(), event)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Tracker,
            SystemaRoutes.Tracker,
            requestOptions,
            body
        )
    }

    override suspend fun sendCartItemAcquiredEvent(
        event: CartItemAcquiredEvent,
        requestOptions: RequestOptions?
    ): HttpResponse {
        val body = JsonNoDefaults.encodeToString(CartItemAcquiredEvent.serializer(), event)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Tracker,
            SystemaRoutes.Tracker,
            requestOptions,
            body
        )
    }

    override suspend fun sendCartItemRelinquishedEvent(
        event: CartItemRelinquishedEvent,
        requestOptions: RequestOptions?
    ): HttpResponse {
        val body = JsonNoDefaults.encodeToString(CartItemRelinquishedEvent.serializer(), event)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Tracker,
            SystemaRoutes.Tracker,
            requestOptions,
            body
        )
    }

    override suspend fun sendCartItemAcquisitionCompleteEvent(
        event: CartItemAcquisitionCompleteEvent,
        requestOptions: RequestOptions?
    ): HttpResponse {
        val body = JsonNoDefaults.encodeToString(CartItemAcquisitionCompleteEvent.serializer(), event)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Tracker,
            SystemaRoutes.Tracker,
            requestOptions,
            body
        )
    }

    override suspend fun sendWishlistItemAcquiredEvent(
        event: WishlistItemAcquiredEvent,
        requestOptions: RequestOptions?
    ): HttpResponse {
        val body = JsonNoDefaults.encodeToString(WishlistItemAcquiredEvent.serializer(), event)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Tracker,
            SystemaRoutes.Tracker,
            requestOptions,
            body
        )
    }

    override suspend fun sendWishlistItemRelinquishedEvent(
        event: WishlistItemRelinquishedEvent,
        requestOptions: RequestOptions?
    ): HttpResponse {
        val body = JsonNoDefaults.encodeToString(WishlistItemRelinquishedEvent.serializer(), event)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Tracker,
            SystemaRoutes.Tracker,
            requestOptions,
            body
        )
    }
}

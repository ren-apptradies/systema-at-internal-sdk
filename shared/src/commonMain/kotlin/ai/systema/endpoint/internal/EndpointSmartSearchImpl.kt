package ai.systema.endpoint.internal

import ai.systema.connection.RequestOptions
import ai.systema.connection.internal.Connector
import ai.systema.endpoint.EndpointSmartSearch
import ai.systema.endpoint.SystemaRoutes
import ai.systema.enums.EndpointType
import ai.systema.helper.internal.JsonNoDefaults
import ai.systema.model.request.SmartSearchRequest
import ai.systema.model.response.SmartSearchResponse
import io.ktor.http.HttpMethod

internal class EndpointSmartSearchImpl(
    private val connector: Connector,
) : EndpointSmartSearch {

    override suspend fun smartSearch(
        payload: SmartSearchRequest,
        requestOptions: RequestOptions?
    ): SmartSearchResponse {
        if (payload.query == null || payload.query.isEmpty()) {
            throw IllegalArgumentException("'query' field cannot be empty")
        }

        return this.sendRequest(payload, SystemaRoutes.SmartSearch, requestOptions)
    }

    private suspend fun sendRequest(
        payload: SmartSearchRequest,
        path: String,
        requestOptions: RequestOptions?
    ): SmartSearchResponse {
        val body = JsonNoDefaults.encodeToString(SmartSearchRequest.serializer(), payload)
        return connector.callApi(
            HttpMethod.Post,
            EndpointType.Search,
            path,
            requestOptions,
            body
        )
    }
}

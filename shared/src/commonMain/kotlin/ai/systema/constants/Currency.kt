package ai.systema.constants

public enum class Currency(public val value: String) {
    USD("USD"),
    AUD("AUD"),
    NZD("NZD"),
    SGD("SGD"),
    HKD("HKD")
}

package ai.systema.constants

public object QueryItemType {
    public const val Product: String = "product"
    public const val Image: String = "image"
}

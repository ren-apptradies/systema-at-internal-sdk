package ai.systema.constants

// set of keys to set tags
public object SystemaTags {
    public const val ProductId: String = "systema_product_id"
    public const val ProductUrl: String = "systema_product_url"
    public const val ProductPrice: String = "systema_product_price"
    public const val ProductCurrency: String = "systema_product_currency"
    public const val ContainerUrl: String = "systema_container_url"
    public const val ResultId: String = "systema_result_id"
    public const val RecId: String = "systema_rec_id"
    public const val ReferrerUrl: String = "systema_referrer_url"
    public const val Observed: String = "systema_observed"
    public const val Visible: String = "systema_visible"
}

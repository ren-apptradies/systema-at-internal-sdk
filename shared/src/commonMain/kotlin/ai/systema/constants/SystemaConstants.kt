package ai.systema.constants

import kotlin.time.Duration
import kotlin.time.ExperimentalTime

public object SystemaConstants {
    public const val Version: String = "0.1.0b1.Mobile"
    public const val MaxKeyLen: Int = 256
    public const val SystemaCacheFileName: String = "systema.json"
    public const val SystemaTagMapping: String = "TAG_MAPPING"

    @OptIn(ExperimentalTime::class)
    public val MaxSessionDuration: Duration = Duration.hours(2)
}

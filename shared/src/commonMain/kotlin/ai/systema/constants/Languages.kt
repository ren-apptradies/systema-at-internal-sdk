package ai.systema.constants

/**
 * Three letters ISO6392 language code
 */
public object Languages {
    public const val English: String = "eng"
}

package ai.systema.enums

public enum class EnvironmentType(public val value: String) {
    PROD("prod"),
    TEST("test"),
    DEV("dev")
}

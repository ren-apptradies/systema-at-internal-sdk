package ai.systema.enums

public enum class ScoreType(public val value: String) {
    Relevance("relevance"),
    User("user")
}

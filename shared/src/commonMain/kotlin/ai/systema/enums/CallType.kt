package ai.systema.enums

public enum class CallType {
    Read,
    Write
}

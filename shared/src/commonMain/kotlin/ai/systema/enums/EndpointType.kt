package ai.systema.enums

public enum class EndpointType {
    Recommend,
    Search,
    Tracker,
    DynamicConfig
}

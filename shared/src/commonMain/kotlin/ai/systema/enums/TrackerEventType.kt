package ai.systema.enums

public enum class TrackerEventType {
    PageView,
    ItemClicked,
    ContainerShown,
    AddToCart,
    RemoveFromCart,
    Purchase,
    AddToWishlist,
    RemoveFromWishlist,
}

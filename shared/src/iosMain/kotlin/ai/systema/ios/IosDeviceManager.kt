package ai.systema.ios

import ai.systema.helper.SystemaDeviceManager
import ai.systema.model.SystemaDevice
import ai.systema.model.toUserAgent
import platform.UIKit.UIDevice


internal class IosDeviceManager() : SystemaDeviceManager {
    override fun getDeviceInfo(): SystemaDevice {
        return SystemaDevice(
            osVersion = UIDevice.currentDevice.systemVersion,
            model = UIDevice.currentDevice.model,
            deviceId = UIDevice.currentDevice.identifierForVendor.toString(),
            deviceName = UIDevice.currentDevice.name,
            fingerprint = UIDevice.currentDevice.identifierForVendor.toString(),
            release = "",
            product = UIDevice.currentDevice.systemName,
            brand = "Apple",
            display = "",
            manufacturer = "Apple"
        )
    }

    /**
     * Get UserAgent String
     *
     * TODO: Is there a better way to generate the UserAgent string similar to
     * what browser would return?
     */
    override fun getUserAgent(): String {
        return getDeviceInfo().toUserAgent()
    }
}

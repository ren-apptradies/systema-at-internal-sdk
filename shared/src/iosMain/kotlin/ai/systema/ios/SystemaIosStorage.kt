package ai.systema.ios

import ai.systema.configuration.SystemaKVStore
import ai.systema.helper.logging.SystemaLogger
import platform.Foundation.NSUserDefaults

public open class SystemaIosStorage : SystemaKVStore {

    private var userDefaults: NSUserDefaults = NSUserDefaults()

    override suspend fun read(key: String): String? {
        val value = userDefaults.objectForKey(key).toString()
        SystemaLogger.debug("read test new - $key : $value", tag = TAG)
        return value
    }

    override suspend fun delete(key: String): String? {
        SystemaLogger.debug("delete test new - $key", tag = TAG)
        userDefaults.removeObjectForKey(key)
        return key
    }

    override suspend fun write(key: String, value: String) {
        SystemaLogger.debug("write test new - $key : $value\"", tag = TAG)
        userDefaults.setObject(value, key)
    }

    public companion object {
        public val TAG: String? = SystemaIosStorage::class.simpleName
    }
}

package ai.systema.ios

import ai.systema.client.SystemaAI
import ai.systema.enums.EndpointType
import ai.systema.enums.EnvironmentType
import ai.systema.helper.logging.SystemaLogLevel
import io.github.aakira.napier.DebugAntilog
import io.github.aakira.napier.Napier

// /**
// * Convert meta into tagMapping<String, Int>
// */
// internal fun SystemaAI.getTagMapping(): Map<String, Int> {
//    val meta: Map<String, Any> = this.getMeta()
//    if (!meta.containsKey(SystemaConstants.SystemaTagMapping)) {
//        return mapOf()
//    }
//
//    @Suppress("UNCHECKED_CAST")
//    return meta[SystemaConstants.SystemaTagMapping] as Map<String, Int>
// }
//
// /**
// * Add product related tags for a view
// * Here view refers to an element that represents a single product
// */
// public fun SystemaAI.addProductTags(v: View, product: Product, tagMapping: Map<String, Int>? = null): View {
//    val tagMap = tagMapping ?: this.getTagMapping()
//
//    Tagger.setProductId(v, product, tagMap)
//    Tagger.setRecId(v, product, tagMap)
//    Tagger.setProductUrl(v, product, tagMap, this.clientID)
//
//    return v
// }
//
// /**
// * Add recommendation response tags to a container view
// * Here view refers to an element that containers other product related view elements. i.e. ListView
// */
// public fun SystemaAI.addContainerTags(
//    v: View,
//    resp: RecommendationResponse,
//    tagMapping: Map<String, Int>? = null
// ): View {
//    val tagMap = tagMapping ?: this.getTagMapping()
//    Tagger.setResultId(v, resp.resultId, tagMap)
//    Tagger.setContainerUrl(v, resp.resultId, tagMap, this.clientID)
//    return v
// }
//
// public fun SystemaAI.addOnClickListener(
//    v: View,
//    listener: View.OnClickListener? = null,
//    tagMapping: Map<String, Int>? = null,
// ) {
//    v.setOnClickListener(this.getOnClickListener(tagMapping).addOnClickListener(listener))
// }
//
// public fun SystemaAI.addOnLayoutChangeListener(
//    v: View,
//    listener: View.OnLayoutChangeListener? = null,
//    tagMapping: Map<String, Int>? = null,
// ) {
//    v.addOnLayoutChangeListener(this.getOnLayoutChangeListener(tagMapping).addOnLayoutChangeListener(listener))
// }
//
// public fun SystemaAI.addOnAttachStateChangeListener(
//    v: View,
//    listener: View.OnAttachStateChangeListener? = null,
//    tagMapping: Map<String, Int>? = null,
// ) {
//    v.addOnAttachStateChangeListener(
//        this.getOnAttachStateChangeListener(tagMapping).addOnAttachStateChangeListener(listener)
//    )
// }
//
// public fun SystemaAI.getOnLayoutChangeListener(tagMapping: Map<String, Int>? = null): SystemaOnLayoutChangeListener {
//    val tagMap = tagMapping ?: this.getTagMapping()
//    return SystemaOnLayoutChangeListener(this, tagMap)
// }
//
// public fun SystemaAI.getOnClickListener(tagMapping: Map<String, Int>? = null): SystemaOnClickListener {
//    val tagMap = tagMapping ?: this.getTagMapping()
//    return SystemaOnClickListener(this, tagMap)
// }
//
// public fun SystemaAI.getOnAttachStateChangeListener(tagMapping: Map<String, Int>? = null): SystemaOnAttachStateChangeListener {
//    val tagMap = tagMapping ?: this.getTagMapping()
//    return SystemaOnAttachStateChangeListener(this, tagMap)
// }
//
// public fun SystemaAI.monitorRecContainer(
//    recContainer: View,
//    resp: RecommendationResponse,
//    layoutChangeListener: View.OnLayoutChangeListener? = null
// ) {
//    // attach systema tags and listeners to the recommendation container
//    this.addContainerTags(recContainer, resp)
//    this.addOnLayoutChangeListener(recContainer, layoutChangeListener)
// }
//
// public fun SystemaAI.monitorProductPage(
//    view: View,
//    product: Product,
//    attachStateChangeListener: View.OnAttachStateChangeListener? = null
// ) {
//    // attach systema tags and listeners to the product details view
//    this.addProductTags(view, product)
//    this.addOnAttachStateChangeListener(view, attachStateChangeListener)
// }
//
// public fun SystemaAI.monitorRecItem(
//    view: View,
//    product: Product,
//    onClickListener: View.OnClickListener? = null
// ) {
//    // attach systema tags and listeners to the product item view
//    this.addProductTags(view, product)
//    this.addOnClickListener(view, onClickListener)
// }

public class SystemaAIClient {
    public suspend fun initialize(
        clientID: String,
        apiKey: String = "NOT_REQUIRED",
        environment: EnvironmentType = EnvironmentType.DEV,
        logLevel: SystemaLogLevel = SystemaLogLevel.INFO,
        proxyUrls: Map<EndpointType, String> = mapOf(),
        meta: Map<String, Any> = mapOf()
    ): SystemaAI {
        val systema = SystemaAI(
            clientID = clientID,
            apiKey = apiKey,
            environment = environment,
            logLevel = logLevel,
            proxyUrls = proxyUrls,
            meta = meta,
            kvStore = SystemaIosStorage(),
            deviceManager = IosDeviceManager()
        )

        Napier.base(DebugAntilog())

        systema.initialize()

        return systema
    }
}

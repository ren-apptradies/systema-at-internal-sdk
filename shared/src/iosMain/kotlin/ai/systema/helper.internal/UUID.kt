package ai.systema.helper.internal

import platform.Foundation.NSUUID

internal actual object UUID {
    actual fun randomUUID(): String = NSUUID.UUID().UUIDString
}

package ai.systema.helper.internal

internal actual object UUID {
    actual fun randomUUID(): String {
        return java.util.UUID.randomUUID().toString()
    }
}

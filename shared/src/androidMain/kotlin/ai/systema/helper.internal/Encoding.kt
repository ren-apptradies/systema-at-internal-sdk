package ai.systema.helper.internal

import org.apache.commons.codec.digest.DigestUtils
import java.net.URLEncoder

internal actual fun String.encodeUTF8(): String {
    return URLEncoder.encode(this, "UTF-8")
}

internal actual fun String.sha256Hex(): String {
    return DigestUtils.sha256Hex(this)
}

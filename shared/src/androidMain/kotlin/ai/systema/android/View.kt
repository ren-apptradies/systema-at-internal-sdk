package ai.systema.android

import ai.systema.helper.logging.SystemaLogger
import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup

/**
 * Check if the view is visible
 *
 * Ref: https://stackoverflow.com/questions/14039454/how-can-you-tell-if-a-view-is-visible-on-screen-in-android
 */
internal fun View.isVisible(): Boolean {
    if (!isShown) {
        return false
    }
    val actualPosition = Rect()
    val isGlobalVisible = getGlobalVisibleRect(actualPosition)
    val screenWidth = Resources.getSystem().displayMetrics.widthPixels
    val screenHeight = Resources.getSystem().displayMetrics.heightPixels
    val screen = Rect(0, 0, screenWidth, screenHeight)
    return isGlobalVisible && Rect.intersects(actualPosition, screen)
}

internal fun View.hasSystemaTag(tag: String, tagMap: Map<String, Int>): Boolean {
    if (tagMap.containsKey(tag) && this.getTag(tagMap[tag] as Int) != null) {
        return true
    }
    return false
}

internal fun View.getSystemaTagVal(tag: String, tagMap: Map<String, Int>): String? {
    if (!this.hasSystemaTag(tag, tagMap)) {
        SystemaLogger.error("Tag '$tag' doesn't exist for view: ${this.id}")
        return null
    }

    val tagId = tagMap[tag] as Int
    return this.getTag(tagId).toString()
}

/**
 * Get children matching the values of the given systema tags
 *
 * It returns children that have all tags in the tagFilter
 * and matched the given value
 */
internal fun View.findChildrenBySystemaTags(
    tagFilters: Map<String, String>,
    tagMapping: Map<String, Int>,
    candidates: MutableList<View>
) {
    if (this !is ViewGroup) return
    for (i in 0 until this.childCount) {
        val v: View = this.getChildAt(i)
        if (v is ViewGroup) {
            // recursive call to find children at deeper level
            v.findChildrenBySystemaTags(tagFilters, tagMapping, candidates)
        }

        // check if tags are matched
        var matchedAll = false
        for (tagFilter in tagFilters.entries) {
            val tagId: Int = tagMapping[tagFilter.key] ?: continue

            if (tagId == 213123100 && v.getTag(tagId) != null) {
                println("Found systema_rec_id")
            }

            val tagVal = v.getTag(tagId)
            if (tagVal != null && (tagFilter.value == "Any" || tagVal.equals(tagFilter.value))) {
                matchedAll = true
            } else {
                matchedAll = false
                break
            }
        }

        // if all tags matched then add to the candidate list
        if (matchedAll) {
            candidates.add(v)
        }
    }
}

/**
 * Get systema tag value (if available) by bubbling through parents from current/given view until ultimate parent.
 *
 * returns systema tag value or null.
 */
internal fun View.getSystemaTagValueFromParentTree(viewId: Int, tagMapping: Map<String, Int>, systemaTagKey: String): String? {
    val tagId: Int = tagMapping[systemaTagKey] ?: return null
    val parentView = this.parent as? View ?: this
    val tagValue = parentView.getTag(tagId)

    return when (parentView.id) {
        viewId -> {
            when (tagValue) {
                null -> null
                else -> tagValue as String
            }
        }
        else -> {
            when (tagValue) {
                null -> parentView.getSystemaTagValueFromParentTree(parentView.id, tagMapping, systemaTagKey)
                else -> tagValue as String
            }
        }
    }
}

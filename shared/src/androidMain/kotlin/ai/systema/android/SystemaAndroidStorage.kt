package ai.systema.android

import ai.systema.configuration.SystemaKVStore
import ai.systema.constants.SystemaConstants
import ai.systema.helper.logging.SystemaLogger
import ai.systema.model.config.SystemaCache
import android.content.Context
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.serialization.json.Json
import java.io.File

public open class SystemaAndroidStorage(
    context: Context,
) : SystemaKVStore {

    private val filePath = File(context.filesDir, SystemaConstants.SystemaCacheFileName)
    private var cache: SystemaCache = SystemaCache(values = mutableMapOf())
    private val cacheMutex = Mutex()

    init {
        this.load()
    }

    private fun load() {
        SystemaLogger.debug("Loading systema settings from: ${filePath.absolutePath}")
        if (!filePath.isFile) {
            filePath.createNewFile()
            return
        }

        val str = filePath.inputStream().readBytes().toString(Charsets.UTF_8)
        if (str.isNotBlank()) {
            cache = Json.decodeFromString(SystemaCache.serializer(), str)
            SystemaLogger.debug("Reading systema settings was successful from: ${filePath.absolutePath}")
        }
    }

    private fun flush() {
        SystemaLogger.debug("Writing systema settings from: ${filePath.absolutePath}")
        val str = Json.encodeToString(SystemaCache.serializer(), cache)
        filePath.outputStream().write(str.toByteArray())
        SystemaLogger.debug("Writing systema settings was successful to: ${filePath.absolutePath}")
    }

    override suspend fun read(key: String): String? {
        if (this.cache.values.containsKey(key)) {
            return this.cache.values[key] as String
        }

        return null
    }

    override suspend fun delete(key: String): String? {
        cacheMutex.withLock {
            val v = this.cache.values.remove(key)
            this.flush()

            return v
        }
    }

    override suspend fun write(key: String, value: String) {
        cacheMutex.withLock {
            this.cache.values[key] = value
            this.flush()
        }
    }
}

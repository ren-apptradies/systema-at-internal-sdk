package ai.systema.android

import ai.systema.client.SystemaAI
import ai.systema.enums.EndpointType
import ai.systema.enums.EnvironmentType
import ai.systema.helper.logging.SystemaLogLevel
import android.content.Context
import io.github.aakira.napier.DebugAntilog
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

public object SystemaAIClient {

    public fun initialize(
        clientID: String,
        apiKey: String = "NOT_REQUIRED",
        environment: EnvironmentType = EnvironmentType.DEV,
        logLevel: SystemaLogLevel = SystemaLogLevel.INFO,
        proxyUrls: Map<EndpointType, String> = mapOf(),
        meta: Map<String, Any> = mapOf(),
        context: Context,
        result: (Result<SystemaAI>) -> Unit,
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val systema =
                initialize(
                    clientID,
                    apiKey,
                    environment,
                    logLevel,
                    proxyUrls,
                    meta,
                    context
                )
            result(Result.success(systema))
        }
    }

    public suspend fun initialize(
        clientID: String,
        apiKey: String = "NOT_REQUIRED",
        environment: EnvironmentType = EnvironmentType.DEV,
        logLevel: SystemaLogLevel = SystemaLogLevel.INFO,
        proxyUrls: Map<EndpointType, String> = mapOf(),
        meta: Map<String, Any> = mapOf(),
        context: Context,
    ): SystemaAI =
        SystemaAI(
            clientID = clientID,
            apiKey = apiKey,
            environment = environment,
            logLevel = logLevel,
            proxyUrls = proxyUrls,
            meta = meta,
            kvStore = SystemaAndroidStorage(context),
            deviceManager = AndroidDeviceManager(context),
        ).also {
            it.initialize()
            if (logLevel != SystemaLogLevel.NONE) {
                Napier.base(DebugAntilog())
            }
        }
}

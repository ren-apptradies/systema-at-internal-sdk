package ai.systema.android.listener

public open class SystemaCompositeListener<T> {
    protected val listenerList: MutableList<T> = mutableListOf()

    public fun getListeners(): List<T> {
        return listenerList.toList()
    }

    public fun addListeners(listeners: Array<T>): SystemaCompositeListener<T> {
        listenerList.addAll(listeners)
        return this
    }

    public fun addListener(listener: T?): SystemaCompositeListener<T> {
        listener?.let { listenerList.add(listener) }
        return this
    }

    public fun removeListener(index: Int = -1): T? {
        if (listenerList.size <= 0) return null

        if (index < 0) {
            return listenerList.removeLast()
        }

        return listenerList.removeAt(index)
    }

    public fun clearAll() {
        listenerList.clear()
    }
}

package ai.systema.android.listener

import ai.systema.android.SystemaTagger
import ai.systema.android.findChildrenBySystemaTags
import ai.systema.android.getSystemaTagVal
import ai.systema.android.getSystemaTagValueFromParentTree
import ai.systema.android.getTagMapping
import ai.systema.android.hasSystemaTag
import ai.systema.android.isVisible
import ai.systema.client.SystemaAI
import ai.systema.constants.SystemaTags
import ai.systema.enums.TrackerEventType
import ai.systema.event.CallbackInvoker
import ai.systema.helper.logging.SystemaLogger
import ai.systema.model.tracker.view.ItemContainer
import ai.systema.model.tracker.view.prepContainerItem
import android.view.View
import android.widget.AdapterView
import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

public class SystemaEventListener(
    private val systema: SystemaAI,
    private val tagMapping: Map<String, Int>,
    private val callback: (Result<HttpResponse>) -> Unit,
) : View.OnClickListener,
    AdapterView.OnItemClickListener,
    View.OnLayoutChangeListener,
    View.OnAttachStateChangeListener {

    private var lastProductId: String = ""

    override fun onClick(view: View?) {
        try {
            val productId = SystemaTagger.getProductId(view, tagMapping)
            if (productId != null) {
                sendItemClickedEvent(
                    productId = productId,
                    recId = SystemaTagger.getRecId(view, tagMapping) ?: "",
                    url = SystemaTagger.getProductUrl(view, tagMapping) ?: "",
                    referrer = SystemaTagger.getReferrerUrl(view, tagMapping) ?: "",
                )
            }
        } catch (ex: Exception) {
            CallbackInvoker.trackerFailure(ex, callback)
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        onClick(view)
    }

    override fun onLayoutChange(
        v: View?,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int,
        oldLeft: Int,
        oldTop: Int,
        oldRight: Int,
        oldBottom: Int,
    ) {
        try {
            if (v != null) {
                val isVisible = v.isVisible()
                val resultId = SystemaTagger.getResultId(v, tagMapping)
                val hasObserved =
                    v.hasSystemaTag(SystemaTags.Observed, tagMapping) && v.getSystemaTagVal(
                        SystemaTags.Observed,
                        tagMapping
                    ) == true.toString()

                SystemaLogger.debug("Layout changed: $left $right, $top, $bottom, $oldLeft, $oldRight, $oldTop $oldBottom")
                SystemaLogger.debug("IsVisible: $isVisible, resultId: $resultId, hasObserved: $hasObserved")

                if (isVisible && resultId != null && !hasObserved) {
                    SystemaTagger.setObserved(v, systema.getTagMapping(), true)
                    sendContainerShownEvent(v, resultId)
                }
            }
        } catch (ex: Exception) {
            CallbackInvoker.trackerFailure(ex, callback)
        }
    }

    override fun onViewAttachedToWindow(v: View?) {
        try {
            val productId = SystemaTagger.getProductId(v, tagMapping)
            SystemaLogger.debug("Attached product view: $productId")

            if (productId != null) {
                SystemaLogger.debug("Send PageView (PDP): $productId")
                sendPageViewEvent(
                    productId = productId,
                    url = SystemaTagger.getProductUrl(v, tagMapping) ?: "",
                    recId = SystemaTagger.getRecId(v, tagMapping) ?: "",
                    referrer = SystemaTagger.getReferrerUrl(v, tagMapping) ?: ""
                )
            } else {
                SystemaLogger.debug(
                    "Send PageView (Non-PDP) - URL = ${
                        SystemaTagger.getProductUrl(
                            v,
                            tagMapping
                        )
                    }"
                )
                sendPageViewEvent(
                    url = SystemaTagger.getProductUrl(v, tagMapping) ?: "",
                    recId = SystemaTagger.getRecId(v, tagMapping) ?: "",
                    referrer = SystemaTagger.getReferrerUrl(v, tagMapping) ?: ""
                )
            }
        } catch (ex: Exception) {
            CallbackInvoker.trackerFailure(ex, callback)
        }
    }

    override fun onViewDetachedFromWindow(v: View?) {
        try {
            val productId = SystemaTagger.getProductId(v, tagMapping)
            SystemaLogger.debug("Detached product view: $productId")
            // TODO: Implementation for "what" needs to happen for "DetachedFromWindow".
            // TODO: We should probably remove this unused function
        } catch (ex: Exception) {
            CallbackInvoker.trackerFailure(ex, callback)
        }
    }

    private fun sendItemClickedEvent(
        productId: String,
        url: String = "",
        recId: String = "",
        referrer: String = ""
    ) {
        if (lastProductId.isNotBlank() && lastProductId == productId) {
            SystemaLogger.warn("Repeated ItemClicked Event: product_id: $productId, rec_id:$recId url:$url, referrer:$referrer. Skipping this event.")
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            SystemaLogger.debug("Send ${TrackerEventType.ItemClicked} Event: product_id: $productId, rec_id:$recId url:$url, referrer:$referrer")
            val result = systema.trackItemClicked(
                productId = productId,
                recId = recId,
                url = url,
                referrer = referrer
            )

            if (result.isSuccessful) {
                SystemaLogger.debug("Sent ${TrackerEventType.ItemClicked} Event: ${result.value().status}")
                CallbackInvoker.trackerSuccess(result.value(), callback)
            } else {
                SystemaLogger.error("Error ${TrackerEventType.ItemClicked} Event: ${result.error().localizedMessage}")
                CallbackInvoker.trackerFailure(result.error(), callback)
            }
        }
    }

    private fun sendPageViewEvent(
        productId: String,
        url: String = "",
        recId: String = "",
        referrer: String = ""
    ) {
        if (lastProductId.isNotBlank() && lastProductId == productId) {
            SystemaLogger.warn("Repeated ${TrackerEventType.PageView} Event: product_id: $productId, rec_id:$recId url:$url, referrer:$referrer. Skipping this event.")
            return
        }

        CoroutineScope(Dispatchers.IO).launch {
            SystemaLogger.debug("Send ${TrackerEventType.PageView} Event (PDP): product_id: $productId, rec_id:$recId url:$url, referrer:$referrer")
            val result = systema.trackPageViewed(
                productId = productId,
                recId = recId,
                url = url,
                referrer = referrer
            )
            if (result.isSuccessful) {
                SystemaLogger.debug("Send ${TrackerEventType.PageView} Event (PDP): ${result.value().status}")
                CallbackInvoker.trackerSuccess(result.value(), callback)
            } else {
                SystemaLogger.error("Error sending ${TrackerEventType.PageView} Event (PDP): ${result.error().localizedMessage}")
                CallbackInvoker.trackerFailure(result.error(), callback)
            }
        }
    }

    private fun sendPageViewEvent(url: String, recId: String, referrer: String) {

        CoroutineScope(Dispatchers.IO).launch {
            SystemaLogger.debug("Send ${TrackerEventType.PageView} Event (Non-PDP): rec_id:$recId url:$url, referrer:$referrer")
            val result = systema.trackPageViewed(
                recId = recId,
                url = url,
                referrer = referrer
            )

            if (result.isSuccessful) {
                SystemaLogger.debug("Send ${TrackerEventType.PageView} Event (Non-PDP): ${result.value().status}")
                CallbackInvoker.trackerSuccess(result.value(), callback)
            } else {
                SystemaLogger.error("Error sending ${TrackerEventType.PageView} Event (Non-PDP): ${result.error().localizedMessage}")
                CallbackInvoker.trackerFailure(result.error(), callback)
            }
        }
    }

    private fun sendContainerShownEvent(v: View, resultId: String) {

        // get recIds from the children of the container
        val recItems: MutableList<Map<String, String>> = mutableListOf()

        val systemaTags: Map<String, String> = mapOf(
            SystemaTags.RecId to "Any",
        )
        val candidates: MutableList<View> = mutableListOf()
        v.findChildrenBySystemaTags(systemaTags, tagMapping, candidates)

        // find candidate recIds
        for (child in candidates) {
            val hasObserved = SystemaTagger.getObserved(child, tagMapping) == true.toString()
            if (!hasObserved) {
                SystemaTagger.setObserved(v, tagMapping, true)
                val recId = SystemaTagger.getRecId(child, tagMapping)
                if (recId != null) {
                    recItems.add(
                        prepContainerItem(recId)
                    )
                }
            }
        }

        // send message
        if (recItems.isNotEmpty()) {
            SystemaLogger.debug("Send ContainerShown: $resultId")
            SystemaLogger.debug(recItems.toString())
            val container = ItemContainer(
                resultId = resultId,
                recItems = recItems,
            )

            // Note: productId could be null for non product related page
            val productIdFromImmediateParent =
                v.getSystemaTagValueFromParentTree(v.id, tagMapping, SystemaTags.ProductId)

            // send the event
            CoroutineScope(Dispatchers.IO).launch {
                val result = systema.trackContainerShown(
                    productId = productIdFromImmediateParent,
                    containers = listOf(container),
                    url = v.getSystemaTagVal(SystemaTags.ContainerUrl, tagMapping)
                )

                if (result.isSuccessful) {
                    SystemaLogger.error("Sent ${TrackerEventType.ContainerShown}: ${result.value().status}")
                    CallbackInvoker.trackerSuccess(result.value(), callback)
                } else {
                    SystemaLogger.error("Error sending ${TrackerEventType.ContainerShown}: ${result.error().localizedMessage}")
                    CallbackInvoker.trackerFailure(result.error(), callback)
                }
            }
        }
    }
}

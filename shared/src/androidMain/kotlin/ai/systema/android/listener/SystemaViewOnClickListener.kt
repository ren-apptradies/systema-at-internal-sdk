package ai.systema.android.listener

import ai.systema.client.SystemaAI
import android.view.View
import io.ktor.client.statement.HttpResponse

public open class SystemaViewOnClickListener(
    systema: SystemaAI,
    tagMapping: Map<String, Int>,
    callback: (Result<HttpResponse>) -> Unit,
) : View.OnClickListener, SystemaOnClickListener() {

    init {
        addListener(SystemaEventListener(systema, tagMapping, callback))
    }

    override fun onClick(v: View?) {
        for (listener in listenerList) {
            listener.onClick(v)
        }
    }
}

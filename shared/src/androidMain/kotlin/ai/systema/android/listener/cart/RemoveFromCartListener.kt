package ai.systema.android.listener.cart

import ai.systema.android.SystemaTagger
import ai.systema.client.SystemaAI
import ai.systema.enums.TrackerEventType
import ai.systema.event.CallbackInvoker
import ai.systema.helper.logging.SystemaLogger
import ai.systema.model.tracker.cart.CartItem
import android.view.View
import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

public class RemoveFromCartListener(
    private val systema: SystemaAI,
    private val getItem: () -> CartItem,
    private val tagMapping: Map<String, Int>,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val callback: (Result<HttpResponse>) -> Unit
) : View.OnClickListener {

    override fun onClick(view: View?) {
        try {
            val productId =
                SystemaTagger.getProductId(view, tagMapping)
                    ?: throw IllegalArgumentException("ProductId is not found")

            sendRemoveFromCartEvent(
                productId = productId,
                item = getItem(),
                url = SystemaTagger.getProductUrl(view, tagMapping) ?: "",
                referrer = SystemaTagger.getReferrerUrl(view, tagMapping) ?: "",
            )
        } catch (ex: Exception) {
            CallbackInvoker.trackerFailure(ex, callback)
        }
    }

    private fun sendRemoveFromCartEvent(
        productId: String,
        item: CartItem,
        url: String,
        referrer: String
    ) {
        CoroutineScope(dispatcher).launch {
            SystemaLogger.debug("Send ${TrackerEventType.RemoveFromCart} Event: product_id: $productId, url:$url")
            val result = systema.trackItemRelinquished(
                productId,
                item,
                url = url,
                referrer = referrer,
            )

            if (result.isSuccessful) {
                SystemaLogger.debug("Sent ${TrackerEventType.RemoveFromCart} Event: ${result.value().status}")
                CallbackInvoker.trackerSuccess(result.value(), callback)
            } else {
                SystemaLogger.error("Error ${TrackerEventType.RemoveFromCart}: ${result.error().localizedMessage}")
                CallbackInvoker.trackerFailure(result.error(), callback)
            }
        }
    }
}

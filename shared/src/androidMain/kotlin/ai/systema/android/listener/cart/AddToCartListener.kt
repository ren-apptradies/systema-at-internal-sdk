package ai.systema.android.listener.cart

import ai.systema.android.SystemaTagger
import ai.systema.client.SystemaAI
import ai.systema.constants.SystemaTags
import ai.systema.enums.TrackerEventType
import ai.systema.event.CallbackInvoker
import ai.systema.helper.logging.SystemaLogger
import ai.systema.model.tracker.cart.CartItem
import android.view.View
import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

public class AddToCartListener(
    private val systema: SystemaAI,
    private val getItems: () -> List<CartItem>,
    private val tagMapping: Map<String, Int>,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val callback: (Result<HttpResponse>) -> Unit
) : View.OnClickListener {

    override fun onClick(view: View?) {
        try {
            val productId = SystemaTagger.getProductId(view, tagMapping)
                ?: throw IllegalArgumentException("${SystemaTags.ProductId} tag is not found")

            val url = SystemaTagger.getProductUrl(view, tagMapping) ?: ""

            val referrer = SystemaTagger.getReferrerUrl(view, tagMapping) ?: ""

            val cartItems = getItems()

            sendItemAcquiredEvent(productId, cartItems, url, referrer)
        } catch (ex: Exception) {
            CallbackInvoker.trackerFailure(ex, callback)
        }
    }

    private fun sendItemAcquiredEvent(
        productId: String,
        cartItems: List<CartItem>,
        url: String,
        referrer: String
    ) {
        CoroutineScope(dispatcher).launch {
            SystemaLogger.debug("Sending ${TrackerEventType.AddToCart} Event: product_id: $productId, url:$url")
            val result = systema.trackItemAcquired(
                productId,
                items = cartItems,
                url = url,
                referrer = referrer,
            )

            if (result.isSuccessful) {
                SystemaLogger.debug("Sent ${TrackerEventType.AddToCart} Event: ${result.value().status}")
                CallbackInvoker.trackerSuccess(result.value(), callback)
            } else {
                SystemaLogger.error("Error ${TrackerEventType.AddToCart}: ${result.error().localizedMessage}")
                CallbackInvoker.trackerFailure(result.error(), callback)
            }
        }
    }
}

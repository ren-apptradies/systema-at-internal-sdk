package ai.systema.android.listener

import ai.systema.client.SystemaAI
import android.view.View
import io.ktor.client.statement.HttpResponse

public class SystemaOnLayoutChangeListener(
    systema: SystemaAI,
    tagMapping: Map<String, Int>,
    callback: (Result<HttpResponse>) -> Unit,
) : View.OnLayoutChangeListener, SystemaCompositeListener<View.OnLayoutChangeListener>() {

    init {
        addListener(SystemaEventListener(systema, tagMapping, callback))
    }

    override fun onLayoutChange(
        v: View?,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int,
        oldLeft: Int,
        oldTop: Int,
        oldRight: Int,
        oldBottom: Int
    ) {
        v ?: return
        for (listener in listenerList) {
            listener.onLayoutChange(
                v,
                left,
                top,
                right,
                bottom,
                oldLeft,
                oldTop,
                oldRight,
                oldBottom
            )
        }
    }
}

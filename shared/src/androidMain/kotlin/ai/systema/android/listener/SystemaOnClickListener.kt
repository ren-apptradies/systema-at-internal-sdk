package ai.systema.android.listener

import android.view.View

public open class SystemaOnClickListener : View.OnClickListener, SystemaCompositeListener<View.OnClickListener>() {

    override fun onClick(v: View?) {
        for (listener in listenerList) {
            listener.onClick(v)
        }
    }
}

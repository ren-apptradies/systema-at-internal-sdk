package ai.systema.android.listener.wishlist

import ai.systema.android.SystemaTagger
import ai.systema.client.SystemaAI
import ai.systema.constants.SystemaTags
import ai.systema.enums.TrackerEventType
import ai.systema.event.CallbackInvoker
import ai.systema.helper.logging.SystemaLogger
import ai.systema.model.tracker.wishlist.WishlistItem
import android.view.View
import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

public class AddToWishlistListener(
    private val systema: SystemaAI,
    private val getItems: () -> List<WishlistItem>,
    private val tagMapping: Map<String, Int>,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val callback: (Result<HttpResponse>) -> Unit,
) : View.OnClickListener {

    override fun onClick(view: View?) {
        try {
            val productId =
                SystemaTagger.getProductId(view, tagMapping)
                    ?: throw IllegalArgumentException("${SystemaTags.ProductId} tag is not found")

            val url =
                SystemaTagger.getProductUrl(view, tagMapping) ?: ""

            val referrer =
                SystemaTagger.getReferrerUrl(view, tagMapping) ?: ""

            val wishlistItems = getItems()

            sendWishlistItemAcquiredEvent(productId, wishlistItems, url, referrer)
        } catch (ex: Exception) {
            CallbackInvoker.trackerFailure(ex, callback)
        }
    }

    private fun sendWishlistItemAcquiredEvent(
        productId: String,
        wishlistItems: List<WishlistItem>,
        url: String,
        referrer: String
    ) {
        CoroutineScope(dispatcher).launch {
            SystemaLogger.debug("Send ${TrackerEventType.AddToWishlist} Event: product_id: $productId, url:$url")
            val result = systema.trackWishlistAcquired(
                productId,
                items = wishlistItems,
                url = url,
                referrer = referrer,
            )

            if (result.isSuccessful) {
                SystemaLogger.debug("Sent ${TrackerEventType.AddToWishlist} Event: ${result.value().status}")
                CallbackInvoker.trackerSuccess(result.value(), callback)
            } else {
                SystemaLogger.error("Error ${TrackerEventType.AddToWishlist}: ${result.error().localizedMessage}")
                CallbackInvoker.trackerFailure(result.error(), callback)
            }
        }
    }
}

package ai.systema.android.listener

import android.view.View

public class SystemaButtonOnClickListener(
    listener: View.OnClickListener
) : View.OnClickListener,
    SystemaOnClickListener() {

    init {
        addListener(listener)
    }
}

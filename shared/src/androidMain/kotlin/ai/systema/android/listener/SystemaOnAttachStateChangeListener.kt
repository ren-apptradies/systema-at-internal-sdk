package ai.systema.android.listener

import ai.systema.client.SystemaAI
import android.view.View
import io.ktor.client.statement.HttpResponse

public class SystemaOnAttachStateChangeListener(
    systema: SystemaAI,
    tagMapping: Map<String, Int>,
    callback: (Result<HttpResponse>) -> Unit
) : View.OnAttachStateChangeListener, SystemaCompositeListener<View.OnAttachStateChangeListener>() {

    init {
        addListener(SystemaEventListener(systema, tagMapping, callback))
    }

    override fun onViewAttachedToWindow(v: View?) {
        v ?: return
        for (listener in listenerList) {
            listener.onViewAttachedToWindow(v)
        }
    }

    override fun onViewDetachedFromWindow(v: View?) {
        v ?: return
        for (listener in listenerList) {
            listener.onViewDetachedFromWindow(v)
        }
    }
}

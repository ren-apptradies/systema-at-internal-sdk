package ai.systema.android

import ai.systema.android.listener.SystemaButtonOnClickListener
import ai.systema.android.listener.SystemaOnAttachStateChangeListener
import ai.systema.android.listener.SystemaOnClickListener
import ai.systema.android.listener.SystemaOnLayoutChangeListener
import ai.systema.android.listener.SystemaViewOnClickListener
import ai.systema.android.listener.cart.AddToCartListener
import ai.systema.android.listener.cart.RemoveFromCartListener
import ai.systema.android.listener.wishlist.AddToWishlistListener
import ai.systema.android.listener.wishlist.RemoveFromWishlistListener
import ai.systema.client.SystemaAI
import ai.systema.constants.SystemaConstants
import ai.systema.model.index.Product
import ai.systema.model.response.RecommendationResponse
import ai.systema.model.tracker.cart.CartItem
import ai.systema.model.tracker.wishlist.WishlistItem
import android.view.View
import io.ktor.client.statement.HttpResponse

/**
 * Various extensions to SystemaAI interface for android
 */

/**
 * Convert meta into tagMapping<String, Int>
 */
internal fun SystemaAI.getTagMapping(): Map<String, Int> {
    if (!this.meta.containsKey(SystemaConstants.SystemaTagMapping)) {
        return mapOf()
    }

    @Suppress("UNCHECKED_CAST")
    return meta[SystemaConstants.SystemaTagMapping] as Map<String, Int>
}

/**
 * Add product related tags for a view or button
 * Here view refers to an element that represents a single product
 */
public fun SystemaAI.addProductTags(
    v: View,
    product: Product?,
    referrerUrl: String = "",
    tagMapping: Map<String, Int>? = null,
): Result<View> {
    return try {
        val tagMap = tagMapping ?: this.getTagMapping()
        product?.let { item ->
            SystemaTagger.setProductId(v, item, tagMap)
            SystemaTagger.setProductPrice(v, item, tagMap)
            SystemaTagger.setProductCurrency(v, item, tagMap)
            SystemaTagger.setRecId(v, item, tagMap)
            SystemaTagger.setProductUrl(v, item, tagMap, this.clientID)
        }

        SystemaTagger.setReferrerUrl(v, referrerUrl, tagMap)
        Result.success(v)
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

/**
 * Add recommendation response tags to a container view
 * Here view refers to an element that containers other product related view elements. i.e. ListView
 */
public fun SystemaAI.addContainerTags(
    v: View,
    resp: RecommendationResponse,
    tagMapping: Map<String, Int>? = null
): Result<View> {
    return try {
        val tagMap = tagMapping ?: this.getTagMapping()
        SystemaTagger.setResultId(v, resp.resultId, tagMap)
        SystemaTagger.setContainerUrl(v, resp.resultId, tagMap, this.clientID)
        Result.success(v)
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.addOnClickListener(
    v: View,
    listener: View.OnClickListener? = null,
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit,
): Result<SystemaViewOnClickListener> {
    return try {
        this.getOnClickListener(tagMapping, callback = callback).fold(
            onSuccess = { compositeListener ->
                compositeListener.addListener(listener)
                v.setOnClickListener(compositeListener)
                Result.success(compositeListener)
            },
            onFailure = { ex ->
                Result.failure(ex)
            }
        )
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.addOnLayoutChangeListener(
    v: View,
    listener: View.OnLayoutChangeListener? = null,
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit,
): Result<SystemaOnLayoutChangeListener> {
    return try {
        this.getOnLayoutChangeListener(tagMapping, callback).fold(
            onSuccess = { compositeListener ->
                compositeListener.addListener(listener)
                v.addOnLayoutChangeListener(compositeListener)
                Result.success(compositeListener)
            },
            onFailure = { ex ->
                Result.failure(ex)
            }
        )
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.addOnAttachStateChangeListener(
    v: View,
    listener: View.OnAttachStateChangeListener? = null,
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit,
): Result<SystemaOnAttachStateChangeListener> {
    return try {
        this.getOnAttachStateChangeListener(tagMapping, callback).fold(
            onSuccess = { compositeListener ->
                compositeListener.addListener(listener)
                v.addOnAttachStateChangeListener(compositeListener)
                Result.success(compositeListener)
            },
            onFailure = { ex ->
                Result.failure(ex)
            }
        )
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.getOnLayoutChangeListener(
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit,
): Result<SystemaOnLayoutChangeListener> {
    return try {
        val tagMap = tagMapping ?: this.getTagMapping()
        val listener = SystemaOnLayoutChangeListener(this, tagMap, callback)
        Result.success(listener)
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.getOnClickListener(
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit
): Result<SystemaViewOnClickListener> {
    return try {
        val tagMap = tagMapping ?: this.getTagMapping()
        Result.success(SystemaViewOnClickListener(this, tagMap, callback))
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.getAddToCartListener(
    getItems: () -> List<CartItem>,
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit,
): Result<SystemaOnClickListener> {
    return try {
        val tagMap = tagMapping ?: this.getTagMapping()
        val compositeListener = SystemaButtonOnClickListener(
            AddToCartListener(
                systema = this,
                getItems = getItems,
                tagMapping = tagMap,
                callback = callback
            )
        )
        Result.success(compositeListener)
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.getRemoveFromCartListener(
    getItem: () -> CartItem,
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit,
): Result<SystemaOnClickListener> {
    return try {
        val tagMap = tagMapping ?: this.getTagMapping()
        val compositeListener = SystemaButtonOnClickListener(
            RemoveFromCartListener(
                systema = this,
                getItem = getItem,
                tagMapping = tagMap,
                callback = callback
            )
        )
        Result.success(compositeListener)
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.getAddToWishlistListener(
    getItems: () -> List<WishlistItem>,
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit,
): Result<SystemaOnClickListener> {
    return try {
        val tagMap = tagMapping ?: this.getTagMapping()
        val compositeListener = SystemaButtonOnClickListener(
            AddToWishlistListener(
                systema = this,
                getItems = getItems,
                tagMapping = tagMap,
                callback = callback
            )
        )
        Result.success(compositeListener)
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

internal fun SystemaAI.getRemoveFromWishlistListener(
    getItem: () -> WishlistItem,
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit
): Result<SystemaOnClickListener> {
    return try {
        val tagMap = tagMapping ?: this.getTagMapping()
        val compositeListener = SystemaButtonOnClickListener(
            RemoveFromWishlistListener(
                systema = this,
                getItem = getItem,
                tagMapping = tagMap,
                callback = callback
            )
        )
        Result.success(compositeListener)
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.getOnAttachStateChangeListener(
    tagMapping: Map<String, Int>? = null,
    callback: (Result<HttpResponse>) -> Unit,
): Result<SystemaOnAttachStateChangeListener> {
    return try {
        val tagMap = tagMapping ?: this.getTagMapping()
        Result.success(SystemaOnAttachStateChangeListener(this, tagMap, callback))
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

public fun SystemaAI.monitorRecContainer(
    recContainer: View,
    resp: RecommendationResponse,
    layoutChangeListener: View.OnLayoutChangeListener? = null,
    callback: (Result<HttpResponse>) -> Unit,
) {
    try {
        // attach systema tags and listeners to the recommendation container
        this.addContainerTags(recContainer, resp).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )

        this.addOnLayoutChangeListener(recContainer, layoutChangeListener, callback = callback).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )
    } catch (ex: Exception) {
        callback(Result.failure(ex))
    }
}

public fun SystemaAI.monitorProductPage(
    view: View,
    product: Product? = null,
    attachStateChangeListener: View.OnAttachStateChangeListener? = null,
    callback: (Result<HttpResponse>) -> Unit,
) {
    try {
        // attach systema tags and listeners to the product details view
        this.addProductTags(view, product).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )

        this.addOnAttachStateChangeListener(view, attachStateChangeListener, callback = callback).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )
    } catch (ex: Exception) {
        callback(Result.failure(ex))
    }
}

public fun SystemaAI.monitorRecItem(
    view: View,
    product: Product,
    onClickListener: View.OnClickListener? = null,
    callback: (Result<HttpResponse>) -> Unit,
) {
    try {
        // attach systema tags and listeners to the product item view
        this.addProductTags(view, product).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )

        this.addOnClickListener(view, onClickListener, callback = callback).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )
    } catch (ex: Exception) {
        callback(Result.failure(ex))
    }
}

public fun SystemaAI.monitorAddToCartButton(
    btn: View,
    product: Product,
    getItems: () -> List<CartItem>,
    onClickListener: View.OnClickListener? = null,
    callback: (Result<HttpResponse>) -> Unit,
) {
    try {
        // attach systema tags and listeners to the btn
        this.addProductTags(btn, product).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )

        this.getAddToCartListener(getItems, callback = callback).fold(
            onSuccess = { compositeListener ->
                compositeListener.addListener(onClickListener)
                btn.setOnClickListener(compositeListener)
            },
            onFailure = { ex ->
                throw ex
            }
        )
    } catch (ex: Exception) {
        callback(Result.failure(ex))
    }
}

public fun SystemaAI.monitorRemoveFromCartButton(
    btn: View,
    product: Product,
    getItem: () -> CartItem,
    onClickListener: View.OnClickListener? = null,
    callback: (Result<HttpResponse>) -> Unit,
) {
    try {
        // attach systema tags and listeners to the btn
        this.addProductTags(btn, product).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )

        this.getRemoveFromCartListener(getItem, callback = callback).fold(
            onSuccess = { compositeListener ->
                compositeListener.addListener(onClickListener)
                btn.setOnClickListener(compositeListener)
            },
            onFailure = { ex ->
                throw ex
            }
        )
    } catch (ex: Exception) {
        callback(Result.failure(ex))
    }
}

public fun SystemaAI.monitorAddToWishlistButton(
    btn: View,
    product: Product,
    getItems: () -> List<WishlistItem>,
    onClickListener: View.OnClickListener? = null,
    callback: ((Result<HttpResponse>) -> Unit),
) {
    return try {
        // attach systema tags and listeners to the btn
        this.addProductTags(btn, product).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )

        this.getAddToWishlistListener(getItems, callback = callback).fold(
            onSuccess = { compositeListener ->
                compositeListener.addListener(onClickListener)
                btn.setOnClickListener(compositeListener)
            },
            onFailure = { ex ->
                throw ex
            }
        )
    } catch (ex: Exception) {
        callback(Result.failure(ex))
    }
}

public fun SystemaAI.monitorRemoveFromWishlistButton(
    btn: View,
    product: Product,
    getItem: () -> WishlistItem,
    onClickListener: View.OnClickListener? = null,
    callback: ((Result<HttpResponse>) -> Unit),
) {
    return try {
        // attach systema tags and listeners to the btn
        this.addProductTags(btn, product).fold(
            onSuccess = {},
            onFailure = { ex ->
                throw ex
            }
        )

        this.getRemoveFromWishlistListener(getItem, callback = callback).fold(
            onSuccess = { compositeListener ->
                compositeListener.addListener(onClickListener)
                btn.setOnClickListener(compositeListener)
            },
            onFailure = { ex ->
                throw ex
            }

        )
    } catch (ex: Exception) {
        callback(Result.failure(ex))
    }
}

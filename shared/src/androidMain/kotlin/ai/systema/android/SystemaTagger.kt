package ai.systema.android

import ai.systema.constants.SystemaTags
import ai.systema.model.SystemaClientID
import ai.systema.model.index.Product
import android.view.View

internal object SystemaTagger {
    private const val missingTagMapMsg = "is not in tagMapping"

    internal fun setObserved(v: View, tagMapping: Map<String, Int>, flag: Boolean): View {
        val keyObserved = tagMapping[SystemaTags.Observed]
            ?: throw IllegalArgumentException("'${SystemaTags.Observed}' $missingTagMapMsg")
        v.setTag(keyObserved, flag.toString())
        return v
    }

    internal fun setVisible(v: View, tagMapping: Map<String, Int>, flag: Boolean): View {
        val keyVisible = tagMapping[SystemaTags.Visible]
            ?: throw IllegalArgumentException("'${SystemaTags.Visible}' $missingTagMapMsg")
        v.setTag(keyVisible, flag.toString())
        return v
    }

    internal fun setProductId(v: View, product: Product, tagMapping: Map<String, Int>): View {
        val keyProductId = tagMapping[SystemaTags.ProductId]
            ?: throw IllegalArgumentException("'${SystemaTags.ProductId}' $missingTagMapMsg")
        v.setTag(keyProductId, product.id)
        return v
    }

    internal fun setProductPrice(v: View, product: Product, tagMapping: Map<String, Int>): View {
        val keyProductPrice = tagMapping[SystemaTags.ProductPrice]
            ?: throw IllegalArgumentException("'${SystemaTags.ProductPrice}' $missingTagMapMsg")
        v.setTag(keyProductPrice, product.price)
        return v
    }

    internal fun setProductCurrency(v: View, product: Product, tagMapping: Map<String, Int>): View {
        val keyProductCurrency = tagMapping[SystemaTags.ProductCurrency]
            ?: throw IllegalArgumentException("'${SystemaTags.ProductCurrency}' $missingTagMapMsg")
        v.setTag(keyProductCurrency, product.currency)
        return v
    }

    internal fun setRecId(v: View, product: Product, tagMapping: Map<String, Int>): View {
        val keyRecId = tagMapping[SystemaTags.RecId]
            ?: throw IllegalArgumentException("'${SystemaTags.RecId}' $missingTagMapMsg")
        v.setTag(keyRecId, product.recId)
        return v
    }

    internal fun setReferrerUrl(v: View, referrerUrl: String, tagMapping: Map<String, Int>): View {
        val keyReferrerId = tagMapping[SystemaTags.ReferrerUrl]
            ?: throw IllegalArgumentException("'${SystemaTags.ReferrerUrl}' $missingTagMapMsg")
        v.setTag(keyReferrerId, referrerUrl)
        return v
    }

    internal fun setProductUrl(v: View, product: Product, tagMapping: Map<String, Int>, clientID: SystemaClientID): View {
        val keyProductUrl = tagMapping[SystemaTags.ProductUrl]
            ?: throw IllegalArgumentException("'${SystemaTags.ProductUrl}' $missingTagMapMsg")

        var productUrl = product.link
        if (!product.link.startsWith("http")) {
            // If it is a relative URL, we append to a mock baseURL
            productUrl = "https://$clientID.apps.systema.ai/${product.link.trimStart('/')}"
        }

        v.setTag(keyProductUrl, productUrl)

        return v
    }

    internal fun setContainerUrl(v: View, containerId: String, tagMapping: Map<String, Int>, clientID: SystemaClientID): View {
        val keyUrl = tagMapping[SystemaTags.ContainerUrl]
            ?: throw IllegalArgumentException("'${SystemaTags.ContainerUrl}' $missingTagMapMsg")

        // Note: We need a proper URL, and therefore we create a mock URL for the container based on the resultId
        v.setTag(
            keyUrl,
            "https://$clientID.apps.systema.ai/containers/$containerId"
        )

        return v
    }

    internal fun setResultId(v: View, resultId: String, tagMapping: Map<String, Int>): View {
        val keyResultId = tagMapping[SystemaTags.ResultId]
            ?: throw IllegalArgumentException("'${SystemaTags.ResultId}' $missingTagMapMsg")

        v.setTag(keyResultId, resultId)

        // when a new result Id is set, set the observed to be false
        if (v.hasSystemaTag(SystemaTags.Observed, tagMapping) &&
            v.getSystemaTagVal(SystemaTags.Observed, tagMapping) == true.toString()
        ) {
            setObserved(v, tagMapping, false)
        }

        return v
    }

    internal fun getProductId(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.ProductId, tagMapping)
    }

    internal fun getProductPrice(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.ProductPrice, tagMapping)
    }

    internal fun getProductCurrency(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.ProductCurrency, tagMapping)
    }

    internal fun getRecId(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.RecId, tagMapping)
    }

    internal fun getReferrerUrl(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.ReferrerUrl, tagMapping)
    }

    internal fun getProductUrl(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.ProductUrl, tagMapping)
    }

    internal fun getResultId(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.ResultId, tagMapping)
    }

    internal fun getObserved(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.Observed, tagMapping)
    }

    internal fun getVisible(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.Visible, tagMapping)
    }

    internal fun getContainerUrl(view: View?, tagMapping: Map<String, Int>): String? {
        if (view == null) {
            return null
        }

        return view.getSystemaTagVal(SystemaTags.ContainerUrl, tagMapping)
    }
}

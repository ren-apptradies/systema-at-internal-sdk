package ai.systema.android

import ai.systema.helper.SystemaDeviceManager
import ai.systema.model.SystemaDevice
import ai.systema.model.toUserAgent
import android.content.Context
import android.os.Build

internal class AndroidDeviceManager(private val context: Context) : SystemaDeviceManager {
    // private variables to cache the values
    private var device: SystemaDevice? = null
    private var userAgentName: String? = null

    override fun getDeviceInfo(): SystemaDevice {
        if (this.device == null) {
            this.device = SystemaDevice(
                osVersion = System.getProperty("os.version"),
                model = Build.MODEL,
                deviceId = Build.ID,
                deviceName = Build.DEVICE,
                fingerprint = Build.FINGERPRINT,
                release = Build.VERSION.RELEASE,
                product = Build.PRODUCT,
                brand = Build.BRAND,
                display = Build.DISPLAY,
                manufacturer = Build.MANUFACTURER,
            )
        }

        return device as SystemaDevice
    }

    /**
     * Get UserAgent String
     *
     * TODO: Is there a better way to generate the UserAgent string similar to
     * what browser would return?
     */
    override fun getUserAgent(): String {
        if (userAgentName == null) {
            userAgentName = getDeviceInfo().toUserAgent()
        }

        return userAgentName as String
    }
}

package ai.systema.android

import ai.systema.helper.internal.JsonNoDefaults
import ai.systema.model.SystemaDevice
import android.content.Context
import io.ktor.util.InternalAPI
import io.ktor.util.encodeBase64
import io.mockk.mockk
import kotlin.test.Test
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

internal class AndroidDeviceManagerTest {

    @Test
    fun testGetDeviceInfo() {
        val context: Context = mockk()
        val deviceManager = AndroidDeviceManager(context)
        val device = deviceManager.getDeviceInfo()
        assertNotNull(device.osVersion)
    }

    @OptIn(InternalAPI::class)
    @Test
    fun testGetUserAgent() {
        val context: Context = mockk()
        val deviceManager = AndroidDeviceManager(context)
        val device = deviceManager.getDeviceInfo()
        val userAgent = deviceManager.getUserAgent()
        assertNotNull(userAgent)
        assertTrue { userAgent.contains(device.osVersion!!) }

        assertTrue {
            userAgent.contains(
                JsonNoDefaults.encodeToString(SystemaDevice.serializer(), device).encodeBase64()
            )
        }
    }
}

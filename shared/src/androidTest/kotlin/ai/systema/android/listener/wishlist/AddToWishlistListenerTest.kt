package ai.systema.android.listener.wishlist

import ai.systema.android.listener.getSystemaDefaultMock
import ai.systema.android.listener.testTagMapping
import ai.systema.exception.SystemaListenerException
import ai.systema.helper.BaseListenerTest
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

@ExperimentalCoroutinesApi
internal class AddToWishlistListenerTest : BaseListenerTest() {
    @Test
    fun testOnClickFailures() {
        // prepare systema mock
        val systemaMock = getSystemaDefaultMock()

        // no productId tag with button should trigger IllegalArgumentException
        val errBtn = getMockButtonWithoutProductId()
        AddToWishlistListener(systemaMock, getWishlistItems, testTagMapping, dispatcher = testDispatcher) {
            it.onSuccess { resp ->
                assertNull(resp) // don't expect
            }
            it.onFailure { ex ->
                assertEquals(SystemaListenerException::class.simpleName, ex::class.simpleName)
                assertEquals(IllegalArgumentException::class.simpleName, ex.cause!!::class.simpleName)
            }
        }.onClick(errBtn)

        // get btn mock with all tags set
        val btn = getMockButton()

        // if tracker throws exception, it should fail with SystemaEventFlowException
        coEvery {
            systemaMock.trackWishlistAcquired(
                productId,
                wishItems,
                productUrl,
                referrerUrl,
            )
        } answers {
            Result.failure(IllegalArgumentException("test"))
        }

        AddToWishlistListener(systemaMock, getWishlistItems, testTagMapping, dispatcher = testDispatcher) {
            it.onSuccess { resp ->
                assertNull(resp)
            }
            it.onFailure { ex ->
                assertEquals(SystemaListenerException::class.simpleName, ex::class.simpleName)
                assertEquals(IllegalArgumentException::class.simpleName, ex.cause!!::class.simpleName)
            }
        }.onClick(btn)
    }

    @Test
    fun testOnClick() {
        val systema = getSystemaDefaultMock()
        val btn = getMockButton()

        coEvery {
            systema.trackWishlistAcquired(
                productId,
                wishItems,
                productUrl,
                referrerUrl,
            )
        } answers {
            val resp: HttpResponse = mockk()
            every { resp.status } returns HttpStatusCode.NoContent
            Result.success(resp)
        }

        AddToWishlistListener(systema, getWishlistItems, testTagMapping, dispatcher = testDispatcher) {
            it.onSuccess { resp ->
                assertNotNull(resp)
                assertEquals(HttpStatusCode.NoContent, resp.status)
            }
            it.onFailure { ex ->
                assertNull(ex)
            }
        }.onClick(btn)
    }
}

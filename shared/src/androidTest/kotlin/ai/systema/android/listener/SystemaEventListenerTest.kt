package ai.systema.android.listener

import ai.systema.testSuspend
import android.view.View
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import kotlin.test.Test
import kotlin.test.assertEquals

internal class SystemaEventListenerTest {

    @Test
    fun testOnClick() = testSuspend {
        val (systema, tagMapping) = getConfig(
            mapOf(
                "systema_product_id" to 0
            )
        )

        val mockView = mockk<View>()
        every { mockView.getTag(0) } returns "product_id"
        every { mockView.id } returns 0

        val mockSystemaEventListener = spyk(SystemaEventListener(systema, tagMapping) {}, recordPrivateCalls = true)
        every {
            mockSystemaEventListener["sendItemClickedEvent"](
                any<String>(),
                any<String>(),
                any<String>(),
                any<String>()
            ) as Unit
        } just Runs
        mockSystemaEventListener.onClick(mockView)
        verify {
            mockSystemaEventListener["sendItemClickedEvent"]("product_id", any<String>(), any<String>(), any<String>())
        }

        // Confirm that the listener does not fail when no View is provided.
        val systemaEventListener = SystemaEventListener(systema, tagMapping) {}
        assertEquals(systemaEventListener.onClick(null), kotlin.Unit)
    }

    @Test
    fun testOnLayoutChange() = testSuspend {
        val (systema, tagMapping) = getConfig(
            mapOf(
                "systema_result_id" to 0
            )
        )

        val mockView = mockk<View>(relaxed = true)
        every { mockView.getTag(0) } returns "result_id"
        every { mockView.id } returns 0

        // TODO: View.isVisible() has call to Resources which needs to be mocked; without which isVisible() will end up in nullpointerexception.

        val mockSystemaEventListener = spyk(SystemaEventListener(systema, tagMapping) {}, recordPrivateCalls = true)
        every {
            mockSystemaEventListener["sendContainerShownEvent"](any<View>(), any<String>()) as Unit
        } just Runs
        mockSystemaEventListener.onLayoutChange(
            v = mockView,
            left = 1,
            top = 1,
            right = 1,
            bottom = 1,
            oldLeft = 1,
            oldTop = 1,
            oldRight = 1,
            oldBottom = 1
        )

        // Uncomment after TODO is fixed.
        //        verify {
        //            mockSystemaEventListener["sendContainerShownEvent"](mockView, any<String>())
        //        }
    }

    @Test
    fun testOnViewAttachedToWindow() = testSuspend {
        val (systema, tagMapping) = getConfig(
            mapOf(
                "systema_product_id" to 0
            )
        )

        val mockView = mockk<View>()
        every { mockView.getTag(0) } returns "product_id"
        every { mockView.id } returns 0

        val mockSystemaEventListener = spyk(SystemaEventListener(systema, tagMapping) {}, recordPrivateCalls = true)
        every {
            mockSystemaEventListener["sendPageViewEvent"](
                any<String>(),
                any<String>(),
                any<String>(),
                any<String>()
            ) as Unit
        } just Runs
        mockSystemaEventListener.onViewAttachedToWindow(mockView)
        verify {
            mockSystemaEventListener["sendPageViewEvent"]("product_id", any<String>(), any<String>(), any<String>())
        }

        // Confirm that the listener does not fail when no View is provided.
        val systemaEventListener = SystemaEventListener(systema, tagMapping) {}
        assertEquals(systemaEventListener.onViewAttachedToWindow(null), kotlin.Unit)
    }
}

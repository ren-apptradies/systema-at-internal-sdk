package ai.systema.android.listener

import ai.systema.testSuspend
import android.view.View
import io.mockk.mockkClass
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

internal class SystemaCompositeListenerTest {

    @Test
    fun testAddListener() = testSuspend {
        val mockListener = mockkClass(View.OnClickListener::class)
        var compositeListener = SystemaCompositeListener<View.OnClickListener>()
        compositeListener.addListener(mockListener)
        assertEquals(1, compositeListener.getListeners().size)

        val listeners = compositeListener.getListeners()
        compositeListener.addListener(null)
        assertContentEquals(listeners, compositeListener.getListeners())
    }

    @Test
    fun testAddListeners() = testSuspend {
        val mockListeners = arrayOf(
            mockkClass(View.OnClickListener::class),
            mockkClass(View.OnClickListener::class),
        )
        var compositeListener = SystemaCompositeListener<View.OnClickListener>()
        compositeListener.addListeners(mockListeners)
        assertEquals(2, compositeListener.getListeners().size)

        val listeners = compositeListener.getListeners()
        compositeListener.addListener(null)
        assertContentEquals(listeners, compositeListener.getListeners())
    }

    @Test
    fun testRemoveListener() = testSuspend {
        val mockListeners = arrayOf(
            mockkClass(View.OnClickListener::class),
            mockkClass(View.OnClickListener::class),
        )
        var compositeListener = SystemaCompositeListener<View.OnClickListener>()
        compositeListener.addListeners(mockListeners)
        assertEquals(2, compositeListener.getListeners().size)
        compositeListener.removeListener()
        assertEquals(1, compositeListener.getListeners().size)
        compositeListener.removeListener()
        assertEquals(0, compositeListener.getListeners().size)

        // further remove will have no effect
        compositeListener.removeListener()
        compositeListener.removeListener()
        assertEquals(0, compositeListener.getListeners().size)

        // remove the first one
        compositeListener.addListeners(mockListeners)
        assertEquals(2, compositeListener.getListeners().size)
        compositeListener.removeListener(0)
        assertEquals(1, compositeListener.getListeners().size)
    }
}

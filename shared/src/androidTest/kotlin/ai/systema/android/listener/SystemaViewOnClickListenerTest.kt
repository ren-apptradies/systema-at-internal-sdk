package ai.systema.android.listener

import ai.systema.testSuspend
import android.view.View
import io.mockk.mockk
import io.mockk.mockkClass
import io.mockk.verify
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class SystemaViewOnClickListenerTest {

    @Test
    fun testAddOnClickListener() = testSuspend {
        val (systema, tagMapping) = getConfig()

        val mockListener =
            mockkClass(View.OnClickListener::class)
        var compositeListener = SystemaViewOnClickListener(systema, tagMapping) {}
        assertEquals(1, compositeListener.getListeners().size)

        compositeListener.addListener(mockListener)
        assertEquals(2, compositeListener.getListeners().size)
        assertTrue { compositeListener.getListeners()[0] is SystemaEventListener }

        val listeners = compositeListener.getListeners()
        compositeListener.addListener(null)
        assertContentEquals(listeners, compositeListener.getListeners())
    }

    @Test
    fun testOnClick() = testSuspend {
        val (systema, tagMapping) = getConfig()

        val systemaViewOnClickListener = SystemaViewOnClickListener(systema, tagMapping) {}
        assertEquals(systemaViewOnClickListener.onClick(null), kotlin.Unit)

        val mockView = mockk<View>(relaxed = true)
        systemaViewOnClickListener.onClick(mockView)
        verify(exactly = 1) {
            SystemaEventListener(systema, tagMapping) {}.onClick(mockView)
        }
    }
}

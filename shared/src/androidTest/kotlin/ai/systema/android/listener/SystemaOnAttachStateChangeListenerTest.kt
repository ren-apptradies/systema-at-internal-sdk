package ai.systema.android.listener

import ai.systema.testSuspend
import android.view.View
import io.mockk.mockk
import io.mockk.mockkClass
import io.mockk.verify
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class SystemaOnAttachStateChangeListenerTest {

    @Test
    fun testAddOnAttachStateChangeListener() = testSuspend {
        val (systema, tagMapping) = getConfig()

        val mockOnAttachStateChangeListener = mockkClass(View.OnAttachStateChangeListener::class)
        var compositeListener = SystemaOnAttachStateChangeListener(systema, tagMapping) {}
        compositeListener.addListener(mockOnAttachStateChangeListener)
        assertEquals(2, compositeListener.getListeners().size)
        assertTrue { compositeListener.getListeners()[0] is SystemaEventListener }

        val listeners = compositeListener.getListeners()
        compositeListener.addListener(null)
        assertContentEquals(listeners, compositeListener.getListeners())
    }

    @Test
    fun testOnViewAttachedToWindow() = testSuspend {
        val (systema, tagMapping) = getConfig()

        val systemaOnAttachStateChangeListener = SystemaOnAttachStateChangeListener(systema, tagMapping) {}
        val onViewAttachedToWindowWithNullView = systemaOnAttachStateChangeListener.onViewAttachedToWindow(null)
        assertEquals(onViewAttachedToWindowWithNullView, kotlin.Unit)

        val mockView = mockk<View>(relaxed = true)
        systemaOnAttachStateChangeListener.onViewAttachedToWindow(mockView)
        verify(atLeast = 1) {
            SystemaEventListener(systema, tagMapping) {}.onViewAttachedToWindow(mockView)
        }
    }

    @Test
    fun testOnViewDetachedFromWindow() = testSuspend {
        val (systema, tagMapping) = getConfig()

        val systemaOnAttachStateChangeListener = SystemaOnAttachStateChangeListener(systema, tagMapping) {}
        val onViewDetachedToWindowWithNullView = systemaOnAttachStateChangeListener.onViewDetachedFromWindow(null)
        assertEquals(onViewDetachedToWindowWithNullView, kotlin.Unit)

        val mockView = mockk<View>(relaxed = true)
        systemaOnAttachStateChangeListener.onViewDetachedFromWindow(mockView)
        verify(exactly = 1) {
            SystemaEventListener(systema, tagMapping) {}.onViewDetachedFromWindow(mockView)
        }
    }
}

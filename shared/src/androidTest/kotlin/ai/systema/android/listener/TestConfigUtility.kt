package ai.systema.android.listener

import ai.systema.TestConfig
import ai.systema.client.SystemaAI
import ai.systema.client.internal.SystemaImpl
import ai.systema.configuration.Configuration
import ai.systema.connection.internal.Connector
import ai.systema.constants.SystemaConstants
import ai.systema.constants.SystemaTags
import ai.systema.helper.internal.httpClientEngine
import ai.systema.helper.logging.SystemaLogLevel
import io.ktor.client.engine.HttpClientEngine
import io.mockk.every
import io.mockk.mockk

internal val testTagMapping = mapOf(
    SystemaTags.ProductId to 0,
    SystemaTags.ProductUrl to 1,
    SystemaTags.ProductPrice to 2,
    SystemaTags.ProductCurrency to 3,
    SystemaTags.ContainerUrl to 4,
    SystemaTags.ResultId to 5,
    SystemaTags.RecId to 6,
    SystemaTags.ReferrerUrl to 7,
    SystemaTags.Observed to 8,
    SystemaTags.Visible to 9,
)

internal fun getTestTagId(key: String): Int {
    return testTagMapping[key] ?: throw IllegalArgumentException("$key does not exist in the mapping")
}

internal fun getSystemaDefaultMock(): SystemaAI {
    val systemaMock = mockk<SystemaAI>()
    every { systemaMock.logLevel } returns SystemaLogLevel.DEBUG
    return systemaMock
}

internal suspend fun getSystemaForTest(engine: HttpClientEngine = httpClientEngine): SystemaAI {
    val credentials = TestConfig.getCredentials()
    val systema = SystemaImpl(
        Connector(
            Configuration(
                credentials = credentials,
                engine = engine,
                logLevel = SystemaLogLevel.DEBUG,
                kvStore = TestConfig.getSystemaKVStore(),
                deviceManager = TestConfig.getDeviceManager(),
            ),
        ),
        meta = mapOf(
            SystemaConstants.SystemaTagMapping to testTagMapping
        ),
    )

    systema.initialize()
    return systema
}

internal data class TestConfigUtility(val systema: SystemaAI, val tagMapping: Map<String, Int> = mapOf("one" to 1))

// TODO refactor
// [LM]: We could use the getSystemaForTest() instead. However, modifying it is causing some strange test errors
// in SystemaOnAttachStateChangeListenerTest, and I am just leaving as it is for now rather than refactoring too much
internal suspend fun getConfig(tagMapping: Map<String, Int>? = null): TestConfigUtility {
    val credentials = TestConfig.getCredentials()
    val meta: Map<String, String> = mapOf(
        "one" to "10K",
        "two" to "20k"
    )

    val systema = SystemaAI(
        clientID = credentials.clientID.basic,
        apiKey = credentials.apiKey.basic,
        proxyUrls = credentials.proxyUrls.entries.associate { it.key to it.value.toString() },
        kvStore = TestConfig.getSystemaKVStore(),
        meta = mapOf(
            "test" to meta,
        ),
        deviceManager = TestConfig.getDeviceManager()
    )
    if (tagMapping != null) {
        return TestConfigUtility(systema, tagMapping)
    }
    return TestConfigUtility(systema)
}

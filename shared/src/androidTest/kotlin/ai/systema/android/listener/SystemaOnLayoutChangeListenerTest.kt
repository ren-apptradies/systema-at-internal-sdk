package ai.systema.android.listener

import ai.systema.testSuspend
import android.view.View
import io.mockk.mockk
import io.mockk.mockkClass
import io.mockk.verify
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class SystemaOnLayoutChangeListenerTest {

    @Test
    fun testAddOnLayoutChangeListener() = testSuspend {
        val (systema, tagMapping) = getConfig()

        val mockListener = mockkClass(View.OnLayoutChangeListener::class)
        var compositeListener = SystemaOnLayoutChangeListener(systema, tagMapping) {}
        compositeListener.addListener(mockListener)
        assertEquals(2, compositeListener.getListeners().size)
        assertTrue { compositeListener.getListeners()[0] is SystemaEventListener }

        val listeners = compositeListener.getListeners()
        compositeListener.addListener(null)
        assertContentEquals(listeners, compositeListener.getListeners())
    }

    @Test
    fun testOnLayoutChange() = testSuspend {
        val (systema, tagMapping) = getConfig()
        val systemaOnLayoutChangeListener = SystemaOnLayoutChangeListener(systema, tagMapping) {}
        val onChangeListenerWithNullView = systemaOnLayoutChangeListener.onLayoutChange(
            v = null, left = 1, top = 1, right = 1, bottom = 1,
            oldLeft = 1, oldTop = 1, oldRight = 1, oldBottom = 1
        )
        assertEquals(onChangeListenerWithNullView, kotlin.Unit)

        val mockView = mockk<View>(relaxed = true)
        systemaOnLayoutChangeListener.onLayoutChange(
            v = mockView, left = 1, top = 1, right = 1, bottom = 1,
            oldLeft = 1, oldTop = 1, oldRight = 1, oldBottom = 1
        )
        verify(exactly = 1) {
            SystemaEventListener(systema, tagMapping) {}.onLayoutChange(
                v = mockView, left = 1, top = 1, right = 1, bottom = 1,
                oldLeft = 1, oldTop = 1, oldRight = 1, oldBottom = 1
            )
        }
    }
}

package ai.systema.android

import ai.systema.android.listener.getTestTagId
import ai.systema.android.listener.testTagMapping
import ai.systema.constants.Currency
import ai.systema.constants.SystemaTags
import ai.systema.helper.toClientID
import ai.systema.model.index.Product
import android.view.View
import io.mockk.every
import io.mockk.mockk
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

internal class SystemaTaggerTest {
    private val clientID = "client01".toClientID()
    private val product: Product = Product(
        id = "24-MB04",
        image = "no-image",
        link = "/product/24-MB04",
        title = "Product",
        recId = "rec-id",
        price = 10.0,
        currency = Currency.AUD.value,
    )

    @Test
    fun testTagObserved() {
        val tagId = getTestTagId(SystemaTags.Observed)
        val observed = true
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setObserved(mockView, mapOf(), observed)
        }

        every { mockView.setTag(tagId, observed.toString()) } returns Unit
        SystemaTagger.setObserved(mockView, testTagMapping, observed)

        assertNull(SystemaTagger.getObserved(null, testTagMapping))

        every { mockView.getTag(tagId) } returns observed.toString()
        assertEquals(observed.toString(), SystemaTagger.getObserved(mockView, testTagMapping))
    }

    @Test
    fun testTagVisible() {
        val tagId = getTestTagId(SystemaTags.Visible)
        val visible = true
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setVisible(mockView, mapOf(), visible)
        }

        every { mockView.setTag(tagId, visible.toString()) } returns Unit
        SystemaTagger.setVisible(mockView, testTagMapping, visible)

        assertNull(SystemaTagger.getVisible(null, testTagMapping))

        every { mockView.getTag(tagId) } returns visible.toString()
        assertEquals(visible.toString(), SystemaTagger.getVisible(mockView, testTagMapping))
    }

    @Test
    fun testTagProductId() {
        val tagId = getTestTagId(SystemaTags.ProductId)
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setProductId(mockView, product, mapOf())
        }

        every { mockView.setTag(tagId, product.id) } returns Unit
        SystemaTagger.setProductId(mockView, product, testTagMapping)

        assertNull(SystemaTagger.getProductId(null, testTagMapping))

        every { mockView.getTag(tagId) } returns product.id
        assertEquals(product.id, SystemaTagger.getProductId(mockView, testTagMapping))
    }

    @Test
    fun testTagProductPrice() {
        val tagId = getTestTagId(SystemaTags.ProductPrice)
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setProductPrice(mockView, product, mapOf())
        }

        every { mockView.setTag(tagId, product.price) } returns Unit
        SystemaTagger.setProductPrice(mockView, product, testTagMapping)

        assertNull(SystemaTagger.getProductPrice(null, testTagMapping))

        every { mockView.getTag(tagId) } returns product.price.toString()
        assertEquals(product.price.toString(), SystemaTagger.getProductPrice(mockView, testTagMapping))
    }

    @Test
    fun testTagProductCurrency() {
        val tagId = getTestTagId(SystemaTags.ProductCurrency)
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setProductCurrency(mockView, product, mapOf())
        }

        every { mockView.setTag(tagId, product.currency) } returns Unit
        SystemaTagger.setProductCurrency(mockView, product, testTagMapping)

        assertNull(SystemaTagger.getProductCurrency(null, testTagMapping))

        every { mockView.getTag(tagId) } returns product.currency
        assertEquals(product.currency, SystemaTagger.getProductCurrency(mockView, testTagMapping))
    }

    @Test
    fun testTagRecId() {
        val tagId = getTestTagId(SystemaTags.RecId)
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setRecId(mockView, product, mapOf())
        }

        every { mockView.setTag(tagId, product.recId) } returns Unit
        SystemaTagger.setRecId(mockView, product, testTagMapping)

        assertNull(SystemaTagger.getRecId(null, testTagMapping))

        every { mockView.getTag(tagId) } returns product.recId
        assertEquals(product.recId, SystemaTagger.getRecId(mockView, testTagMapping))
    }

    @Test
    fun testTagReferrerUrl() {
        val tagId = getTestTagId(SystemaTags.ReferrerUrl)
        val referrer = "https://example.com"
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setReferrerUrl(mockView, referrer, mapOf())
        }

        every { mockView.setTag(tagId, referrer) } returns Unit
        SystemaTagger.setReferrerUrl(mockView, referrer, testTagMapping)

        assertNull(SystemaTagger.getReferrerUrl(null, testTagMapping))

        every { mockView.getTag(tagId) } returns referrer
        assertEquals(referrer, SystemaTagger.getReferrerUrl(mockView, testTagMapping))
    }

    @Test
    fun testTagProductUrlRelative() {
        val tagId = getTestTagId(SystemaTags.ProductUrl)
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setProductUrl(mockView, product, mapOf(), clientID)
        }

        // relative product url will be appended to a mock base URL
        val productUrl = "https://$clientID.apps.systema.ai/${product.link.trimStart('/')}"
        every { mockView.setTag(tagId, productUrl) } returns Unit
        SystemaTagger.setProductUrl(mockView, product, testTagMapping, clientID)

        assertNull(SystemaTagger.getProductUrl(null, testTagMapping))

        every { mockView.getTag(tagId) } returns productUrl
        assertEquals(productUrl, SystemaTagger.getProductUrl(mockView, testTagMapping))
    }

    @Test
    fun testTagProductUrlAbsolute() {
        val tagId = getTestTagId(SystemaTags.ProductUrl)
        val mockView = mockk<View>()

        // full product url will be set and retrieved as it is
        val product2 = Product(
            id = "24-MB04",
            image = "no-image",
            link = "https://example.com/product/24-MB04",
            title = "Product",
            recId = "rec-id",
            price = 10.0,
            currency = Currency.AUD.value,
        )

        every { mockView.setTag(tagId, product2.link) } returns Unit
        SystemaTagger.setProductUrl(mockView, product2, testTagMapping, clientID)

        assertNull(SystemaTagger.getProductUrl(null, testTagMapping))

        every { mockView.getTag(tagId) } returns product2.link
        assertEquals(product2.link, SystemaTagger.getProductUrl(mockView, testTagMapping))
    }

    @Test
    fun testTagContainerUrl() {
        val tagId = getTestTagId(SystemaTags.ContainerUrl)
        val containerId = "c01"
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setContainerUrl(mockView, containerId, mapOf(), clientID)
        }

        // relative product url will be appended to a mock base URL
        val containerUrl = "https://$clientID.apps.systema.ai/containers/$containerId"
        every { mockView.setTag(tagId, containerUrl) } returns Unit
        SystemaTagger.setContainerUrl(mockView, containerId, testTagMapping, clientID)

        assertNull(SystemaTagger.getContainerUrl(null, testTagMapping))

        every { mockView.getTag(tagId) } returns containerUrl
        assertEquals(containerUrl, SystemaTagger.getContainerUrl(mockView, testTagMapping))
    }

    @Test
    fun testTagResultId() {
        val tagId = getTestTagId(SystemaTags.ResultId)
        val observedTagId = getTestTagId(SystemaTags.Observed)
        val resultId = "r01"
        val mockView = mockk<View>()

        // invalid tag mapping would throw exception
        assertFailsWith<IllegalArgumentException> {
            SystemaTagger.setResultId(mockView, resultId, mapOf())
        }

        every { mockView.setTag(tagId, resultId) } returns Unit
        every { mockView.getTag(observedTagId) } returns true.toString()
        every { mockView.setTag(observedTagId, false.toString()) } returns Unit
        SystemaTagger.setResultId(mockView, resultId, testTagMapping)

        assertNull(SystemaTagger.getResultId(null, testTagMapping))

        every { mockView.getTag(tagId) } returns resultId
        assertEquals(resultId, SystemaTagger.getResultId(mockView, testTagMapping))
    }
}

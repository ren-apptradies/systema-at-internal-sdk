package ai.systema.android

import ai.systema.android.listener.SystemaEventListener
import ai.systema.android.listener.getSystemaForTest
import ai.systema.android.listener.getTestTagId
import ai.systema.android.listener.testTagMapping
import ai.systema.constants.Currency
import ai.systema.constants.SystemaTags
import ai.systema.helper.RecommendationMockClient
import ai.systema.model.index.Product
import ai.systema.model.tracker.cart.CartItem
import ai.systema.model.tracker.wishlist.WishlistItem
import ai.systema.testSuspend
import android.view.View
import android.widget.Button
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

internal class SystemaAITest {
    companion object {
        // mock values for this test
        val product: Product = Product(
            id = "24-MB02",
            image = "http://sdk.systema.ai/products/24-MB02.jpg",
            title = "Product",
            recId = "recID",
            link = "http://sdk.systema.ai/products/24-MB02",
            price = 10.0,
            currency = Currency.AUD.value
        )

        val relatedProducts = RecommendationMockClient.getMockResp()
    }

    private fun getMockView(): View {
        val v = mockk<View>()
        every { v.id } returns 0
        every { v.setTag(any(), any()) } returns mockk()
        every { v.getTag(getTestTagId(SystemaTags.ProductId)) } returns product.id
        every { v.getTag(getTestTagId(SystemaTags.Observed)) } returns false.toString()
        every { v.getTag(getTestTagId(SystemaTags.ResultId)) } returns relatedProducts.resultId
        every { v.getTag(getTestTagId(SystemaTags.ContainerUrl)) } returns product.link

        return v
    }

    private fun getMockBtn(): Button {
        val btn = mockk<Button>()
        every { btn.id } returns 0
        every { btn.setTag(any(), any()) } returns mockk()
        every { btn.getTag(getTestTagId(SystemaTags.ProductId)) } returns product.id
        every { btn.getTag(getTestTagId(SystemaTags.ProductUrl)) } returns product.link
        every { btn.getTag(getTestTagId(SystemaTags.ReferrerUrl)) } returns "https://app.systema/referrer"
        every { btn.setOnClickListener(any()) } returns mockk()

        return btn
    }

    @Test
    fun addProductTags() = testSuspend {
        val systema = getSystemaForTest()

        // fail
        val btn = getMockBtn()
        every { btn.setTag(any(), any()) } throws IllegalArgumentException("test")
        systema.addProductTags(btn, product).fold(
            onSuccess = {
                assertNull(it)
            },
            onFailure = { ex ->
                assertEquals("test", ex.message)
            }
        )

        // succeed
        every { btn.setTag(any(), any()) } returns Unit
        systema.addProductTags(btn, product, tagMapping = testTagMapping).fold(
            onSuccess = {
                assertNotNull(it)
            },
            onFailure = { ex ->
                assertNull(ex)
            }
        )
    }

    @Test
    fun addContainerTags() = testSuspend {
        val systema = getSystemaForTest()

        // fail
        val v = getMockView()
        every { v.setTag(any(), any()) } throws IllegalArgumentException("test")
        systema.addContainerTags(v, relatedProducts).fold(
            onSuccess = {
                assertNull(it)
            },
            onFailure = { ex ->
                assertEquals("test", ex.message)
            }
        )

        // succeed
        every { v.setTag(any(), any()) } returns Unit
        systema.addContainerTags(v, relatedProducts, tagMapping = testTagMapping).fold(
            onSuccess = {
                assertNotNull(it)
            },
            onFailure = { ex ->
                assertNull(ex)
            }
        )
    }

    @Test
    fun addOnClickListener() = testSuspend {
        val systema = getSystemaForTest()

        // fail
        val v = getMockView()
        every { v.setOnClickListener(any()) } throws IllegalArgumentException("test")
        systema.addOnClickListener(v, callback = {}).fold(
            onSuccess = {
                assertNull(it)
            },
            onFailure = { ex ->
                assertEquals("test", ex.message)
            }
        )

        // succeed
        every { v.setOnClickListener(any()) } just Runs
        systema.addOnClickListener(v, callback = {}).fold(
            onSuccess = { li ->
                assertNotNull(li)
                assertEquals(1, li.getListeners().size)
                assertTrue { li.getListeners()[0] is SystemaEventListener }
            },
            onFailure = { ex ->
                assertNull(ex)
            }
        )
    }

    @Test
    fun addOnLayoutChangeListener() = testSuspend {
        val systema = getSystemaForTest()

        // fail
        val v = getMockView()
        every { v.addOnLayoutChangeListener(any()) } throws IllegalArgumentException("test")
        systema.addOnLayoutChangeListener(v, callback = {}).fold(
            onSuccess = {
                assertNull(it)
            },
            onFailure = { ex ->
                assertEquals("test", ex.message)
            }
        )

        // succeed
        every { v.addOnLayoutChangeListener(any()) } just Runs
        systema.addOnLayoutChangeListener(v, callback = {}).fold(
            onSuccess = { li ->
                assertNotNull(li)
                assertEquals(1, li.getListeners().size)
                assertTrue { li.getListeners()[0] is SystemaEventListener }
            },
            onFailure = { ex ->
                assertNull(ex)
            }
        )
    }

    @Test
    fun addOnAttachStateChangeListener() = testSuspend {
        val systema = getSystemaForTest()

        // fail
        val v = getMockView()
        every { v.addOnAttachStateChangeListener(any()) } throws IllegalArgumentException("test")
        systema.addOnAttachStateChangeListener(v, callback = {}).fold(
            onSuccess = {
                assertNull(it)
            },
            onFailure = { ex ->
                assertEquals("test", ex.message)
            }
        )

        // succeed
        every { v.addOnAttachStateChangeListener(any()) } just Runs
        systema.addOnAttachStateChangeListener(v, callback = {}).fold(
            onSuccess = { li ->
                assertNotNull(li)
                assertEquals(1, li.getListeners().size)
                assertTrue { li.getListeners()[0] is SystemaEventListener }
            },
            onFailure = { ex ->
                assertNull(ex)
            }
        )
    }

    @Test
    fun monitorRecContainer() = testSuspend {
        val systema = getSystemaForTest()
        val tagMapping = testTagMapping

        // fail in tagger
        var v = getMockView()
        every { v.setTag(any(), any()) } throws IllegalArgumentException("test")
        systema.monitorRecContainer(v, relatedProducts) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // fail in setting listener
        v = getMockView()
        every { v.addOnLayoutChangeListener(any()) } throws IllegalArgumentException("test")
        systema.monitorRecContainer(v, relatedProducts) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // succeed
        v = getMockView()
        every { v.addOnLayoutChangeListener(any()) } just runs
        systema.monitorRecContainer(v, relatedProducts) {
            it.onSuccess {
                assertEquals(relatedProducts.resultId, SystemaTagger.getResultId(v, tagMapping))
            }

            it.onFailure { ex ->
                assertNull(ex)
            }
        }
    }

    @Test
    fun monitorProductPage() = testSuspend {
        val systema = getSystemaForTest()
        val tagMapping = testTagMapping

        // fail in tagger
        var v = getMockView()
        every { v.setTag(any(), any()) } throws IllegalArgumentException("test")
        systema.monitorProductPage(v, product) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // fail in setting listener
        v = getMockView()
        every { v.addOnAttachStateChangeListener(any()) } throws IllegalArgumentException("test")
        systema.monitorProductPage(v, product) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // succeed
        v = getMockView()
        every { v.addOnAttachStateChangeListener(any()) } just runs
        systema.monitorProductPage(v, product) {
            it.onSuccess {
                assertEquals(product.id, SystemaTagger.getProductId(v, tagMapping))
                assertNotNull(SystemaTagger.getProductUrl(v, tagMapping))
                assertNotNull(SystemaTagger.getProductPrice(v, tagMapping))
                assertNotNull(SystemaTagger.getProductCurrency(v, tagMapping))
            }

            it.onFailure { ex ->
                assertNull(ex)
            }
        }
    }

    @Test
    fun monitorRecItem() = testSuspend {
        val systema = getSystemaForTest()
        val tagMapping = testTagMapping

        // fail in tagger
        var v = getMockView()
        every { v.setTag(any(), any()) } throws IllegalArgumentException("test")
        systema.monitorRecItem(v, product) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // fail in setting listener
        v = getMockView()
        every { v.setOnClickListener(any()) } throws IllegalArgumentException("test")
        systema.monitorRecItem(v, product) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // succeed
        v = getMockView()
        every { v.setOnClickListener(any()) } just runs
        systema.monitorRecItem(v, product) {
            it.onSuccess {
                assertEquals(product.id, SystemaTagger.getProductId(v, tagMapping))
                assertNotNull(SystemaTagger.getProductUrl(v, tagMapping))
                assertNotNull(SystemaTagger.getProductPrice(v, tagMapping))
                assertNotNull(SystemaTagger.getProductCurrency(v, tagMapping))
            }

            it.onFailure { ex ->
                assertNull(ex)
            }
        }
    }

    @Test
    fun monitorAddToCartButton() = testSuspend {
        val systema = getSystemaForTest()
        val tagMapping = testTagMapping

        // create the callback function
        val items: List<CartItem> =
            listOf(
                CartItem(
                    itemId = product.id,
                    quantity = 2,
                    price = 10.0,
                    currency = Currency.AUD
                )
            )
        val getItems: () -> List<CartItem> = { items }

        // fail in tagger
        var btn = getMockBtn()
        every { btn.setTag(any(), any()) } throws IllegalArgumentException("test")
        systema.monitorAddToCartButton(btn, product, getItems) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // succeed
        btn = getMockBtn()
        systema.monitorAddToCartButton(btn, product, getItems) {
            it.onSuccess {
                assertEquals(product.id, SystemaTagger.getProductId(btn, tagMapping))
            }

            it.onFailure { ex ->
                assertNull(ex)
            }
        }
    }

    @Test
    fun monitorRemoveFromCartButton() = testSuspend {
        val systema = getSystemaForTest()
        val tagMapping = testTagMapping

        // create the callback function
        val item = CartItem(
            itemId = product.id,
            quantity = 2,
            price = 10.0,
            currency = Currency.AUD
        )
        val getItem: () -> CartItem = { item }

        // fail in tagger
        var btn = getMockBtn()
        every { btn.setTag(any(), any()) } throws IllegalArgumentException("test")
        systema.monitorRemoveFromCartButton(btn, product, getItem) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // succeed
        btn = getMockBtn()
        systema.monitorRemoveFromCartButton(btn, product, getItem) {
            it.onSuccess {
                assertEquals(product.id, SystemaTagger.getProductId(btn, tagMapping))
            }

            it.onFailure { ex ->
                assertNull(ex)
            }
        }
    }

    @Test
    fun monitorAddToWishlistButton() = testSuspend {
        val systema = getSystemaForTest()
        val tagMapping = testTagMapping

        // create the callback function
        val items: List<WishlistItem> =
            listOf(
                WishlistItem(
                    itemId = product.id,
                )
            )
        val getItems: () -> List<WishlistItem> = { items }

        // fail in tagger
        var btn = getMockBtn()
        every { btn.setTag(any(), any()) } throws IllegalArgumentException("test")
        systema.monitorAddToWishlistButton(btn, product, getItems) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // succeed
        btn = getMockBtn()
        systema.monitorAddToWishlistButton(btn, product, getItems) {
            it.onSuccess {
                assertEquals(product.id, SystemaTagger.getProductId(btn, tagMapping))
            }

            it.onFailure { ex ->
                assertNull(ex)
            }
        }
    }

    @Test
    fun monitorRemoveFromWishlistButton() = testSuspend {
        val systema = getSystemaForTest()
        val tagMapping = testTagMapping

        // create the callback function
        val item = WishlistItem(
            itemId = product.id,
        )
        val getItem: () -> WishlistItem = { item }

        // fail in tagger
        var btn = getMockBtn()
        every { btn.setTag(any(), any()) } throws IllegalArgumentException("test")
        systema.monitorRemoveFromWishlistButton(btn, product, getItem) {
            it.onSuccess { resp ->
                assertNull(resp)
            }

            it.onFailure { ex ->
                assertEquals("test", ex.message)
            }
        }

        // succeed
        btn = getMockBtn()
        systema.monitorRemoveFromWishlistButton(btn, product, getItem) {
            it.onSuccess {
                assertEquals(product.id, SystemaTagger.getProductId(btn, tagMapping))
            }

            it.onFailure { ex ->
                assertNull(ex)
            }
        }
    }
}

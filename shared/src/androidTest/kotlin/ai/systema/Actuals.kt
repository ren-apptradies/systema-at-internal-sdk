package ai.systema

import ai.systema.android.AndroidDeviceManager
import ai.systema.configuration.Configuration
import ai.systema.configuration.Credentials
import ai.systema.configuration.SystemaKVStore
import ai.systema.configuration.internal.SystemaInMemStorage
import ai.systema.connection.internal.Connector
import ai.systema.connection.internal.hosts
import ai.systema.enums.EndpointType
import ai.systema.enums.EnvironmentType
import ai.systema.helper.SystemaDeviceManager
import ai.systema.helper.logging.SystemaLogLevel
import ai.systema.model.SystemaAPIKey
import ai.systema.model.SystemaClientID
import android.content.Context
import io.ktor.client.engine.HttpClientEngine
import io.ktor.http.Url
import io.mockk.mockk
import kotlinx.coroutines.runBlocking

internal actual fun <T> testSuspend(block: suspend () -> T) {
    runBlocking { block() }
}

internal actual object TestConfig {
    private val kvStore = SystemaInMemStorage()
    private val proxyUrl = Url(System.getenv("SYSTEMA_SDK_PROXY_URL") ?: "http://localhost")
    private val proxyApiKey = System.getenv("SYSTEMA_SDK_PROXY_API_KEY") ?: "NOT_REQUIRED"

    private val credentials = Credentials(
        clientID = SystemaClientID("unreal"),
        apiKey = SystemaAPIKey(proxyApiKey),
        environment = EnvironmentType.TEST,
        proxyUrls = mapOf(
            EndpointType.DynamicConfig to proxyUrl,
            EndpointType.Tracker to proxyUrl,
            EndpointType.Recommend to proxyUrl
        )
    )

    actual suspend fun getSystemaKVStore(): SystemaKVStore {
        return kvStore
    }

    actual suspend fun getCredentials(): Credentials {
        return credentials
    }

    actual suspend fun getDeviceManager(): SystemaDeviceManager {
        val context: Context = mockk()
        return AndroidDeviceManager(context)
    }

    actual suspend fun getConnector(mockEngine: HttpClientEngine?): Connector {
        val connector = Connector(
            Configuration(
                credentials = credentials,
                hosts = credentials.hosts,
                logLevel = SystemaLogLevel.DEBUG,
                kvStore = kvStore,
                engine = mockEngine,
                deviceManager = getDeviceManager()
            )
        )

        connector.initialize()

        return connector
    }
}

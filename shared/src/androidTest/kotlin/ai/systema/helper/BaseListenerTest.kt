package ai.systema.helper

import ai.systema.android.listener.getTestTagId
import ai.systema.constants.Currency
import ai.systema.constants.SystemaTags
import ai.systema.model.tracker.cart.CartItem
import ai.systema.model.tracker.wishlist.WishlistItem
import android.widget.Button
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.After

internal open class BaseListenerTest {
    protected companion object {
        // mock values for this test
        const val productId = "24-MB02"
        const val productUrl = "http://sdk.systema.ai/products/24-MB02"
        const val referrerUrl = "http://sdk.systema.ai/home"

        // create the callback function
        val items: List<CartItem> =
            listOf(
                CartItem(
                    itemId = productId,
                    quantity = 2,
                    price = 10.0,
                    currency = Currency.AUD
                )
            )

        val getItems: () -> List<CartItem> = { items }

        val getItem: () -> CartItem = { items[0] }

        val wishItems: List<WishlistItem> = listOf(
            WishlistItem(
                itemId = productId,
            )
        )

        val getWishlistItems: () -> List<WishlistItem> = { wishItems }
        val getWishlistItem: () -> WishlistItem = { wishItems[0] }
    }

    protected fun getMockButtonWithoutProductId(): Button {
        val errBtn: Button = mockk<Button>()
        every { errBtn.id } returns 0
        every { errBtn.getTag(getTestTagId(SystemaTags.ProductId)) } returns null

        return errBtn
    }

    protected fun getMockButton(): Button {
        val btn = mockk<Button>()
        every { btn.id } returns 0
        every { btn.getTag(getTestTagId(SystemaTags.ProductId)) } returns productId
        every { btn.getTag(getTestTagId(SystemaTags.ProductUrl)) } returns productUrl
        every { btn.getTag(getTestTagId(SystemaTags.ReferrerUrl)) } returns referrerUrl
        return btn
    }

    @ExperimentalCoroutinesApi
    protected val testDispatcher = TestCoroutineDispatcher()

    @After
    @ExperimentalCoroutinesApi
    fun tearDown() {
        testDispatcher.cleanupTestCoroutines()
    }
}

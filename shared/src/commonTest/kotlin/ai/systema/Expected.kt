package ai.systema

import ai.systema.configuration.Credentials
import ai.systema.configuration.SystemaKVStore
import ai.systema.connection.internal.Connector
import ai.systema.helper.SystemaDeviceManager
import io.ktor.client.engine.HttpClientEngine

internal expect fun <T> testSuspend(block: suspend () -> T)

internal expect object TestConfig {
    suspend fun getSystemaKVStore(): SystemaKVStore
    suspend fun getCredentials(): Credentials
    suspend fun getConnector(mockEngine: HttpClientEngine? = null): Connector
    suspend fun getDeviceManager(): SystemaDeviceManager
}

package ai.systema.connection

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

internal class RequestOptionsTest {

    @Test
    fun testSetParameter() {
        val requestOptions = RequestOptions()
        assertNull(requestOptions.urlParameters["q"])
        requestOptions.parameter("q", "test")
        assertEquals("test", requestOptions.urlParameters["q"])
    }

    @Test
    fun testSetBody() {
        val requestOptions = RequestOptions()
        assertNull(requestOptions.body)
        requestOptions.body = Json.decodeFromString(JsonObject.serializer(), "{}")
        assertNotNull(requestOptions.body)
    }

    @Test
    fun testSetHeader() {
        val requestOptions = RequestOptions()
        assertNull(requestOptions.headers["x-test-key"])
        requestOptions.header("x-test-key", "test")
        assertEquals("test", requestOptions.headers["x-test-key"])
    }
}

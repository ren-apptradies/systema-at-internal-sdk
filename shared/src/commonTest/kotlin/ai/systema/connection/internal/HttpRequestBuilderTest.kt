package ai.systema.connection.internal

import ai.systema.TestConfig
import ai.systema.constants.SystemaKeys
import ai.systema.testSuspend
import io.ktor.client.request.HttpRequestBuilder
import kotlin.test.Test
import kotlin.test.assertEquals

internal class HttpRequestBuilderTest {

    @Test
    fun setCredentials() = testSuspend {
        val credentials = TestConfig.getCredentials()
        val req = HttpRequestBuilder().apply {
            setCredentials(credentials)
        }

        val headers = req.headers.build()
        assertEquals(credentials.clientID.basic, headers[SystemaKeys.SystemaClientID])
        assertEquals(credentials.apiKey.basic, headers[SystemaKeys.SystemaAPIKey])
        assertEquals(credentials.environment.value, headers[SystemaKeys.SystemaEnvironment])
    }

    @Test
    fun setFingerprint() = testSuspend {
        val connector = TestConfig.getConnector()
        val req = HttpRequestBuilder().apply {
            setFingerprint(connector.clientUser.fingerprint)
        }

        val headers = req.headers.build()
        assertEquals(connector.clientUser.fingerprint, headers[SystemaKeys.SystemaFingerprint])
    }
}

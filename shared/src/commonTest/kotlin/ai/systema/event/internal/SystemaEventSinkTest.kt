package ai.systema.event.internal

import ai.systema.model.index.Product
import kotlin.test.Test

internal class SystemaEventSinkTest {
    @Test
    fun testLog() {
        var trigger = EventTriggererImpl()
        var p: Product = Product(
            id = "id",
            recId = "rec-id",
            title = "title",
            link = "link",
            image = "image"
        )

        trigger.triggerLogEvent(Product.serializer(), p)
    }

    @Test
    fun testError() {
        var trigger = EventTriggererImpl()
        var ex: Throwable = IllegalArgumentException("test")
        trigger.triggerErrorEvent(ex)
    }
}

package ai.systema.configuration

import ai.systema.TestConfig
import ai.systema.constants.SystemaKeys
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull

internal class ImpersonateTest {
    @Test
    fun testClientUser() = testSuspend {
        val kvStore = TestConfig.getSystemaKVStore()
        val deviceManager = TestConfig.getDeviceManager()
        val clientUser = Impersonate.clientUser(kvStore, deviceManager)
        assertNotNull(clientUser.fingerprint)
        assertNotNull(clientUser.sessionId)
        assertNotNull(clientUser.userAgent)

        // getting again a clientUser would return the same fingerprint and sessionId from kvStore
        val clientUser2 = Impersonate.clientUser(kvStore, deviceManager)
        assertNotNull(clientUser2.fingerprint)
        assertNotNull(clientUser2.sessionId)
        assertEquals(clientUser.fingerprint, clientUser2.fingerprint)
        assertEquals(clientUser.sessionId, clientUser2.sessionId)

        // clearing fingerprint from kvStore, would return a new fingerprint and sessionId
        kvStore.delete(SystemaKeys.Fingerprint)
        val clientUser3 = Impersonate.clientUser(kvStore, deviceManager)
        assertNotNull(clientUser3.fingerprint)
        assertNotNull(clientUser3.sessionId)
        assertNotEquals(clientUser.fingerprint, clientUser3.fingerprint)
        assertNotEquals(clientUser.sessionId, clientUser3.sessionId)
    }
}

package ai.systema.endpoint.internal

import ai.systema.TestConfig
import ai.systema.constants.QueryItemType
import ai.systema.endpoint.EndpointSmartSearch
import ai.systema.helper.SmartSearchMockClient
import ai.systema.model.request.QueryItem
import ai.systema.model.request.SmartSearchRequest
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertFailsWith
import kotlin.test.assertNotEquals

internal open class EndpointSmartSearchImplTest : SmartSearchMockClient() {

    @Test
    fun testSmartSearch() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointSmartSearch(connector)

        // non-null query field is required
        assertFailsWith<IllegalArgumentException> {
            val payload = SmartSearchRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
            )

            ep.smartSearch(payload)
        }

        assertFailsWith<IllegalArgumentException> {
            val payload = SmartSearchRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                query = listOf()
            )

            ep.smartSearch(payload)
        }

        var payload = SmartSearchRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
            query = listOf(QueryItem(id = "WSH10_White", type = QueryItemType.Product))
        )

        var resp = ep.smartSearch(payload)
        assertNotEquals(0, resp.results.size)
        this.checkSmartSearchResponse(resp)
    }
}

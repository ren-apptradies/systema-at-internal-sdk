package ai.systema.endpoint.internal

import ai.systema.TestConfig
import ai.systema.endpoint.EndpointSmartSuggest
import ai.systema.helper.SmartSuggestMockClient
import ai.systema.model.request.SmartSuggestRequest
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertFailsWith

internal open class EndpointSmartSuggestImplTest : SmartSuggestMockClient() {
    @Test
    fun testSmartSuggest() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointSmartSuggest(connector)

        // non-empty query field is required
        assertFailsWith<IllegalArgumentException> {
            val payload = SmartSuggestRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                query = ""
            )

            ep.smartSuggest(payload)
        }

        var payload = SmartSuggestRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
            query = "WSH10",
        )

        var resp = ep.smartSuggest(payload)
        this.checkSmartSuggestResponse(resp)
    }
}

package ai.systema.endpoint.internal

import ai.systema.TestConfig
import ai.systema.endpoint.EndpointRecommendation
import ai.systema.helper.RecommendationMockClient
import ai.systema.model.request.CartRecommendationRequest
import ai.systema.model.request.RecommendationRequest
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertFailsWith
import kotlin.test.assertNotEquals

internal open class EndpointRecommendationImplTest : RecommendationMockClient() {
    @Test
    fun testGetRelated() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointRecommendation(connector)

        // empty id should throw exception
        assertFailsWith<IllegalArgumentException> {
            var payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = ""
            )

            ep.getRelated(payload)
        }

        // null id should throw exception
        assertFailsWith<IllegalArgumentException> {
            var payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = null
            )

            ep.getRelated(payload)
        }

        var payload = RecommendationRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
            id = "WSH10_White"
        )
        var resp = ep.getRelated(payload)
        assertNotEquals(0, resp.results.size)
    }

    @Test
    fun testGetComplimentary() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointRecommendation(connector)

        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = ""
            )

            ep.getComplementary(payload)
        }

        // null id should throw exception
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = null
            )

            ep.getComplementary(payload)
        }

        var payload = RecommendationRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
            id = "WSH10_White"
        )
        var resp = ep.getComplementary(payload)
        assertNotEquals(0, resp.results.size)
        this.checkRecommendationResponse(resp)
    }

    @Test
    fun testGetSimilar() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointRecommendation(connector)

        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = ""
            )

            ep.getSimilar(payload)
        }

        // null id should throw exception
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = null
            )

            ep.getSimilar(payload)
        }

        var payload = RecommendationRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
            id = "WSH10_White"
        )
        var resp = ep.getSimilar(payload)
        assertNotEquals(0, resp.results.size)
        this.checkRecommendationResponse(resp)
    }

    @Test
    fun testGetCategoryTrending() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointRecommendation(connector)

        // null category will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                category = null
            )

            ep.getCategoryTrending(payload)
        }

        // empty category will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                category = listOf()
            )

            ep.getCategoryTrending(payload)
        }

        // non-null id will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = "",
                category = listOf("Men"),
            )

            ep.getCategoryTrending(payload)
        }

        var payload = RecommendationRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
            category = listOf("Men"),
        )
        var resp = ep.getCategoryTrending(payload)
        assertNotEquals(0, resp.results.size)
        this.checkRecommendationResponse(resp)
    }

    @Test
    fun testGetCategoryPopular() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointRecommendation(connector)

        // no category will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                category = null
            )

            ep.getCategoryPopular(payload)
        }

        // empty category will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                category = listOf()
            )

            ep.getCategoryPopular(payload)
        }

        // non-null id will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = "",
                category = listOf("Men"),
            )

            ep.getCategoryPopular(payload)
        }

        var payload = RecommendationRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
            category = listOf("Men"),
        )
        var resp = ep.getCategoryPopular(payload)
        assertNotEquals(0, resp.results.size)
        this.checkRecommendationResponse(resp)
    }

    @Test
    fun testGetPopular() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointRecommendation(connector)

        // non-null category will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                category = listOf()
            )

            ep.getPopular(payload)
        }

        // non-null id will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = "",
            )

            ep.getPopular(payload)
        }

        var payload = RecommendationRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
        )
        var resp = ep.getPopular(payload)
        assertNotEquals(0, resp.results.size)
        this.checkRecommendationResponse(resp)
    }

    @Test
    fun testGetTrending() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointRecommendation(connector)

        // non-null category will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                category = listOf()
            )

            ep.getTrending(payload)
        }

        // non-null id will error
        assertFailsWith<IllegalArgumentException> {
            val payload = RecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = "",
            )

            ep.getTrending(payload)
        }

        var payload = RecommendationRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
        )
        var resp = ep.getTrending(payload)
        assertNotEquals(0, resp.results.size)
        this.checkRecommendationResponse(resp)
    }

    @Test
    fun testGetCartRelated() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointRecommendation(connector)

        assertFailsWith<IllegalArgumentException> {
            val payload = CartRecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = listOf(),
            )

            ep.getCartRelated(payload)
        }

        assertFailsWith<IllegalArgumentException> {
            val payload = CartRecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = null
            )

            ep.getCartRelated(payload)
        }

        var payload = CartRecommendationRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
            id = listOf("WSH10_White"),
        )
        var resp = ep.getCartRelated(payload)
        assertNotEquals(0, resp.results.size)
        this.checkRecommendationResponse(resp)
    }

    @Test
    fun testGetCartComplimentary() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointRecommendation(connector)

        assertFailsWith<IllegalArgumentException> {
            val payload = CartRecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = null,
            )

            ep.getCartComplementary(payload)
        }

        assertFailsWith<IllegalArgumentException> {
            val payload = CartRecommendationRequest(
                environment = connector.credentials.environment.value,
                user = connector.clientUser.toRequestUser(),
                id = listOf(),
            )

            ep.getCartComplementary(payload)
        }

        var payload = CartRecommendationRequest(
            environment = connector.credentials.environment.value,
            user = connector.clientUser.toRequestUser(),
            id = listOf<String>("WSH10_White"),
        )
        var resp = ep.getCartComplementary(payload)
        assertNotEquals(0, resp.results.size)
        this.checkRecommendationResponse(resp)
    }
}

package ai.systema.endpoint.internal

import ai.systema.TestConfig
import ai.systema.constants.Currency
import ai.systema.endpoint.EndpointTracker
import ai.systema.helper.TrackEventBuilder
import ai.systema.helper.TrackerMockClient
import ai.systema.model.tracker.cart.CartItem
import ai.systema.model.tracker.cart.OrderItem
import ai.systema.model.tracker.cart.PurchaseOrder
import ai.systema.model.tracker.cart.ShippingAddress
import ai.systema.model.tracker.view.ItemContainer
import ai.systema.model.tracker.view.prepContainerItem
import ai.systema.model.tracker.wishlist.WishlistItem
import ai.systema.testSuspend
import io.ktor.http.HttpStatusCode
import kotlin.test.Test
import kotlin.test.assertEquals

internal open class EndpointTrackerImplTest : TrackerMockClient() {
    @Test
    fun testSendContainerShownEvent() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointTracker(connector)

        val containers = listOf(
            ItemContainer(
                resultId = "tw8fwtDsGNXD10O_",
                recItems = listOf(
                    prepContainerItem("tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1")
                )
            )
        )
        val event = TrackEventBuilder(
            connector,
        ).setContainers(containers)
            .setUrl("https://sdk-test.systema.ai/products/24-MB05")
            .setReferrer("https://sdk-test.systema.ai/home/search")
            .buildContainerShownEvent()

        val resp = ep.sendContainerShownEvent(event)
        assertEquals(HttpStatusCode.NoContent, resp.status)
    }

    @Test
    fun testSendItemClickEvent() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointTracker(connector)

        val event = TrackEventBuilder(
            connector,
        ).setProductId("24-MB05")
            .setRecId("tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1")
            .setUrl("https://sdk-test.systema.ai/products/24-MB05")
            .setReferrer("https://sdk-test.systema.ai/home/search")
            .buildItemClickEvent()

        val resp = ep.sendItemClickEvent(event)
        assertEquals(HttpStatusCode.NoContent, resp.status)
    }

    @Test
    fun testSendPageViewEvent() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointTracker(connector)

        val event = TrackEventBuilder(
            connector,
        ).setProductId("24-MB05")
            .setRecId("tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1")
            .setUrl("https://sdk-test.systema.ai/products/24-MB05")
            .setReferrer("https://sdk-test.systema.ai/home/search")
            .buildPageViewEvent()

        val resp = ep.sendPageViewEvent(event)
        assertEquals(HttpStatusCode.NoContent, resp.status)
    }

    @Test
    fun testCartItemAcquiredEvent() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointTracker(connector)

        val items: List<CartItem> = listOf(
            CartItem(itemId = "24-MB02", quantity = 1),
            CartItem(itemId = "24-MB05", quantity = 1),
        )

        val event = TrackEventBuilder(
            connector,
        ).setProductId("24-MB02")
            .setCartItems(items)
            .setUrl("https://sdk-test.systema.ai/products/24-MB05")
            .setReferrer("https://sdk-test.systema.ai/home/search")
            .buildCartItemAcquired()

        val resp = ep.sendCartItemAcquiredEvent(event)
        assertEquals(HttpStatusCode.NoContent, resp.status)
    }

    @Test
    fun testCartItemRelinquishedEvent() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointTracker(connector)

        val item = CartItem(itemId = "24-MB02", quantity = 1)
        val event = TrackEventBuilder(
            connector,
        ).setProductId("24-MB02")
            .setCartItem(item)
            .setUrl("https://sdk-test.systema.ai/products/24-MB05")
            .setReferrer("https://sdk-test.systema.ai/home/search")
            .buildCartItemRelinquished()

        val resp = ep.sendCartItemRelinquishedEvent(event)
        assertEquals(HttpStatusCode.NoContent, resp.status)
    }

    @Test
    fun testCartItemAcquisitionComplete() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointTracker(connector)

        val purchaseOrder = PurchaseOrder(
            orderId = "test-order-1",
            chargedAmount = 10.0,
            totalAmount = 10.0,
            taxAmount = 1.00,
            shippingAmount = 10.00,
            currency = Currency.AUD.value,
            shippingAddress = ShippingAddress(
                city = "Melbourne",
                state = "Victoria",
                postCode = "3019",
                country = "Australia",
            ),
            items = listOf(OrderItem(itemId = "24-MB02", quantity = 1, unitCost = 10.0))
        )

        val event = TrackEventBuilder(
            connector,
        ).setProductId("24-MB02")
            .setPurchaseOrder(purchaseOrder)
            .setUrl("https://sdk-test.systema.ai/products/24-MB05")
            .setReferrer("https://sdk-test.systema.ai/home/search")
            .buildCartItemAcquisitionComplete()

        val resp = ep.sendCartItemAcquisitionCompleteEvent(event)
        assertEquals(HttpStatusCode.NoContent, resp.status)
    }

    @Test
    fun testWishlistItemAcquiredEvent() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointTracker(connector)

        val items = listOf(
            WishlistItem(itemId = "24-MB02"),
            WishlistItem(itemId = "24-MB05"),
        )

        val event = TrackEventBuilder(
            connector,
        ).setProductId("24-MB02")
            .setWishlistItems(items)
            .setUrl("https://sdk-test.systema.ai/products/24-MB05")
            .setReferrer("https://sdk-test.systema.ai/home/search")
            .buildWishlistItemAcquired()

        val resp = ep.sendWishlistItemAcquiredEvent(event)
        assertEquals(HttpStatusCode.NoContent, resp.status)
    }

    @Test
    fun testWishlistItemRelinquishedEvent() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val ep = EndpointTracker(connector)

        val item = WishlistItem(itemId = "24-MB02")

        val event = TrackEventBuilder(
            connector,
        ).setProductId("24-MB02")
            .setWishlistItem(item)
            .setUrl("https://sdk-test.systema.ai/products/24-MB05")
            .setReferrer("https://sdk-test.systema.ai/home/search")
            .buildWishlistItemRelinquished()

        val resp = ep.sendWishlistItemRelinquishedEvent(event)
        assertEquals(HttpStatusCode.NoContent, resp.status)
    }
}

package ai.systema.client.internal

import ai.systema.TestConfig
import ai.systema.client.Tracker
import ai.systema.constants.Currency
import ai.systema.endpoint.EndpointTracker
import ai.systema.exception.SystemaApiException
import ai.systema.helper.TrackerMockClient
import ai.systema.model.tracker.cart.CartItem
import ai.systema.model.tracker.cart.OrderItem
import ai.systema.model.tracker.cart.PurchaseOrder
import ai.systema.model.tracker.cart.ShippingAddress
import ai.systema.model.tracker.view.ItemContainer
import ai.systema.model.tracker.view.prepContainerItem
import ai.systema.model.tracker.wishlist.WishlistItem
import ai.systema.testSuspend
import io.ktor.http.HttpStatusCode
import io.mockk.coEvery
import io.mockk.mockk
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

internal open class TrackerImplTest : TrackerMockClient() {
    companion object {
        private val containers = listOf(
            ItemContainer(
                resultId = "tw8fwtDsGNXD10O_",
                recItems = listOf(
                    prepContainerItem("tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1")
                )
            )
        )

        private val cartItems: List<CartItem> = listOf(
            CartItem(itemId = "24-MB02", quantity = 1),
            CartItem(itemId = "24-MB05", quantity = 1),
        )

        private val orderItems: List<OrderItem> = listOf(
            OrderItem(itemId = "24-MB02", quantity = 1, unitCost = 10.0),
            OrderItem(itemId = "24-MB05", quantity = 1, unitCost = 15.99),
        )

        private val order = PurchaseOrder(
            orderId = "ORDER-123-11",
            chargedAmount = 100.00,
            totalAmount = 110.00,
            taxAmount = 0.00,
            shippingAmount = 10.00,
            discountAmount = 0.00,
            items = orderItems,
            currency = Currency.AUD.value,
            shippingAddress = ShippingAddress(
                city = "Melbourne",
                state = "Victoria",
                postCode = "3000",
                country = "Australia"
            )
        )

        val wishListItems: List<WishlistItem> = listOf(
            WishlistItem(itemId = "24-MB02"),
            WishlistItem(itemId = "24-MB05"),
        )
    }

    private fun getEndpointTrackerMock(): EndpointTracker {
        val ep: EndpointTracker = mockk()

        coEvery {
            ep.sendContainerShownEvent(any(), any())
        } throws SystemaApiException(IllegalArgumentException("test"))

        coEvery {
            ep.sendItemClickEvent(any(), any())
        } throws SystemaApiException(IllegalArgumentException("test"))

        coEvery {
            ep.sendPageViewEvent(any(), any())
        } throws SystemaApiException(IllegalArgumentException("test"))

        coEvery {
            ep.sendCartItemAcquiredEvent(any(), any())
        } throws SystemaApiException(IllegalArgumentException("test"))

        coEvery {
            ep.sendCartItemRelinquishedEvent(any(), any())
        } throws SystemaApiException(IllegalArgumentException("test"))

        coEvery {
            ep.sendCartItemAcquisitionCompleteEvent(any(), any())
        } throws SystemaApiException(IllegalArgumentException("test"))

        coEvery {
            ep.sendWishlistItemAcquiredEvent(any(), any())
        } throws SystemaApiException(IllegalArgumentException("test"))

        coEvery {
            ep.sendWishlistItemRelinquishedEvent(any(), any())
        } throws SystemaApiException(IllegalArgumentException("test"))

        return ep
    }

    @Test
    fun testTrackContainerShown() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep = getEndpointTrackerMock()
        var tracker: Tracker = TrackerImpl(connector, ep)
        val resultError = tracker.trackContainerShown(
            productId = null,
            containers = containers,
            url = "https://sdk-test.systema.ai/products",
            referrer = "https://sdk-test.systema.ai/home",
        )

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success without productId
        tracker = TrackerImpl(connector, EndpointTracker(connector))
        val resultSuccessNoProductId = tracker.trackContainerShown(
            productId = null,
            containers = containers,
            url = "https://sdk-test.systema.ai/products",
            referrer = "https://sdk-test.systema.ai/home",
        )

        if (resultSuccessNoProductId.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultSuccessNoProductId.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }

        // check for success with productId
        tracker = TrackerImpl(connector, EndpointTracker(connector))
        val resultSuccessWithProductId = tracker.trackContainerShown(
            productId = "24-MB02",
            containers = containers,
            url = "https://sdk-test.systema.ai/products",
            referrer = "https://sdk-test.systema.ai/home",
        )

        if (resultSuccessWithProductId.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultSuccessWithProductId.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }
    }

    @Test
    fun testTrackContainerShownWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val tracker = TrackerImpl(connector, EndpointTracker(connector))
        val result = tracker.trackContainerShown(
            productId = "24-MB02",
            containers = containers,
            url = "https://sdk-test.systema.ai/products",
            referrer = "https://sdk-test.systema.ai/home",
        )

        if (result.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, result.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }
    }

    @Test
    fun testTrackItemClicked() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        var ep = getEndpointTrackerMock()
        var tracker: Tracker = TrackerImpl(connector, ep)
        val resultError = tracker.trackItemClicked(
            productId = "24-MB02",
            url = "https://sdk-test.systema.ai/products/24-MB02",
            referrer = "https://sdk-test.systema.ai/home",
            recId = "tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1"
        )

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        tracker = TrackerImpl(connector, EndpointTracker(connector))
        val resultSuccess = tracker.trackItemClicked(
            productId = "24-MB02",
            url = "https://sdk-test.systema.ai/products/24-MB02",
            referrer = "https://sdk-test.systema.ai/home",
            recId = "tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1"
        )

        if (resultSuccess.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultSuccess.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }
    }

    @Test
    fun testTrackItemClickedWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for success
        val tracker = TrackerImpl(connector, EndpointTracker(connector))
        val resultSuccess = tracker.trackItemClicked(
            productId = "24-MB02",
            url = "https://sdk-test.systema.ai/products/24-MB02",
            referrer = "https://sdk-test.systema.ai/home",
            recId = "tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1"
        )

        if (resultSuccess.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultSuccess.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }
    }

    @Test
    fun testTrackPageViewed() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep = getEndpointTrackerMock()
        var tracker: Tracker = TrackerImpl(connector, ep)
        val resultError = tracker.trackPageViewed(
            productId = "24-MB02",
            url = "https://sdk-test.systema.ai/products/24-MB02",
            referrer = "https://sdk-test.systema.ai/home",
            recId = "tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1"
        )

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        tracker = TrackerImpl(connector, EndpointTracker(connector))
        val resultSuccess = tracker.trackPageViewed(
            productId = "24-MB02",
            url = "https://sdk-test.systema.ai/products/24-MB02",
            referrer = "https://sdk-test.systema.ai/home",
            recId = "tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1"
        )

        if (resultSuccess.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultSuccess.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }
    }

    @Test
    fun testTrackPageViewedWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for success
        val tracker = TrackerImpl(connector, EndpointTracker(connector))
        val resultSuccess = tracker.trackPageViewed(
            productId = "24-MB02",
            url = "https://sdk-test.systema.ai/products/24-MB02",
            referrer = "https://sdk-test.systema.ai/home",
            recId = "tw8fwtDsGNXD10O_:t5hZuh0UeImGMxJ1"
        )

        if (resultSuccess.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultSuccess.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }
    }

    @Test
    fun testTrackItemAcquiredEventFailures() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep = getEndpointTrackerMock()
        val tracker: Tracker = TrackerImpl(connector, ep)

        val resultItemAcquiredError = tracker.trackItemAcquired(cartItems[0].itemId, cartItems, "https://sdk-test.systema.ai/cart")
        if (resultItemAcquiredError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultItemAcquiredError.error()::class)
            assertEquals("API error occurred", resultItemAcquiredError.error().message)
        }

        val resultItemRelinquishedError = tracker.trackItemRelinquished(cartItems[0].itemId, cartItems[0], "https://sdk-test.systema.ai/cart")
        if (resultItemRelinquishedError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultItemRelinquishedError.error()::class)
            assertEquals("API error occurred", resultItemRelinquishedError.error().message)
        }

        val resultItemAcquisitionCompleteError = tracker.trackAcquisitionComplete(order, "https://sdk-test.systema.ai/cart")
        if (resultItemAcquisitionCompleteError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultItemAcquisitionCompleteError.error()::class)
            assertEquals("API error occurred", resultItemAcquisitionCompleteError.error().message)
        }
    }

    @Test
    fun testTrackCartEventsWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val tracker = TrackerImpl(connector, EndpointTracker(connector))

        // add to cart
        val resultAddToCartSuccess = tracker.trackItemAcquired("24-MB02", cartItems, "https://sdk-test.systema.ai/cart")
        if (resultAddToCartSuccess.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultAddToCartSuccess.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }

        // remove from cart
        val resultItemRelinquishedSuccess = tracker.trackItemRelinquished("24-MB02", cartItems[0], "https://sdk-test.systema.ai/cart")
        if (resultItemRelinquishedSuccess.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultItemRelinquishedSuccess.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }

        // purchase
        val resultItemAcquisitionCompleteSuccess = tracker.trackAcquisitionComplete(order, "https://sdk-test.systema.ai/cart")
        if (resultItemAcquisitionCompleteSuccess.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultItemAcquisitionCompleteSuccess.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }
    }

    @Test
    fun testTrackWishlistEventsFailures() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val tracker = TrackerImpl(connector, getEndpointTrackerMock())

        val resultAddWishlistError = tracker.trackWishlistAcquired(
            "24-MB02",
            wishListItems,
            "https://sdk-test.systema.ai/wishlist"
        )
        if (resultAddWishlistError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultAddWishlistError.error()::class)
            assertEquals("API error occurred", resultAddWishlistError.error().message)
        }

        val resultWishlistRelinquishedError = tracker.trackWishlistRelinquished(
            "24-MB02",
            wishListItems[0],
            "https://sdk-test.systema.ai/wishlist"
        )
        if (resultWishlistRelinquishedError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultWishlistRelinquishedError.error()::class)
            assertEquals("API error occurred", resultWishlistRelinquishedError.error().message)
        }
    }

    @Test
    fun testTrackWishlistEventsWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val tracker = TrackerImpl(connector, EndpointTracker(connector))

        val resultWishlistAddedSuccess = tracker.trackWishlistAcquired(
            "24-MB02",
            wishListItems,
            "https://sdk-test.systema.ai/wishlist"
        )
        if (resultWishlistAddedSuccess.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultWishlistAddedSuccess.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }

        val resultWishlistRelinquishedSuccess = tracker.trackWishlistRelinquished(
            "24-MB02",
            wishListItems[0],
            "https://sdk-test.systema.ai/wishlist"
        )
        if (resultWishlistRelinquishedSuccess.isSuccessful) {
            assertEquals(HttpStatusCode.NoContent, resultItemRelinquishedSuccess.value().status)
        } else {
            throw IllegalStateException("Should not error")
        }
    }
}

package ai.systema.client.internal

import ai.systema.TestConfig
import ai.systema.client.SystemaAI
import ai.systema.helper.SmartSearchMockClient
import ai.systema.helper.internal.sha256Hex
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull

internal open class SystemaUserManagerImplTest : SmartSearchMockClient() {
    @Test
    fun testInitialize() = testSuspend {
        val credentials = TestConfig.getCredentials()
        var meta: Map<String, String> = mapOf(
            "one" to "10K",
            "two" to "20k"
        )
        val systema = SystemaAI(
            clientID = credentials.clientID.basic,
            apiKey = credentials.apiKey.basic,
            proxyUrls = credentials.proxyUrls.entries.associate { it.key to it.value.toString() },
            kvStore = TestConfig.getSystemaKVStore(),
            meta = mapOf(
                "test" to meta,
            ),
            deviceManager = TestConfig.getDeviceManager()
        )

        // without doing systema.initialize(), it should throw exception
        assertFailsWith<UninitializedPropertyAccessException> {
            systema.getUserSnapshot()
        }

        systema.initialize()

        val systemaUser = systema.getUserSnapshot()
        assertNotNull(systemaUser.fingerprint)
        assertNotNull(systemaUser.sessionId)
        assertNotNull(systemaUser.sessionCreatedAt)
        assertNotNull(systemaUser.userAgent)
        assertNotNull(systemaUser.snapshotAt)
        assertEquals(0, systemaUser.sequence)
        assertEquals("", systemaUser.userIdHash)

        assertEquals(meta, systema.meta["test"])
    }

    @Test
    fun testSetUserId() = testSuspend {
        val credentials = TestConfig.getCredentials()
        val systema = SystemaAI(
            clientID = credentials.clientID.basic,
            apiKey = credentials.apiKey.basic,
            proxyUrls = credentials.proxyUrls.entries.associate { it.key to it.value.toString() },
            kvStore = TestConfig.getSystemaKVStore(),
            deviceManager = TestConfig.getDeviceManager()
        )

        // without doing systema.initialize(), it should throw exception
        assertFailsWith<UninitializedPropertyAccessException> {
            systema.getUserSnapshot()
        }

        systema.initialize()

        assertFailsWith<IllegalArgumentException> {
            systema.setUserIdHash("   ")
        }

        assertFailsWith<IllegalArgumentException> {
            systema.setUserIdHash("")
        }

        val email: String = "lenin@systema.ai"
        assertFailsWith<IllegalArgumentException> {
            systema.setUserIdHash(email)
        }

        systema.setUserIdHash(email.sha256Hex())

        val systemaUser = systema.getUserSnapshot()
        assertNotNull(systemaUser.fingerprint)
        assertNotNull(systemaUser.sessionId)
        assertEquals(email.sha256Hex(), systemaUser.userIdHash)

        systema.clearUserIdHash()
        assertEquals("", systema.getUserSnapshot().userIdHash)
    }
}

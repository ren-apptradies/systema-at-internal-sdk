package ai.systema.client.internal

import ai.systema.TestConfig
import ai.systema.client.SmartSearch
import ai.systema.constants.QueryItemType
import ai.systema.endpoint.EndpointSmartSearch
import ai.systema.exception.SystemaApiException
import ai.systema.helper.SmartSearchMockClient
import ai.systema.model.request.QueryItem
import ai.systema.model.request.RecommendationRequest
import ai.systema.model.request.SmartSearchRequest
import ai.systema.testSuspend
import io.mockk.coEvery
import io.mockk.mockk
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNull

internal open class SmartSearchImplTest : SmartSearchMockClient() {
    @Test
    fun testSmartSearch() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointSmartSearch = mockk()
        coEvery { ep.smartSearch(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var searcher: SmartSearch = SmartSearchImpl(connector, ep)
        var payload = SmartSearchRequest(
            query = listOf(QueryItem(id = "WSH10_White", type = QueryItemType.Product))
        )

        val resultError = searcher.smartSearch(payload)

        if (resultError.isSuccessful) {
            assertNull(resultError.value())
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        payload = SmartSearchRequest(
            query = listOf(QueryItem(id = "WSH10_White", type = QueryItemType.Product))
        )
        searcher = SmartSearch(connector)
        val resultSuccess = searcher.smartSearch(payload)

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkSmartSearchResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testSmartSearchWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val searcher = SmartSearch(connector)
        val payload = SmartSearchRequest(
            query = listOf(QueryItem(id = "WSH10_White", type = QueryItemType.Product))
        )

        val resultSuccess = searcher.smartSearch(payload)

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkSmartSearchResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }
}

package ai.systema.client.internal

import ai.systema.TestConfig
import ai.systema.client.SmartSearch
import ai.systema.client.SmartSuggest
import ai.systema.constants.QueryItemType
import ai.systema.endpoint.EndpointSmartSuggest
import ai.systema.exception.SystemaApiException
import ai.systema.helper.SmartSuggestMockClient
import ai.systema.model.request.QueryItem
import ai.systema.model.request.SmartSearchRequest
import ai.systema.model.request.SmartSuggestRequest
import ai.systema.testSuspend
import io.mockk.coEvery
import io.mockk.mockk
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNull

internal open class SmartSuggestImplTest : SmartSuggestMockClient() {
    @Test
    fun testSmartSuggest() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointSmartSuggest = mockk()
        coEvery { ep.smartSuggest(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var suggest: SmartSuggest = SmartSuggestImpl(connector, ep)
        var payload = SmartSuggestRequest(
            query = "WSH10"
        )

        val resultError = suggest.smartSuggest(payload)

        if (resultError.isSuccessful) {
            assertNull(resultError.value())
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        suggest = SmartSuggest(connector)
        payload = SmartSuggestRequest(
            query = "WSH10"
        )
        val resultSuccess = suggest.smartSuggest(payload)

        if (resultSuccess.isSuccessful) {
            this.checkSmartSuggestResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testSmartSuggestWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val searcher = SmartSuggest(connector)
        val payload = SmartSuggestRequest(
            query = "WSH10"
        )

        val resultSuccess = searcher.smartSuggest(payload)

        if (resultSuccess.isSuccessful) {
            this.checkSmartSuggestResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }
}

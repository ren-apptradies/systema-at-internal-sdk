package ai.systema.client.internal

import ai.systema.TestConfig
import ai.systema.client.Recommender
import ai.systema.endpoint.EndpointRecommendation
import ai.systema.exception.SystemaApiException
import ai.systema.helper.RecommendationMockClient
import ai.systema.model.request.CartRecommendationRequest
import ai.systema.model.request.RecommendationRequest
import ai.systema.testSuspend
import io.mockk.coEvery
import io.mockk.mockk
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal open class RecommenderImplTest : RecommendationMockClient() {
    @Test
    fun testGetRelated() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointRecommendation = mockk()
        coEvery { ep.getRelated(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var recommender: Recommender = RecommenderImpl(connector, ep)
        val resultError = recommender.getRelated(RecommendationRequest())

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        recommender = Recommender(connector)
        val resultSuccess = recommender.getRelated(RecommendationRequest(id = "WSH10_White"))

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetRelatedWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val recommender = Recommender(connector)
        val payload = RecommendationRequest(
            id = "WSH10_White"
        )

        val resultSuccess = recommender.getRelated(payload)
        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetComplementary() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointRecommendation = mockk()
        coEvery { ep.getComplementary(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var recommender: Recommender = RecommenderImpl(connector, ep)
        val resultError = recommender.getComplementary(RecommendationRequest(id = "WSH10_White"))

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        recommender = Recommender(connector)
        val resultSuccess = recommender.getComplementary(RecommendationRequest(id = "WSH10_White"))

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetComplementaryWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val recommender = Recommender(connector)
        val payload = RecommendationRequest(
            id = "WSH10_White"
        )

        val resultSuccess = recommender.getComplementary(payload)
        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetSimilar() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointRecommendation = mockk()
        coEvery { ep.getSimilar(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var recommender: Recommender = RecommenderImpl(connector, ep)
        val resultError = recommender.getSimilar(RecommendationRequest())

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        recommender = Recommender(connector)
        val resultSuccess = recommender.getSimilar(RecommendationRequest(id = "WSH10_White"))

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetSimilarWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val recommender = Recommender(connector)
        val payload = RecommendationRequest(
            id = "WSH10_White"
        )

        val resultSuccess = recommender.getSimilar(payload)
        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetCategoryTrending() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointRecommendation = mockk()
        coEvery { ep.getCategoryTrending(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var recommender: Recommender = RecommenderImpl(connector, ep)
        val resultError = recommender.getCategoryTrending(RecommendationRequest(category = listOf("Men")))

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        recommender = Recommender(connector)
        val resultSuccess = recommender.getCategoryTrending(RecommendationRequest(category = listOf("Men")))

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetCategoryTrendingWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val recommender = Recommender(connector)
        val payload = RecommendationRequest(
            category = listOf("Men"),
        )

        val resultSuccess = recommender.getCategoryTrending(payload)
        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetCategoryPopular() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointRecommendation = mockk()
        coEvery { ep.getCategoryPopular(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var recommender: Recommender = RecommenderImpl(connector, ep)
        val resultError = recommender.getCategoryPopular(RecommendationRequest(category = listOf("Men")))

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        recommender = Recommender(connector)
        val resultSuccess = recommender.getCategoryPopular(RecommendationRequest(category = listOf("Men")))

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetCategoryPopularWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val recommender = Recommender(connector)
        val payload = RecommendationRequest(
            category = listOf("Men"),
        )

        val resultSuccess = recommender.getCategoryPopular(payload)
        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetTrending() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointRecommendation = mockk()
        coEvery { ep.getTrending(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var recommender: Recommender = RecommenderImpl(connector, ep)
        val resultError = recommender.getTrending(RecommendationRequest())

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        recommender = Recommender(connector)
        val resultSuccess = recommender.getTrending(RecommendationRequest())

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetTrendingWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val recommender = Recommender(connector)
        val payload = RecommendationRequest()

        val resultSuccess = recommender.getTrending(payload)
        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetPopular() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointRecommendation = mockk()
        coEvery { ep.getPopular(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var recommender: Recommender = RecommenderImpl(connector, ep)
        val resultError = recommender.getPopular(RecommendationRequest())

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        recommender = Recommender(connector)
        val resultSuccess = recommender.getPopular(RecommendationRequest())

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetPopularWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val recommender = Recommender(connector)
        val payload = RecommendationRequest()

        val resultSuccess = recommender.getPopular(payload)
        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetCartRelated() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointRecommendation = mockk()
        coEvery { ep.getCartRelated(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var recommender: Recommender = RecommenderImpl(connector, ep)
        val resultError = recommender.getCartRelated(CartRecommendationRequest(id = listOf("WSH10_White")))

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        recommender = Recommender(connector)
        val resultSuccess = recommender.getCartRelated(CartRecommendationRequest(id = listOf("WSH10_White")))

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetCartRelatedWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val recommender = Recommender(connector)
        val payload = CartRecommendationRequest(
            id = listOf("WSH10_White"),
        )

        val resultSuccess = recommender.getCartRelated(payload)
        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetCartComplementary() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())

        // check for failure
        val ep: EndpointRecommendation = mockk()
        coEvery { ep.getCartComplementary(any(), any()) } throws SystemaApiException(IllegalArgumentException("test"))
        var recommender: Recommender = RecommenderImpl(connector, ep)
        val resultError = recommender.getCartComplementary(CartRecommendationRequest(id = listOf("WSH10_White")))

        if (resultError.isSuccessful) {
            throw IllegalStateException("Should not succeed")
        } else {
            assertEquals(SystemaApiException::class, resultError.error()::class)
            assertEquals("API error occurred", resultError.error().message)
        }

        // check for success
        recommender = Recommender(connector)
        val resultSuccess = recommender.getCartComplementary(CartRecommendationRequest(id = listOf("WSH10_White")))

        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }
    }

    @Test
    fun testGetCartComplementaryWithCallback() = testSuspend {
        val connector = TestConfig.getConnector(this.initEngine())
        val recommender = Recommender(connector)

        val resultSuccess = recommender.getCartComplementary(CartRecommendationRequest(id = listOf("WSH10_White")))
        if (resultSuccess.isSuccessful) {
            assertNotEquals(0, resultSuccess.value().results.size)
            this.checkRecommendationResponse(resultSuccess.value())
        } else {
            throw IllegalStateException("Should not error", resultSuccess.error())
        }

    }
}

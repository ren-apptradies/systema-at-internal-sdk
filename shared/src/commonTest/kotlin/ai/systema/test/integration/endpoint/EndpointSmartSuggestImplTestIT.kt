package ai.systema.test.integration.endpoint

import ai.systema.endpoint.internal.EndpointSmartSuggestImplTest
import io.ktor.client.engine.HttpClientEngine
import org.junit.Ignore

@Ignore
internal class EndpointSmartSuggestImplTestIT : EndpointSmartSuggestImplTest() {
    override fun initEngine(): HttpClientEngine? {
        return null
    }
}

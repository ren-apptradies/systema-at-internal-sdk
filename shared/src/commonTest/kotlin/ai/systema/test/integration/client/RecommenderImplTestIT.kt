package ai.systema.test.integration.client

import ai.systema.client.internal.RecommenderImplTest
import io.ktor.client.engine.HttpClientEngine
import org.junit.Ignore

@Ignore
internal class RecommenderImplTestIT : RecommenderImplTest() {
    override fun initEngine(): HttpClientEngine? {
        return null
    }
}

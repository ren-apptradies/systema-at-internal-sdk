package ai.systema.test.integration.client

import ai.systema.client.internal.SmartSuggestImplTest
import io.ktor.client.engine.HttpClientEngine
import org.junit.Ignore

@Ignore
internal class SmartSuggestImplTestIT : SmartSuggestImplTest() {
    override fun initEngine(): HttpClientEngine? {
        return null
    }
}

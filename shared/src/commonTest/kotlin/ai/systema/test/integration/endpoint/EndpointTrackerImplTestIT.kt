package ai.systema.test.integration.endpoint

import ai.systema.endpoint.internal.EndpointTrackerImplTest
import io.ktor.client.engine.HttpClientEngine
import org.junit.Ignore

@Ignore
internal class EndpointTrackerImplTestIT : EndpointTrackerImplTest() {
    override fun initEngine(): HttpClientEngine? {
        return null
    }
}

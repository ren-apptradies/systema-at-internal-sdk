package ai.systema.test.integration.endpoint

import ai.systema.endpoint.internal.EndpointRecommendationImplTest
import io.ktor.client.engine.HttpClientEngine
import org.junit.Ignore

@Ignore
internal class EndpointRecommendationImplTestIT : EndpointRecommendationImplTest() {
    override fun initEngine(): HttpClientEngine? {
        return null
    }
}

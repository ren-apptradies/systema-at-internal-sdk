package ai.systema.test.integration.client

import ai.systema.client.internal.SmartSearchImplTest
import io.ktor.client.engine.HttpClientEngine
import org.junit.Ignore

@Ignore
internal class SmartSearchImplTestIT : SmartSearchImplTest() {
    override fun initEngine(): HttpClientEngine? {
        return null
    }
}

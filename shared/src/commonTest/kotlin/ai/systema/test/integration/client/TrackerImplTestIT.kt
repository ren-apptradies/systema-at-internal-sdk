package ai.systema.test.integration.client

import ai.systema.client.internal.TrackerImplTest
import io.ktor.client.engine.HttpClientEngine
import org.junit.Ignore

@Ignore
internal class TrackerImplTestIT : TrackerImplTest() {
    override fun initEngine(): HttpClientEngine? {
        return null
    }
}

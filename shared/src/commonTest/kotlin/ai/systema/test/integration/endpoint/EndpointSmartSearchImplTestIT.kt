package ai.systema.test.integration.endpoint

import ai.systema.endpoint.internal.EndpointSmartSearchImplTest
import io.ktor.client.engine.HttpClientEngine
import org.junit.Ignore

@Ignore
internal class EndpointSmartSearchImplTestIT : EndpointSmartSearchImplTest() {
    override fun initEngine(): HttpClientEngine? {
        return null
    }
}

package ai.systema.helper

import ai.systema.TestConfig
import ai.systema.constants.Currency
import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.CurTrackEventDate
import ai.systema.model.tracker.cart.CartItem
import ai.systema.model.tracker.cart.CartItemAcquiredEvent
import ai.systema.model.tracker.cart.CartItemAcquisitionCompleteEvent
import ai.systema.model.tracker.cart.CartItemAcquisitionCompleteEventTest
import ai.systema.model.tracker.cart.CartItemRelinquishedEvent
import ai.systema.model.tracker.cart.OrderItem
import ai.systema.model.tracker.cart.PurchaseOrder
import ai.systema.model.tracker.cart.ShippingAddress
import ai.systema.model.tracker.view.ContainerShownEvent
import ai.systema.model.tracker.view.ItemClickEvent
import ai.systema.model.tracker.view.ItemContainer
import ai.systema.model.tracker.view.PageViewEvent
import ai.systema.model.tracker.view.prepContainerItem
import ai.systema.model.tracker.wishlist.WishlistItem
import ai.systema.model.tracker.wishlist.WishlistItemAcquiredEvent
import ai.systema.model.tracker.wishlist.WishlistItemRelinquishedEvent
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

internal class TrackEventBuilderTest {

    companion object MockConstants {
        internal const val productId = "product_id_1"
        internal const val recId = "rec_id_1"
        internal const val referrer = "referrer_1"
        internal const val url = "https://fake-url-does-not-exist.com.au"
        internal val containers = listOf(
            ItemContainer(
                resultId = "mock_result_id_1",
                recItems = listOf(
                    prepContainerItem("mock_result_id_1:mock_result_id")
                )
            )
        )
        internal val item = CartItem(
            itemId = "cart_item_id_1",
            quantity = 1
        )
        internal val items = listOf(
            CartItem(
                itemId = "cart_item_id_1",
                quantity = 1,
                price = 1.00,
                currency = Currency.AUD
            )
        )
        private val orderItem = OrderItem(
            itemId = "item_id",
            quantity = 1,
            unitCost = 1.00,
            unitTaxAmount = 1.00,
            currency = Currency.AUD.value
        )
        private val shippingAddress = ShippingAddress(
            city = "Melbourne",
            state = "VIC",
            postCode = "3000",
            country = "Australia"
        )
        internal val order = PurchaseOrder(
            orderId = "order_id_1",
            chargedAmount = 1.00,
            totalAmount = 1.00,
            taxAmount = 1.00,
            shippingAmount = 1.00,
            discountAmount = 1.00,
            discountCodes = "discount_code",
            currency = "AUD",
            shippingAddress = shippingAddress,
            items = listOf(
                orderItem
            )
        )
        internal val wishlistItem = WishlistItem(
            itemId = "item_id_1"
        )
        internal val wishlistItems = listOf(
            wishlistItem
        )
        internal val currentDate = CurTrackEventDate
    }

    @Test
    fun testBuildContainerShownEvent() = testSuspend {
        val connector = TestConfig.getConnector()

        // missing url and containers
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector).buildContainerShownEvent()
        }

        // missing container
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector).setUrl(url).buildContainerShownEvent()
        }

        // missing url
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector).setContainers(containers).buildContainerShownEvent()
        }

        // without productId
        var event: ContainerShownEvent = TrackEventBuilder(connector)
            .setUrl(url)
            .setContainers(containers)
            .buildContainerShownEvent()
        assertContentEquals(containers, event.containers)
        assertNull(event.productId)
        assertEquals(url, event.url)
        assertEquals(TrackerEventType.ContainerShown, event.type)

        // with productId
        event = TrackEventBuilder(connector)
            .setProductId(productId)
            .setUrl(url)
            .setContainers(containers)
            .buildContainerShownEvent()
        assertContentEquals(containers, event.containers)
        assertEquals(productId, event.productId)
        assertEquals(url, event.url)
        assertEquals(TrackerEventType.ContainerShown, event.type)
    }

    @Test
    fun testBuildItemClickEvent() = testSuspend {
        val connector = TestConfig.getConnector()

        // missing url, productId and recId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector).buildItemClickEvent()
        }

        // missing productId and recId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setUrl(url)
                .buildItemClickEvent()
        }

        // missing recId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setProductId(productId)
                .setUrl(url)
                .buildItemClickEvent()
        }

        // missing productId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setRecId(recId)
                .setUrl(url)
                .buildItemClickEvent()
        }

        val event: ItemClickEvent = TrackEventBuilder(connector)
            .setUrl(url)
            .setProductId(productId)
            .setRecId(recId)
            .buildItemClickEvent()
        assertEquals(url, event.url)
        assertEquals(productId, event.productId)
        assertEquals(recId, event.recId)
        assertEquals(TrackerEventType.ItemClicked, event.type)
    }

    @Test
    fun testBuildPageViewEvent() = testSuspend {
        val connector = TestConfig.getConnector()

        // Empty ProductId
        val nonPdpEvent: PageViewEvent = TrackEventBuilder(connector)
            .setUrl(url)
            .setProductId("")
            .setRecId(recId)
            .setReferrer(referrer)
            .buildPageViewEvent()
        assertEquals(nonPdpEvent.type, TrackerEventType.PageView)
        assertEquals(nonPdpEvent.productId, null)

        val event: PageViewEvent = TrackEventBuilder(connector)
            .setUrl(url)
            .setProductId(productId)
            .setRecId(recId)
            .setReferrer(referrer)
            .buildPageViewEvent()
        assertEquals(url, event.url)
        assertEquals(productId, event.productId)
        assertEquals(recId, event.recId)
        assertEquals(referrer, event.referrer)
        assertEquals(TrackerEventType.PageView, event.type)
    }

    @Test
    fun testBuildCartItemAcquiredEvent() = testSuspend {
        val connector = TestConfig.getConnector()

        // missing items
        assertFailsWith<NullPointerException> {
            TrackEventBuilder(connector).buildCartItemAcquired()
        }

        // missing productId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setCartItems(items)
                .setUrl(url)
                .buildCartItemAcquired()
        }

        // missing url
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setCartItems(items)
                .setProductId(productId)
                .buildCartItemAcquired()
        }

        // missing productId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setCartItems(items)
                .setUrl(url)
                .buildCartItemAcquired()
        }

        val event: CartItemAcquiredEvent = TrackEventBuilder(connector)
            .setProductId(productId)
            .setCartItems(items)
            .setUrl(url)
            .buildCartItemAcquired()
        assertEquals(url, event.url)
        assertEquals(productId, event.productId)
        assertContentEquals(items, event.items)
        assertEquals(TrackerEventType.AddToCart, event.type)
    }

    @Test
    fun testBuildCartItemRelinquishedEvent() = testSuspend {
        val connector = TestConfig.getConnector()

        // missing item
        assertFailsWith<NullPointerException> {
            TrackEventBuilder(connector).buildCartItemRelinquished()
        }

        // missing productId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setCartItem(item)
                .setUrl(url)
                .buildCartItemRelinquished()
        }

        // missing url
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setCartItem(item)
                .setProductId(productId)
                .buildCartItemRelinquished()
        }

        // missing productId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setCartItem(item)
                .setUrl(url)
                .buildCartItemRelinquished()
        }

        val event: CartItemRelinquishedEvent = TrackEventBuilder(connector)
            .setProductId(productId)
            .setCartItem(item)
            .setUrl(url)
            .buildCartItemRelinquished()
        assertEquals(url, event.url)
        assertEquals(productId, event.productId)
        assertEquals(item, event.item)
        assertEquals(TrackerEventType.RemoveFromCart, event.type)
    }

    @Test
    fun testBuildCartAcquisitionCompleteEvent() = testSuspend {
        val connector = TestConfig.getConnector()

        // missing item
        assertFailsWith<NullPointerException> {
            TrackEventBuilder(connector).buildCartItemAcquisitionComplete()
        }

        // missing url
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setPurchaseOrder(order)
                .buildCartItemAcquisitionComplete()
        }

        // empty items order
        assertFailsWith<IllegalArgumentException> {
            val emptyItemOrder = PurchaseOrder(
                orderId = "order_id_1",
                chargedAmount = 1.00,
                totalAmount = 1.00,
                taxAmount = 1.00,
                shippingAmount = 1.00,
                discountAmount = 1.00,
                discountCodes = "discount_code",
                currency = "AUD",
                shippingAddress = CartItemAcquisitionCompleteEventTest.shippingAddress,
                items = listOf()
            )
            TrackEventBuilder(connector)
                .setPurchaseOrder(emptyItemOrder)
                .setUrl(url)
                .buildCartItemAcquisitionComplete()
        }

        val event: CartItemAcquisitionCompleteEvent = TrackEventBuilder(connector)
            .setPurchaseOrder(order)
            .setUrl(url)
            .buildCartItemAcquisitionComplete()
        assertEquals(url, event.url)
        assertEquals(order, event.order)
        assertEquals(TrackerEventType.Purchase, event.type)
    }

    @Test
    fun testBuildWishlistItemAcquiredEvent() = testSuspend {
        val connector = TestConfig.getConnector()

        // missing items
        assertFailsWith<NullPointerException> {
            TrackEventBuilder(connector).buildWishlistItemAcquired()
        }

        // missing productId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setWishlistItems(wishlistItems)
                .setUrl(url)
                .buildWishlistItemAcquired()
        }

        // missing url
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setWishlistItems(wishlistItems)
                .setProductId(productId)
                .buildWishlistItemAcquired()
        }

        // missing productId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setWishlistItems(wishlistItems)
                .setUrl(url)
                .buildWishlistItemAcquired()
        }

        val event: WishlistItemAcquiredEvent = TrackEventBuilder(connector)
            .setProductId(productId)
            .setWishlistItems(wishlistItems)
            .setUrl(url)
            .buildWishlistItemAcquired()
        assertEquals(url, event.url)
        assertEquals(productId, event.productId)
        assertContentEquals(wishlistItems, event.items)
        assertEquals(TrackerEventType.AddToWishlist, event.type)
    }

    @Test
    fun testBuildWishlistItemRelinquishedEvent() = testSuspend {
        val connector = TestConfig.getConnector()

        // missing items
        assertFailsWith<NullPointerException> {
            TrackEventBuilder(connector).buildWishlistItemRelinquished()
        }

        // missing productId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setWishlistItem(wishlistItem)
                .setUrl(url)
                .buildWishlistItemRelinquished()
        }

        // missing url
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setWishlistItem(wishlistItem)
                .setProductId(productId)
                .buildWishlistItemRelinquished()
        }

        // missing productId
        assertFailsWith<IllegalArgumentException> {
            TrackEventBuilder(connector)
                .setWishlistItem(wishlistItem)
                .setUrl(url)
                .buildWishlistItemRelinquished()
        }

        val event: WishlistItemRelinquishedEvent = TrackEventBuilder(connector)
            .setProductId(productId)
            .setWishlistItem(wishlistItem)
            .setUrl(url)
            .buildWishlistItemRelinquished()
        assertEquals(url, event.url)
        assertEquals(productId, event.productId)
        assertEquals(wishlistItem, event.item)
        assertEquals(TrackerEventType.RemoveFromWishlist, event.type)
    }
}

package ai.systema.helper.internal

import kotlin.test.Test
import kotlin.test.assertEquals

internal class EncodingTest {
    @Test
    fun testUTF8Encode() {
        val samples = mapOf(
            "How're you?" to "How%27re+you%3F",
            "I'm fine!" to "I%27m+fine%21",
        )

        for ((text, expected) in samples) {
            val encoded = text.encodeUTF8()
            assertEquals(expected = expected, actual = encoded)
        }
    }
}

package ai.systema.helper

import ai.systema.endpoint.SystemaRoutes
import ai.systema.model.response.SmartSearchResponse
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.MockEngineConfig
import io.ktor.client.engine.mock.respond
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.headersOf
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

internal open class SmartSearchMockClient : EndpointMockClient {
    companion object {
        private lateinit var mockEngine: MockEngine
        private var hasMockInitialized: Boolean = false
        private val searchRespJson: Map<String, String> = mapOf(
            SystemaRoutes.SmartSearch to """ 
            {
                "results": [
                    {
                        "attributes": {
                            "colour": [
                                "White"
                            ],
                            "sizes": [
                                "28",
                                "29"
                            ],
                            "rating_percentage": "62",
                            "rating_count": "17"
                        },
                        "id": "WSH10_White",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>Time to lace up your kicks and beat that personal best in the Ana Running Short. It's designed with breathable mesh side panels to help keep you cool while you master the miles.</p>\n<p>&bull; Black/pink two-layer shorts.<br />&bull; Low-rise elastic waistband.<br />&bull; Relaxed fit. <br />&bull; Ultra-lightweight fabric. <br />&bull; Internal drawstring. <br />&bull; Machine wash/dry.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh10-white_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh10-white_main_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH10",
                        "link": "/ana-running-short",
                        "price": 40.0,
                        "promotion": "",
                        "sale_price": 40.0,
                        "title": "Ana Running Short",
                        "tags": [
                            "Eco Friendly"
                        ],
                        "rec_id": "tvw-CFSRaTPjfvIU:tVfbSPAsLYv-iiKw"
                    },
                    {
                        "attributes": {
                            "colour": [
                                "White"
                            ],
                            "sizes": [
                                "28",
                                "29"
                            ],
                            "rating_percentage": "52",
                            "rating_count": "22"
                        },
                        "id": "WSH09_White",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>You can run, bike or swim in the do-anything, water-resistance Mimi Short. No need to worry about rubbing-induced soreness either, with flatlock seams and soft, chafe-resistant material.</p>\n<p>&bull; Gray/seafoam two-layer shorts.<br />&bull; Water-resistant construction.<br />&bull; Inner mesh brief for breathable support.<br />&bull; 2.0\" inseam.<br />&bull; Reflective trim for visibility.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh09-white_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh09-white_main_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH09",
                        "link": "/mimi-all-purpose-short",
                        "price": 44.0,
                        "promotion": "",
                        "sale_price": 44.0,
                        "title": "Mimi All-Purpose Short",
                        "tags": [],
                        "rec_id": "tvw-CFSRaTPjfvIU:tbFjiGMzQkezsGmf"
                    },
                    {
                        "attributes": {
                            "colour": [
                                "Gray"
                            ],
                            "sizes": [
                                "28",
                                "29"
                            ],
                            "rating_percentage": "60",
                            "rating_count": "2"
                        },
                        "id": "WSH06_Gray",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>The Angel Light Running Short offers comfort in an ultra-lightweight, breathable package. With fabric infused with all-natural Cocona&reg; performance technology, it can whisk away sweat and block UV rays.</p>\n<p>&bull; Dark heather gray running shorts.<br />&bull; Snug fit. <br />&bull; Elastic waistband. <br />&bull; Cocona&reg; performance fabric. <br />&bull; Machine wash/dry.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_main_1.jpg",
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_alt1_1.jpg",
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_back_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH06",
                        "link": "/angel-light-running-short",
                        "price": 42.0,
                        "promotion": "",
                        "sale_price": 42.0,
                        "title": "Angel Light Running Short",
                        "tags": [],
                        "rec_id": "tvw-CFSRaTPjfvIU:t8a_xGzPeQ4M2_4Y"
                    },
                    {
                        "attributes": {
                            "colour": [
                                "Red"
                            ],
                            "sizes": [
                                "28",
                                "29"
                            ],
                            "rating_percentage": "74",
                            "rating_count": "24"
                        },
                        "id": "WSH11_Red",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>One of Luma's most popular items, the Ina Compression Short has you covered with exceptional support and comfort, whether you're running the trail, riding a bike or ripping out reps. The ventilating fabric offers cool relief and prevents irritating chafing.</p>\n<p>&bull; Royal blue bike shorts.<br />&bull; Compression fit. <br />&bull; Moisture-wicking. <br />&bull; Anti-microbial. <br />&bull; Machine wash/dry.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh11-red_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh11-red_main_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH11",
                        "link": "/ina-compression-short",
                        "price": 49.0,
                        "promotion": "",
                        "sale_price": 49.0,
                        "title": "Ina Compression Short",
                        "tags": [
                            "Erin Recommends"
                        ],
                        "rec_id": "tvw-CFSRaTPjfvIU:t58s5yr02OsqFaDC"
                    },
                    {
                        "attributes": {
                            "colour": [
                                "Green"
                            ],
                            "sizes": [
                                "28",
                                "29",
                                "30",
                                "31",
                                "32"
                            ],
                            "rating_percentage": "72",
                            "rating_count": "22"
                        },
                        "id": "WSH01_Green",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>Don't let the plain style fool you: the Fiona Fitness Short demands notice on several levels. Comfort, of course. But also a performance-grade wicking fabric that takes everything you can give.</p>\n<p>&bull; Black run shorts <br />- cotton/spandex.<br />&bull; 5&rdquo; inseam.<br />&bull; Machine wash/Line dry.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh01-green_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh01-green_main_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH01",
                        "link": "/fiona-fitness-short",
                        "price": 29.0,
                        "promotion": "",
                        "sale_price": 29.0,
                        "title": "Fiona Fitness Short",
                        "tags": [
                            "New Luma Yoga Collection",
                            "Eco Friendly"
                        ],
                        "rec_id": "tvw-CFSRaTPjfvIU:t28ONO63V04rDgN4"
                    },
                    {
                        "attributes": {
                            "colour": [
                                "Blue"
                            ],
                            "sizes": [
                                "28",
                                "29"
                            ],
                            "rating_percentage": "83",
                            "rating_count": "23"
                        },
                        "id": "WSH07_Blue",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>Your muscles know it's go time the second you pull on the Echo Fit Compression Short. A balance of firm, stimulating squeeze with breathability, it offers the support and comfort you need to give it your all.</p>\n<p>&bull; Black compression shorts.<br />&bull; High-waisted cut.<br />&bull; Compression fit.<br />&bull; Inseam: 1.0\". <br />&bull; Machine wash/dry.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh07-blue_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh07-blue_main_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH07",
                        "link": "/echo-fit-compression-short",
                        "price": 24.0,
                        "promotion": "",
                        "sale_price": 24.0,
                        "title": "Echo Fit Compression Short",
                        "tags": [
                            "New Luma Yoga Collection"
                        ],
                        "rec_id": "tvw-CFSRaTPjfvIU:tIsQf4HpTQ8qKJAf"
                    },
                    {
                        "attributes": {
                            "colour": [
                                "Black"
                            ],
                            "sizes": [
                                "28",
                                "29",
                                "30",
                                "31",
                                "32"
                            ],
                            "rating_percentage": "64",
                            "rating_count": "27"
                        },
                        "id": "WSH04_Black",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>Discover smooth jogging and chic comfort each time you slip into the Artemis Running Short. A unique maritime-inspired design and oolor theme features a stretchy drawstring waist.</p>\n<p>&bull; Black rouched shorts with mint waist. <br />&bull; Soft, lightweight construction.<br />&bull; LumaTech&trade; wicking technology.<br />&bull; Semi-fitted.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_main_1.jpg",
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_alt1_1.jpg",
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_back_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH04",
                        "link": "/artemis-running-short",
                        "price": 45.0,
                        "promotion": "",
                        "sale_price": 45.0,
                        "title": "Artemis Running Short",
                        "tags": [],
                        "rec_id": "tvw-CFSRaTPjfvIU:tRmw63zGa0ZoPOTo"
                    },
                    {
                        "attributes": {
                            "colour": [
                                "Gray"
                            ],
                            "sizes": [
                                "28",
                                "29",
                                "30",
                                "31",
                                "32"
                            ],
                            "rating_percentage": "95",
                            "rating_count": "9"
                        },
                        "id": "WSH02_Gray",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>Even laid-back women like to stay stylish, and that's just what the Maxima Drawstring Short delivers. An elastic waist keeps the fit flexible, on the deck at home or on a trail walk. Sporty tennis flair adds an athletic accent.</p>\n<p>&bull; Light gray run shorts <br />- cotton polyester. <br />&bull; Contrast binding. <br />&bull; 3\" inseam. <br />&bull; Machine wash/dry.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh02-gray_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh02-gray_main_1.jpg",
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh02-gray_back_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH02",
                        "link": "/maxima-drawstring-short",
                        "price": 28.0,
                        "promotion": "",
                        "sale_price": 28.0,
                        "title": "Maxima Drawstring Short",
                        "tags": [
                            "Women Sale"
                        ],
                        "rec_id": "tvw-CFSRaTPjfvIU:tp5GZtsQPjLJTzH9"
                    },
                    {
                        "attributes": {
                            "colour": [
                                "Red"
                            ],
                            "sizes": [
                                "28",
                                "29",
                                "30",
                                "31",
                                "32"
                            ],
                            "rating_percentage": "84",
                            "rating_count": "8"
                        },
                        "id": "WSH12_Red",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>A great short with a body-hugging design, the Erika Running Short is perfect for runners who prefer a fitted short rather than the traditional baggy variety.</p>\n<p>&bull; Seafoam pattern running shorts.<br />&bull; Elastic waistband.<br />&bull; Snug fit.<br />&bull; 4'' inseam.<br />&bull; 76% premium brushed Nylon / 24% Spandex.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh12-red_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh12-red_main_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH12",
                        "link": "/erika-running-short",
                        "price": 45.0,
                        "promotion": "",
                        "sale_price": 45.0,
                        "title": "Erika Running Short",
                        "tags": [
                            "Erin Recommends"
                        ],
                        "rec_id": "tvw-CFSRaTPjfvIU:t0u9_pW0vIX2ndPs"
                    },
                    {
                        "attributes": {
                            "colour": [
                                "Blue"
                            ],
                            "sizes": [
                                "28",
                                "29",
                                "30",
                                "31",
                                "32"
                            ],
                            "rating_percentage": "52",
                            "rating_count": "12"
                        },
                        "id": "WSH05_Blue",
                        "brand": "none",
                        "currency": "AUD",
                        "description": "<p>Designed for intense physical activity &ndash; think bikram &ndash; our Bess Yoga Short features moisture-wicking, four-way stretch fabric that lets you move in every which way. A vented gusset adds breathability and range of motion.</p>\n<p>&bull; Navy cotton shorts with light bue waist detail.<br />&bull; Front shirred waistband.<br />&bull; Flat-lock, chafe-free side seams.<br />&bull; Vented gusset.<br />&bull; Hidden interior pocket.<br />&bull; Sustainable and recycled fabric.</p>",
                        "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh05-blue_main_1.jpg",
                        "images": [
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh05-blue_main_1.jpg",
                            "https://demo.systema.ai/media/catalog/product/w/s/wsh05-blue_back_1.jpg"
                        ],
                        "in_stock": true,
                        "item_group_id": "WSH05",
                        "link": "/bess-yoga-short",
                        "price": 28.0,
                        "promotion": "",
                        "sale_price": 28.0,
                        "title": "Bess Yoga Short",
                        "tags": [
                            "Women Sale"
                        ],
                        "rec_id": "tvw-CFSRaTPjfvIU:tkk5rqNe-D-pajH4"
                    }
                ],
                "size": 10,
                "total": 10,
                "facets": {
                    "tags": [
                        {
                            "name": "eco friendly",
                            "count": 6
                        },
                        {
                            "name": "erin recommends",
                            "count": 6
                        },
                        {
                            "name": "new luma yoga collection",
                            "count": 6
                        },
                        {
                            "name": "women sale",
                            "count": 6
                        }
                    ],
                    "colour": [
                        {
                            "name": "orange",
                            "count": 5
                        },
                        {
                            "name": "black",
                            "count": 4
                        },
                        {
                            "name": "green",
                            "count": 4
                        },
                        {
                            "name": "purple",
                            "count": 4
                        },
                        {
                            "name": "gray",
                            "count": 3
                        },
                        {
                            "name": "red",
                            "count": 3
                        },
                        {
                            "name": "blue",
                            "count": 3
                        },
                        {
                            "name": "white",
                            "count": 2
                        },
                        {
                            "name": "yellow",
                            "count": 2
                        }
                    ],
                    "rating_percentage": [
                        {
                            "name": "52",
                            "count": 2
                        },
                        {
                            "name": "98",
                            "count": 2
                        },
                        {
                            "name": "72",
                            "count": 2
                        },
                        {
                            "name": "92",
                            "count": 2
                        },
                        {
                            "name": "75",
                            "count": 2
                        },
                        {
                            "name": "83",
                            "count": 2
                        },
                        {
                            "name": "62",
                            "count": 1
                        },
                        {
                            "name": "87",
                            "count": 1
                        },
                        {
                            "name": "53",
                            "count": 1
                        },
                        {
                            "name": "60",
                            "count": 1
                        }
                    ],
                    "sizes": [
                        {
                            "name": "28",
                            "count": 30
                        },
                        {
                            "name": "29",
                            "count": 30
                        },
                        {
                            "name": "30",
                            "count": 15
                        },
                        {
                            "name": "31",
                            "count": 15
                        },
                        {
                            "name": "32",
                            "count": 15
                        }
                    ],
                    "rating_count": [
                        {
                            "name": "22",
                            "count": 4
                        },
                        {
                            "name": "25",
                            "count": 2
                        },
                        {
                            "name": "29",
                            "count": 2
                        },
                        {
                            "name": "2",
                            "count": 2
                        },
                        {
                            "name": "9",
                            "count": 2
                        },
                        {
                            "name": "23",
                            "count": 2
                        },
                        {
                            "name": "27",
                            "count": 2
                        },
                        {
                            "name": "12",
                            "count": 2
                        },
                        {
                            "name": "17",
                            "count": 1
                        },
                        {
                            "name": "13",
                            "count": 1
                        }
                    ],
                    "sub_categories": []
                },
                "time": "0.11150",
                "pagination_timestamp": 1629332105220,
                "result_id": "tvw-CFSRaTPjfvIU"
            }
            """.trimIndent()
        )

        internal fun initMockEngine(): HttpClientEngine? {
            if (!hasMockInitialized) {
                val config = MockEngineConfig()

                config.addHandler { request ->
                    when (request.url.encodedPath) {
                        "/${SystemaRoutes.SmartSearch}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = searchRespJson[SystemaRoutes.SmartSearch] as String
                            )
                        }
                        else -> error("Unhandled path: ${request.url.encodedPath}")
                    }
                }

                mockEngine = MockEngine(config)

                hasMockInitialized = true
            }

            return mockEngine
        }
    }

    // unit test should override and return MockEngine
    // integration test should override and return null
    override fun initEngine(): HttpClientEngine? {
        return initMockEngine()
    }

    protected fun checkSmartSearchResponse(resp: SmartSearchResponse) {
        assertNotNull(resp.results)

        assertNotNull(resp.resultId)
        assertTrue { resp.resultId.isNotBlank() }

        assertNotNull(resp.size)
        assertTrue { resp.size > 0 }

        assertNotNull(resp.total)
        assertTrue { resp.total > 0 }
        assertTrue { resp.size <= resp.total }

        assertNotNull(resp.facets)

        assertNotNull(resp.paginationTimestamp)
        assertTrue { resp.paginationTimestamp > 0 }

        assertNotNull(resp.time)
        assertTrue { resp.time.isNotBlank() }
    }
}

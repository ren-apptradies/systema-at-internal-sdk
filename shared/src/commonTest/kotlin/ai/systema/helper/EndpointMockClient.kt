package ai.systema.helper

import io.ktor.client.engine.HttpClientEngine

internal interface EndpointMockClient {
    // unit test should override and return MockEngine
    // integration test should override and return null
    fun initEngine(): HttpClientEngine?
}

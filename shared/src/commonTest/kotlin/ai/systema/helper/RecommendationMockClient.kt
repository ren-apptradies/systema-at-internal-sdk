package ai.systema.helper

import ai.systema.endpoint.SystemaRoutes
import ai.systema.model.response.RecommendationResponse
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.MockEngineConfig
import io.ktor.client.engine.mock.respond
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.headersOf
import kotlinx.serialization.json.Json
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

internal open class RecommendationMockClient : EndpointMockClient {
    companion object {
        private lateinit var mockEngine: MockEngine
        private var hasInitialized: Boolean = false
        private val respJson: Map<String, String> = mapOf(
            SystemaRoutes.ProductRelated to """ 
        {
            "results": [
                {
                    "attributes": {
                        "colour": [
                            "White"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "52",
                        "rating_count": "22"
                    },
                    "id": "WSH09_White",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>You can run, bike or swim in the do-anything, water-resistance Mimi Short. No need to worry about rubbing-induced soreness either, with flatlock seams and soft, chafe-resistant material.</p>\n<p>&bull; Gray/seafoam two-layer shorts.<br />&bull; Water-resistant construction.<br />&bull; Inner mesh brief for breathable support.<br />&bull; 2.0\" inseam.<br />&bull; Reflective trim for visibility.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh09-white_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh09-white_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH09",
                    "link": "/mimi-all-purpose-short",
                    "price": 44,
                    "promotion": "",
                    "sale_price": 44,
                    "title": "Mimi All-Purpose Short",
                    "tags": [],
                    "rec_id": "tVUupDtsj1vSYmcp:tJdndTpAJmmta1-J"
                },
                {
                    "attributes": {
                        "colour": [
                            "Gray"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "60",
                        "rating_count": "2"
                    },
                    "id": "WSH06_Gray",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>The Angel Light Running Short offers comfort in an ultra-lightweight, breathable package. With fabric infused with all-natural Cocona&reg; performance technology, it can whisk away sweat and block UV rays.</p>\n<p>&bull; Dark heather gray running shorts.<br />&bull; Snug fit. <br />&bull; Elastic waistband. <br />&bull; Cocona&reg; performance fabric. <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH06",
                    "link": "/angel-light-running-short",
                    "price": 42,
                    "promotion": "",
                    "sale_price": 42,
                    "title": "Angel Light Running Short",
                    "tags": [],
                    "rec_id": "tVUupDtsj1vSYmcp:tIpUpUbl07n2hf-t"
                },
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "28",
                            "29",
                            "30",
                            "31",
                            "32"
                        ],
                        "rating_percentage": "64",
                        "rating_count": "27"
                    },
                    "id": "WSH04_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Discover smooth jogging and chic comfort each time you slip into the Artemis Running Short. A unique maritime-inspired design and oolor theme features a stretchy drawstring waist.</p>\n<p>&bull; Black rouched shorts with mint waist. <br />&bull; Soft, lightweight construction.<br />&bull; LumaTech&trade; wicking technology.<br />&bull; Semi-fitted.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH04",
                    "link": "/artemis-running-short",
                    "price": 45,
                    "promotion": "",
                    "sale_price": 45,
                    "title": "Artemis Running Short",
                    "tags": [],
                    "rec_id": "tVUupDtsj1vSYmcp:tR40rmJDS-dOJJuY"
                },
                {
                    "attributes": {
                        "colour": [
                            "Red"
                        ],
                        "sizes": [
                            "28",
                            "29",
                            "30",
                            "31",
                            "32"
                        ],
                        "rating_percentage": "84",
                        "rating_count": "8"
                    },
                    "id": "WSH12_Red",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>A great short with a body-hugging design, the Erika Running Short is perfect for runners who prefer a fitted short rather than the traditional baggy variety.</p>\n<p>&bull; Seafoam pattern running shorts.<br />&bull; Elastic waistband.<br />&bull; Snug fit.<br />&bull; 4'' inseam.<br />&bull; 76% premium brushed Nylon / 24% Spandex.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh12-red_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh12-red_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH12",
                    "link": "/erika-running-short",
                    "price": 45,
                    "promotion": "",
                    "sale_price": 45,
                    "title": "Erika Running Short",
                    "tags": [
                        "Erin Recommends"
                    ],
                    "rec_id": "tVUupDtsj1vSYmcp:tPAyYFoFYB9ZscU5"
                }
            ],
            "size": 4,
            "pagination_timestamp": 1629333151194,
            "time": "0.15688",
            "result_id": "tVUupDtsj1vSYmcp"
        }
            """.trimIndent(),

            SystemaRoutes.ProductComplementary to """
        {
            "results": [
                {
                    "attributes": {
                        "colour": [
                            "Orange"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "76",
                        "rating_count": "8"
                    },
                    "id": "WT01_Orange",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Style, performance and comfort mix it up in the Bella Tank. With striking color block contrast, fitted form and a built-in bra, you'll be supported and stylish at the same time.</p>\n<p>&bull; Navy blue tank top - cotton.<br />&bull; Feminine scoop neckline.<br />&bull; Power mesh lining in shelf bra for superior support.<br />&bull; Soft, breathable fabric.<br />&bull; Dry wick fabric to stay cool and dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/t/wt01-orange_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/t/wt01-orange_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WT01",
                    "link": "/bella-tank",
                    "price": 29,
                    "promotion": "",
                    "sale_price": 29,
                    "title": "Bella Tank",
                    "tags": [
                        "Eco Friendly"
                    ],
                    "rec_id": "tCuUkIGqP-FOBTN-:tbf88-XZqpdndVc7"
                },
                {
                    "attributes": {
                        "colour": [
                            "Purple"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "85",
                        "rating_count": "17"
                    },
                    "id": "WH06_Purple",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>The Daphne Hoodie is an attractive yet rugged layering or outer piece, especially for chilly days en route the studio, doing yardwork or enjoying outdoor recreation.</p>\n<p>&bull; Purple full zip hoodie with pink accents. <br />&bull; Heather texture.<br />&bull; 4-way stretch.<br />&bull; Pre-shrunk.<br />&bull; Hood lined in vegan Sherpa for added warmth.<br />&bull; Ribbed hem on hood and front pouch pocket.<br />&bull; 60% Cotton / 40% Polyester.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/h/wh06-purple_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/h/wh06-purple_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/h/wh06-purple_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/h/wh06-purple_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WH06",
                    "link": "/daphne-full-zip-hoodie",
                    "price": 59,
                    "promotion": "",
                    "sale_price": 59,
                    "title": "Daphne Full-Zip Hoodie",
                    "tags": [],
                    "rec_id": "tCuUkIGqP-FOBTN-:tAatB_wHffTGGqbZ"
                },
                {
                    "attributes": {
                        "colour": [
                            "Red"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "88",
                        "rating_count": "15"
                    },
                    "id": "WJ03_Red",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>It's hard to be uncomfortable in the Augusta Pullover Jacket with &frac14; zip. With an incredibly soft fleece lining and textured outer fabric, it offers reliable protection from the elements and a cozy fit as well.</p>\n<p>&bull; Pink half-zip pullover. <br />&bull; Front pouch pockets. <br />&bull; Fold-down collar.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/j/wj03-red_main_2.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/j/wj03-red_main_2.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/j/wj03-red_alt1_2.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/j/wj03-red_back_2.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WJ03",
                    "link": "/augusta-pullover-jacket",
                    "price": 57,
                    "promotion": "",
                    "sale_price": 57,
                    "title": "Augusta Pullover Jacket",
                    "tags": [],
                    "rec_id": "tCuUkIGqP-FOBTN-:tKUu6gsJ6TFJLdRH"
                },
                {
                    "attributes": {
                        "colour": [
                            "White"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "68",
                        "rating_count": "23"
                    },
                    "id": "WH05_White",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>The Selene Yoga Hoodie gets you to and from the studio in semi form-fitted comfort. Snug, sleek contours add fashion to function in this free-moving yoga warm up sweatshirt.</p>\n<p>&bull; Ivory heather full zip 3/4 sleeve hoodie.<br />&bull; Zip pocket at arm for convenient storage.<br />&bull; 24.0\" body length.<br />&bull; 89% Polyester / 11% Spandex.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/h/wh05-white_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/h/wh05-white_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/h/wh05-white_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WH05",
                    "link": "/selene-yoga-hoodie",
                    "price": 42,
                    "promotion": "",
                    "sale_price": 42,
                    "title": "Selene Yoga Hoodie",
                    "tags": [],
                    "rec_id": "tCuUkIGqP-FOBTN-:tuhSxPo5iMH1urvV"
                }
            ],
            "size": 4,
            "pagination_timestamp": 1629333250106,
            "time": "0.44522",
            "result_id": "tCuUkIGqP-FOBTN-"
        } 
            """.trimIndent(),

            SystemaRoutes.ProductSimilar to """ 
        {
            "results": [
                {
                    "attributes": {
                        "colour": [
                            "White"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "52",
                        "rating_count": "22"
                    },
                    "id": "WSH09_White",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>You can run, bike or swim in the do-anything, water-resistance Mimi Short. No need to worry about rubbing-induced soreness either, with flatlock seams and soft, chafe-resistant material.</p>\n<p>&bull; Gray/seafoam two-layer shorts.<br />&bull; Water-resistant construction.<br />&bull; Inner mesh brief for breathable support.<br />&bull; 2.0\" inseam.<br />&bull; Reflective trim for visibility.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh09-white_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh09-white_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH09",
                    "link": "/mimi-all-purpose-short",
                    "price": 44,
                    "promotion": "",
                    "sale_price": 44,
                    "title": "Mimi All-Purpose Short",
                    "tags": [],
                    "rec_id": "t4_Hy6ZBiBbZTwfQ:tIp_OCfoDZD9YTW0"
                },
                {
                    "attributes": {
                        "colour": [
                            "Gray"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "60",
                        "rating_count": "2"
                    },
                    "id": "WSH06_Gray",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>The Angel Light Running Short offers comfort in an ultra-lightweight, breathable package. With fabric infused with all-natural Cocona&reg; performance technology, it can whisk away sweat and block UV rays.</p>\n<p>&bull; Dark heather gray running shorts.<br />&bull; Snug fit. <br />&bull; Elastic waistband. <br />&bull; Cocona&reg; performance fabric. <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH06",
                    "link": "/angel-light-running-short",
                    "price": 42,
                    "promotion": "",
                    "sale_price": 42,
                    "title": "Angel Light Running Short",
                    "tags": [],
                    "rec_id": "t4_Hy6ZBiBbZTwfQ:t4QIp9U2C6th9FR3"
                },
                {
                    "attributes": {
                        "colour": [
                            "Red"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "74",
                        "rating_count": "24"
                    },
                    "id": "WSH11_Red",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>One of Luma's most popular items, the Ina Compression Short has you covered with exceptional support and comfort, whether you're running the trail, riding a bike or ripping out reps. The ventilating fabric offers cool relief and prevents irritating chafing.</p>\n<p>&bull; Royal blue bike shorts.<br />&bull; Compression fit. <br />&bull; Moisture-wicking. <br />&bull; Anti-microbial. <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh11-red_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh11-red_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH11",
                    "link": "/ina-compression-short",
                    "price": 49,
                    "promotion": "",
                    "sale_price": 49,
                    "title": "Ina Compression Short",
                    "tags": [
                        "Erin Recommends"
                    ],
                    "rec_id": "t4_Hy6ZBiBbZTwfQ:t4lpWnpRMG_6CsX6"
                },
                {
                    "attributes": {
                        "colour": [
                            "Green"
                        ],
                        "sizes": [
                            "28",
                            "29",
                            "30",
                            "31",
                            "32"
                        ],
                        "rating_percentage": "72",
                        "rating_count": "22"
                    },
                    "id": "WSH01_Green",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Don't let the plain style fool you: the Fiona Fitness Short demands notice on several levels. Comfort, of course. But also a performance-grade wicking fabric that takes everything you can give.</p>\n<p>&bull; Black run shorts <br />- cotton/spandex.<br />&bull; 5&rdquo; inseam.<br />&bull; Machine wash/Line dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh01-green_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh01-green_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH01",
                    "link": "/fiona-fitness-short",
                    "price": 29,
                    "promotion": "",
                    "sale_price": 29,
                    "title": "Fiona Fitness Short",
                    "tags": [
                        "New Luma Yoga Collection",
                        "Eco Friendly"
                    ],
                    "rec_id": "t4_Hy6ZBiBbZTwfQ:tqxGVQNuOoA8yPo1"
                }
            ],
            "size": 4,
            "pagination_timestamp": 1629333301235,
            "time": "0.04634",
            "result_id": "t4_Hy6ZBiBbZTwfQ"
        }
            """.trimIndent(),

            SystemaRoutes.CategoryPopular to """ 
        {
            "results": [
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "63",
                        "rating_count": "22"
                    },
                    "id": "WSH07_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Your muscles know it's go time the second you pull on the Echo Fit Compression Short. A balance of firm, stimulating squeeze with breathability, it offers the support and comfort you need to give it your all.</p>\n<p>&bull; Black compression shorts.<br />&bull; High-waisted cut.<br />&bull; Compression fit.<br />&bull; Inseam: 1.0\". <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH07",
                    "link": "/echo-fit-compression-short",
                    "price": 24,
                    "promotion": "",
                    "sale_price": 24,
                    "title": "Echo Fit Compression Short",
                    "tags": [
                        "New Luma Yoga Collection"
                    ],
                    "rec_id": "titEWd8pHAlUMWcN:t9yyhTcjpcjhjRF1"
                },
                {
                    "attributes": {
                        "colour": [
                            "Blue"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "87",
                        "rating_count": "27"
                    },
                    "id": "WT06_Blue",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>You've earned your figure, so stay cool and let it do all the talking in the form-fitted, sleek racerback Chloe Compete Tank. Designed for total range of motion and performance, the Nona is made with highly breathable mesh fabric.</p>\n<p>&bull; Royal blue tank top - nylon/spandex.<br />&bull; Flatlock stitching.<br />&bull; Moisture-wicking fabric. <br />&bull; Ergonomic seaming. <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/t/wt06-blue_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/t/wt06-blue_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/t/wt06-blue_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WT06",
                    "link": "/chloe-compete-tank",
                    "price": 39,
                    "promotion": "",
                    "sale_price": 39,
                    "title": "Chloe Compete Tank",
                    "tags": [],
                    "rec_id": "titEWd8pHAlUMWcN:ts9rkWEc55w9qQXV"
                },
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "54",
                        "rating_count": "8"
                    },
                    "id": "WS05_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>When you're too far to turn back, thank yourself for choosing the Desiree Fitness Tee. Its ultra-lightweight, ultra-breathable fabric wicks sweat away from your body and helps keeps you cool for the distance.</p>\n<p>&bull; Short-Sleeves.<br />&bull; Performance fabric.<br />&bull; Machine wash/line dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/ws05-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/ws05-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/ws05-black_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/ws05-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WS05",
                    "link": "/desiree-fitness-tee",
                    "price": 24,
                    "promotion": "",
                    "sale_price": 24,
                    "title": "Desiree Fitness Tee",
                    "tags": [
                        "Tees"
                    ],
                    "rec_id": "titEWd8pHAlUMWcN:tSNP_PRS4RD19y5f"
                },
                {
                    "attributes": {
                        "colour": [
                            "Blue"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "54",
                        "rating_count": "13"
                    },
                    "id": "WJ06_Blue",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>On colder-than-comfortable mornings, you'll love warming up in the Juno All-Ways Performanc Jacket, designed to compete with wind and chill. Built-in Cocona&reg; technology aids evaporation, while a special zip placket and stand-up collar keep your neck protected.</p>\n<p>&bull; Adjustable hood.</br>&bull; Fleece-lined, zippered hand pockets.</br>&bull; Thumbhole cuffs.</br>&bull; Full zip.</br>&bull; Mock-neck collar.</br>&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/j/wj06-blue_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/j/wj06-blue_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WJ06",
                    "link": "/juno-jacket",
                    "price": 77,
                    "promotion": "",
                    "sale_price": 77,
                    "title": "Juno Jacket",
                    "tags": [
                        "Women Sale",
                        "Performance Fabrics"
                    ],
                    "rec_id": "titEWd8pHAlUMWcN:tfR8-1KWre7D7EDC"
                }
            ],
            "size": 4,
            "pagination_timestamp": 1629333458832,
            "time": "0.06632",
            "result_id": "titEWd8pHAlUMWcN"
        }
            """.trimIndent(),

            SystemaRoutes.CategoryTrending to """ 
        {
            "results": [
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "63",
                        "rating_count": "22"
                    },
                    "id": "WSH07_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Your muscles know it's go time the second you pull on the Echo Fit Compression Short. A balance of firm, stimulating squeeze with breathability, it offers the support and comfort you need to give it your all.</p>\n<p>&bull; Black compression shorts.<br />&bull; High-waisted cut.<br />&bull; Compression fit.<br />&bull; Inseam: 1.0\". <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH07",
                    "link": "/echo-fit-compression-short",
                    "price": 24,
                    "promotion": "",
                    "sale_price": 24,
                    "title": "Echo Fit Compression Short",
                    "tags": [
                        "New Luma Yoga Collection"
                    ],
                    "rec_id": "tjrSNWDDf2YvgZFN:t59sY2Q1B6zH7qzc"
                },
                {
                    "attributes": {
                        "colour": [
                            "Purple"
                        ],
                        "sizes": [
                            "28",
                            "29",
                            "30",
                            "31",
                            "32"
                        ],
                        "rating_percentage": "88",
                        "rating_count": "21"
                    },
                    "id": "WSH08_Purple",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Fortunately, it's okay to look cute while you're working out. The Sybil Running Short combines a fun, color-blocked design with breathable mesh fabric for sporty-fun style.</p>\n<p>&bull; Blue running shorts with green waist.<br />&bull; Drawstring-adjustable waist.<br />&bull; 4\" inseam. Machine wash/line dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh08-purple_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh08-purple_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh08-purple_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH08",
                    "link": "/sybil-running-short",
                    "price": 44,
                    "promotion": "",
                    "sale_price": 44,
                    "title": "Sybil Running Short",
                    "tags": [
                        "Performance Fabrics"
                    ],
                    "rec_id": "tjrSNWDDf2YvgZFN:t8ygD6gaDW4p8YIx"
                },
                {
                    "attributes": {
                        "colour": [
                            "Gray"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "98",
                        "rating_count": "13"
                    },
                    "id": "WSH09_Gray",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>You can run, bike or swim in the do-anything, water-resistance Mimi Short. No need to worry about rubbing-induced soreness either, with flatlock seams and soft, chafe-resistant material.</p>\n<p>&bull; Gray/seafoam two-layer shorts.<br />&bull; Water-resistant construction.<br />&bull; Inner mesh brief for breathable support.<br />&bull; 2.0\" inseam.<br />&bull; Reflective trim for visibility.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh09-gray_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh09-gray_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh09-gray_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh09-gray_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH09",
                    "link": "/mimi-all-purpose-short",
                    "price": 44,
                    "promotion": "",
                    "sale_price": 44,
                    "title": "Mimi All-Purpose Short",
                    "tags": [],
                    "rec_id": "tjrSNWDDf2YvgZFN:tQ34yBSDGJM_ZtHs"
                },
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "53",
                        "rating_count": "29"
                    },
                    "id": "WSH10_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Time to lace up your kicks and beat that personal best in the Ana Running Short. It's designed with breathable mesh side panels to help keep you cool while you master the miles.</p>\n<p>&bull; Black/pink two-layer shorts.<br />&bull; Low-rise elastic waistband.<br />&bull; Relaxed fit. <br />&bull; Ultra-lightweight fabric. <br />&bull; Internal drawstring. <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH10",
                    "link": "/ana-running-short",
                    "price": 40,
                    "promotion": "",
                    "sale_price": 40,
                    "title": "Ana Running Short",
                    "tags": [
                        "Eco Friendly"
                    ],
                    "rec_id": "tjrSNWDDf2YvgZFN:tYMTN4N_Y5JkNAYQ"
                }
            ],
            "size": 4,
            "pagination_timestamp": 1629333427933,
            "time": "0.06242",
            "result_id": "tjrSNWDDf2YvgZFN"
        }
            """.trimIndent(),

            SystemaRoutes.Popular to """ 
        {
            "results": [
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "63",
                        "rating_count": "22"
                    },
                    "id": "WSH07_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Your muscles know it's go time the second you pull on the Echo Fit Compression Short. A balance of firm, stimulating squeeze with breathability, it offers the support and comfort you need to give it your all.</p>\n<p>&bull; Black compression shorts.<br />&bull; High-waisted cut.<br />&bull; Compression fit.<br />&bull; Inseam: 1.0\". <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH07",
                    "link": "/echo-fit-compression-short",
                    "price": 24,
                    "promotion": "",
                    "sale_price": 24,
                    "title": "Echo Fit Compression Short",
                    "tags": [
                        "New Luma Yoga Collection"
                    ],
                    "rec_id": "tM1loTB-XnJuI9Vs:teDp1JK39fA7WakK"
                },
                {
                    "attributes": {
                        "colour": [
                            "Blue"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "87",
                        "rating_count": "27"
                    },
                    "id": "WT06_Blue",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>You've earned your figure, so stay cool and let it do all the talking in the form-fitted, sleek racerback Chloe Compete Tank. Designed for total range of motion and performance, the Nona is made with highly breathable mesh fabric.</p>\n<p>&bull; Royal blue tank top - nylon/spandex.<br />&bull; Flatlock stitching.<br />&bull; Moisture-wicking fabric. <br />&bull; Ergonomic seaming. <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/t/wt06-blue_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/t/wt06-blue_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/t/wt06-blue_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WT06",
                    "link": "/chloe-compete-tank",
                    "price": 39,
                    "promotion": "",
                    "sale_price": 39,
                    "title": "Chloe Compete Tank",
                    "tags": [],
                    "rec_id": "tM1loTB-XnJuI9Vs:tOT4PpXNWeWkyGaW"
                },
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "54",
                        "rating_count": "8"
                    },
                    "id": "WS05_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>When you're too far to turn back, thank yourself for choosing the Desiree Fitness Tee. Its ultra-lightweight, ultra-breathable fabric wicks sweat away from your body and helps keeps you cool for the distance.</p>\n<p>&bull; Short-Sleeves.<br />&bull; Performance fabric.<br />&bull; Machine wash/line dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/ws05-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/ws05-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/ws05-black_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/ws05-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WS05",
                    "link": "/desiree-fitness-tee",
                    "price": 24,
                    "promotion": "",
                    "sale_price": 24,
                    "title": "Desiree Fitness Tee",
                    "tags": [
                        "Tees"
                    ],
                    "rec_id": "tM1loTB-XnJuI9Vs:t1361AjUhcW-wxR6"
                },
                {
                    "attributes": {
                        "colour": [
                            "Blue"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "54",
                        "rating_count": "13"
                    },
                    "id": "WJ06_Blue",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>On colder-than-comfortable mornings, you'll love warming up in the Juno All-Ways Performanc Jacket, designed to compete with wind and chill. Built-in Cocona&reg; technology aids evaporation, while a special zip placket and stand-up collar keep your neck protected.</p>\n<p>&bull; Adjustable hood.</br>&bull; Fleece-lined, zippered hand pockets.</br>&bull; Thumbhole cuffs.</br>&bull; Full zip.</br>&bull; Mock-neck collar.</br>&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/j/wj06-blue_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/j/wj06-blue_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WJ06",
                    "link": "/juno-jacket",
                    "price": 77,
                    "promotion": "",
                    "sale_price": 77,
                    "title": "Juno Jacket",
                    "tags": [
                        "Women Sale",
                        "Performance Fabrics"
                    ],
                    "rec_id": "tM1loTB-XnJuI9Vs:t2RFH9rPcLwwMhu4"
                }
            ],
            "size": 4,
            "pagination_timestamp": 1629334244604,
            "time": "0.06328",
            "result_id": "tM1loTB-XnJuI9Vs"
        }
            """.trimIndent(),

            SystemaRoutes.Trending to """
        {
            "results": [
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "63",
                        "rating_count": "22"
                    },
                    "id": "WSH07_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Your muscles know it's go time the second you pull on the Echo Fit Compression Short. A balance of firm, stimulating squeeze with breathability, it offers the support and comfort you need to give it your all.</p>\n<p>&bull; Black compression shorts.<br />&bull; High-waisted cut.<br />&bull; Compression fit.<br />&bull; Inseam: 1.0\". <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH07",
                    "link": "/echo-fit-compression-short",
                    "price": 24,
                    "promotion": "",
                    "sale_price": 24,
                    "title": "Echo Fit Compression Short",
                    "tags": [
                        "New Luma Yoga Collection"
                    ],
                    "rec_id": "tMUb6f5YIW2fo36Q:t7AAl8OCP_YzSevD"
                },
                {
                    "attributes": {
                        "colour": [
                            "Purple"
                        ],
                        "sizes": [
                            "28",
                            "29",
                            "30",
                            "31",
                            "32"
                        ],
                        "rating_percentage": "88",
                        "rating_count": "21"
                    },
                    "id": "WSH08_Purple",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Fortunately, it's okay to look cute while you're working out. The Sybil Running Short combines a fun, color-blocked design with breathable mesh fabric for sporty-fun style.</p>\n<p>&bull; Blue running shorts with green waist.<br />&bull; Drawstring-adjustable waist.<br />&bull; 4\" inseam. Machine wash/line dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh08-purple_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh08-purple_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh08-purple_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH08",
                    "link": "/sybil-running-short",
                    "price": 44,
                    "promotion": "",
                    "sale_price": 44,
                    "title": "Sybil Running Short",
                    "tags": [
                        "Performance Fabrics"
                    ],
                    "rec_id": "tMUb6f5YIW2fo36Q:tShoPlmhWNRNPuC2"
                },
                {
                    "attributes": {
                        "colour": [
                            "Gray"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "98",
                        "rating_count": "13"
                    },
                    "id": "WSH09_Gray",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>You can run, bike or swim in the do-anything, water-resistance Mimi Short. No need to worry about rubbing-induced soreness either, with flatlock seams and soft, chafe-resistant material.</p>\n<p>&bull; Gray/seafoam two-layer shorts.<br />&bull; Water-resistant construction.<br />&bull; Inner mesh brief for breathable support.<br />&bull; 2.0\" inseam.<br />&bull; Reflective trim for visibility.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh09-gray_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh09-gray_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh09-gray_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh09-gray_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH09",
                    "link": "/mimi-all-purpose-short",
                    "price": 44,
                    "promotion": "",
                    "sale_price": 44,
                    "title": "Mimi All-Purpose Short",
                    "tags": [],
                    "rec_id": "tMUb6f5YIW2fo36Q:t6_zXaHjvrPiQqqt"
                },
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "53",
                        "rating_count": "29"
                    },
                    "id": "WSH10_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Time to lace up your kicks and beat that personal best in the Ana Running Short. It's designed with breathable mesh side panels to help keep you cool while you master the miles.</p>\n<p>&bull; Black/pink two-layer shorts.<br />&bull; Low-rise elastic waistband.<br />&bull; Relaxed fit. <br />&bull; Ultra-lightweight fabric. <br />&bull; Internal drawstring. <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH10",
                    "link": "/ana-running-short",
                    "price": 40,
                    "promotion": "",
                    "sale_price": 40,
                    "title": "Ana Running Short",
                    "tags": [
                        "Eco Friendly"
                    ],
                    "rec_id": "tMUb6f5YIW2fo36Q:tO2vW_e9Uf-5cu-f"
                }
            ],
            "size": 4,
            "pagination_timestamp": 1629334304054,
            "time": "0.06608",
            "result_id": "tMUb6f5YIW2fo36Q"
        }
            """.trimIndent(),

            SystemaRoutes.CartRelated to """ 
        {
            "results": [
                {
                    "attributes": {
                        "colour": [
                            "White"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "52",
                        "rating_count": "22"
                    },
                    "id": "WSH09_White",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>You can run, bike or swim in the do-anything, water-resistance Mimi Short. No need to worry about rubbing-induced soreness either, with flatlock seams and soft, chafe-resistant material.</p>\n<p>&bull; Gray/seafoam two-layer shorts.<br />&bull; Water-resistant construction.<br />&bull; Inner mesh brief for breathable support.<br />&bull; 2.0\" inseam.<br />&bull; Reflective trim for visibility.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh09-white_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh09-white_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH09",
                    "link": "/mimi-all-purpose-short",
                    "price": 44,
                    "promotion": "",
                    "sale_price": 44,
                    "title": "Mimi All-Purpose Short",
                    "tags": [],
                    "rec_id": "tODhUFyha6fnb45z:tWjjixBzUlyq2n7Y"
                },
                {
                    "attributes": {
                        "colour": [
                            "Gray"
                        ],
                        "sizes": [
                            "28",
                            "29"
                        ],
                        "rating_percentage": "60",
                        "rating_count": "2"
                    },
                    "id": "WSH06_Gray",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>The Angel Light Running Short offers comfort in an ultra-lightweight, breathable package. With fabric infused with all-natural Cocona&reg; performance technology, it can whisk away sweat and block UV rays.</p>\n<p>&bull; Dark heather gray running shorts.<br />&bull; Snug fit. <br />&bull; Elastic waistband. <br />&bull; Cocona&reg; performance fabric. <br />&bull; Machine wash/dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh06-gray_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH06",
                    "link": "/angel-light-running-short",
                    "price": 42,
                    "promotion": "",
                    "sale_price": 42,
                    "title": "Angel Light Running Short",
                    "tags": [],
                    "rec_id": "tODhUFyha6fnb45z:tZHUpeKGIIDN3XbP"
                },
                {
                    "attributes": {
                        "colour": [
                            "Black"
                        ],
                        "sizes": [
                            "28",
                            "29",
                            "30",
                            "31",
                            "32"
                        ],
                        "rating_percentage": "64",
                        "rating_count": "27"
                    },
                    "id": "WSH04_Black",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Discover smooth jogging and chic comfort each time you slip into the Artemis Running Short. A unique maritime-inspired design and oolor theme features a stretchy drawstring waist.</p>\n<p>&bull; Black rouched shorts with mint waist. <br />&bull; Soft, lightweight construction.<br />&bull; LumaTech&trade; wicking technology.<br />&bull; Semi-fitted.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh04-black_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH04",
                    "link": "/artemis-running-short",
                    "price": 45,
                    "promotion": "",
                    "sale_price": 45,
                    "title": "Artemis Running Short",
                    "tags": [],
                    "rec_id": "tODhUFyha6fnb45z:tm48B7vk0M9pUYcN"
                },
                {
                    "attributes": {
                        "colour": [
                            "Red"
                        ],
                        "sizes": [
                            "28",
                            "29",
                            "30",
                            "31",
                            "32"
                        ],
                        "rating_percentage": "84",
                        "rating_count": "8"
                    },
                    "id": "WSH12_Red",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>A great short with a body-hugging design, the Erika Running Short is perfect for runners who prefer a fitted short rather than the traditional baggy variety.</p>\n<p>&bull; Seafoam pattern running shorts.<br />&bull; Elastic waistband.<br />&bull; Snug fit.<br />&bull; 4'' inseam.<br />&bull; 76% premium brushed Nylon / 24% Spandex.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh12-red_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/s/wsh12-red_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WSH12",
                    "link": "/erika-running-short",
                    "price": 45,
                    "promotion": "",
                    "sale_price": 45,
                    "title": "Erika Running Short",
                    "tags": [
                        "Erin Recommends"
                    ],
                    "rec_id": "tODhUFyha6fnb45z:tiwHMsbeJ1olp427"
                }
            ],
            "size": 4,
            "pagination_timestamp": 1629333350487,
            "time": "0.23756",
            "result_id": "tODhUFyha6fnb45z"
        }
            """.trimIndent(),

            SystemaRoutes.CartComplementary to """ 
        {
            "results": [
                {
                    "attributes": {
                        "colour": [
                            "Orange"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "76",
                        "rating_count": "8"
                    },
                    "id": "WT01_Orange",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>Style, performance and comfort mix it up in the Bella Tank. With striking color block contrast, fitted form and a built-in bra, you'll be supported and stylish at the same time.</p>\n<p>&bull; Navy blue tank top - cotton.<br />&bull; Feminine scoop neckline.<br />&bull; Power mesh lining in shelf bra for superior support.<br />&bull; Soft, breathable fabric.<br />&bull; Dry wick fabric to stay cool and dry.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/t/wt01-orange_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/t/wt01-orange_main_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WT01",
                    "link": "/bella-tank",
                    "price": 29,
                    "promotion": "",
                    "sale_price": 29,
                    "title": "Bella Tank",
                    "tags": [
                        "Eco Friendly"
                    ],
                    "rec_id": "tZ49-lH8UBGHL1LN:tqEb2oYjT5TVgPOu"
                },
                {
                    "attributes": {
                        "colour": [
                            "Purple"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "85",
                        "rating_count": "17"
                    },
                    "id": "WH06_Purple",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>The Daphne Hoodie is an attractive yet rugged layering or outer piece, especially for chilly days en route the studio, doing yardwork or enjoying outdoor recreation.</p>\n<p>&bull; Purple full zip hoodie with pink accents. <br />&bull; Heather texture.<br />&bull; 4-way stretch.<br />&bull; Pre-shrunk.<br />&bull; Hood lined in vegan Sherpa for added warmth.<br />&bull; Ribbed hem on hood and front pouch pocket.<br />&bull; 60% Cotton / 40% Polyester.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/h/wh06-purple_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/h/wh06-purple_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/h/wh06-purple_alt1_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/h/wh06-purple_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WH06",
                    "link": "/daphne-full-zip-hoodie",
                    "price": 59,
                    "promotion": "",
                    "sale_price": 59,
                    "title": "Daphne Full-Zip Hoodie",
                    "tags": [],
                    "rec_id": "tZ49-lH8UBGHL1LN:tbQqknlM1AZ8cnpu"
                },
                {
                    "attributes": {
                        "colour": [
                            "Red"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "88",
                        "rating_count": "15"
                    },
                    "id": "WJ03_Red",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>It's hard to be uncomfortable in the Augusta Pullover Jacket with &frac14; zip. With an incredibly soft fleece lining and textured outer fabric, it offers reliable protection from the elements and a cozy fit as well.</p>\n<p>&bull; Pink half-zip pullover. <br />&bull; Front pouch pockets. <br />&bull; Fold-down collar.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/j/wj03-red_main_2.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/j/wj03-red_main_2.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/j/wj03-red_alt1_2.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/j/wj03-red_back_2.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WJ03",
                    "link": "/augusta-pullover-jacket",
                    "price": 57,
                    "promotion": "",
                    "sale_price": 57,
                    "title": "Augusta Pullover Jacket",
                    "tags": [],
                    "rec_id": "tZ49-lH8UBGHL1LN:tFROpjYrPFvCXAzZ"
                },
                {
                    "attributes": {
                        "colour": [
                            "White"
                        ],
                        "sizes": [
                            "L",
                            "M",
                            "S",
                            "XL",
                            "XS"
                        ],
                        "rating_percentage": "68",
                        "rating_count": "23"
                    },
                    "id": "WH05_White",
                    "brand": "none",
                    "currency": "AUD",
                    "description": "<p>The Selene Yoga Hoodie gets you to and from the studio in semi form-fitted comfort. Snug, sleek contours add fashion to function in this free-moving yoga warm up sweatshirt.</p>\n<p>&bull; Ivory heather full zip 3/4 sleeve hoodie.<br />&bull; Zip pocket at arm for convenient storage.<br />&bull; 24.0\" body length.<br />&bull; 89% Polyester / 11% Spandex.</p>",
                    "image": "https://demo.systema.ai/media/catalog/product/w/h/wh05-white_main_1.jpg",
                    "images": [
                        "https://demo.systema.ai/media/catalog/product/w/h/wh05-white_main_1.jpg",
                        "https://demo.systema.ai/media/catalog/product/w/h/wh05-white_back_1.jpg"
                    ],
                    "in_stock": true,
                    "item_group_id": "WH05",
                    "link": "/selene-yoga-hoodie",
                    "price": 42,
                    "promotion": "",
                    "sale_price": 42,
                    "title": "Selene Yoga Hoodie",
                    "tags": [],
                    "rec_id": "tZ49-lH8UBGHL1LN:tT9kCtE0QTopocpW"
                }
            ],
            "size": 4,
            "pagination_timestamp": 1629333389998,
            "time": "0.34313",
            "result_id": "tZ49-lH8UBGHL1LN"
        }
            """.trimIndent()
        )

        internal fun getMockResp(): RecommendationResponse {
            return Json.decodeFromString(
                RecommendationResponse.serializer(),
                respJson[SystemaRoutes.ProductRelated]!!
            )
        }

        internal fun initMockEngine(): MockEngine {
            if (!hasInitialized) {
                val config = MockEngineConfig()

                config.addHandler { request ->
                    when (request.url.encodedPath) {
                        "/${SystemaRoutes.CartRelated}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = respJson[SystemaRoutes.CartRelated] as String
                            )
                        }
                        "/${SystemaRoutes.CartComplementary}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = respJson[SystemaRoutes.CartComplementary] as String
                            )
                        }
                        "/${SystemaRoutes.ProductRelated}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = respJson[SystemaRoutes.ProductRelated] as String
                            )
                        }
                        "/${SystemaRoutes.ProductComplementary}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = respJson[SystemaRoutes.ProductComplementary] as String
                            )
                        }
                        "/${SystemaRoutes.ProductSimilar}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = respJson[SystemaRoutes.ProductSimilar] as String
                            )
                        }
                        "/${SystemaRoutes.CategoryPopular}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = respJson[SystemaRoutes.CategoryPopular] as String
                            )
                        }
                        "/${SystemaRoutes.CategoryTrending}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = respJson[SystemaRoutes.CategoryTrending] as String
                            )
                        }
                        "/${SystemaRoutes.Popular}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = respJson[SystemaRoutes.Popular] as String
                            )
                        }
                        "/${SystemaRoutes.Trending}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = respJson[SystemaRoutes.Trending] as String
                            )
                        }
                        else -> error("Unhandled path: ${request.url.encodedPath}")
                    }
                }

                mockEngine = MockEngine(config)

                hasInitialized = true
            }

            return mockEngine
        }
    }

    // unit test should override and return MockEngine
    // integration test should override and return null
    override fun initEngine(): HttpClientEngine? {
        return initMockEngine()
    }

    protected fun checkRecommendationResponse(resp: RecommendationResponse) {
        assertNotNull(resp.results)

        assertNotNull(resp.resultId)
        assertTrue { resp.resultId.isNotBlank() }

        assertNotNull(resp.size)
        assertTrue { resp.size > 0 }

        assertNotNull(resp.paginationTimestamp)
        assertTrue { resp.paginationTimestamp > 0 }

        assertNotNull(resp.time)
        assertTrue { resp.time.isNotBlank() }
    }
}

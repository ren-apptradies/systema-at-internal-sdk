package ai.systema.helper

import ai.systema.endpoint.SystemaRoutes
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.MockEngineConfig
import io.ktor.client.engine.mock.respond
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.headersOf

internal open class TrackerMockClient : EndpointMockClient {
    companion object {
        private lateinit var mockEngine: MockEngine
        private var hasInitialized: Boolean = false

        fun initMockEngine(): MockEngine {
            if (!hasInitialized) {
                val config = MockEngineConfig()

                config.addHandler { request ->
                    when (request.url.encodedPath) {
                        "/${SystemaRoutes.Tracker}" -> {
                            respond(
                                status = HttpStatusCode.NoContent,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = ""
                            )
                        }
                        else -> error("Unhandled path: ${request.url.encodedPath}")
                    }
                }

                mockEngine = MockEngine(config)
                hasInitialized = true
            }

            return mockEngine
        }
    }

    override fun initEngine(): HttpClientEngine? {
        return initMockEngine()
    }
}

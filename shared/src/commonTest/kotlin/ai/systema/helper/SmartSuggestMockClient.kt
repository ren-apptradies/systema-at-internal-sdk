package ai.systema.helper

import ai.systema.endpoint.SystemaRoutes
import ai.systema.model.response.SmartSuggestResponse
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.MockEngineConfig
import io.ktor.client.engine.mock.respond
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.headersOf
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

internal open class SmartSuggestMockClient : EndpointMockClient {
    companion object {

        private lateinit var mockEngine: MockEngine
        private var hasInitialized: Boolean = false

        private val suggestRespJson: Map<String, String> = mapOf(
            SystemaRoutes.SmartSuggest to """{
                "query": "WSH10",
                "results": {
                    "terms": [
                        "wsh10_black",
                        "wsh10-29-black",
                        "wsh10-28-black"
                    ],
                    "did_you_mean": [],
                    "categories": [],
                    "products": [
                        {
                            "attributes": {
                                "colour": [
                                    "Black"
                                ],
                                "sizes": [
                                    "28",
                                    "29"
                                ],
                                "rating_percentage": "53",
                                "rating_count": "29"
                            },
                            "id": "WSH10_Black",
                            "brand": "none",
                            "currency": "AUD",
                            "description": "<p>Time to lace up your kicks and beat that personal best in the Ana Running Short. It's designed with breathable mesh side panels to help keep you cool while you master the miles.</p>\n<p>&bull; Black/pink two-layer shorts.<br />&bull; Low-rise elastic waistband.<br />&bull; Relaxed fit. <br />&bull; Ultra-lightweight fabric. <br />&bull; Internal drawstring. <br />&bull; Machine wash/dry.</p>",
                            "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_main_1.jpg",
                            "images": [
                                "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_main_1.jpg",
                                "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_alt1_1.jpg",
                                "https://demo.systema.ai/media/catalog/product/w/s/wsh10-black_back_1.jpg"
                            ],
                            "in_stock": true,
                            "item_group_id": "WSH10",
                            "link": "/ana-running-short",
                            "price": 40.0,
                            "promotion": "",
                            "sale_price": 40.0,
                            "title": "Ana Running Short",
                            "tags": [
                                "Eco Friendly"
                            ],
                            "rec_id": "tN4K8b6OFbcJ-rMs:t5q65ufwvtf1ieYT"
                        },
                        {
                            "attributes": {
                                "colour": [
                                    "Black"
                                ],
                                "sizes": [
                                    "28",
                                    "29"
                                ],
                                "rating_percentage": "63",
                                "rating_count": "22"
                            },
                            "id": "WSH07_Black",
                            "brand": "none",
                            "currency": "AUD",
                            "description": "<p>Your muscles know it's go time the second you pull on the Echo Fit Compression Short. A balance of firm, stimulating squeeze with breathability, it offers the support and comfort you need to give it your all.</p>\n<p>&bull; Black compression shorts.<br />&bull; High-waisted cut.<br />&bull; Compression fit.<br />&bull; Inseam: 1.0\". <br />&bull; Machine wash/dry.</p>",
                            "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                            "images": [
                                "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_main_1.jpg",
                                "https://demo.systema.ai/media/catalog/product/w/s/wsh07-black_back_1.jpg"
                            ],
                            "in_stock": true,
                            "item_group_id": "WSH07",
                            "link": "/echo-fit-compression-short",
                            "price": 24.0,
                            "promotion": "",
                            "sale_price": 24.0,
                            "title": "Echo Fit Compression Short",
                            "tags": [
                                "New Luma Yoga Collection"
                            ],
                            "rec_id": "tN4K8b6OFbcJ-rMs:tnrjoGx4QhK3wpYS"
                        },
                        {
                            "attributes": {
                                "colour": [
                                    "Purple"
                                ],
                                "sizes": [
                                    "28",
                                    "29",
                                    "30",
                                    "31",
                                    "32"
                                ],
                                "rating_percentage": "88",
                                "rating_count": "21"
                            },
                            "id": "WSH08_Purple",
                            "brand": "none",
                            "currency": "AUD",
                            "description": "<p>Fortunately, it's okay to look cute while you're working out. The Sybil Running Short combines a fun, color-blocked design with breathable mesh fabric for sporty-fun style.</p>\n<p>&bull; Blue running shorts with green waist.<br />&bull; Drawstring-adjustable waist.<br />&bull; 4\" inseam. Machine wash/line dry.</p>",
                            "image": "https://demo.systema.ai/media/catalog/product/w/s/wsh08-purple_main_1.jpg",
                            "images": [
                                "https://demo.systema.ai/media/catalog/product/w/s/wsh08-purple_main_1.jpg",
                                "https://demo.systema.ai/media/catalog/product/w/s/wsh08-purple_back_1.jpg"
                            ],
                            "in_stock": true,
                            "item_group_id": "WSH08",
                            "link": "/sybil-running-short",
                            "price": 44.0,
                            "promotion": "",
                            "sale_price": 44.0,
                            "title": "Sybil Running Short",
                            "tags": [
                                "Performance Fabrics"
                            ],
                            "rec_id": "tN4K8b6OFbcJ-rMs:tS7re2mwBfOQDpGR"
                        }
                    ]
                },
                "result_id": "tN4K8b6OFbcJ-rMs",
                "time": "0.19113"
            }
            """.trimIndent()
        )

        fun initMockEngine(): HttpClientEngine? {
            if (!hasInitialized) {
                val config = MockEngineConfig()

                config.addHandler { request ->
                    when (request.url.encodedPath) {
                        "/${SystemaRoutes.SmartSuggest}" -> {
                            respond(
                                status = HttpStatusCode.OK,
                                headers = headersOf("Content-Type", listOf(ContentType.Application.Json.toString())),
                                content = suggestRespJson[SystemaRoutes.SmartSuggest] as String
                            )
                        }
                        else -> error("Unhandled path: ${request.url.encodedPath}")
                    }
                }

                mockEngine = MockEngine(config)

                hasInitialized = true
            }

            return mockEngine
        }
    }

    // unit test should override and return MockEngine
    // integration test should override and return null
    override fun initEngine(): HttpClientEngine? {
        return initMockEngine()
    }

    protected fun checkSmartSuggestResponse(resp: SmartSuggestResponse) {
        assertNotNull(resp.results)
        assertNotNull(resp.results.terms)
        assertNotNull(resp.results.didYouMean)
        assertNotNull(resp.results.categories)
        assertNotNull(resp.results.products)
        assertTrue { resp.results.products.isNotEmpty() }

        assertNotNull(resp.resultId)
        assertTrue { resp.resultId.isNotBlank() }

        assertNotNull(resp.time)
        assertTrue { resp.time.isNotBlank() }

        assertNotNull(resp.query)
        assertTrue { resp.query.isNotBlank() }
    }
}

package ai.systema.exception

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

internal class SystemaRuntimeExceptionTest {
    @Test
    fun testSystemaApiExceptionTest() {
        val cause = IllegalArgumentException("test")
        val ex = SystemaApiException(cause)
        assertEquals("API error occurred", ex.message)
        assertNotNull(ex.cause)
    }

    @Test
    fun testDataNotFoundExceptionTest() {
        val cause = IllegalArgumentException("test")
        val ex = DataNotFoundException(cause)
        assertEquals("Data not found", ex.message)
        assertNotNull(ex.cause)
    }

    @Test
    fun testEventFlowExceptionTest() {
        val cause = IllegalStateException("invalid state")
        val ex = SystemaListenerException(cause)
        assertEquals("Exception occurred in listener", ex.message)
        assertNotNull(ex.cause)
    }
}

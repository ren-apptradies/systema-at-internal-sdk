package ai.systema.model

import ai.systema.helper.internal.JsonNoDefaults
import io.ktor.http.encodeURLParameter
import io.ktor.util.InternalAPI
import io.ktor.util.encodeBase64
import kotlin.test.Test
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

internal class SystemaDeviceTest {

    @OptIn(InternalAPI::class)
    @Test
    fun testToUserAgent() {
        val deviceInfo = SystemaDevice(
            osVersion = "",
            model = "model; '!@#$%^&*(){}~`|\\;:,<.>/?", // some garbage that needs URL encoding
            deviceId = "buildID",
            deviceName = "device",
            fingerprint = "abcdefgh",
            release = "0.0.0",
            product = "product",
            brand = "brand",
            display = "100x200",
            manufacturer = "Systema"
        )

        val str = deviceInfo.toUserAgent()
        assertNotNull(str)
        assertTrue { str.contains(deviceInfo.osVersion!!) }
        assertTrue { str.contains(deviceInfo.model!!.encodeURLParameter()) }
        assertTrue {
            str.contains(
                JsonNoDefaults.encodeToString(SystemaDevice.serializer(), deviceInfo).encodeBase64()
            )
        }
    }
}

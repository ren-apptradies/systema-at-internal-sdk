package ai.systema.model.tracker.wishlist

import ai.systema.TestConfig
import ai.systema.constants.SystemaConstants
import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.CurTrackEventDate
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class WishlistItemRelinquishedEventTest {

    companion object MockConstants {
        private const val productId = "product_id_1"
        private const val referrer = "referrer_1"
        private const val url = "https://fake-url-does-not-exist.com.au"
        private val wishlistItem = WishlistItem(
            itemId = "item_id_1"
        )
        private val currentDate = CurTrackEventDate
    }

    @Test
    fun testBuildWishlistItemAcquiredEvent() = testSuspend {
        val connector = TestConfig.getConnector()

        val instanceWrongEventType = assertFailsWith<IllegalArgumentException> {
            WishlistItemRelinquishedEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.AddToCart,
                productId = productId,
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                item = wishlistItem,
                version = SystemaConstants.Version,
            )
        }

        val instanceBlankUrl = assertFailsWith<IllegalArgumentException> {
            WishlistItemRelinquishedEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.RemoveFromWishlist,
                productId = productId,
                referrer = referrer,
                url = " ",
                eventDate = currentDate,
                item = wishlistItem,
                version = SystemaConstants.Version,
            )
        }

        val instanceBlankProductId = assertFailsWith<IllegalArgumentException> {
            WishlistItemRelinquishedEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.RemoveFromWishlist,
                productId = "",
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                item = wishlistItem,
                version = SystemaConstants.Version,
            )
        }

        assertEquals(instanceWrongEventType.message, "Invalid \"Type\". Expecting \"RemoveFromWishlist\"")
        assertEquals(instanceBlankUrl.message, "Invalid \"Url\". Cannot be blank.")
        assertEquals(instanceBlankProductId.message, "Invalid \"ProductId\". Cannot be blank.")
    }
}

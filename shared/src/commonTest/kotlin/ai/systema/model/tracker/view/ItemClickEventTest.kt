package ai.systema.model.tracker.view

import ai.systema.TestConfig
import ai.systema.constants.SystemaConstants
import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.CurTrackEventDate
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class ItemClickEventTest {

    private companion object MockConstants {
        private const val productId = "product_id_1"
        private const val recId = "rec_id_1"
        private const val referrer = "referrer_1"
        private const val url = "https://fake-url-does-not-exist.com.au"
        private val currentDate = CurTrackEventDate
    }

    @Test
    fun testItemClickEventRequires() = testSuspend {
        val connector = TestConfig.getConnector()
        val instanceWrongEventType = assertFailsWith<IllegalArgumentException> {
            ItemClickEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.AddToCart,
                productId = productId,
                recId = recId,
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                version = SystemaConstants.Version,
            )
        }
        val instanceBlankUrl = assertFailsWith<IllegalArgumentException> {
            ItemClickEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.ItemClicked,
                productId = productId,
                recId = recId,
                referrer = referrer,
                url = "  ",
                eventDate = currentDate,
                version = SystemaConstants.Version,
            )
        }
        val instanceBlankProductId = assertFailsWith<IllegalArgumentException> {
            ItemClickEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.ItemClicked,
                productId = "  ",
                recId = recId,
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                version = SystemaConstants.Version,
            )
        }
        val instanceBlankRecId = assertFailsWith<IllegalArgumentException> {
            ItemClickEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.ItemClicked,
                productId = productId,
                recId = "  ",
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                version = SystemaConstants.Version,
            )
        }

        assertEquals(instanceWrongEventType.message, "Invalid \"Type\". Expecting \"ItemClicked\"")
        assertEquals(instanceBlankProductId.message, "Invalid \"ProductId\". Cannot be blank.")
        assertEquals(instanceBlankRecId.message, "Invalid \"RecId\". Cannot be blank.")
        assertEquals(instanceBlankUrl.message, "Invalid \"Url\". Cannot be blank.")
    }
}

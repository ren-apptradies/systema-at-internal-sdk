package ai.systema.model.tracker.wishlist

import ai.systema.TestConfig
import ai.systema.constants.SystemaConstants
import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.CurTrackEventDate
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class WishlistItemAcquiredEventTest {

    companion object MockConstants {
        private const val productId = "product_id_1"
        private const val referrer = "referrer_1"
        private const val url = "https://fake-url-does-not-exist.com.au"
        private val wishlistItem = WishlistItem(
            itemId = "item_id_1"
        )
        private val wishlistItems = listOf(
            wishlistItem
        )
        private val currentDate = CurTrackEventDate
    }

    @Test
    fun testWishlistItemAcquiredEventRequires() = testSuspend {
        val connector = TestConfig.getConnector()

        val instanceWrongEventType = assertFailsWith<IllegalArgumentException> {
            WishlistItemAcquiredEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.AddToCart,
                productId = productId,
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                items = wishlistItems,
                version = SystemaConstants.Version,
            )
        }

        val instanceBlankUrl = assertFailsWith<IllegalArgumentException> {
            WishlistItemAcquiredEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.AddToWishlist,
                productId = productId,
                referrer = referrer,
                url = " ",
                eventDate = currentDate,
                items = wishlistItems,
                version = SystemaConstants.Version,
            )
        }

        val instanceBlankProductId = assertFailsWith<IllegalArgumentException> {
            WishlistItemAcquiredEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.AddToWishlist,
                productId = " ",
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                items = wishlistItems,
                version = SystemaConstants.Version,
            )
        }

        assertEquals(instanceWrongEventType.message, "Invalid \"Type\". Expecting \"AddToWishlist\"")
        assertEquals(instanceBlankUrl.message, "Invalid \"Url\". Cannot be blank.")
        assertEquals(instanceBlankProductId.message, "Invalid \"ProductId\". Cannot be blank.")
    }
}

package ai.systema.model.tracker.cart

import ai.systema.TestConfig
import ai.systema.constants.Currency
import ai.systema.constants.SystemaConstants
import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.CurTrackEventDate
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class CartItemAcquisitionCompleteEventTest {

    companion object MockConstants {
        private const val productId = "product_id_1"
        private const val referrer = "referrer_1"
        private const val url = "https://fake-url-does-not-exist.com.au"
        internal val orderItem = OrderItem(
            itemId = "item_id",
            quantity = 1,
            unitCost = 1.00,
            unitTaxAmount = 1.00,
            currency = Currency.AUD.value
        )
        internal val shippingAddress = ShippingAddress(
            city = "Melbourne",
            state = "VIC",
            postCode = "3000",
            country = "Australia"
        )
        internal val order = PurchaseOrder(
            orderId = "order_id_1",
            chargedAmount = 1.00,
            totalAmount = 1.00,
            taxAmount = 1.00,
            shippingAmount = 1.00,
            discountAmount = 1.00,
            discountCodes = "discount_code",
            currency = "AUD",
            shippingAddress = shippingAddress,
            items = listOf(
                orderItem
            )
        )
        private val currentDate = CurTrackEventDate
    }

    @Test
    fun testCartItemAcquisitionCompleteEventRequires() = testSuspend {
        val connector = TestConfig.getConnector()

        val instanceWrongEventType = assertFailsWith<IllegalArgumentException> {
            CartItemAcquisitionCompleteEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.AddToWishlist,
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                order = order,
                version = SystemaConstants.Version,
            )
        }
        val instanceEmptyOrder = assertFailsWith<IllegalArgumentException> {
            val emptyItemOrder = PurchaseOrder(
                orderId = "order_id_1",
                chargedAmount = 1.00,
                totalAmount = 1.00,
                taxAmount = 1.00,
                shippingAmount = 1.00,
                discountAmount = 1.00,
                discountCodes = "discount_code",
                currency = "AUD",
                shippingAddress = shippingAddress,
                items = listOf()
            )
            CartItemAcquisitionCompleteEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.Purchase,
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                order = emptyItemOrder,
                version = SystemaConstants.Version,
            )
        }

        val instanceBlankUrl = assertFailsWith<IllegalArgumentException> {
            CartItemAcquisitionCompleteEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.Purchase,
                referrer = referrer,
                url = " ",
                eventDate = currentDate,
                order = order,
                version = SystemaConstants.Version,
            )
        }

        assertEquals(
            instanceWrongEventType.message,
            "Invalid \"Type\". Expecting \"${TrackerEventType.Purchase}\""
        )
        assertEquals(instanceBlankUrl.message, "Invalid \"Url\". Cannot be blank.")
        assertEquals(instanceEmptyOrder.message, "Invalid \"Order.Items\". Cannot be empty.")
    }
}

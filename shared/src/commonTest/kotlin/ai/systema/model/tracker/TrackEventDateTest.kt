package ai.systema.model.tracker

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

internal class TrackEventDateTest {
    @Test
    fun testCurTrackEventBuilder() {
        val currentMoment: Instant = Clock.System.now()
        val ev = CurTrackEventDate
        assertNotNull(ev.localDate)
        assertEquals(
            ev.localDate.subSequence(0..18),
            currentMoment.toLocalDateTime(TimeZone.currentSystemDefault()).toString().subSequence(0..18)
        )
        assertEquals(ev.timeZone, TimeZone.currentSystemDefault().id)
        assertEquals(
            ev.utcDate.subSequence(0..18),
            currentMoment.toLocalDateTime(TimeZone.UTC).toString().subSequence(0..18)
        )

        // test serialization
        val serialized = Json.encodeToString(TrackEventDate.serializer(), ev)
        val decoded = Json.decodeFromString(TrackEventDate.serializer(), serialized)
        assertEquals(ev, decoded)
    }
}

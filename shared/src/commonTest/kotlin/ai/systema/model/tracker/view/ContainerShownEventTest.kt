package ai.systema.model.tracker.view

import ai.systema.TestConfig
import ai.systema.constants.SystemaConstants
import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.CurTrackEventDate
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class ContainerShownEventTest {

    private companion object MockConstants {
        private const val referrer = "referrer_1"
        private const val url = "https://fake-url-does-not-exist.com.au"
        private val currentDate = CurTrackEventDate
        private val containers = listOf(
            ItemContainer(
                resultId = "mock_result_id_1",
                recItems = listOf(
                    prepContainerItem("mock_result_id_1:mock_result_id")
                )
            )
        )
    }

    @Test
    fun testContainerShownEventRequires() = testSuspend {
        val connector = TestConfig.getConnector()
        val instanceWrongEventType = assertFailsWith<IllegalArgumentException> {
            ContainerShownEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.ItemClicked,
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                containers = containers,
                version = SystemaConstants.Version,
            )
        }
        val instanceBlankUrl = assertFailsWith<IllegalArgumentException> {
            ContainerShownEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.ContainerShown,
                referrer = referrer,
                url = " ",
                eventDate = currentDate,
                containers = containers,
                version = SystemaConstants.Version,
            )
        }
        val instanceNullContainers = assertFailsWith<IllegalArgumentException> {
            ContainerShownEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.ContainerShown,
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                containers = null,
                version = SystemaConstants.Version,
            )
        }

        assertEquals(instanceWrongEventType.message, "Invalid \"Type\". Expecting \"ContainerShown\"")
        assertEquals(instanceBlankUrl.message, "Invalid \"Url\". Cannot be blank.")
        assertEquals(instanceNullContainers.message, "Invalid \"Containers\". Cannot be null.")
    }
}

package ai.systema.model.tracker.cart

import ai.systema.TestConfig
import ai.systema.constants.Currency
import ai.systema.constants.SystemaConstants
import ai.systema.enums.TrackerEventType
import ai.systema.model.tracker.CurTrackEventDate
import ai.systema.testSuspend
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class CartItemAcquiredEventTest {

    companion object MockConstants {
        private const val productId = "product_id_1"
        private const val referrer = "referrer_1"
        private const val url = "https://fake-url-does-not-exist.com.au"
        private val item = CartItem(
            itemId = "cart_item_id_1",
            quantity = 1
        )
        private val items = listOf(
            CartItem(
                itemId = "cart_item_id_1",
                quantity = 1,
                price = 1.00,
                currency = Currency.AUD
            )
        )
        private val currentDate = CurTrackEventDate
    }

    @Test
    fun testCartItemAcquiredEventRequires() = testSuspend {
        val connector = TestConfig.getConnector()

        val instanceWrongEventType = assertFailsWith<IllegalArgumentException> {
            CartItemAcquiredEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.AddToWishlist,
                productId = productId,
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                items = items,
                version = SystemaConstants.Version,
            )
        }
        val instanceBlankProductId = assertFailsWith<IllegalArgumentException> {
            CartItemAcquiredEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.AddToCart,
                productId = " ",
                referrer = referrer,
                url = url,
                eventDate = currentDate,
                items = items,
                version = SystemaConstants.Version,
            )
        }

        val instanceBlankUrl = assertFailsWith<IllegalArgumentException> {
            CartItemAcquiredEvent(
                clientId = connector.credentials.clientID.basic,
                environment = connector.credentials.environment.value,
                fingerprint = connector.clientUser.fingerprint,
                sessionId = connector.clientUser.sessionId,
                userAgent = connector.clientUser.userAgent,
                userName = connector.clientUser.userIdHash,
                sequence = connector.clientUser.nexSeq(),
                type = TrackerEventType.AddToCart,
                productId = productId,
                referrer = referrer,
                url = "",
                eventDate = currentDate,
                items = items,
                version = SystemaConstants.Version,
            )
        }

        assertEquals(instanceWrongEventType.message, "Invalid \"Type\". Expecting \"AddToCart\"")
        assertEquals(instanceBlankProductId.message, "Invalid \"ProductId\". Cannot be blank.")
        assertEquals(instanceBlankUrl.message, "Invalid \"Url\". Cannot be blank.")
    }
}

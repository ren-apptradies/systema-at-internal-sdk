import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

apply(plugin = "org.jmailen.kotlinter")

plugins {
    val kotlinVersion = "1.5.31"
    kotlin("multiplatform")
//    kotlin("native.cocoapods")
    id("com.prof18.kmp.fatframework.cocoa") version "0.2.1"
    kotlin("plugin.serialization") version kotlinVersion
    id("com.android.library")
}

version = "1.0"

kotlin {
    android()

//    val iosTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget = when {
//        System.getenv("SDK_NAME")?.startsWith("iphoneos") == true -> ::iosArm64
//        else -> ::iosX64
//    }

//    iosTarget("ios") {}

//    cocoapods {
//        summary = "Systema iOS SDK shared module"
//        homepage = "Link to the Shared Module homepage"
//        ios.deploymentTarget = "14.1"
//        frameworkName = "shared"
//        podfile = project.file("../iosApp/Podfile")
//    }

    ios {
        binaries.framework("shared")
    }

    sourceSets {
        val kotlinVersion = "1.5.21"
        val ktorVersion = "1.6.2"
        val napierVersion = "2.1.0"
        val coroutinesVersion = "1.5.1-native-mt"

        all {
            languageSettings.apply {
                progressiveMode = true
                useExperimentalAnnotation("kotlin.RequiresOptIn")
            }
        }

        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-stdlib-common:$kotlinVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.2.1")
                implementation("io.ktor:ktor-client-core:${ktorVersion}")
                implementation("io.ktor:ktor-client-json:${ktorVersion}")
                implementation("io.ktor:ktor-client-logging:${ktorVersion}")
                implementation("io.ktor:ktor-client-serialization:${ktorVersion}")
                implementation("io.github.aakira:napier:$napierVersion")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-annotations-common"))
                implementation("io.ktor:ktor-client-mock:$ktorVersion")
            }
        }
        val androidMain by getting {
            dependencies {
                implementation ("io.ktor:ktor-client-android:${ktorVersion}")
                implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion")
                implementation("commons-codec:commons-codec:1.10")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-annotations-common"))
                implementation("io.mockk:mockk:1.12.0")
                implementation("com.github.stefanbirkner:system-lambda:1.2.0")
            }
        }

        val iosMain by getting {
            dependencies {
                dependsOn(commonMain)
                implementation ("io.ktor:ktor-client-ios:${ktorVersion}")
            }
        }
    }
}

android {
    compileSdk = 31
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdk = 26
        targetSdk = 31
    }
}

fatFrameworkCocoaConfig {
    frameworkName = "shared"
    outputPath = "$rootDir/systema-ios-shared-xc"
    versionName = "1.0"
    useXCFramework = true

    cocoaPodRepoInfo {
        summary = "This is a test Systema iOS framework"
        homepage = "https://gitlab.com/ren-apptradies/systema-at-internal-sdk"
        license = "Apache"
        authors = "\"Ren Decano\" => \"ren@apptradies.com\""
        gitUrl = "git@gitlab.com:ren-apptradies/systema-at-internal-sdk.git"
    }
}